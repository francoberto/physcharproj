// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class PhysCharProjTarget : TargetRules
{
	public PhysCharProjTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("PhysCharProj");
		WindowsPlatform.Compiler = WindowsCompiler.VisualStudio2019;
	}
}
