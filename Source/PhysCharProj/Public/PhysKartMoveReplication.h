// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "Engine/NetSerialization.h"
#include "Serialization/BitWriter.h"
#include "Containers/BitArray.h"
#include "PhysKartMoveReplication.generated.h"

class UPackageMap;
class FSavedMove_PhysKart;
class UPhysKartMoveComp;
struct FRootMotionSourceGroup;
//struct FReplicatedPhysKartState;

USTRUCT()
struct PHYSCHARPROJ_API FReplicatedPhysKartState
{
	GENERATED_USTRUCT_BODY()

	// input replication: steering
	UPROPERTY()
	float SteeringInput;

	// input replication: throttle
	UPROPERTY()
	float ThrottleInput;

	// input replication: brake
	UPROPERTY()
	float BrakeInput;

	// input replication: handbrake
	UPROPERTY()
	float HandbrakeInput;

	//// state replication: current gear
	//UPROPERTY()
	//int32 CurrentGear;

	FReplicatedPhysKartState() : SteeringInput(0.0f), ThrottleInput(0.0f), BrakeInput(0.0f), HandbrakeInput(0.0f) {}
};

// If defined and not zero, deprecated RPCs on UPhysKartMoveComp will not be marked deprecated at compile time, to aid in migration of older projects. New projects should prefer the new API.
#ifndef SUPPORT_DEPRECATED_PHYSKART_MOVEMENT_RPCS
#define SUPPORT_DEPRECATED_PHYSKART_MOVEMENT_RPCS 0
#endif

#if SUPPORT_DEPRECATED_PHYSKART_MOVEMENT_RPCS
#define DEPRECATED_PHYSKART_MOVEMENT_RPC(...)
#else
#define DEPRECATED_PHYSKART_MOVEMENT_RPC(DeprecatedFunction, NewFunction) UE_DEPRECATED_FORGAME(4.26, #DeprecatedFunction "() is deprecated, use " #NewFunction "() instead, or define SUPPORT_DEPRECATED_PHYSKART_MOVEMENT_RPCS=1 in the project and set CVar p.NetUsePackedMovementRPCs=0 to use the old code path.")
#endif

// Number of bits to reserve in serialization container. Make this large enough to try to avoid re-allocation during the worst case RPC calls (dual move + unacknowledged "old important" move).
#ifndef PHYSKART_SERIALIZATION_PACKEDBITS_RESERVED_SIZE
#define PHYSKART_SERIALIZATION_PACKEDBITS_RESERVED_SIZE 1024
#endif

//////////////////////////////////////////////////////////////////////////
/**
 * Intermediate data stream used for network serialization of Character RPC data.
 * This is basically an array of bits that is packed/unpacked via NetSerialize into custom data structs on the sending and receiving ends.
 */
USTRUCT()
struct PHYSCHARPROJ_API FPhysKartNetworkSerializationPackedBits
{
	GENERATED_USTRUCT_BODY()

	FPhysKartNetworkSerializationPackedBits()
		: SavedPackageMap(nullptr)
	{
	}

	bool NetSerialize(FArchive& Ar, UPackageMap* PackageMap, bool& bOutSuccess);
	UPackageMap* GetPackageMap() const { return SavedPackageMap; }

	//------------------------------------------------------------------------
	// Data

	// TInlineAllocator used with TBitArray takes the number of 32-bit dwords, but the define is in number of bits, so convert here by dividing by 32.
	TBitArray<TInlineAllocator<PHYSKART_SERIALIZATION_PACKEDBITS_RESERVED_SIZE / NumBitsPerDWORD>> DataBits;

private:
	UPackageMap* SavedPackageMap;
};

template<>
struct TStructOpsTypeTraits<FPhysKartNetworkSerializationPackedBits> : public TStructOpsTypeTraitsBase2<FPhysKartNetworkSerializationPackedBits>
{
	enum
	{
		WithNetSerializer = true,
	};
};


//////////////////////////////////////////////////////////////////////////
// Client to Server movement data
//////////////////////////////////////////////////////////////////////////

/**
 * FPhysKartNetworkMoveData encapsulates a client move that is sent to the server for UPhysKartMoveComp networking.
 *
 * Adding custom data to the network move is accomplished by deriving from this struct, adding new data members, implementing ClientFillNetworkMoveData(), implementing Serialize(), 
 * and setting up the UPhysKartMoveComp to use an instance of a custom FPhysKartNetworkMoveDataContainer (see that struct for more details).
 * 
 * @see FPhysKartNetworkMoveDataContainer
 */

struct PHYSCHARPROJ_API FPhysKartNetworkMoveData
{
public:

	enum class ENetworkMoveType
	{
		NewMove,
		PendingMove,
		OldMove
	};

	FPhysKartNetworkMoveData()
		: NetworkMoveType(ENetworkMoveType::NewMove)
		, TimeStamp(0.f)
		, Acceleration(ForceInitToZero)
		, AngAcceleration(ForceInitToZero)
		, Location(ForceInitToZero)
		, ClientQuatAxis(ForceInitToZero)
		, ClientQuatAngle(0.f)
		, ReplicatedInputState(FReplicatedPhysKartState())
		, ControlRotation(ForceInitToZero)
		, CompressedMoveFlags(0)
		, MovementBase(nullptr)
		, MovementBaseBoneName(NAME_None)
		, MovementMode(0)
	{
	}
	
	virtual ~FPhysKartNetworkMoveData()
	{
	}

	/**
	 * Given a FSavedMove_PhysKart from UPhysKartMoveComp, fill in data in this struct with relevant movement data.
	 * Note that the instance of the FSavedMove_PhysKart is likely a custom struct of a derived struct of your own, if you have added your own saved move data.
	 * @see UPhysKartMoveComp::AllocateNewMove()
	 */
	virtual void ClientFillNetworkMoveData(const FSavedMove_PhysKart& ClientMove, ENetworkMoveType MoveType);

	/**
	 * Serialize the data in this struct to or from the given FArchive. This packs or unpacks the data in to a variable-sized data stream that is sent over the
	 * network from client to server.
	 * @see UPhysKartMoveComp::CallServerMovePacked
	 */
	virtual bool Serialize(UPhysKartMoveComp& CharacterMovement, FArchive& Ar, UPackageMap* PackageMap, ENetworkMoveType MoveType);

	// Indicates whether this was the latest new move, a pending/dual move, or old important move.
	ENetworkMoveType NetworkMoveType;

	//------------------------------------------------------------------------
	// Basic movement data.

	float TimeStamp;
	FVector_NetQuantize10 Acceleration;
	FVector_NetQuantize10 AngAcceleration;
	FVector_NetQuantize100 Location;		// Either world location or relative to MovementBase if that is set.
	FVector_NetQuantize100 ClientQuatAxis;
	float ClientQuatAngle;
	FReplicatedPhysKartState ReplicatedInputState;
	FRotator ControlRotation;
	uint8 CompressedMoveFlags;

	class UPrimitiveComponent* MovementBase;
	FName MovementBaseBoneName;
	uint8 MovementMode;
};


//////////////////////////////////////////////////////////////////////////
/**
 * Struct used for network RPC parameters between client/server by ACharacter and UPhysKartMoveComp.
 * To extend network move data and add custom parameters, you typically override this struct with a custom derived struct and set the CharacterMovementComponent
 * to use your container with UPhysKartMoveComp::SetNetworkMoveDataContainer(). Your derived struct would then typically (in the constructor) replace the
 * NewMoveData, PendingMoveData, and OldMoveData pointers to use your own instances of a struct derived from FPhysKartNetworkMoveData, where you add custom fields
 * and implement custom serialization to be able to pack and unpack your own additional data.
 * 
 * @see UPhysKartMoveComp::SetNetworkMoveDataContainer()
 */
struct PHYSCHARPROJ_API FPhysKartNetworkMoveDataContainer
{
public:

	/**
	 * Default constructor. Sets data storage (NewMoveData, PendingMoveData, OldMoveData) to point to default data members. Override those pointers to instead point to custom data if you want to use derived classes.
	 */
	FPhysKartNetworkMoveDataContainer()
		: bHasPendingMove(false)
		//, bIsDualHybridRootMotionMove(false)
		, bHasOldMove(false)
		, bDisableCombinedScopedMove(false)
	{
		NewMoveData		= &BaseDefaultMoveData[0];
		PendingMoveData	= &BaseDefaultMoveData[1];
		OldMoveData		= &BaseDefaultMoveData[2];
	}

	virtual ~FPhysKartNetworkMoveDataContainer()
	{
	}

	/**
	 * Passes through calls to ClientFillNetworkMoveData on each FPhysKartNetworkMoveData matching the client moves. Note that ClientNewMove will never be null, but others may be.
	 */
	virtual void ClientFillNetworkMoveData(const FSavedMove_PhysKart* ClientNewMove, const FSavedMove_PhysKart* ClientPendingMove, const FSavedMove_PhysKart* ClientOldMove);

	/**
	 * Serialize movement data. Passes Serialize calls to each FPhysKartNetworkMoveData as applicable, based on bHasPendingMove and bHasOldMove.
	 */
	virtual bool Serialize(UPhysKartMoveComp& CharacterMovement, FArchive& Ar, UPackageMap* PackageMap);

	//------------------------------------------------------------------------
	// Basic movement data. NewMoveData is the most recent move, PendingMoveData is a move right before it (dual move). OldMoveData is an "important" move not yet acknowledged.

	FORCEINLINE FPhysKartNetworkMoveData* GetNewMoveData() const		{ return NewMoveData; }
	FORCEINLINE FPhysKartNetworkMoveData* GetPendingMoveData() const	{ return PendingMoveData; }
	FORCEINLINE FPhysKartNetworkMoveData* GetOldMoveData() const		{ return OldMoveData; }

	//------------------------------------------------------------------------
	// Optional pending data used in "dual moves".
	bool bHasPendingMove;
	//bool bIsDualHybridRootMotionMove;
	
	// Optional "old move" data, for redundant important old moves not yet ack'd.
	bool bHasOldMove;

	// True if we want to disable a scoped move around both dual moves (optional from bEnableServerDualMoveScopedMovementUpdates), typically set if bForceNoCombine was true which can indicate an important change in moves.
	bool bDisableCombinedScopedMove;
	
protected:

	FPhysKartNetworkMoveData* NewMoveData;
	FPhysKartNetworkMoveData* PendingMoveData;	// Only valid if bHasPendingMove is true
	FPhysKartNetworkMoveData* OldMoveData;		// Only valid if bHasOldMove is true

private:

	FPhysKartNetworkMoveData BaseDefaultMoveData[3];
};


//////////////////////////////////////////////////////////////////////////
/**
 * Structure used internally to handle serialization of FPhysKartNetworkMoveDataContainer over the network.
 */
USTRUCT()
struct PHYSCHARPROJ_API FPhysKartServerMovePackedBits : public FPhysKartNetworkSerializationPackedBits
{
	GENERATED_USTRUCT_BODY()
	FPhysKartServerMovePackedBits() {}
};

template<>
struct TStructOpsTypeTraits<FPhysKartServerMovePackedBits> : public TStructOpsTypeTraitsBase2<FPhysKartServerMovePackedBits>
{
	enum
	{
		WithNetSerializer = true,
	};
};


//////////////////////////////////////////////////////////////////////////
// Server to Client response
//////////////////////////////////////////////////////////////////////////

// ClientAdjustPosition replication (event called at end of frame by server)
struct FPhysKartClientAdjustment
{
public:

	FPhysKartClientAdjustment()
		: TimeStamp(0.f)
		, DeltaTime(0.f)
		, NewLoc(ForceInitToZero)
		, NewVel(ForceInitToZero)
		, NewQuat(ForceInitToZero)
		, NewAngVel(ForceInitToZero)
		//, NewRot(ForceInitToZero)
		//, NewBase(NULL)
		//, NewBaseBoneName(NAME_None)
		, bAckGoodMove(false)
		, bBaseRelativePosition(false)
		, MovementMode(0)
	{
	}

	float TimeStamp;
	float DeltaTime;
	FVector NewLoc;
	FVector NewVel;
	FQuat NewQuat;
	FVector NewAngVel;
	FRotator NewRot;
	//UPrimitiveComponent* NewBase;
	//FName NewBaseBoneName;
	bool bAckGoodMove;
	bool bBaseRelativePosition;
	uint8 MovementMode;
};


//////////////////////////////////////////////////////////////////////////
/**
 * Response from the server to the client about a move that is being acknowledged.
 * Internally it mainly copies the FPhysKartClientAdjustment from the UPhysKartMoveComp indicating the response, as well as
 * setting a few relevant flags about the response and serializing the response to and from an FArchive for handling the variable-size
 * payload over the network.
 */
struct PHYSCHARPROJ_API FPhysKartMoveResponseDataContainer
{
public:

	FPhysKartMoveResponseDataContainer()
		//: bHasBase(false)
		: bHasRotation(false)
		//, bRootMotionMontageCorrection(false)
		//, bRootMotionSourceCorrection(false)
		//, RootMotionTrackPosition(-1.0f)
		//, RootMotionRotation(ForceInitToZero)
	{
	}

	virtual ~FPhysKartMoveResponseDataContainer()
	{
	}

	/**
	 * Copy the FPhysKartClientAdjustment and set a few flags relevant to that data.
	 */
	virtual void ServerFillResponseData(const UPhysKartMoveComp& CharacterMovement, const FPhysKartClientAdjustment& PendingAdjustment);

	/**
	 * Serialize the FPhysKartClientAdjustment data and other internal flags.
	 */
	virtual bool Serialize(UPhysKartMoveComp& CharacterMovement, FArchive& Ar, UPackageMap* PackageMap);

	bool IsGoodMove() const		{ return ClientAdjustment.bAckGoodMove;}
	bool IsCorrection() const	{ return !IsGoodMove(); }

	//FRootMotionSourceGroup* GetRootMotionSourceGroup(UPhysKartMoveComp& CharacterMovement) const;

	//bool bHasBase;
	bool bHasRotation; // By default ClientAdjustment.NewRot is not serialized. Set this to true after base ServerFillResponseData if you want Rotation to be serialized.
	//bool bRootMotionMontageCorrection;
	//bool bRootMotionSourceCorrection;

	// Client adjustment. All data other than bAckGoodMove and TimeStamp is only valid if this is a correction (not an ack).
	FPhysKartClientAdjustment ClientAdjustment;

	//float RootMotionTrackPosition;
	//FVector_NetQuantizeNormal RootMotionRotation;
};

//////////////////////////////////////////////////////////////////////////
/**
 * Structure used internally to handle serialization of FPhysKartMoveResponseDataContainer over the network.
 */
USTRUCT()
struct PHYSCHARPROJ_API FPhysKartMoveResponsePackedBits : public FPhysKartNetworkSerializationPackedBits
{
	GENERATED_USTRUCT_BODY()
	FPhysKartMoveResponsePackedBits() {}
};

template<>
struct TStructOpsTypeTraits<FPhysKartMoveResponsePackedBits> : public TStructOpsTypeTraitsBase2<FPhysKartMoveResponsePackedBits>
{
	enum
	{
		WithNetSerializer = true,
	};
};


//// Fill out your copyright notice in the Description page of Project Settings.
//
//#pragma once
//
//#include "CoreMinimal.h"
//
///**
// * 
// */
//class PHYSCHARPROJ_API PhysKartMoveReplication
//{
//public:
//	PhysKartMoveReplication();
//	~PhysKartMoveReplication();
//};
