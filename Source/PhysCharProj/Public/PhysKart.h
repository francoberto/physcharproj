// Francisco Elizalde * PaCorp * 2021

#pragma once

#include "CoreMinimal.h"
#include "PhysKartMoveReplication.h"
#include "GameFramework/Pawn.h"
#include "PhysKartMoveComp.h"
#include "PhysKart.generated.h"

class UBoxComponent;
class UStaticMeshComponent;
class USkeletalMeshComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPhysKartMovementModeChangedSignature, class APhysKart*, PhysKart, EMovementMode, PrevMovementMode, uint8, PreviousCustomMode);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPhysKartMovementUpdatedSignature, float, DeltaSeconds, FVector, OldLocation, FVector, OldVelocity);

UCLASS()
class PHYSCHARPROJ_API APhysKart : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APhysKart();

public:
	// Sets default values for this pawn's properties
	//APhysKart();
	APhysKart(const FObjectInitializer& ObjectInitializer);

	/** Returns MovementComponent subobject **/
	UFUNCTION(BlueprintCallable, Category = PhysKart)
	UPhysKartMoveComp* GetPhysKartMoveComp() const { return PhysKartMovementComponent; }

	/** Returns MovementComponent subobject **/
	UFUNCTION(BlueprintCallable, Category = PhysKart)
	//class UBoxComponent* GetPhysKartRoot() const { return PhysKartRoot; }
	//class UStaticMeshComponent* GetPhysKartRoot() const { return PhysKartRoot; }
	class USkeletalMeshComponent* GetPhysKartRoot() const { return PhysKartRoot; }

	/** Returns Animated Skeletal Mesh Component subobject. IF valid. **/
	UFUNCTION(BlueprintCallable, Category = PhysKart)
	class USkeletalMeshComponent* GetPhysKartMesh() const { return PhysKartMesh; }

	void MoveForward(float Value);
	void MoveRight(float Value);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Name of the VehicleMovement. Use this name if you want to use a different class (with ObjectInitializer.SetDefaultSubobjectClass). */
	static FName PhysKartMoveCompName;

	/** Name of the VehicleMovement. Use this name if you want to use a different class (with ObjectInitializer.SetDefaultSubobjectClass). */
	static FName PhysKartRootName;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UPhysKartMoveComp* PhysKartMovementComponent = Cast<UPhysKartMoveComp>(GetMovementComponent());

	/** The main 'physics' mesh associated with this PhysKart (NOT an optional sub-object). */
	UPROPERTY(Category = PhysKart, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	//UBoxComponent* PhysKartRoot;
	//UStaticMeshComponent* PhysKartRoot;
	USkeletalMeshComponent* PhysKartRoot;

	/** The main skeletal mesh associated with this PhysKart (optional sub-object). */
	UPROPERTY(Category = PhysKart, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* PhysKartMesh;

	//-------------------------------------------------------------------------------------------//
public:
	//~ Begin AActor Interface.
	virtual void PostInitializeComponents() override;
	/** Called on the actor right before replication occurs */
	virtual void PreReplication(IRepChangedPropertyTracker& ChangedPropertyTracker) override;
	virtual void PreNetReceive() override;
	virtual void PostNetReceive() override;
	/** Update and smooth simulated physic state, replaces PostNetReceiveLocation() and PostNetReceiveVelocity() */
	virtual void PostNetReceivePhysicState() override;			// CUSTOMPhysPawn
	//~ End AActor Interface
	/** [server] called to determine if we should pause replication this actor to a specific player */
	virtual bool IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer) override;
	/** [client] called when replication is paused for this actor */
	virtual void OnReplicationPausedChanged(bool bIsReplicationPaused) override;
	//-------------------------------------------------------------------------------------------//

public:
	/** When true, player wants to use item */
	UPROPERTY(BlueprintReadOnly, Category = "Vehicle Items")
	uint32 bPressedItem : 1;
	virtual FVector GetVelocity() const override;
	virtual FVector GetAngularVelocity() const;

protected:
	/** PhysKartMovement ServerLastTransformUpdateTimeStamp value, replicated to simulated proxies. */
	UPROPERTY(Replicated)
	float ReplicatedServerLastTransformUpdateTimeStamp;
	UPROPERTY(ReplicatedUsing=OnRep_ReplayLastTransformUpdateTimeStamp)
	float ReplayLastTransformUpdateTimeStamp;
public:
	UFUNCTION()
	void OnRep_ReplayLastTransformUpdateTimeStamp();
	/** Accessor for ReplicatedServerLastTransformUpdateTimeStamp. */
	FORCEINLINE float GetReplicatedServerLastTransformUpdateTimeStamp() const { return ReplicatedServerLastTransformUpdateTimeStamp; }

public:
	
	//////////////////////////////////////////////////////////////////////////
	// Server RPC that passes through to CharacterMovement (avoids RPC overhead for components).
	// The base RPC function (eg 'ServerMove') is auto-generated for clients to trigger the call to the server function,
	// eventually going to the _Implementation function (which we just pass to the CharacterMovementComponent).
	//////////////////////////////////////////////////////////////////////////
	UFUNCTION(unreliable, server, WithValidation)
	void ServerMovePacked(const FPhysKartServerMovePackedBits& PackedBits);
	void ServerMovePacked_Implementation(const FPhysKartServerMovePackedBits& PackedBits);
	bool ServerMovePacked_Validate(const FPhysKartServerMovePackedBits& PackedBits);
	
//////////////////////////////////////////////////////////////////////////
	// Client RPC that passes through to CharacterMovement (avoids RPC overhead for components).
	//////////////////////////////////////////////////////////////////////////
	UFUNCTION(unreliable, client, WithValidation)
	void ClientMoveResponsePacked(const FPhysKartMoveResponsePackedBits& PackedBits);
	void ClientMoveResponsePacked_Implementation(const FPhysKartMoveResponsePackedBits& PackedBits);
	bool ClientMoveResponsePacked_Validate(const FPhysKartMoveResponsePackedBits& PackedBits);
	
	//////////////////////////////////////////////////////////////////////////
	// BEGIN DEPRECATED RPCs that don't use variable sized payloads. Use ServerMovePacked and ClientMoveResponsePacked instead.
	//////////////////////////////////////////////////////////////////////////
	
	///** Pass current state to server *
	/** Replicated function sent by client to server - contains client movement and view info. */
	// PaCorp * Removed  UPrimitiveComponent* ClientMovementBase, FName ClientBaseBoneName, parameters as PhysKartVehicle does not care.
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMove, ServerMovePacked)
	UFUNCTION(unreliable, server, WithValidation)
	//void ServerMove(float TimeStamp, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, UPrimitiveComponent* ClientMovementBase, FName ClientBaseBoneName, uint8 ClientMovementMode);
	void ServerMove(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis = FVector_NetQuantize100::ZeroVector, uint8 ClientQuatAngle = 0);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveNoBase_Implementation, ServerMovePacked_Implementation)
	void ServerMove_Implementation(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis = FVector_NetQuantize100::ZeroVector, uint8 ClientQuatAngle = 0);
	bool ServerMove_Validate(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis = FVector_NetQuantize100::ZeroVector, uint8 ClientQuatAngle = 0);

	/** Replicated function sent by client to server - contains client movement and view info for two moves. */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveDual, ServerMovePacked)
	UFUNCTION(unreliable, server, WithValidation)
	void ServerMoveDual(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveDual_Implementation, ServerMovePacked_Implementation)
	void ServerMoveDual_Implementation(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode);
	bool ServerMoveDual_Validate(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode);
	/* Resending an (important) old move. Process it if not already processed. */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveOld, ServerMovePacked)
	UFUNCTION(unreliable, server, WithValidation)
	void ServerMoveOld(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveOld_Implementation, ServerMovePacked_Implementation)
	void ServerMoveOld_Implementation(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags);
	bool ServerMoveOld_Validate(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags);
	//////////////////////////////////////////////////////////////////////////
	// Client RPCS that pass through to PhysKartMovement (avoids RPC overhead for components).
	//////////////////////////////////////////////////////////////////////////
	/** If no client adjustment is needed after processing received ServerMove(), ack the good move so client can remove it from SavedMoves */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientAckGoodMove, ClientMoveResponsePacked)
	UFUNCTION(unreliable, client)
	void ClientAckGoodMove(float TimeStamp);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientAckGoodMove_Implementation, ClientMoveResponsePacked_Implementation)
	void ClientAckGoodMove_Implementation(float TimeStamp);
//241
	/** Replicate position correction to client, associated with a timestamped servermove.  Client will replay subsequent moves after applying adjustment.  */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientAckGoodMove, ClientMoveResponsePacked)
	UFUNCTION(unreliable, client)
	void ClientAdjustPosition(float TimeStamp, FVector NewLoc, FVector NewVel, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientAdjustPosition_Implementation, ClientMoveResponsePacked_Implementation)
	void ClientAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FVector NewVel, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode);
	/* Bandwidth saving version, when velocity is zeroed */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientVeryShortAdjustPosition, ClientMoveResponsePacked)
	UFUNCTION(unreliable, client)
	void ClientVeryShortAdjustPosition(float TimeStamp, FVector NewLoc, FQuat NewQuat, uint8 ServerMovementMode);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientVeryShortAdjustPosition_Implementation, ClientMoveResponsePacked_Implementation)
	void ClientVeryShortAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FQuat NewQuat, uint8 ServerMovementMode);
		
public:
	/** When true, applying updates to network client (replaying saved moves for a locally controlled PhysKart) */
	UPROPERTY(Transient)
	uint32 bClientUpdating : 1;
	/**
	 * Called from PhysKartMovementComponent to notify the PhysKart that the movement mode has changed.
	 * @param	PrevMovementMode	Movement mode before the change
	 * @param	PrevCustomMode		Custom mode before the change (applicable if PrevMovementMode is Custom)
	 */
	virtual void OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode = 0);
	/** Multicast delegate for MovementMode changing. */
	UPROPERTY(BlueprintAssignable, Category = PhysKart)
	FPhysKartMovementModeChangedSignature MovementModeChangedDelegate;
	/**
	 * Event triggered at the end of a PhysKartMovementComponent movement update.
	 * This is the preferred event to use rather than the Tick event when performing custom updates to PhysKartMovement properties based on the current state.
	 * This is mainly due to the nature of network updates, where client corrections in position from the server can cause multiple iterations of a movement update,
	 * which allows this event to update as well, while a Tick event would not.
	 *
	 * @param	DeltaSeconds		Delta time in seconds for this update
	 * @param	InitialLocation		Location at the start of the update. May be different than the current location if movement occurred.
	 * @param	InitialVelocity		Velocity at the start of the update. May be different than the current velocity.
	 */
	UPROPERTY(BlueprintAssignable, Category=PhysKart)
	FPhysKartMovementUpdatedSignature OnPhysKartMovementUpdated;
	/** Incremented every time there is an Actor overlap event (start or stop) on this actor. */
	uint32 NumActorOverlapEventsCounter;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor);
	virtual void NotifyActorEndOverlap(AActor* OtherActor);
};
