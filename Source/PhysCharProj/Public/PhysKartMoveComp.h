// Francisco Elizalde * PaCorp * 2021

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/EngineTypes.h"
#include "PhysKartMoveReplication.h"
#include "Interfaces/NetworkPredictionInterface.h"

#include "PhysicsEngine/BodyInstance.h" // Add this include
#include "Physics/ImmediatePhysics/ImmediatePhysicsDeclares.h"

#include "GameFramework/PawnMovementComponent.h"
#include "PhysKartMoveComp.generated.h"

class APhysKart;
class FNetworkPredictionData_Server_PhysKart;
class FNetworkPredictionData_Client_PhysKart;
class FSavedMove_PhysKart;
class APlayerController;
class AController;

struct FBodyInstance;
struct FConstraintInstance;

/** Determines in what space the simulation should run */
UENUM()
enum class EPhysKartSimulationSpace : uint8
{
	/** Simulate in component space. Moving the entire skeletal mesh will have no affect on velocities */
	ComponentSpace,
	/** Simulate in world space. Moving the skeletal mesh will generate velocity changes */
	WorldSpace,
	/** Simulate in another bone space. Moving the entire skeletal mesh and individually modifying the base bone will have no affect on velocities */
	BaseBoneSpace,
};

//DECLARE_STATS_GROUP(TEXT("BulletPool"), STATGROUP_BulletPool, STATCAT_Advanced);
//DECLARE_CYCLE_STAT_EXTERN(TEXT("RequestBulletTypeFromPool"), STAT_RequestBulletTypeFromPool, STATGROUP_BulletPool, SHOTGUNHADES_API);
//DECLARE_CYCLE_STAT_EXTERN(TEXT("ReturnBulletTypeToPool"), STAT_ReturnBulletTypeToPool, STATGROUP_BulletPool, SHOTGUNHADES_API);

DECLARE_STATS_GROUP(TEXT("PhysKart"), STATGROUP_PhysKart, STATCAT_Advanced);

USTRUCT()
struct FPhysKartInputRate
{
	GENERATED_USTRUCT_BODY()

	// Rate at which the input value rises
	UPROPERTY(EditAnywhere, Category=VehicleInputRate)
	float RiseRate;

	// Rate at which the input value falls
	UPROPERTY(EditAnywhere, Category=VehicleInputRate)
	float FallRate;

	FPhysKartInputRate() : RiseRate(5.0f), FallRate(5.0f) { }

	/** Change an output value using max rise and fall rates */
	float InterpInputValue( float DeltaTime, float CurrentValue, float NewValue ) const
	{
		const float DeltaValue = NewValue - CurrentValue;

		// We are "rising" when DeltaValue has the same sign as CurrentValue (i.e. delta causes an absolute magnitude gain)
		// OR we were at 0 before, and our delta is no longer 0.
		const bool bRising = (( DeltaValue > 0.0f ) == ( CurrentValue > 0.0f )) ||
								(( DeltaValue != 0.f ) && ( CurrentValue == 0.f ));

		const float MaxDeltaValue = DeltaTime * ( bRising ? RiseRate : FallRate );
		const float ClampedDeltaValue = FMath::Clamp( DeltaValue, -MaxDeltaValue, MaxDeltaValue );
		return CurrentValue + ClampedDeltaValue;
	}
};

/**
 * Tick function that calls UCharacterMovementComponent::PostPhysicsTickComponent
 **/
USTRUCT()
struct FPhysKartMoveCompPostPhysicsTickFunction : public FTickFunction
{
	GENERATED_USTRUCT_BODY()

	/** CharacterMovementComponent that is the target of this tick **/
	class UPhysKartMoveComp* Target;

	/**
	 * Abstract function actually execute the tick.
	 * @param DeltaTime - frame time to advance, in seconds
	 * @param TickType - kind of tick for this frame
	 * @param CurrentThread - thread we are executing on, useful to pass along as new tasks are created
	 * @param MyCompletionGraphEvent - completion event for this task. Useful for holding the completion of this task until certain child tasks are complete.
	 **/
	virtual void ExecuteTick(float DeltaTime, enum ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent) override;

	/** Abstract function to describe this tick. Used to print messages about illegal cycles in the dependency graph **/
	virtual FString DiagnosticMessage() override;
	/** Function used to describe this tick for active tick reporting. **/
	virtual FName DiagnosticContext(bool bDetailed) override;
};

template<>
struct TStructOpsTypeTraits<FPhysKartMoveCompPostPhysicsTickFunction> : public TStructOpsTypeTraitsBase2<FPhysKartMoveCompPostPhysicsTickFunction>
{
	enum
	{
		WithCopy = false
	};
};

/** Shared pointer for easy memory management of FSavedMove_PhysKart, for accumulating and replaying network moves. */
//typedef TSharedPtr<class FSavedMove_PhysKart> FSavedMovePtr;
typedef TSharedPtr<class FSavedMove_PhysKart> FPhysKartSavedMovePtr;

//
	/**
 * Settings for the system which passes motion of the simulation's space into the simulation. This allows the simulation to pass a 
 * fraction of the world space motion onto the bodies which allows Bone-Space and Component-Space simulations to react to world-space 
 * movement in a controllable way.
 */
USTRUCT(BlueprintType)
struct PHYSCHARPROJ_API FPhysKartSimSpaceSettings
{
	GENERATED_USTRUCT_BODY()
	FPhysKartSimSpaceSettings();
	// Global multipler on the effects of simulation space movement. Must be in range [0, 1]. If MasterAlpha = 0.0, the system is disabled and the simulation will
	// be fully local (i.e., world-space actor movement and rotation does not affect the simulation). When MasterAlpha = 1.0 the simulation effectively acts as a 
	// world-space sim, but with the ability to apply limits using the other parameters.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings, meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float MasterAlpha;
	// Multiplier on the Z-component of velocity and acceleration that is passed to the simulation. Usually from 0.0 to 1.0 to 
	// reduce the effects of jumping and crouching on the simulation, but it can be higher than 1.0 if you need to exaggerate this motion for some reason.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings, meta = (ClampMin = "0.0"))
	float VelocityScaleZ;
	// A clamp on the effective world-space velocity that is passed to the simulation. Units are cm/s. The default value effectively means "unlimited". It is not usually required to
	// change this but you would reduce this to limit the effects of drag on the bodies in the simulation (if you have bodies that have LinearDrag set to non-zero in the physics asset). 
	// Expected values in this case would be somewhat less than the usual velocities of your object which is commonly a few hundred for a character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings, meta = (ClampMin = "0.0"))
	float MaxLinearVelocity;
	// A clamp on the effective world-space angular velocity that is passed to the simulation. Units are radian/s, so a value of about 6.0 is one rotation per second.
	// The default value effectively means "unlimited". You would reduce this (and MaxAngularAcceleration) to limit how much bodies "fly out" when the actor spins on the spot. 
	// This is especially useful if you have characters than can rotate very quickly and you would probably want values around or less than 10 in this case.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings, meta = (ClampMin = "0.0"))
	float MaxAngularVelocity;
	
	// A clamp on the effective world-space acceleration that is passed to the simulation. Units are cm/s/s. The default value effectively means "unlimited". 
	// This property is used to stop the bodies of the simulation flying out when suddenly changing linear speed. It is useful when you have characters than can 
	// changes from stationary to running very quickly such as in an FPS. A common value for a character might be in the few hundreds.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings, meta = (ClampMin = "0.0"))
	float MaxLinearAcceleration;
	
	// A clamp on the effective world-space angular accleration that is passed to the simulation. Units are radian/s/s. The default value effectively means "unlimited". 
	// This has a similar effect to MaxAngularVelocity, except that it is related to the flying out of bodies when the rotation speed suddenly changes. Typical limist for
	// a character might be around 100.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings, meta = (ClampMin = "0.0"))
	float MaxAngularAcceleration;
	UPROPERTY(meta = (DeprecatedProperty, DeprecationMessage = "ExternalLinearDrag is deprecated. Please use ExternalLinearDragV instead."))
	float ExternalLinearDrag_DEPRECATED;
	// Additional linear drag applied to every body in addition to linear drag specified on them in the physics asset. 
	// When combined with ExternalLinearVelocity, this can be used to add a temporary wind-blown effect without having to tune linear drag on 
	// all the bodies in the physics asset. The result is that each body has a force equal to -ExternalLinearDragV * ExternalLinearVelocity applied to it, in 
	// additional to all other forces. The vector is in simulation local space.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
	FVector ExternalLinearDragV;
	// Additional velocity that is added to the component velocity so the simulation acts as if the actor is moving at speed, even when stationary. 
	// Vector is in world space. Units are cm/s. Could be used for a wind effects etc. Typical values are similar to the velocity of the object or effect, 
	// and usually around or less than 1000 for characters/wind.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
	FVector ExternalLinearVelocity;
	// Additional angular velocity that is added to the component angular velocity. This can be used to make the simulation act as if the actor is rotating
	// even when it is not. E.g., to apply physics to a character on a podium as the camera rotates around it, to emulate the podium itself rotating.
	// Vector is in world space. Units are rad/s.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Settings)
	FVector ExternalAngularVelocity;
	void PostSerialize(const FArchive& Ar);
};
#if WITH_EDITORONLY_DATA
template<>
struct TStructOpsTypeTraits<FPhysKartSimSpaceSettings> : public TStructOpsTypeTraitsBase2<FPhysKartSimSpaceSettings>
{
	enum
	{
		WithPostSerialize = true
	};
};
#endif
//

/**
 * 
 */
UCLASS()
class PHYSCHARPROJ_API UPhysKartMoveComp : public UPawnMovementComponent, public INetworkPredictionInterface
{
	GENERATED_BODY()

public:
 
	/**
	 * Default UObject constructor.
	 */
	UPhysKartMoveComp(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	//Begin UActorComponent Interface
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void OnRegister() override;
	virtual void BeginDestroy() override;
	//virtual void PostLoad() override;
	virtual void Deactivate() override;
	virtual void RegisterComponentTickFunctions(bool bRegister) override;
	//virtual void ApplyWorldOffset(const FVector& InOffset, bool bWorldShift) override;
	//End UActorComponent Interface

	virtual void InitializeComponent() override;
	virtual void BeginPlay() override;

	/** Post-physics tick function for this character */
	UPROPERTY()
	struct FPhysKartMoveCompPostPhysicsTickFunction PostPhysicsTickFunction;

	/** Tick function called after physics (sync scene) has finished simulation, before cloth */
	virtual void PostPhysicsTickComponent(float DeltaTime, FPhysKartMoveCompPostPhysicsTickFunction& ThisTickFunction);

	/** Get the Character that owns UpdatedComponent. */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PhysKartMovement")
	APhysKart* GetPhysKartOwner() const;

protected:

	/** Character movement component belongs to */
	UPROPERTY(Transient, DuplicateTransient)
	APhysKart* PhysKartOwner;

	//-------------------------------------------------------------------------------------------//
	// NetworkPredictionInterface

public:

	//BEGIN UMovementComponent Interface
	virtual void AddRadialForce(const FVector& Origin, float Radius, float Strength, enum ERadialImpulseFalloff Falloff) override;
	virtual void AddRadialImpulse(const FVector& Origin, float Radius, float Strength, enum ERadialImpulseFalloff Falloff, bool bVelChange) override;
	//END UMovementComponent Interface

protected:

	/** @note Movement update functions should only be called through StartNewPhysics()*/
	virtual void PhysRolling(float deltaTime, int32 Iterations);

public:
	
	/** This represents the magnitude of Lateral Wheel Force to exert on each wheel. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PhysKartMoveComponent)
	float fThrottleWheelScale;

	/** This represents the magnitude of Lateral Wheel Force to exert on each wheel. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PhysKartMoveComponent)
	float fSteeringWheelScale;

	/** This represents the magnitude of Longitudinal Wheel Force to exert on each wheel. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PhysKartMoveComponent)
	uint8 UseEuclideanTransformation : 1;
		
	/** This represents the magnitude of Longitudinal Wheel Force to exert on each wheel. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PhysKartMoveComponent)
	float fLongitudinalWheelForce;

	/** This represents the magnitude of Lateral Wheel Force to exert on each wheel. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PhysKartMoveComponent)
	float fLateralWheelForce;

	/** changes physics based on MovementMode */
	virtual void StartNewPhysics(float deltaTime, int32 Iterations);

	/** 
	 * Add impulse to character. Impulses are accumulated each tick and applied together
	 * so multiple calls to this function will accumulate.
	 * An impulse is an instantaneous force, usually applied once. If you want to continually apply
	 * forces each frame, use AddForce().
	 * Note that changing the momentum of characters like this can change the movement mode.
	 * 
	 * @param	Impulse				Impulse to apply.
	 * @param	bVelocityChange		Whether or not the impulse is relative to mass.
	 */
	UFUNCTION(BlueprintCallable, Category="Pawn|Components|CharacterMovement")
	virtual void AddImpulse( FVector Impulse, bool bVelocityChange = false );

	/** 
	 * Add force to character. Forces are accumulated each tick and applied together
	 * so multiple calls to this function will accumulate.
	 * Forces are scaled depending on timestep, so they can be applied each frame. If you want an
	 * instantaneous force, use AddImpulse.
	 * Adding a force always takes the actor's mass into account.
	 * Note that changing the momentum of characters like this can change the movement mode.
	 * 
	 * @param	Force			Force to apply.
	 */
	UFUNCTION(BlueprintCallable, Category="Pawn|Components|CharacterMovement")
	virtual void AddForce( FVector Force );

	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|CharacterMovement")
	virtual void AddTorque(FVector Torque);

	/** Applies momentum accumulated through AddImpulse() and AddForce(), then clears those forces. Does *not* use ClearAccumulatedForces() since that would clear pending launch velocity as well. */
	virtual void ApplyAccumulatedForces(float DeltaSeconds);

	/** Clears forces accumulated through AddImpulse() and AddForce(), and also pending launch velocity. */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|CharacterMovement")
	virtual void ClearAccumulatedForces();

	/**
	 * Actor's current movement mode (walking, falling, etc).
	 *    - walking:  Walking on a surface, under the effects of friction, and able to "step up" barriers. Vertical velocity is zero.
	 *    - falling:  Falling under the effects of gravity, after jumping or walking off the edge of a surface.
	 *    - flying:   Flying, ignoring the effects of gravity.
	 *    - swimming: Swimming through a fluid volume, under the effects of gravity and buoyancy.
	 *    - custom:   User-defined custom movement mode, including many possible sub-modes.
	 * This is automatically replicated through the Character owner and for client-server movement functions.
	 * @see SetMovementMode(), CustomMovementMode
	 */
	UPROPERTY(Category="Character Movement: MovementMode", BlueprintReadOnly)
	TEnumAsByte<enum EMovementMode> MovementMode;

	/**
	 * Current custom sub-mode if MovementMode is set to Custom.
	 * This is automatically replicated through the Character owner and for client-server movement functions.
	 * @see SetMovementMode()
	 */
	UPROPERTY(Category="Character Movement: MovementMode", BlueprintReadOnly)
	uint8 CustomMovementMode;

	/** Smoothing mode for simulated proxies in network game. */
	UPROPERTY(Category = "Character Movement (Networking)", EditAnywhere, BlueprintReadOnly)
	ENetworkSmoothingMode NetworkSmoothingMode;

	/** Max Acceleration (rate of change of velocity) */
	UPROPERTY(Category = "Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", UIMin = "0"))
	float MaxAcceleration;

	/** Returns maximum acceleration for the current state. */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PhysKartMovement")
	virtual float GetMaxAcceleration() const;

	/** Returns maximum acceleration for the current state. */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PhysKartMovement")
	virtual FVector GetAngularVelocity() const { return AngularVelocity; }

	/** Returns maximum acceleration for the current state. */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PhysKartMovement")
	virtual void SetLinearVelocity(FVector InAngVel);

	/** Returns maximum acceleration for the current state. */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PhysKartMovement")
	virtual void SetAngularVelocity(FVector InAngVel);


	virtual FVector GetPendingForceToApply() const { return PendingForceToApply; }
	virtual FVector GetPendingTorqueToApply() const { return PendingTorqueToApply; }
	virtual FVector GetPendingImpulseToApply() const { return PendingImpulseToApply; }

public:
	/** Set the user input for the vehicle throttle */
	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	void SetThrottleInput(float Throttle);

	/** Set the user input for the vehicle Brake */
	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	void SetBrakeInput(float Brake);

	/** Set the user input for the vehicle steering */
	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	void SetSteeringInput(float Steering);

	/** Set the user input for handbrake */
	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	void SetHandbrakeInput(bool bNewHandbrake);

	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	float GetSteeringInput()	const { return SteeringInput; }

	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	float GetThrottleInput()	const { return ThrottleInput; }

	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	float GetBrakeInput()		const { return BrakeInput; }

	UFUNCTION(BlueprintCallable, Category = "Game|Components|WheeledVehicleMovement")
	float GetHandbrakeInput()	const { return HandbrakeInput; }

	/** Temporarily holds launch velocity when pawn is to be launched so it happens at end of movement. */
	UPROPERTY()
	FVector PendingLaunchVelocity;

protected:

	// Wheeled Vehicle Inputs

	// Steering output to physics system. Range -1...1
	UPROPERTY(Transient)
	float SteeringInput;

	// Accelerator output to physics system. Range 0...1
	UPROPERTY(Transient)
	float ThrottleInput;

	// Brake output to physics system. Range 0...1
	UPROPERTY(Transient)
	float BrakeInput;

	// Handbrake output to physics system. Range 0...1
	UPROPERTY(Transient)
	float HandbrakeInput;

	// How much to press the brake when the player has release throttle
	UPROPERTY(EditAnywhere, Category=VehicleInput)
	float IdleBrakeInput;

	// Auto-brake when absolute vehicle forward speed is less than this (cm/s)
	UPROPERTY(EditAnywhere, Category=VehicleInput)
	float StopThreshold;

	// Auto-brake when vehicle forward speed is opposite of player input by at least this much (cm/s)
	UPROPERTY(EditAnywhere, Category = VehicleInput)
	float WrongDirectionThreshold;

protected:

	AController* GetController() const;

	// replicated state of vehicle 
	UPROPERTY()		//UPROPERTY(Transient, Replicated) // PaCorp * Changed to use INetworkPredictionInterface
	FReplicatedPhysKartState ReplicatedState;

	/** Current angular velocity of updated component. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector AngularVelocity;

	/** Current angular velocity of updated component. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector AngularAcceleration;

	/**
	 * Current acceleration vector (with magnitude).
	 * This is calculated each update based on the input vector and the constraints of MaxAcceleration and the current movement mode.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector Acceleration;

	/** current physx linear velocity */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector PhysX_Velocity;

	/** current physx angular velocity */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector PhysX_AngVelocity;

	/** current physx force to apply in physics step. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector PhysX_Force;

	/** current physx torque to apply in physics step. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector PhysX_Torque;

	/** current physx impulse to apply in physics step. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Velocity)
	FVector PhysX_Impulse;

	/**
	 * Current Delta Velocity.
	 * Calculated as the difference in velocity from Last frame to actual frame.
	 */
	UPROPERTY()
	FVector DeltaVelocity;

	/**
	 * Rotation after last PerformMovement or SimulateMovement update.
	 */
	UPROPERTY()
	FQuat LastUpdateRotation;

	/**
	 * Location after last PerformMovement or SimulateMovement update. Used internally to detect changes in position from outside character movement to try to validate the current floor.
	 */
	UPROPERTY()
	FVector LastUpdateLocation;

	/**
	 * Velocity after last PerformMovement or SimulateMovement update. Used internally to detect changes in velocity from external sources.
	 */
	UPROPERTY()
	FVector LastUpdateVelocity;

	/**
	 * Angular Velocity after last PerformMovement or SimulateMovement update. Used internally to detect changes in angular velocity from external sources.
	 */
	UPROPERTY()
	FVector LastUpdateAngularVelocity;

	/** Timestamp when location or rotation last changed during an update. Only valid on the server. */
	UPROPERTY(Transient)
	float ServerLastTransformUpdateTimeStamp;

	/** Timestamp of last client adjustment sent. See NetworkMinTimeBetweenClientAdjustments. */
	UPROPERTY(Transient)
	float ServerLastClientGoodMoveAckTime;

	/** Timestamp of last client adjustment sent. See NetworkMinTimeBetweenClientAdjustments. */
	UPROPERTY(Transient)
	float ServerLastClientAdjustmentTime;

	/** Accumulated impulse to be added next tick. */
	UPROPERTY()
	FVector PendingImpulseToApply;

	/** Accumulated force to be added next tick. */
	UPROPERTY()
	FVector PendingForceToApply;

	/** Accumulated force to be added next tick. */
	UPROPERTY()
	FVector PendingTorqueToApply;

	/**
	 * Modifier to applied to values such as acceleration and max speed due to analog input.
	 */
	UPROPERTY()
	float AnalogInputModifier;

	/** Computes the analog input modifier based on current input vector and/or acceleration. */
	virtual float ComputeAnalogInputModifier() const;

	/** Used for throttling "stuck in geometry" logging. */
	float LastStuckWarningTime;

	/** Used when throttling "stuck in geometry" logging, to output the number of events we skipped if throttling. */
	uint32 StuckWarningCountSinceNotify;

	/**
	 * Used to limit number of jump apex attempts per tick.
	 * @see MaxJumpApexAttemptsPerSimulation
	 */
	int32 NumJumpApexAttempts;

	/**
	 * True during movement update.
	 * Used internally so that attempts to change PhysKartOwner and UpdatedComponent are deferred until after an update.
	 * @see IsMovementInProgress()
	 */
	UPROPERTY()
	uint8 bMovementInProgress:1;

public:

	/**
	 * If true, high-level movement updates will be wrapped in a movement scope that accumulates updates and defers a bulk of the work until the end.
	 * When enabled, touch and hit events will not be triggered until the end of multiple moves within an update, which can improve performance.
	 *
	 * @see FScopedMovementUpdate
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, AdvancedDisplay)
	uint8 bEnableScopedMovementUpdates:1;

	/**
	 * Optional scoped movement update to combine moves for cheaper performance on the server when the client sends two moves in one packet.
	 * Be warned that since this wraps a larger scope than is normally done with bEnableScopedMovementUpdates, this can result in subtle changes in behavior
	 * in regards to when overlap events are handled, when attached components are moved, etc.
	 *
	 * @see bEnableScopedMovementUpdates
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, AdvancedDisplay)
	uint8 bEnableServerDualMoveScopedMovementUpdates : 1;

	/** Ignores size of acceleration component, and forces max acceleration to drive character at full velocity. */
	UPROPERTY()
	uint8 bForceMaxAccel : 1;

	/**
	 * If true, movement will be performed even if there is no Controller for the Character owner.
	 * Normally without a Controller, movement will be aborted and velocity and acceleration are zeroed if the character is walking.
	 * Characters that are spawned without a Controller but with this flag enabled will initialize the movement mode to DefaultLandMovementMode or DefaultWaterMovementMode appropriately.
	 * @see DefaultLandMovementMode, DefaultWaterMovementMode
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
	uint8 bRunPhysicsWithNoController:1;

	/**
	 * Force the Character in MOVE_Walking to do a check for a valid floor even if he hasn't moved. Cleared after next floor check.
	 * Normally if bAlwaysCheckFloor is false we try to avoid the floor check unless some conditions are met, but this can be used to force the next check to always run.
	 */
	UPROPERTY(Category = "Character Movement: Walking", VisibleInstanceOnly, BlueprintReadWrite, AdvancedDisplay)
	uint8 bForceNextFloorCheck : 1;

	/**
	 * Signals that smoothed position/rotation has reached target, and no more smoothing is necessary until a future update.
	 * This is used as an optimization to skip calls to SmoothClientPosition() when true. SmoothCorrection() sets it false when a new network update is received.
	 * SmoothClientPosition_Interpolate() sets this to true when the interpolation reaches the target, before one last call to SmoothClientPosition_UpdateVisuals().
	 * If this is not desired, override SmoothClientPosition() to always set this to false to avoid this feature.
	 */
	uint8 bNetworkSmoothingComplete:1;

	/** Flag indicating the client correction was larger than NetworkLargeClientCorrectionThreshold. */
	uint8 bNetworkLargeClientCorrection : 1;

	/**
	 * Whether we skip prediction on frames where a proxy receives a network update. This can avoid expensive prediction on those frames,
	 * with the side-effect of predicting with a frame of additional latency.
	 */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly)
	uint8 bNetworkSkipProxyPredictionOnNetUpdate:1;

	/**
	 * Flag used on the server to determine whether to always replicate ReplicatedServerLastTransformUpdateTimeStamp to clients.
	 * Normally this is only sent when the network smoothing mode on character movement is set to Linear smoothing (on the server), to save bandwidth.
	 * Setting this to true will force the timestamp to replicate regardless, in case the server doesn't know about the smoothing mode, or if the timestamp is used for another purpose.
	 */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, AdvancedDisplay)
	uint8 bNetworkAlwaysReplicateTransformUpdateTimestamp:1;

public:

	/** true to update PhysKartOwner and UpdatedComponent after movement ends */
	UPROPERTY()
	uint8 bDeferUpdateMoveComponent:1;

	/** Used by movement code to determine if a change in position is based on normal movement or a teleport. If not a teleport, velocity can be recomputed based on the change in position. */
	UPROPERTY(Category = "Character Movement (General Settings)", Transient, VisibleInstanceOnly, BlueprintReadWrite)
	uint8 bJustTeleported : 1;
	
	/** What to update PhysKartOwner and UpdatedComponent after movement ends */
	UPROPERTY()
	USceneComponent* DeferredUpdatedMoveComponent;	
	
	/** Get the value of ServerLastTransformUpdateTimeStamp. */
	FORCEINLINE float GetServerLastTransformUpdateTimeStamp() const { return ServerLastTransformUpdateTimeStamp; }

		/**
	 * Compute remaining time step given remaining time and current iterations.
	 * The last iteration (limited by MaxSimulationIterations) always returns the remaining time, which may violate MaxSimulationTimeStep.
	 *
	 * @param RemainingTime		Remaining time in the tick.
	 * @param Iterations		Current iteration of the tick (starting at 1).
	 * @return The remaining time step to use for the next sub-step of iteration.
	 * @see MaxSimulationTimeStep, MaxSimulationIterations
	 */
	float GetSimulationTimeStep(float RemainingTime, int32 Iterations) const;

	/**
	 * Max time delta for each discrete simulation step.
	 * Used primarily in the the more advanced movement modes that break up larger time steps (usually those applying gravity such as falling and walking).
	 * Lowering this value can address issues with fast-moving objects or complex collision scenarios, at the cost of performance.
	 *
	 * WARNING: if (MaxSimulationTimeStep * MaxSimulationIterations) is too low for the min framerate, the last simulation step may exceed MaxSimulationTimeStep to complete the simulation.
	 * @see MaxSimulationIterations
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta=(ClampMin="0.0166", ClampMax="0.50", UIMin="0.0166", UIMax="0.50"))
	float MaxSimulationTimeStep;

	/**
	 * Max number of iterations used for each discrete simulation step.
	 * Used primarily in the the more advanced movement modes that break up larger time steps (usually those applying gravity such as falling and walking).
	 * Increasing this value can address issues with fast-moving objects or complex collision scenarios, at the cost of performance.
	 *
	 * WARNING: if (MaxSimulationTimeStep * MaxSimulationIterations) is too low for the min framerate, the last simulation step may exceed MaxSimulationTimeStep to complete the simulation.
	 * @see MaxSimulationTimeStep
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta=(ClampMin="1", ClampMax="25", UIMin="1", UIMax="25"))
	int32 MaxSimulationIterations;

	/**
	 * Max number of attempts per simulation to attempt to exactly reach the jump apex when falling movement reaches the top of the arc.
	 * Limiting this prevents deep recursion when special cases cause collision or other conditions which reactivate the apex condition.
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta=(ClampMin="1", ClampMax="4", UIMin="1", UIMax="4"))
	int32 MaxJumpApexAttemptsPerSimulation;

	/**
	* Max distance we allow simulated proxies to depenetrate when moving out of anything but Pawns.
	* This is generally more tolerant than with Pawns, because other geometry is either not moving, or is moving predictably with a bit of delay compared to on the server.
	* @see MaxDepenetrationWithGeometryAsProxy, MaxDepenetrationWithPawn, MaxDepenetrationWithPawnAsProxy
	*/
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta=(ClampMin="0", UIMin="0"))
	float MaxDepenetrationWithGeometry;

	/**
	* Max distance we allow simulated proxies to depenetrate when moving out of anything but Pawns.
	* This is generally more tolerant than with Pawns, because other geometry is either not moving, or is moving predictably with a bit of delay compared to on the server.
	* @see MaxDepenetrationWithGeometry, MaxDepenetrationWithPawn, MaxDepenetrationWithPawnAsProxy
	*/
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta=(ClampMin="0", UIMin="0"))
	float MaxDepenetrationWithGeometryAsProxy;

	/**
	* Max distance we are allowed to depenetrate when moving out of other Pawns.
	* @see MaxDepenetrationWithGeometry, MaxDepenetrationWithGeometryAsProxy, MaxDepenetrationWithPawnAsProxy
	*/
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta=(ClampMin="0", UIMin="0"))
	float MaxDepenetrationWithPawn;

	/**
	 * Max distance we allow simulated proxies to depenetrate when moving out of other Pawns.
	 * Typically we don't want a large value, because we receive a server authoritative position that we should not then ignore by pushing them out of the local player.
	 * @see MaxDepenetrationWithGeometry, MaxDepenetrationWithGeometryAsProxy, MaxDepenetrationWithPawn
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta=(ClampMin="0", UIMin="0"))
	float MaxDepenetrationWithPawnAsProxy;

	/**
	 * How long to take to smoothly interpolate from the old pawn position on the client to the corrected one sent by the server. Not used by Linear smoothing.
	 */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, AdvancedDisplay, meta=(ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float NetworkSimulatedSmoothLocationTime;

	/**
	 * How long to take to smoothly interpolate from the old pawn rotation on the client to the corrected one sent by the server. Not used by Linear smoothing.
	 */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, AdvancedDisplay, meta=(ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float NetworkSimulatedSmoothRotationTime;

	/**
	* Similar setting as NetworkSimulatedSmoothLocationTime but only used on Listen servers.
	*/
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, AdvancedDisplay, meta=(ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float ListenServerNetworkSimulatedSmoothLocationTime;

	/**
	* Similar setting as NetworkSimulatedSmoothRotationTime but only used on Listen servers.
	*/
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, AdvancedDisplay, meta=(ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float ListenServerNetworkSimulatedSmoothRotationTime;

	/** Maximum distance character is allowed to lag behind server location when interpolating between updates. */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, meta=(ClampMin="0.0", UIMin="0.0"))
	float NetworkMaxSmoothUpdateDistance;

	/**
	 * Maximum distance beyond which character is teleported to the new server location without any smoothing.
	 */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, meta=(ClampMin="0.0", UIMin="0.0"))
	float NetworkNoSmoothUpdateDistance;

	/**
	 * Minimum time on the server between acknowledging good client moves. This can save on bandwidth. Set to 0 to disable throttling.
	 */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, meta=(ClampMin="0.0", UIMin="0.0"))
	float NetworkMinTimeBetweenClientAckGoodMoves;

	/**
	 * Minimum time on the server between sending client adjustments when client has exceeded allowable position error.
	 * Should be >= NetworkMinTimeBetweenClientAdjustmentsLargeCorrection (the larger value is used regardless).
  	 * This can save on bandwidth. Set to 0 to disable throttling.
	 * @see ServerLastClientAdjustmentTime
	 */
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, meta=(ClampMin="0.0", UIMin="0.0"))
	float NetworkMinTimeBetweenClientAdjustments;

	/**
	* Minimum time on the server between sending client adjustments when client has exceeded allowable position error by a large amount (NetworkLargeClientCorrectionDistance).
	* Should be <= NetworkMinTimeBetweenClientAdjustments (the smaller value is used regardless).
	* @see NetworkMinTimeBetweenClientAdjustments
	*/
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, meta=(ClampMin="0.0", UIMin="0.0"))
	float NetworkMinTimeBetweenClientAdjustmentsLargeCorrection;

	/**
	* If client error is larger than this, sets bNetworkLargeClientCorrection to reduce delay between client adjustments.
	* @see NetworkMinTimeBetweenClientAdjustments, NetworkMinTimeBetweenClientAdjustmentsLargeCorrection
	*/
	UPROPERTY(Category="Character Movement (Networking)", EditDefaultsOnly, meta=(ClampMin="0.0", UIMin="0.0"))
	float NetworkLargeClientCorrectionDistance;

	/**
	* If client error is larger than this, sets bNetworkLargeClientCorrection to reduce delay between client adjustments.
	* @see NetworkMinTimeBetweenClientAdjustments, NetworkMinTimeBetweenClientAdjustmentsLargeCorrection
	*/
	UPROPERTY(Category = "Character Movement (Networking)", EditDefaultsOnly, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float NetworkLargeClientCorrectionQuat;

	UPROPERTY(Category = "Character Movement (Networking)", EditDefaultsOnly, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float NetworkMaxQuatErrorScale;

	/** Used in determining if pawn is going off ledge.  If the ledge is "shorter" than this value then the pawn will be able to walk off it. **/
	UPROPERTY(Category="Character Movement: Walking", EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
	float LedgeCheckThreshold;

	/** When exiting water, jump if control pitch angle is this high or above. */
	UPROPERTY(Category="Character Movement: Swimming", EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
	float JumpOutOfWaterPitch;

	///** Information about the floor the Character is standing on (updated only during walking movement). */
	//UPROPERTY(Category="Character Movement: Walking", VisibleInstanceOnly, BlueprintReadOnly)
	//FFindFloorResult CurrentFloor;

	/**
	 * Default movement mode when not in water. Used at player startup or when teleported.
	 * @see DefaultWaterMovementMode
	 * @see bRunPhysicsWithNoController
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<enum EMovementMode> DefaultLandMovementMode;

	/**
	 * Default movement mode when in water. Used at player startup or when teleported.
	 * @see DefaultLandMovementMode
	 * @see bRunPhysicsWithNoController
	 */
	UPROPERTY(Category="Character Movement (General Settings)", EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<enum EMovementMode> DefaultWaterMovementMode;


private:
	/**
	 * Ground movement mode to switch to after falling and resuming ground movement.
	 * Only allowed values are: MOVE_Walking, MOVE_NavWalking.
	 * @see SetGroundMovementMode(), GetGroundMovementMode()
	 */
	UPROPERTY(Transient)
	TEnumAsByte<enum EMovementMode> GroundMovementMode;

public:

	/** True when a network replication update is received for simulated proxies. */
	UPROPERTY(Transient)
	uint8 bNetworkUpdateReceived : 1;

	/** True when the networked movement mode has been replicated. */
	UPROPERTY(Transient)
	uint8 bNetworkMovementModeChanged : 1;

	/** 
	 * If true, we should ignore server location difference checks for client error on this movement component.
	 * This can be useful when character is moving at extreme speeds for a duration and you need it to look
	 * smooth on clients without the server correcting the client. Make sure to disable when done, as this would
	 * break this character's server-client movement correction.
	 * @see bServerAcceptClientAuthoritativePosition, ServerCheckClientError()
	 */
	UPROPERTY(Transient, Category="Character Movement", EditAnywhere, BlueprintReadWrite)
	uint8 bIgnoreClientMovementErrorChecksAndCorrection:1;

	/**
	 * If true, and server does not detect client position error, server will copy the client movement location/velocity/etc after simulating the move.
	 * This can be useful for short bursts of movement that are difficult to sync over the network.
	 * Note that if bIgnoreClientMovementErrorChecksAndCorrection is used, this means the server will not detect an error.
	 * Also see GameNetworkManager->ClientAuthorativePosition which permanently enables this behavior.
	 * @see bIgnoreClientMovementErrorChecksAndCorrection, ServerShouldUseAuthoritativePosition()
	 */
	UPROPERTY(Transient, Category="Character Movement", EditAnywhere, BlueprintReadWrite)
	uint8 bServerAcceptClientAuthoritativePosition : 1;

public:

	/**
	 * Change movement mode.
	 *
	 * @param NewMovementMode	The new movement mode
	 * @param NewCustomMode		The new custom sub-mode, only applicable if NewMovementMode is Custom.
	 */
	UFUNCTION(BlueprintCallable, Category="Pawn|Components|PhysKartMovement")
	virtual void SetMovementMode(EMovementMode NewMovementMode, uint8 NewCustomMode = 0);

	/**
	 * Set movement mode to use when returning to walking movement (either MOVE_Walking or MOVE_NavWalking).
	 * If movement mode is currently one of Walking or NavWalking, this will also change the current movement mode (via SetMovementMode())
	 * if the new mode is not the current ground mode.
	 * 
	 * @param  NewGroundMovementMode New ground movement mode. Must be either MOVE_Walking or MOVE_NavWalking, other values are ignored.
	 * @see GroundMovementMode
	 */
	 void SetGroundMovementMode(EMovementMode NewGroundMovementMode);

	/**
	 * Get current GroundMovementMode value.
	 * @return current GroundMovementMode
	 * @see GroundMovementMode, SetGroundMovementMode()
	 */
	 EMovementMode GetGroundMovementMode() const { return GroundMovementMode; }

protected:

	/** Called after MovementMode has changed. Base implementation does special handling for starting certain modes, then notifies the PhysKartOwner. */
	virtual void OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode);

public:

	virtual uint8 PackNetworkMovementMode() const;
	virtual void UnpackNetworkMovementMode(const uint8 ReceivedMode, TEnumAsByte<EMovementMode>& OutMode, uint8& OutCustomMode, TEnumAsByte<EMovementMode>& OutGroundMode) const;
	virtual void ApplyNetworkMovementMode(const uint8 ReceivedMode);

	//

	/** Update the character state in PerformMovement right before doing the actual position change */
	virtual void UpdateCharacterStateBeforeMovement(float DeltaSeconds);

	/** Update the character state in PerformMovement after the position change. Some rotation updates happen after this. */
	virtual void UpdateCharacterStateAfterMovement(float DeltaSeconds);


	/** Updates acceleration and perform movement, called from the TickComponent on the authoritative side for controlled characters,
	 *	or on the client for characters without a controller when either playing root motion or bRunPhysicsWithNoController is true.
	 */
	virtual void ControlledCharacterMove(const FVector& InputVector, float DeltaSeconds);

	//

	/** Set movement mode to the default based on the current physics volume. */
	virtual void SetDefaultMovementMode();

	/** Returns MovementMode string */
	virtual FString GetMovementName() const;

public:

	/**
	 * Returns true if currently performing a movement update.
	 * @see bMovementInProgress
	 */
	bool IsMovementInProgress() const { return bMovementInProgress; }

	/** Return true if we have a valid PhysKartOwner and UpdatedComponent. */
	virtual bool HasValidData() const;

	/** Round acceleration, for better consistency and lower bandwidth in networked games. */
	virtual FVector RoundAcceleration(FVector InAccel) const;

	virtual void FlushServerMoves();

protected:

	/** Get the collision shape for the Pawn owner, possibly reduced in size depending on ShrinkMode.
	 * @param ShrinkMode			Controls the way the capsule is resized.
	 * @param CustomShrinkAmount	The amount to shrink the capsule, used only for ShrinkModes that specify custom.
	 * @return The capsule extent of the Pawn owner, possibly reduced in size depending on ShrinkMode.
	 */
	FCollisionShape GetWheeledPawnCollisionShape() const;

	/** Enforce constraints on input given current state. For instance, don't move upwards if walking and looking up. */
	virtual FVector ConstrainInputAcceleration(const FVector& InputAcceleration) const;

	/** Scale input acceleration, based on movement acceleration rate. */
	virtual FVector ScaleInputAcceleration(const FVector& InputAcceleration) const;

	/**
	 * Event triggered at the end of a movement update. If scoped movement updates are enabled (bEnableScopedMovementUpdates), this is within such a scope.
	 * If that is not desired, bind to the PhysKartOwner's OnMovementUpdated event instead, as that is triggered after the scoped movement update.
	 */
	virtual void OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity);

	/** Internal function to call OnMovementUpdated delegate on PhysKartOwner. */
	virtual void CallMovementUpdateDelegate(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity);

protected:

	virtual void ClearInput();

	/** Read current state for simulation */
	virtual void UpdateState(float DeltaTime);

	/** Perform movement on an autonomous client */
	virtual void PerformMovement(float DeltaTime);

public:

	/** Force a client update by making it appear on the server that the client hasn't updated in a long time. */
	virtual void ForceReplicationUpdate();

	/** Force a client adjustment. Resets ServerLastClientAdjustmentTime. */
	void ForceClientAdjustment();

	//--------------------------------
	// INetworkPredictionInterface implementation

	//--------------------------------
	// Server hook
	//--------------------------------
	virtual void SendClientAdjustment() override;
	virtual bool ForcePositionUpdate(float DeltaTime) override;

	//--------------------------------
	// Client hook
	//--------------------------------

	/**
	 * React to new transform from network update. Sets bNetworkSmoothingComplete to false to ensure future smoothing updates.
	 * IMPORTANT: It is expected that this function triggers any movement/transform updates to match the network update if desired.
	 */
	virtual void SmoothCorrection(const FVector& OldLocation, const FQuat& OldRotation, const FVector& NewLocation, const FQuat& NewRotation) override;


	/** Get prediction data for a client game. Should not be used if not running as a client. Allocates the data on demand and can be overridden to allocate a custom override if desired. Result must be a FNetworkPredictionData_Client_PhysKart. */
	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;
	/** Get prediction data for a server game. Should not be used if not running as a server. Allocates the data on demand and can be overridden to allocate a custom override if desired. Result must be a FNetworkPredictionData_Server_PhysKart. */
	virtual class FNetworkPredictionData_Server* GetPredictionData_Server() const override;

	class FNetworkPredictionData_Client_PhysKart* GetPredictionData_Client_PhysKart() const;
	class FNetworkPredictionData_Server_PhysKart* GetPredictionData_Server_PhysKart() const;

	virtual bool HasPredictionData_Client() const override;
	virtual bool HasPredictionData_Server() const override;

	virtual void ResetPredictionData_Client() override;
	virtual void ResetPredictionData_Server() override;

	static uint32 PackYawAndPitchTo32(const float Yaw, const float Pitch);

protected:

	class FNetworkPredictionData_Client_PhysKart* ClientPredictionData;
	class FNetworkPredictionData_Server_PhysKart* ServerPredictionData;

	// PaCorp * SECOND BATCH INTEGRATION
	FRandomStream RandomStream;

	/**
	 * Smooth mesh location for network interpolation, based on values set up by SmoothCorrection.
	 * Internally this simply calls SmoothClientPosition_Interpolate() then SmoothClientPosition_UpdateVisuals().
	 * This function is not called when bNetworkSmoothingComplete is true.
	 * @param DeltaSeconds Time since last update.
	 */
	virtual void SmoothClientPosition(float DeltaSeconds);

	/**
	 * Update interpolation values for client smoothing. Does not change actual mesh location.
	 * Sets bNetworkSmoothingComplete to true when the interpolation reaches the target.
	 */
	void SmoothClientPosition_Interpolate(float DeltaSeconds);

	/** Update mesh location based on interpolated values. */
	void SmoothClientPosition_UpdateVisuals();

	/*
	========================================================================
	Here's how player movement prediction, replication and correction works in network games:
	
	Every tick, the TickComponent() function is called.  It figures out the acceleration and rotation change for the frame,
	and then calls PerformMovement() (for locally controlled Characters), or ReplicateMoveToServer() (if it's a network client).
	
	ReplicateMoveToServer() saves the move (in the PendingMove list), calls PerformMovement(), and then replicates the move
	to the server by calling the replicated function ServerMove() - passing the movement parameters, the client's
	resultant position, and a timestamp.
	
	ServerMove() is executed on the server.  It decodes the movement parameters and causes the appropriate movement
	to occur.  It then looks at the resulting position and if enough time has passed since the last response, or the
	position error is significant enough, the server calls ClientAdjustPosition(), a replicated function.
	
	ClientAdjustPosition() is executed on the client.  The client sets its position to the servers version of position,
	and sets the bUpdatePosition flag to true.
	
	When TickComponent() is called on the client again, if bUpdatePosition is true, the client will call
	ClientUpdatePosition() before calling PerformMovement().  ClientUpdatePosition() replays all the moves in the pending
	move list which occurred after the timestamp of the move the server was adjusting.
	*/

	/** Perform local movement and send the move to the server. */
	virtual void ReplicateMoveToServer(float DeltaTime, const FVector& NewAcceleration);

	FPhysKartSavedMovePtr PostPhysMovePtr;
	FPhysKartSavedMovePtr PostPhysOldMove;

	virtual void ReplicateMoveToServerPostPhysics(float DeltaTime, const FVector& NewAcceleration);

	/** If bUpdatePosition is true, then replay any unacked moves. Returns whether any moves were actually replayed. */
	virtual bool ClientUpdatePositionAfterServerUpdate();

	/** Call the appropriate replicated ServerMove() function to send a client player move to the server. */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(CallServerMove, CallServerMovePacked)
	virtual void CallServerMove(const FSavedMove_PhysKart* NewMove, const FSavedMove_PhysKart* OldMove);
	/**
	 * On the client, calls the ServerMovePacked_ClientSend() function with packed movement data.
	 * First the FPhysKartNetworkMoveDataContainer from GetNetworkMoveDataContainer() is updated with ClientFillNetworkMoveData(), then serialized into a data stream to send client player moves to the server.
	 */
	virtual void CallServerMovePacked(const FSavedMove_PhysKart* NewMove, const FSavedMove_PhysKart* PendingMove, const FSavedMove_PhysKart* OldMove);
	
	/**
	 * Have the server check if the client is outside an error tolerance, and queue a client adjustment if so.
	 * If either GetPredictionData_Server_PhysKart()->bForceClientUpdate or ServerCheckClientError() are true, the client adjustment will be sent.
	 * RelativeClientLocation will be a relative location if MovementBaseUtility::UseRelativePosition(ClientMovementBase) is true, or a world location if false.
	 * @see ServerCheckClientError()
	 * PaCorp * Removed UPrimitiveComponent* ClientMovementBase, FName ClientBaseBoneName,  parameters as WheeledVehicle does not care.
	 */
	virtual void ServerMoveHandleClientError(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& RelativeClientLocation, const FQuat& RelativeClientRotation, uint8 ClientMovementMode);

	/**
	 * Check for Server-Client disagreement in position or other movement state important enough to trigger a client correction.
	 * @see ServerMoveHandleClientError()
	 */
	//virtual bool ServerCheckClientError(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& ClientWorldLocation, const FVector& RelativeClientLocation, uint8 ClientMovementMode);
	virtual bool ServerCheckClientError(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& ClientWorldLocation, const FVector& RelativeClientLocation, uint8 ClientMovementMode, const FVector& AngAccel, FQuat& ClientWorldQuat, const FQuat& RelativeClientQuat);

	/**
	 * Check position error within ServerCheckClientError(). Set bNetworkLargeClientCorrection to true if the correction should be prioritized (delayed less in SendClientAdjustment).
	 */
	virtual bool ServerExceedsAllowablePositionError(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& ClientWorldLocation, const FVector& RelativeClientLocation, uint8 ClientMovementMode);

	/**
	 * If ServerCheckClientError() does not find an error, this determines if the server should also copy the client's movement params rather than keep the server sim result.
	 */
	virtual bool ServerShouldUseAuthoritativePosition(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& ClientWorldLocation, const FVector& RelativeClientLocation, uint8 ClientMovementMode);

	/**
	 * Check position error within ServerCheckClientError(). Set bNetworkLargeClientCorrection to true if the correction should be prioritized (delayed less in SendClientAdjustment).
	 */
	virtual bool ServerExceedsAllowableQuatError(float ClientTimeStamp, float DeltaTime, const FVector& AngAccel, const FQuat& ClientWorldQuat, const FQuat& RelativeClientQuat, uint8 ClientMovementMode);

	/**
	 * If ServerCheckClientError() does not find an error, this determines if the server should also copy the client's movement params rather than keep the server sim result.
	 */
	virtual bool ServerShouldUseAuthoritativeQuat(float ClientTimeStamp, float DeltaTime, const FVector& AngAccel, const FQuat& ClientWorldQuat, const FQuat& RelativeClientQuat, uint8 ClientMovementMode);


	/* Process a move at the given time stamp, given the compressed flags representing various events that occurred (ie jump). */
	virtual void MoveAutonomous( float ClientTimeStamp, float DeltaTime, FReplicatedPhysKartState InReplicatedState, uint8 CompressedFlags, const FVector& NewAccel, FTransform NewTransform = FTransform::Identity);

	/*  **/
	//virtual void SetWorldTransformFromPivot(FTransform NewTransform, FTransform InPivotTransform);

	/** Unpack compressed flags from a saved move and set state accordingly. See FSavedMove_PhysKart. */
	virtual void UpdateFromCompressedFlags(uint8 Flags);

	/** Return true if it is OK to delay sending this player movement to the server, in order to conserve bandwidth. */
	virtual bool CanDelaySendingMove(const FPhysKartSavedMovePtr& NewMove);

	/** Determine minimum delay between sending client updates to the server. If updates occur more frequently this than this time, moves may be combined delayed. */
	virtual float GetClientNetSendDeltaTime(const APlayerController* PC, const FNetworkPredictionData_Client_PhysKart* ClientData, const FPhysKartSavedMovePtr& NewMove) const;

	/** Ticks the characters pose and accumulates root motion */
	void TickCharacterPose(float DeltaTime);

	/** On the server if we know we are having our replication rate throttled, this method checks if important replicated properties have changed that should cause us to return to the normal replication rate. */
	virtual bool ShouldCancelAdaptiveReplication() const;

public:

	/** React to instantaneous change in position. Invalidates cached floor recomputes it if possible if there is a current movement base. */
	virtual void UpdateFloorFromAdjustment();

	/** Minimum time between client TimeStamp resets.
	 !! This has to be large enough so that we don't confuse the server if the client can stall or timeout.
	 We do this as we use floats for TimeStamps, and server derives DeltaTime from two TimeStamps. 
	 As time goes on, accuracy decreases from those floating point numbers.
	 So we trigger a TimeStamp reset at regular intervals to maintain a high level of accuracy. */
	UPROPERTY()
	float MinTimeBetweenTimeStampResets;

	/** On the Server, verify that an incoming client TimeStamp is valid and has not yet expired.
		It will also handle TimeStamp resets if it detects a gap larger than MinTimeBetweenTimeStampResets / 2.f
		!! ServerData.CurrentClientTimeStamp can be reset !!
		@returns true if TimeStamp is valid, or false if it has expired. */
	virtual bool VerifyClientTimeStamp(float TimeStamp, FNetworkPredictionData_Server_PhysKart & ServerData);

protected:

	/** Clock time on the server of the last timestamp reset. */
	float LastTimeStampResetServerTime;

	/** Internal const check for client timestamp validity without side-effects. 
	  * @see VerifyClientTimeStamp */
	bool IsClientTimeStampValid(float TimeStamp, const FNetworkPredictionData_Server_PhysKart& ServerData, bool& bTimeStampResetDetected) const;

	/** Called by UCharacterMovementComponent::VerifyClientTimeStamp() when a client timestamp reset has been detected and is valid. */
	virtual void OnClientTimeStampResetDetected();

	/** 
	 * Processes client timestamps from ServerMoves, detects and protects against time discrepancy between client-reported times and server time
	 * Called by UCharacterMovementComponent::VerifyClientTimeStamp() for valid timestamps.
	 */
	virtual void ProcessClientTimeStampForTimeDiscrepancy(float ClientTimeStamp, FNetworkPredictionData_Server_PhysKart& ServerData);

	/** 
	 * Called by UCharacterMovementComponent::ProcessClientTimeStampForTimeDiscrepancy() (on server) when the time from client moves 
	 * significantly differs from the server time, indicating potential time manipulation by clients (speed hacks, significant network 
	 * issues, client performance problems) 
	 * @param CurrentTimeDiscrepancy		Accumulated time difference between client ServerMove and server time - this is bounded
	 *										by MovementTimeDiscrepancy config variables in AGameNetworkManager, and is the value with which
	 *										we test against to trigger this function. This is reset when MovementTimeDiscrepancy resolution
	 *										is enabled
	 * @param LifetimeRawTimeDiscrepancy	Accumulated time difference between client ServerMove and server time - this is unbounded
	 *										and does NOT get affected by MovementTimeDiscrepancy resolution, and is useful as a longer-term
	 *										view of how the given client is performing. High magnitude unbounded error points to
	 *										intentional tampering by a client vs. occasional "naturally caused" spikes in error due to
	 *										burst packet loss/performance hitches
	 * @param Lifetime						Game time over which LifetimeRawTimeDiscrepancy has accrued (useful for determining severity
	 *										of LifetimeUnboundedError)
	 * @param CurrentMoveError				Time difference between client ServerMove and how much time has passed on the server for the
	 *										current move that has caused TimeDiscrepancy to accumulate enough to trigger detection.
	 */
	virtual void OnTimeDiscrepancyDetected(float CurrentTimeDiscrepancy, float LifetimeRawTimeDiscrepancy, float Lifetime, float CurrentMoveError);


public:

	/**
	 * The actual network RPCs for character movement are passed to APhysKart, which wrap to the _Implementation call here, to avoid Component RPC overhead.
	 * For example:
	 *		Client: UCharacterMovementComponent::ServerMovePacked_ClientSend() => Calls CharacterOwner->ServerMove() triggering RPC on the server.
	 *		Server: APhysKart::ServerMovePacked_Implementation() from the RPC => Calls CharacterMovement->ServerMove_ServerReceive(), unpacked and sent to ServerMove_ServerHandleMoveData().
	 *
	 *	ServerMove_ClientSend() and ServerMove_ServerReceive() use a bitstream created from the current FPhysKartNetworkMoveData data container that contains the client move.
	 *	See GetNetworkMoveDataContainer()/SetNetworkMoveDataContainer() for details on setting a custom container with custom unpacking through FPhysKartNetworkMoveData::Serialize().
	 * 
	*/

	/**
	 * Wrapper to send packed move data to the server, through the Character.
	 * @see CallServerMovePacked()
	 */
	void ServerMovePacked_ClientSend(const FPhysKartServerMovePackedBits& PackedBits);

	/**
	 * On the server, receives packed move data from the Character RPC, unpacks them into the FPhysKartNetworkMoveDataContainer returned from GetNetworkMoveDataContainer(),
	 * and passes the data container to ServerMove_HandleMoveData().
	 */
	void ServerMovePacked_ServerReceive(const FPhysKartServerMovePackedBits& PackedBits);

	/**
	 * Determines whether to use packed movement RPCs with variable length payloads, or legacy code which has multiple functions required for different situations.
	 * The default implementation checks the console variable "p.NetUsePackedMovementRPCs" and returns true if it is non-zero.
	 */
	virtual bool ShouldUsePackedMovementRPCs() const;

	/* Sends a move response from the server to the client (through character to avoid component RPC overhead), eventually calling MoveResponsePacked_ClientReceive() on the client. */
	void MoveResponsePacked_ServerSend(const FPhysKartMoveResponsePackedBits& PackedBits);

	/* On the client, receives a packed move response from the server, unpacks it by serializing into the MoveResponseContainer from GetMoveResponseDataContainer(), and passes the data container to ClientHandleMoveResponse(). */
	void MoveResponsePacked_ClientReceive(const FPhysKartMoveResponsePackedBits& PackedBits);

	/* If no client adjustment is needed after processing received ServerMove(), ack the good move so client can remove it from SavedMoves */
	virtual void ClientAckGoodMove_Implementation(float TimeStamp);

	/* Replicate position correction to client, associated with a timestamped servermove.  Client will replay subsequent moves after applying adjustment. */
	//virtual void ClientAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FVector NewVel, UPrimitiveComponent* NewBase, FName NewBaseBoneName, bool bHasBase, bool bBaseRelativePosition, uint8 ServerMovementMode);
	virtual void ClientAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FVector NewVel, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode);

	/* Bandwidth saving version, when velocity is zeroed */
	//virtual void ClientVeryShortAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, UPrimitiveComponent* NewBase, FName NewBaseBoneName, bool bHasBase, bool bBaseRelativePosition, uint8 ServerMovementMode);
	virtual void ClientVeryShortAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FQuat NewQuat, uint8 ServerMovementMode);

	/**
	 * Handle movement data after it's unpacked from the ServerMovePacked_ServerReceive() call.
	 * Default implementation passes through to ServerMove_PerformMovement(), which may be called twice in the case of a "dual move", and one additional time for an "old important move".
	 */
	virtual void ServerMove_HandleMoveData(const FPhysKartNetworkMoveDataContainer& MoveDataContainer);

	/**
	 * Check timestamps, generate a delta time, and pass through movement params to MoveAutonomous. Error checking is optionally done on the final location, compared to 'ClientLoc'.
	 * The FPhysKartNetworkMoveData parameter to this function is also the same returned by GetCurrentNetworkMoveData(), to assist in migration of code that may want to access the data without changing function signatures.
	 * (Note: this is similar to "ServerMove_Implementation" in legacy versions).
	 */
	virtual void ServerMove_PerformMovement(const FPhysKartNetworkMoveData& MoveData);

	/**
	 * On the server, sends a packed move response to the client. First the FPhysKartMoveResponseDataContainer from GetMoveResponseDataContainer() is filled in with ServerFillResponseData().
	 * Then this data is serialized to a bit stream that is sent to the client via MoveResponsePacked_ServerSend().
	 */
	void ServerSendMoveResponse(const FPhysKartClientAdjustment& PendingAdjustment);

	/**
	 * On the client, handles the move response from the server after it has been received and unpacked in MoveResponsePacked_ClientReceive.
	 * Based on the data in the response, dispatches a call to ClientAckGoodMove_Implementation if there was no error from the server.
	 * Otherwise dispatches a call to one of ClientAdjustRootMotionSourcePosition_Implementation, ClientAdjustRootMotionPosition_Implementation,
	 * or ClientAdjustPosition_Implementation depending on the payload.
	 */
	virtual void ClientHandleMoveResponse(const FPhysKartMoveResponseDataContainer& MoveResponse);


public:

	/////////////////////////////////////////////////////////////////////////////////////
	// BEGIN DEPRECATED movement RPCs. Use the Packed versions above instead. 
	/////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Replicated function sent by client to server - contains client movement and view info.
	 * Calls either PhysKartOwner->ServerMove() or PhysKartOwner->ServerMoveNoBase() depending on whehter ClientMovementBase is null.
	 * PaCorp * Removed UPrimitiveComponent* ClientMovementBase, FName ClientBaseBoneName, parameters as WheeledVehicle doesnt care.
	 */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMove, ServerMovePacked_ClientSend)
	virtual void ServerMove(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis = FVector_NetQuantize100::ZeroVector, float ClientQuatAngle = 0);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMove_Implementation, ServerMove_PerformMovement)
	virtual void ServerMove_Implementation(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis = FVector_NetQuantize100::ZeroVector, float ClientQuatAngle = 0);
	virtual bool ServerMove_Validate(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis = FVector_NetQuantize100::ZeroVector, float ClientQuatAngle = 0);

	/**
	 * Replicated function sent by client to server - contains client movement and view info for two moves.
	 * Calls either PhysKartOwner->ServerMoveDual() or PhysKartOwner->ServerMoveDualNoBase() depending on whehter ClientMovementBase is null.
	 */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveDual, ServerMovePacked_ClientSend)
	virtual void ServerMoveDual(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveDual_Implementation, ServerMove_PerformMovement)
	virtual void ServerMoveDual_Implementation(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode);
	virtual bool ServerMoveDual_Validate(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode);

	/* Resending an (important) old move. Process it if not already processed. */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveOld, ServerMovePacked_ClientSend)
	virtual void ServerMoveOld(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags);
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ServerMoveOld_Implementation, ServerMove_PerformMovement)
	virtual void ServerMoveOld_Implementation(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags);
	virtual bool ServerMoveOld_Validate(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags);
	
	/** If no client adjustment is needed after processing received ServerMove(), ack the good move so client can remove it from SavedMoves */
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientAckGoodMove, ClientHandleMoveResponse)
	virtual void ClientAckGoodMove(float TimeStamp);
	//virtual void ClientAckGoodMove_Implementation(float TimeStamp);	// PaCorp 4.26 sub

	/** Replicate position correction to client, associated with a timestamped servermove.  Client will replay subsequent moves after applying adjustment.  */
	// PaCorp * Removed UPrimitiveComponent* NewBase, FName NewBaseBoneName, bool bHasBase, bool bBaseRelativePosition, parameters as WheeledVehicle does not care.
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientAdjustPosition, ClientHandleMoveResponse)
	virtual void ClientAdjustPosition(float TimeStamp, FVector NewLoc, FVector NewVel, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode);
	//virtual void ClientAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FVector NewVel, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode);	// PaCorp 4.26 sub

	/* Bandwidth saving version, when velocity is zeroed */
	// PaCorp * Removed UPrimitiveComponent* NewBase, FName NewBaseBoneName, bool bHasBase, bool bBaseRelativePosition, parameters as WheeledVehicle does not care.
	DEPRECATED_PHYSKART_MOVEMENT_RPC(ClientVeryShortAdjustPosition, ClientHandleMoveResponse)
	virtual void ClientVeryShortAdjustPosition(float TimeStamp, FVector NewLoc, FQuat NewQuat, uint8 ServerMovementMode);

protected:

	/** Event notification when client receives correction data from the server, before applying the data. Base implementation logs relevant data and draws debug info if "p.NetShowCorrections" is not equal to 0. */
	virtual void OnClientCorrectionReceived(class FNetworkPredictionData_Client_PhysKart& ClientData, float TimeStamp, FVector NewLocation, FVector NewVelocity, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode);
		/**
	 * Set custom struct used for client to server move RPC serialization.
	 * This is typically set in the constructor for this component and should persist for the lifetime of the component.
	 * @see GetNetworkMoveDataContainer(), ServerMovePacked_ServerReceive(), ServerMove_HandleMoveData()
	 */
	void SetNetworkMoveDataContainer(FPhysKartNetworkMoveDataContainer& PersistentDataStorage) { NetworkMoveDataContainerPtr = &PersistentDataStorage; }

	/**
	 * Get the struct used for client to server move RPC serialization.
	 * @see SetNetworkMoveDataContainer()
	 */
	FPhysKartNetworkMoveDataContainer& GetNetworkMoveDataContainer() const { return *NetworkMoveDataContainerPtr; }

	/**
	 * Current move data being processed or handled.
	 * This is set before MoveAutonomous (for replayed moves and server moves), and cleared thereafter.
	 * Useful for being able to access custom movement data during internal movement functions such as MoveAutonomous() or UpdateFromCompressedFlags() to be able to maintain backwards API compatibility.
	 */
	FPhysKartNetworkMoveData* GetCurrentNetworkMoveData() const { return CurrentNetworkMoveData; }

	/**
	 * Used internally to set the FPhysKartNetworkMoveData currently being processed, either being serialized or replayed on the client, or being received and processed on the server.
	 * @see GetCurrentNetworkMoveData()
	 */
	void SetCurrentNetworkMoveData(FPhysKartNetworkMoveData* CurrentData) { CurrentNetworkMoveData = CurrentData; }

	/**
	 * Set custom struct used for server response RPC serialization.
	 * This is typically set in the constructor for this component and should persist for the lifetime of the component.
	 * @see GetMoveResponseDataContainer()
	 */
	void SetMoveResponseDataContainer(FPhysKartMoveResponseDataContainer& PersistentDataStorage) { MoveResponseDataContainerPtr = &PersistentDataStorage; }

	/**
	 * Get the struct used for server response RPC serialization.
	 * @see SetMoveResponseDataContainer(), ClientHandleMoveResponse(), ServerSendMoveResponse().
	 */
	FPhysKartMoveResponseDataContainer& GetMoveResponseDataContainer() const { return *MoveResponseDataContainerPtr; }

private:

	//////////////////////////////////////////////////////////////////////////
	// Server move data

	/** Default client to server move RPC data container. Can be bypassed via SetNetworkMoveDataContainer(). */
	FPhysKartNetworkMoveDataContainer DefaultNetworkMoveDataContainer;

	/** Pointer to server move RPC data container. */
	FPhysKartNetworkMoveDataContainer* NetworkMoveDataContainerPtr;

	/** Used for writing server move RPC bits. */
	FNetBitWriter ServerMoveBitWriter;

	/** Used for reading server move RPC bits. */
	FNetBitReader ServerMoveBitReader;

	/** Current network move data being processed or handled within the NetworkMoveDataContainer. */
	FPhysKartNetworkMoveData* CurrentNetworkMoveData;

	//////////////////////////////////////////////////////////////////////////
	// Server response data

	/** Default server response RPC data container. Can be bypassed via SetMoveResponseDataContainer(). */
	FPhysKartMoveResponseDataContainer DefaultMoveResponseDataContainer;

	/** Pointer to server response RPC data container. */
	FPhysKartMoveResponseDataContainer* MoveResponseDataContainerPtr;

	/** Used for writing server response RPC bits. */
	FNetBitWriter MoveResponseBitWriter;

	/** Used for reading server response RPC bits. */
	FNetBitReader MoveResponseBitReader;

//--------------------------------
// INetworkPredictionInterface implementation END

public:

	//////////////////////////////////////////////////////////////////////////
	// Root Motion



public:

	/**
	 * When moving the character, we should inform physics as to whether we are teleporting.
	 * This allows physics to avoid injecting forces into simulations from client corrections (etc.)
	 */
	ETeleportType GetTeleportType() const;

public:

	/** Minimum delta time considered when ticking. Delta times below this are not considered. This is a very small non-zero value to avoid potential divide-by-zero in simulation code. */
	static const float MIN_TICK_TIME;

	static const float DEGREES_IN_RAD;

	//-------------------------------------------------------------------------------------------//
	// PhysX Immediate Mode 

public:

	// TEMP: Exposed for use in PhAt as a quick way to get drag handles working with Chaos
	virtual ImmediatePhysics::FSimulation* GetSimulation() { return PhysicsSimulation; }

	/** Physics asset to use. If empty use the skeletal mesh's default physics asset */
	UPROPERTY(EditAnywhere, Category = Settings)
	UPhysicsAsset* OverridePhysicsAsset;

	/** Override this to perform game-thread work prior to NON-GAME thread Update() being called */
	virtual void PreUpdate_ImmediateMode();		//(const UAnimInstance* InAnimInstance);

	virtual void PostUpdate_ImmediateMode();

	/** From AnimNode
	*	Interface for derived skeletal controls to implement
	*	use this function to update for skeletal control base
	*	PaCorp * Maybe inside loop ?
	*/
	virtual void UpdateInternal_ImmediateMode(float DeltaSeconds);		 //(const FAnimationUpdateContext& Context);

	virtual bool ClientUpdatePositionAfterServerUpdate_ImmediateMode();

protected:

	/* Process a move at the given time stamp, given the compressed flags representing various events that occurred (ie jump). */
	//virtual void MoveAutonomous_ImmediateMode(float ClientTimeStamp, float DeltaTime, FReplicatedPhysKartState InReplicatedState, uint8 CompressedFlags, const FVector& NewAccel, FTransform NewTransform = FTransform::Identity);

private:
	FTransform PreviousCompWorldSpaceTM;
	FTransform CurrentTransform;
	FTransform PreviousTransform;

	UPhysicsAsset* UsePhysicsAsset;
public:
	/** Override gravity*/
	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinHiddenByDefault, editcondition = "bOverrideWorldGravity"))
	FVector OverrideWorldGravity;

	/** Applies a uniform external force in world space. This allows for easily faking inertia of movement while still simulating in component space for example */
	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinShownByDefault))
	FVector ExternalForce;

	/** When using non-world-space sim, this controls how much of the components world-space acceleration is passed on to the local-space simulation. */
	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinHiddenByDefault))
	FVector ComponentLinearAccScale;

	/** When using non-world-space sim, this applies a 'drag' to the bodies in the local space simulation, based on the components world-space velocity. */
	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinHiddenByDefault))
	FVector ComponentLinearVelScale;

	/** When using non-world-space sim, this is an overall clamp on acceleration derived from ComponentLinearAccScale and ComponentLinearVelScale, to ensure it is not too large. */
	UPROPERTY(EditAnywhere, Category = Settings)
	FVector	ComponentAppliedLinearAccClamp;

	/**		// PaCorp * 4.26
	 * Settings for the system which passes motion of the simulation's space
	 * into the simulation. This allows the simulation to pass a
	 * fraction of the world space motion onto the bodies which allows Bone-Space
	 * and Component-Space simulations to react to world-space movement in a
	 * controllable way.
	 * This system is a superset of the functionality provided by ComponentLinearAccScale,
	 * ComponentLinearVelScale, and ComponentAppliedLinearAccClamp. In general
	 * you should not have both systems enabled.
	 */
	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinHiddenByDefault))
	FPhysKartSimSpaceSettings SimSpaceSettings;

	/**
	 * Scale of cached bounds (vs. actual bounds).
	 * Increasing this may improve performance, but overlaps may not work as well.
	 * (A value of 1.0 effectively disables cached bounds).
	 */
	UPROPERTY(EditAnywhere, Category = Settings, meta = (ClampMin="1.0", ClampMax="2.0"))
	float CachedBoundsScale;

	///** Matters if SimulationSpace is BaseBone */
	// Causes crash in editor.
	//UPROPERTY(EditAnywhere, Category = Settings)
	//FBoneReference BaseBoneRef;

	/** The channel we use to find static geometry to collide with */
	UPROPERTY(EditAnywhere, Category = Settings, meta = (editcondition = "bEnableWorldGeometry"))
	TEnumAsByte<ECollisionChannel> OverlapChannel;

	/** What space to simulate the bodies in. This affects how velocities are generated */
	UPROPERTY(EditAnywhere, Category = Settings)
	EPhysKartSimulationSpace SimulationSpace;

	/** Whether to allow collisions between two bodies joined by a constraint  */
	UPROPERTY(EditAnywhere, Category = Settings)
	bool bForceDisableCollisionBetweenConstraintBodies;


private:
	ETeleportType ResetSimulatedTeleportType;

public:
	UPROPERTY(EditAnywhere, Category = Settings, meta = (InlineEditConditionToggle))
	uint8 bEnableWorldGeometry : 1;

	UPROPERTY(EditAnywhere, Category = Settings, meta = (InlineEditConditionToggle))
	uint8 bOverrideWorldGravity : 1;

	/** 
		When simulation starts, transfer previous bone velocities (from animation)
		to make transition into simulation seamless.
	*/
	UPROPERTY(EditAnywhere, Category = Settings, meta=(PinHiddenByDefault))
	uint8 bTransferBoneVelocities : 1;

	/**
		When simulation starts, freeze incoming pose.
		This is useful for ragdolls, when we want the simulation to take over.
		It prevents non simulated bones from animating.
	*/
	UPROPERTY(EditAnywhere, Category = Settings)
	uint8 bFreezeIncomingPoseOnStart : 1;

	/**
		Correct for linear tearing on bodies with all axes Locked.
		This only works if all axes linear translation are locked
	*/
	UPROPERTY(EditAnywhere, Category = Settings)
	uint8 bClampLinearTranslationLimitToRefPose : 1;

	// PaCorp * 4.26
	/**
		For world-space simulations, if the magnitude of the component's 3D scale is less than WorldSpaceMinimumScale, do not update the node.
	*/
	UPROPERTY(EditAnywhere, Category = Settings)
	float WorldSpaceMinimumScale;
	/**
		If the node is not evaluated for this amount of time (seconds), either because a lower LOD was in use for a while or the component was
		not visible, reset the simulation to the default pose on the next evaluation. Set to 0 to disable time-based reset.
	*/
	UPROPERTY(EditAnywhere, Category = Settings)
	float EvaluationResetTime;
	// -

	/**
	 * Solver iteration settings overrides (defaults are set in the Physics Asset).
	 * These can be varied in the runtime and set through blueprint (e.g., to increase
	 * iterations during difficult movements).
	 * Set to -1 to leave an individual iteration value at its Physics Asset value.
	 */
	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinHiddenByDefault))
	FSolverIterations OverrideSolverIterations;

private:

#if WITH_EDITORONLY_DATA
	UPROPERTY()
		bool bComponentSpaceSimulation_DEPRECATED;	//use SimulationSpace
#endif

	// FAnimNode_SkeletalControlBase interface
	//virtual void InitializeBoneReferences(const FBoneContainer& RequiredBones);
	// End of FAnimNode_SkeletalControlBase interface

	void InitPhysics_ImmediateMode();		//(const UAnimInstance* InAnimInstance);

	virtual void InitPhysics_ImmediateMode_BoxComponent();
	virtual void InitPhysics_ImmediateMode_StaticMeshComponent();
	virtual void InitPhysics_ImmediateMode_SkeletalMeshComponent();

	//void UpdateWorldGeometry(const UWorld& World, const USkeletalMeshComponent& SKC);
	void UpdateWorldGeometry(const UWorld& World);		// , const USkeletalMeshComponent& SKC);
	void UpdateWorldForces(const FTransform& ComponentToWorld, const FTransform& RootBoneTM);

	//void InitializeNewBodyTransformsDuringSimulation(FComponentSpacePoseContext& Output, const FTransform& ComponentTransform, const FTransform& BaseBoneTM);

	// PaCorp * 4.26
	void InitSimulationSpace(
		const FTransform& ComponentToWorld,
		const FTransform& BoneToComponent);
	// Calculate simulation space transform, velocity etc to pass into the solver
	void CalculateSimulationSpace(
		EPhysKartSimulationSpace Space,
		const FTransform& ComponentToWorld,
		const FTransform& BoneToComponent,
		const float Dt,
		const FPhysKartSimSpaceSettings& Settings,
		FTransform& SpaceTransform,
		FVector& SpaceLinearVel,
		FVector& SpaceAngularVel,
		FVector& SpaceLinearAcc,
		FVector& SpaceAngularAcc);
	// Gather nearby world objects and add them to the sim
	void CollectWorldObjects();
	// Flag invalid world objects to be removed from the sim
	void ExpireWorldObjects();
	// Remove simulation objects that are flagged as expired
	void PurgeExpiredWorldObjects();
	// Update sim-space transforms of world objects
	void UpdateWorldObjects(const FTransform& SpaceTransform);
	//

	uint8 bEnabled : 1;
	uint8 bSimulationStarted : 1;
	uint8 bCheckForBodyTransformInit : 1;

	bool bFirstEvalSinceReset;

private:
	// The delegate used to register substepped physics
	//FCalculateCustomPhysics CalculateCustomPhysics;

	float WorldTimeSeconds;		// PaCorp * 4.26
	float LastEvalTimeSeconds;	// PaCorp * 4.26

	///** Physics scene for this client to perform network replay in physics simulating components. */
	//FPhysScene* ClientPhysicsScene;
	float AccumulatedDeltaTime;
	float AnimPhysicsMinDeltaTime;
	bool bSimulateAnimPhysicsAfterReset;
	/** This should only be used for removing the delegate during termination. Do NOT use this for any per frame work */
	TWeakObjectPtr<USkeletalMeshComponent> SkelMeshCompWeakPtr;

	ImmediatePhysics::FSimulation* PhysicsSimulation;
	FSolverIterations SolverIterations;

	struct FPhysKartOutputBoneData
	{
		FPhysKartOutputBoneData()
			: CompactPoseBoneIndex(INDEX_NONE)
		{}

		TArray<FCompactPoseBoneIndex> BoneIndicesToParentBody;
		FCompactPoseBoneIndex CompactPoseBoneIndex;
		int32 BodyIndex;
		int32 ParentBodyIndex;
	};

	struct FPhysKartBodyAnimData
	{
		FPhysKartBodyAnimData()
			: TransferedBoneAngularVelocity(ForceInit)
			, TransferedBoneLinearVelocity(ForceInitToZero)
			, LinearXMotion(ELinearConstraintMotion::LCM_Locked)
			, LinearYMotion(ELinearConstraintMotion::LCM_Locked)
			, LinearZMotion(ELinearConstraintMotion::LCM_Locked)
			, LinearLimit(0.0f)
			, RefPoseLength (0.f)
			, bIsSimulated(false)
			, bBodyTransformInitialized(false)
		{}

		FQuat TransferedBoneAngularVelocity;
		FVector TransferedBoneLinearVelocity;

		ELinearConstraintMotion LinearXMotion;
		ELinearConstraintMotion LinearYMotion;
		ELinearConstraintMotion LinearZMotion;
		float LinearLimit;
		// we don't use linear limit but use default length to limit the bodies
		// linear limits are defined per constraint - it can be any two joints that can limit
		// this is just default length of the local space from parent, and we use that info to limit
		// the translation
		float RefPoseLength;

		bool bIsSimulated : 1;
		bool bBodyTransformInitialized : 1;
	};

	struct FWorldObject
	{
		FWorldObject() : ActorHandle(nullptr), LastSeenTick(0), bExpired(false) {}
		FWorldObject(ImmediatePhysics::FActorHandle* InActorHandle, int32 InLastSeenTick) : ActorHandle(InActorHandle), LastSeenTick(InLastSeenTick), bExpired(false) {}
		ImmediatePhysics::FActorHandle* ActorHandle;
		int32 LastSeenTick;
		bool bExpired;
	};

	TArray<FPhysKartOutputBoneData> OutputBoneData;
	TArray<ImmediatePhysics::FActorHandle*> Bodies;
	bool bRootIsSkeletalMesh;
	int32 UpdatedComponentBodyIndex;
	TArray<int32>  UpdatedComponentBodiesIndex;
	TArray<int32> SkeletonBoneIndexToBodyIndex;
	TArray<FPhysKartBodyAnimData> BodyAnimData;

	TArray<FPhysicsConstraintHandle*> Constraints;
	TArray<USkeletalMeshComponent::FPendingRadialForces> PendingRadialForces;

	//TSet<UPrimitiveComponent*> ComponentsInSim;
	TMap<const UPrimitiveComponent*, FWorldObject> ComponentsInSim;
	int32 ComponentsInSimTick;

	FVector WorldSpaceGravity;

	//FSphere Bounds;

	float TotalMass;

	// Bounds used to gather world objects copied into the simulation
	FSphere CachedBounds;

	FCollisionQueryParams QueryParams;

	FPhysScene* PhysScene;

	//// Evaluation counter, to detect when we haven't be evaluated in a while.
	//FGraphTraversalCounter EvalCounter;
	// 
	// Used by CollectWorldObjects and UpdateWorldGeometry in Task Thread
	// Typically, World should never be accessed off the Game Thread.
	// However, since we're just doing overlaps this should be OK.
	const UWorld* UnsafeWorld;

	// Used by CollectWorldObjects and UpdateWorldGeometry in Task Thread
	// Only used for a pointer comparison.
	const AActor* UnsafeOwner;

	FBoneContainer CapturedBoneVelocityBoneContainer;
	FCSPose<FCompactHeapPose> CapturedBoneVelocityPose;
	FCSPose<FCompactHeapPose> CapturedFrozenPose;
	FBlendedHeapCurve CapturedFrozenCurves;

	// PaCorp * 4.26
	// Used by the world-space to simulation-space motion transfer system in Component- or Bone-Space sims
	FTransform PreviousComponentToWorld;
	FTransform PreviousBoneToComponent;
	FVector PreviousComponentLinearVelocity;
	FVector PreviousComponentAngularVelocity;
	FVector PreviousBoneLinearVelocity;
	FVector PreviousBoneAngularVelocity;
	//

	FVector ImmediateModeAngularVelocityFactored(FVector InVector);

public:
	//// The function used to apply substepped physics
	//virtual void SubstepTick(float DeltaTime, FBodyInstance* BodyInstance);

	//bool bIsInSubstepTick;
};

FORCEINLINE APhysKart* UPhysKartMoveComp::GetPhysKartOwner() const
{
	return PhysKartOwner;
}

//-----------------------------------------------------------------------------------------------//

FORCEINLINE uint32 UPhysKartMoveComp::PackYawAndPitchTo32(const float Yaw, const float Pitch)
{
	const uint32 YawShort = FRotator::CompressAxisToShort(Yaw);
	const uint32 PitchShort = FRotator::CompressAxisToShort(Pitch);
	const uint32 Rotation32 = (YawShort << 16) | PitchShort;
	return Rotation32;
}


/** FSavedMove_PhysKart represents a saved move on the client that has been sent to the server and might need to be played back. */
class FSavedMove_PhysKart
{
public:
	FSavedMove_PhysKart();
	virtual ~FSavedMove_PhysKart();

	// UE_DEPRECATED_FORGAME(4.20)
	FSavedMove_PhysKart(const FSavedMove_PhysKart&);
	FSavedMove_PhysKart(FSavedMove_PhysKart&&);
	FSavedMove_PhysKart& operator=(const FSavedMove_PhysKart&);
	FSavedMove_PhysKart& operator=(FSavedMove_PhysKart&&);

	APhysKart* PhysKartOwner;

	uint32 bForceMaxAccel:1;

	uint32 bPressedItem : 1;

	FReplicatedPhysKartState ReplicatedInputState;

	/** If true, can't combine this move with another move. */
	uint32 bForceNoCombine:1;

	/** If true this move is using an old TimeStamp, before a reset occurred. */
	uint32 bOldTimeStampBeforeReset:1;

	uint32 bWasJumping:1;

	float TimeStamp;    // Time of this move.
	float DeltaTime;    // amount of time for this move
	float ServerWorldTime;		
	float CustomTimeDilation;
	
	UE_DEPRECATED_FORGAME(4.20, "This property is deprecated, use StartPackedMovementMode or EndPackedMovementMode instead.")
	uint8 MovementMode;

	// Information at the start of the move
	uint8 StartPackedMovementMode;
	FVector StartLocation;
	FVector StartRelativeLocation;
	FVector StartVelocity;
	FVector StartAngVelocity;
	FRotator StartRotation;
	FRotator StartControlRotation;
	uint32 StartActorOverlapCounter;
	uint32 StartComponentOverlapCounter;

	// Information after the move has been performed
	uint8 EndPackedMovementMode;
	FVector SavedLocation;
	FRotator SavedRotation;
	FVector SavedVelocity;
	FVector SavedAngVelocity;
	FVector SavedRelativeLocation;
	FRotator SavedControlRotation;

	FVector SavedForce;
	FVector SavedTorque;
	FVector SavedImpulse;

	//FName EndBoneName;
	uint32 EndActorOverlapCounter;
	uint32 EndComponentOverlapCounter;

	FVector Acceleration;
	float MaxSpeed;

	// Cached to speed up iteration over IsImportantMove().
	FVector AccelNormal;
	float AccelMag;

	/** Threshold for deciding this is an "important" move based on DP with last acked acceleration. */
	float AccelDotThreshold;    
	/** Threshold for deciding is this is an important move because acceleration magnitude has changed too much */
	float AccelMagThreshold;	
	/** Threshold for deciding if we can combine two moves, true if cosine of angle between them is <= this. */
	float AccelDotThresholdCombine;
	/** Client saved moves will not combine if the result of GetMaxSpeed() differs by this much between moves. */
	float MaxSpeedThresholdCombine;
	
	/** Clear saved move properties, so it can be re-used. */
	virtual void Clear();

	/** Called to set up this saved move (when initially created) to make a predictive correction. */
	virtual void SetMoveFor(APhysKart* C, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_PhysKart & ClientData);

	/** Set the properties describing the position, etc. of the moved pawn at the start of the move. */
	virtual void SetInitialPosition(APhysKart* C);

	/** Returns true if this move is an "important" move that should be sent again if not acked by the server */
	virtual bool IsImportantMove(const FPhysKartSavedMovePtr& LastAckedMove) const;
	
	/** Returns starting position if we were to revert the move, either absolute StartLocation, or StartRelativeLocation offset from MovementBase's current location (since we want to try to move forward at this time). */
	virtual FVector GetRevertedLocation() const;

	enum EPostUpdateMode
	{
		PostUpdate_Record,		// Record a move after having run the simulation
		PostUpdate_Replay,		// Update after replaying a move for a client correction
	};

	/** Set the properties describing the final position, etc. of the moved pawn. */
	virtual void PostUpdate(APhysKart* C, EPostUpdateMode PostUpdateMode);
	
	/** Returns true if this move can be combined with NewMove for replication without changing any behavior */
	virtual bool CanCombineWith(const FPhysKartSavedMovePtr& NewMove, APhysKart* InCharacter, float MaxDelta) const;

	/** Combine this move with an older move and update relevant state. */
	virtual void CombineWith(const FSavedMove_PhysKart* OldMove, APhysKart* InCharacter, APlayerController* PC, const FVector& OldStartLocation);
	
	/** Called before ClientUpdatePosition uses this SavedMove to make a predictive correction	 */
	virtual void PrepMoveFor(APhysKart* C);

	/** Returns a byte containing encoded special movement information (jumping, crouching, etc.)	 */
	virtual uint8 GetCompressedFlags() const;

	/** Compare current control rotation with stored starting data */
	virtual bool IsMatchingStartControlRotation(const APlayerController* PC) const;

	/** Packs control rotation for network transport */
	virtual void GetPackedAngles(uint32& YawAndPitchPack, uint8& RollPack) const;

	// Bit masks used by GetCompressedFlags() to encode movement information.
	enum CompressedFlags
	{
		FLAG_ItemPressed	= 0x01,	// Jump pressed		//FLAG_JumpPressed
		FLAG_WantsToCrouch	= 0x02,	// Wants to crouch
		FLAG_Reserved_1		= 0x04,	// Reserved for future use
		FLAG_Reserved_2		= 0x08,	// Reserved for future use
		// Remaining bit masks are available for custom flags.
		FLAG_Custom_0		= 0x10,
		FLAG_Custom_1		= 0x20,
		FLAG_Custom_2		= 0x40,
		FLAG_Custom_3		= 0x80,
	};
};

//UE_DEPRECATED_FORGAME(4.20)
PRAGMA_DISABLE_DEPRECATION_WARNINGS
inline FSavedMove_PhysKart::FSavedMove_PhysKart(const FSavedMove_PhysKart&) = default;
inline FSavedMove_PhysKart::FSavedMove_PhysKart(FSavedMove_PhysKart&&) = default;
inline FSavedMove_PhysKart& FSavedMove_PhysKart::operator=(const FSavedMove_PhysKart&) = default;
inline FSavedMove_PhysKart& FSavedMove_PhysKart::operator=(FSavedMove_PhysKart&&) = default;
PRAGMA_ENABLE_DEPRECATION_WARNINGS

class FPhysKartReplaySample
{
public:
	FPhysKartReplaySample() : RemoteViewPitch( 0 ), Time( 0.0f )
	{
	}

	friend FArchive& operator<<( FArchive& Ar, FPhysKartReplaySample& V );

	FVector			Location;
	FRotator		Rotation;
	FVector			Velocity;
	FVector			AngularVelocity;
	FVector			Acceleration;
	uint8			RemoteViewPitch;
	float			Time;					// This represents time since replay started
};

class FNetworkPredictionData_Client_PhysKart : public FNetworkPredictionData_Client, protected FNoncopyable
{
public:

	FNetworkPredictionData_Client_PhysKart(const UPhysKartMoveComp& ClientMovement);
	virtual ~FNetworkPredictionData_Client_PhysKart();

	/** Client timestamp of last time it sent a servermove() to the server. This is an increasing timestamp from the owning UWorld. Used for holding off on sending movement updates to save bandwidth. */
	float ClientUpdateTime;

	/** Current TimeStamp for sending new Moves to the Server. This time resets to zero at a frequency of MinTimeBetweenTimeStampResets. */
	float CurrentTimeStamp;

	/** Server World Time Seconds. */
	float ServerTimeStamp;

	/** Last World timestamp (undilated, real time) at which we received a server ack for a move. This could be either a good move or a correction from the server. */
	float LastReceivedAckRealTime;

	TArray<FPhysKartSavedMovePtr> SavedMoves;		// Buffered moves pending position updates, orderd oldest to newest. Moves that have been acked by the server are removed.
	TArray<FPhysKartSavedMovePtr> FreeMoves;		// freed moves, available for buffering
	FPhysKartSavedMovePtr PendingMove;				// PendingMove already processed on client - waiting to combine with next movement to reduce client to server bandwidth
	FPhysKartSavedMovePtr LastAckedMove;			// Last acknowledged sent move.

	int32 MaxFreeMoveCount;					// Limit on size of free list
	int32 MaxSavedMoveCount;				// Limit on the size of the saved move buffer

	///** RootMotion saved while animation is updated, so we can store it and replay if needed in case of a position correction. */
	//FRootMotionMovementParams RootMotionMovement;

	uint32 bUpdatePosition:1; // when true, update the position (via ClientUpdatePosition)

	// Mesh smoothing variables (for network smoothing)
	//
	/** Whether to smoothly interpolate pawn position corrections on clients based on received location updates */
	UE_DEPRECATED(4.11, "bSmoothNetUpdates will be removed, use UPhysKartMoveComp::NetworkSmoothingMode instead.")
	uint32 bSmoothNetUpdates:1;

	/** Used for position smoothing in net games */
	FVector OriginalMeshTranslationOffset;

	/** World space offset of the mesh. Target value is zero offset. Used for position smoothing in net games. */
	FVector MeshTranslationOffset;

	/** Used for rotation smoothing in net games (only used by linear smoothing). */
	FQuat OriginalMeshRotationOffset;

	/** Component space offset of the mesh. Used for rotation smoothing in net games. */
	FQuat MeshRotationOffset;

	/** Target for mesh rotation interpolation. */
	FQuat MeshRotationTarget;

	/** Used for remembering how much time has passed between server corrections */
	float LastCorrectionDelta;

	/** Used to track time of last correction */
	float LastCorrectionTime;

	/** Max time delta between server updates over which client smoothing is allowed to interpolate. */
	float MaxClientSmoothingDeltaTime;

	/** Used to track the timestamp of the last server move. */
	double SmoothingServerTimeStamp;

	/** Used to track the client time as we try to match the server.*/
	double SmoothingClientTimeStamp;

	/** Used to track how much time has elapsed since last correction. It can be computed as World->TimeSince(LastCorrectionTime). */
	UE_DEPRECATED(4.11, "CurrentSmoothTime will be removed, use LastCorrectionTime instead.")
	float CurrentSmoothTime;

	/** Used to signify that linear smoothing is desired */
	UE_DEPRECATED(4.11, "bUseLinearSmoothing will be removed, use UPhysKartMoveComp::NetworkSmoothingMode instead.")
	bool bUseLinearSmoothing;

	/**
	 * Copied value from UPhysKartMoveComp::NetworkMaxSmoothUpdateDistance.
	 * @see UPhysKartMoveComp::NetworkMaxSmoothUpdateDistance
	 */
	float MaxSmoothNetUpdateDist;

	/**
	 * Copied value from UPhysKartMoveComp::NetworkNoSmoothUpdateDistance.
	 * @see UPhysKartMoveComp::NetworkNoSmoothUpdateDistance
	 */
	float NoSmoothNetUpdateDist;

	/** How long to take to smoothly interpolate from the old pawn position on the client to the corrected one sent by the server.  Must be >= 0. Not used for linear smoothing. */
	float SmoothNetUpdateTime;

	/** How long to take to smoothly interpolate from the old pawn rotation on the client to the corrected one sent by the server.  Must be >= 0. Not used for linear smoothing. */
	float SmoothNetUpdateRotationTime;

	/** (DEPRECATED) How long server will wait for client move update before setting position */
	UE_DEPRECATED(4.12, "MaxResponseTime has been renamed to MaxMoveDeltaTime for clarity in what it does and will be removed, use MaxMoveDeltaTime instead.")
	float MaxResponseTime;
	
	/** 
	 * Max delta time for a given move, in real seconds
	 * Based off of AGameNetworkManager::MaxMoveDeltaTime config setting, but can be modified per actor
	 * if needed.
	 * This value is mirrored in FNetworkPredictionData_Server, which is what server logic runs off of.
	 * Client needs to know this in order to not send move deltas that are going to get clamped anyway (meaning
	 * they'll be rejected/corrected).
	 * Note: This was previously named MaxResponseTime, but has been renamed to reflect what it does more accurately
	 */
	float MaxMoveDeltaTime;

	/** Values used for visualization and debugging of simulated net corrections */
	FVector LastSmoothLocation;
	FVector LastServerLocation;
	float	SimulatedDebugDrawTime;

	/** Array of replay samples that we use to interpolate between to get smooth location/rotation/velocity/ect */
	TArray< FPhysKartReplaySample > ReplaySamples;

	/** Finds SavedMove index for given TimeStamp. Returns INDEX_NONE if not found (move has been already Acked or cleared). */
	int32 GetSavedMoveIndex(float TimeStamp) const;

	/** Ack a given move. This move will become LastAckedMove, SavedMoves will be adjusted to only contain unAcked moves. */
	void AckMove(int32 AckedMoveIndex, UPhysKartMoveComp& CharacterMovementComponent);

	/** Allocate a new saved move. Subclasses should override this if they want to use a custom move class. */
	virtual FPhysKartSavedMovePtr AllocateNewMove();

	/** Return a move to the free move pool. Assumes that 'Move' will no longer be referenced by anything but possibly the FreeMoves list. Clears PendingMove if 'Move' is PendingMove. */
	virtual void FreeMove(const FPhysKartSavedMovePtr& Move);

	/** Tries to pull a pooled move off the free move list, otherwise allocates a new move. Returns NULL if the limit on saves moves is hit. */
	virtual FPhysKartSavedMovePtr CreateSavedMove();

	/** Update CurentTimeStamp from passed in DeltaTime.
		It will measure the accuracy between passed in DeltaTime and how Server will calculate its DeltaTime.
		If inaccuracy is too high, it will reset CurrentTimeStamp to maintain a high level of accuracy.
		@return DeltaTime to use for Client's physics simulation prior to replicate move to server. */
	float UpdateTimeStampAndDeltaTime(float DeltaTime, APhysKart & PhysKartOwner, class UPhysKartMoveComp & CharacterMovementComponent);

	/** Used for simulated packet loss in development builds. */
	float DebugForcedPacketLossTimerStart;
};


class FNetworkPredictionData_Server_PhysKart : public FNetworkPredictionData_Server, protected FNoncopyable
{
public:

	FNetworkPredictionData_Server_PhysKart(const UPhysKartMoveComp& ServerMovement);
	virtual ~FNetworkPredictionData_Server_PhysKart();

	FPhysKartClientAdjustment PendingAdjustment;

	/** Timestamp from the client of most recent ServerMove() processed for this player. Reset occasionally for timestamp resets (to maintain accuracy). */
	float CurrentClientTimeStamp;

	/** Timestamp from the client of most recent ServerMove() received for this player, including rejected requests. */
	float LastReceivedClientTimeStamp;

	/** Timestamp of total elapsed client time. Similar to CurrentClientTimestamp but this is accumulated with the calculated DeltaTime for each move on the server. */
	double ServerAccumulatedClientTimeStamp;

	/** Last time server updated client with a move correction */
	float LastUpdateTime;

	/** Server clock time when last server move was received from client (does NOT include forced moves on server) */
	float ServerTimeStampLastServerMove;

	/** (DEPRECATED) How long server will wait for client move update before setting position */
	UE_DEPRECATED(4.12, "MaxResponseTime has been renamed to MaxMoveDeltaTime for clarity in what it does and will be removed, use MaxMoveDeltaTime instead.")
	float MaxResponseTime;
	
	/** 
	 * Max delta time for a given move, in real seconds
	 * Based off of AGameNetworkManager::MaxMoveDeltaTime config setting, but can be modified per actor
	 * if needed.
	 * Note: This was previously named MaxResponseTime, but has been renamed to reflect what it does more accurately
	 */
	float MaxMoveDeltaTime;

	/** Force client update on the next ServerMoveHandleClientError() call. */
	uint32 bForceClientUpdate:1;

	/** Accumulated timestamp difference between autonomous client and server for tracking long-term trends */
	float LifetimeRawTimeDiscrepancy;

	/** 
	 * Current time discrepancy between client-reported moves and time passed
	 * on the server. Time discrepancy resolution's goal is to keep this near zero.
	 */
	float TimeDiscrepancy;

	/** True if currently in the process of resolving time discrepancy */
	bool bResolvingTimeDiscrepancy;

	/** 
	 * When bResolvingTimeDiscrepancy is true, we are in time discrepancy resolution mode whose output is
	 * this value (to be used as the DeltaTime for current ServerMove)
	 */
	float TimeDiscrepancyResolutionMoveDeltaOverride;

	/** 
	 * When bResolvingTimeDiscrepancy is true, we are in time discrepancy resolution mode where we bound
	 * move deltas by Server Deltas. In cases where there are multiple ServerMove RPCs handled within one
	 * server frame tick, we need to accumulate the client deltas of the "no tick" Moves so that the next
	 * Move processed that the server server has ticked for takes into account those previous deltas. 
	 * If we did not use this, the higher the framerate of a client vs the server results in more 
	 * punishment/payback time.
	 */
	float TimeDiscrepancyAccumulatedClientDeltasSinceLastServerTick;

	/** Creation time of this prediction data, used to contextualize LifetimeRawTimeDiscrepancy */
	float WorldCreationTime;

	/** Returns time delta to use for the current ServerMove(). Takes into account time discrepancy resolution if active. */
	float GetServerMoveDeltaTime(float ClientTimeStamp, float ActorTimeDilation) const;

	/** Returns base time delta to use for a ServerMove, default calculation (no time discrepancy resolution) */
	float GetBaseServerMoveDeltaTime(float ClientTimeStamp, float ActorTimeDilation) const;

};