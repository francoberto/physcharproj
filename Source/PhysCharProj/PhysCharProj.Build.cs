// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PhysCharProj : ModuleRules
{
	public PhysCharProj(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(
		new string[]
		{
			"Core",
			"HeadMountedDisplay",
			"NavigationSystem",
			"AIModule",
			"PhysicsCore",
			"PhysX",
			// ... add other public dependencies that you statically link with here ...
			"CoreUObject", "Engine", "InputCore", "PhysXVehicles"	//, "VehicleSystemPlugin",
		}
		);		
		
	PrivateDependencyModuleNames.AddRange(
		new string[]
		{
			"CoreUObject",
			"Engine",
			"Slate",
			"SlateCore",
			// ... add private dependencies that you statically link with here ...	
		}
		);

		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				"OnlineSubsystemNull",
				"NetworkReplayStreaming",
				"NullNetworkReplayStreaming",
				"HttpNetworkReplayStreaming",
				"LocalFileNetworkReplayStreaming",
				"PerfCounters"
				// ... add any modules that your module loads dynamically here ...
			}
		);

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"NetworkReplayStreaming",
				"PerfCounters"
			}
		);
	}
}
