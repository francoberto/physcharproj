// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PhysCharProjGameMode.generated.h"

UCLASS(minimalapi)
class APhysCharProjGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APhysCharProjGameMode();
};



