// Francisco Elizalde * PaCorp * 2021

#include "PhysKartMoveComp.h"
#include "PhysKart.h"

#include "EngineStats.h"
#include "Components/PrimitiveComponent.h"
#include "AI/NavigationSystemBase.h"
#include "AI/Navigation/NavigationDataInterface.h"
#include "UObject/Package.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/PhysicsVolume.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/NetDriver.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/GameNetworkManager.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/GameStateBase.h"
#include "Engine/Canvas.h"
#include "AI/Navigation/PathFollowingAgentInterface.h"
#include "AI/Navigation/AvoidanceManager.h"
#include "Components/BrushComponent.h"
#include "Misc/App.h"

#include "Engine/DemoNetDriver.h"
#include "Engine/NetworkObjectList.h"

#include "Net/PerfCountersHelpers.h"
#include "ProfilingDebugging/CsvProfiler.h"

//

#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine\BodyInstance.h"
#include "Physics/ImmediatePhysics/ImmediatePhysicsActorHandle.h"
#include "Physics/ImmediatePhysics/ImmediatePhysicsSimulation.h"
#include "Physics/ImmediatePhysics/ImmediatePhysicsPhysX/ImmediatePhysicsSimulation_PhysX.h"
//#include "Physics/ImmediatePhysics/ImmediatePhysicsStats.h"
#include "PhysicsEngine/PhysicsSettings.h"

#include "PhysicsCore\Public\PhysXPublicCore.h"

#include "BoneContainer.h"

//
int32 RBAN_MaxSubSteps = 4;
bool bRBAN_EnableTimeBasedReset = true;
bool bRBAN_EnableComponentAcceleration = true;	// PaCorp * 4.26
int32 RBAN_WorldObjectExpiry = 4;				// PaCorp * 4.26

FPhysKartSimSpaceSettings::FPhysKartSimSpaceSettings()
	: MasterAlpha(0)
	, VelocityScaleZ(1)
	, MaxLinearVelocity(10000)
	, MaxAngularVelocity(10000)
	, MaxLinearAcceleration(10000)
	, MaxAngularAcceleration(10000)
	, ExternalLinearDrag_DEPRECATED(0)
	, ExternalLinearDragV(FVector::ZeroVector)
	, ExternalLinearVelocity(FVector::ZeroVector)
	, ExternalAngularVelocity(FVector::ZeroVector)
{
}

void FPhysKartSimSpaceSettings::PostSerialize(const FArchive& Ar)
{
	if (Ar.IsLoading())
	{
		if (ExternalLinearDrag_DEPRECATED != 0.0f)
		{
			ExternalLinearDragV = FVector(ExternalLinearDrag_DEPRECATED, ExternalLinearDrag_DEPRECATED, ExternalLinearDrag_DEPRECATED);
		}
	}
}
//



CSV_DEFINE_CATEGORY(PhysKartMovement, true);

DEFINE_LOG_CATEGORY_STATIC(LogPhysKartMovement, Log, All);
DEFINE_LOG_CATEGORY_STATIC(LogNavMeshMovement, Log, All);
DEFINE_LOG_CATEGORY_STATIC(LogPhysKartNetSmoothing, Log, All);

//DEFINE_STAT(STAT_RequestBulletTypeFromPool);
//DEFINE_STAT(STAT_ReturnBulletTypeToPool);

//DECLARE_CYCLE_STAT_EXTERN(TEXT("Char Movement Total"), STAT_CharacterMovement, STATGROUP_Game, );
//DECLARE_CYCLE_STAT_EXTERN(TEXT("Kart Movement Total"), STAT_PhysKartMovement, STATGROUP_PhysKart, );

/**
 * PhysKart stats
 */
DECLARE_CYCLE_STAT(TEXT("Kart Movement Total"), STAT_PhysKartMovement, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart Tick"), STAT_PhysKartMovementTick, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart NonSimulated Time"), STAT_PhysKartMovementNonSimulated, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart Simulated Time"), STAT_PhysKartMovementSimulated, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart PerformMovement"), STAT_PhysKartMovementPerformMovement, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart ReplicateMoveToServer"), STAT_PhysKartMovementReplicateMoveToServer, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart ReplicateMoveToServerPostPhysics"), STAT_PhysKartMovementReplicateMoveToServerPostPhysics, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart CallServerMove"), STAT_PhysKartMovementCallServerMove, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart ServerMove"), STAT_PhysKartMovementServerMove, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart ServerForcePositionUpdate"), STAT_PhysKartMovementForcePositionUpdate, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart RootMotionSource Calculate"), STAT_PhysKartMovementRootMotionSourceCalculate, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart RootMotionSource Apply"), STAT_PhysKartMovementRootMotionSourceApply, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart ClientUpdatePositionAfterServerUpdate"), STAT_PhysKartMovementClientUpdatePositionAfterServerUpdate, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart CombineNetMove"), STAT_PhysKartMovementCombineNetMove, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart NetSmoothCorrection"), STAT_PhysKartMovementSmoothCorrection, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart SmoothClientPosition"), STAT_PhysKartMovementSmoothClientPosition, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart SmoothClientPosition_Interp"), STAT_PhysKartMovementSmoothClientPosition_Interp, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart SmoothClientPosition_Visual"), STAT_PhysKartMovementSmoothClientPosition_Visual, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart Physics Interation"), STAT_KartPhysicsInteraction, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart StepUp"), STAT_KartStepUp, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart FindFloor"), STAT_KartFindFloor, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart AdjustFloorHeight"), STAT_KartAdjustFloorHeight, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart Update Acceleration"), STAT_KartUpdateAcceleration, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart MoveUpdateDelegate"), STAT_KartMoveUpdateDelegate, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart PhysWalking"), STAT_KartPhysWalking, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart PhysFalling"), STAT_KartPhysFalling, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart PhysNavWalking"), STAT_KartPhysNavWalking, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart NavProjectPoint"), STAT_KartNavProjectPoint, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart NavProjectLocation"), STAT_KartNavProjectLocation, STATGROUP_PhysKart);
DECLARE_CYCLE_STAT(TEXT("Kart ProcessLanded"), STAT_KartProcessLanded, STATGROUP_PhysKart);

DECLARE_CYCLE_STAT(TEXT("Kart HandleImpact"), STAT_KartHandleImpact, STATGROUP_PhysKart);

const float UPhysKartMoveComp::MIN_TICK_TIME = 1e-6f;
const float UPhysKartMoveComp::DEGREES_IN_RAD = 57.295779513082320876798154814105f;

static const FString PerfCounter_NumServerMoves = TEXT("NumServerMoves");
static const FString PerfCounter_NumServerMoveCorrections = TEXT("NumServerMoveCorrections");

// Defines for build configs
#if DO_CHECK && !UE_BUILD_SHIPPING // Disable even if checks in shipping are enabled.
#define devCode( Code )		checkCode( Code )
#else
#define devCode(...)
#endif

// CVars
namespace PhysKartMovementCVars
{
	// Use newer RPCs and RPC parameter serialization that allow variable length data without changing engine APIs.
	static int32 NetUsePackedMovementRPCs = 1;
	FAutoConsoleVariableRef CVarNetUsePackedMovementRPCs(
		TEXT("p.NetUsePackedMovementRPCs"),
		NetUsePackedMovementRPCs,
		TEXT("Whether to use newer movement RPC parameter packed serialization. If disabled, old deprecated movement RPCs will be used instead.\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Default);

	static int32 NetPackedMovementMaxBits = 2048;
	FAutoConsoleVariableRef CVarNetPackedMovementMaxBits(
		TEXT("p.NetPackedMovementMaxBits"),
		NetPackedMovementMaxBits,
		TEXT("Max number of bits allowed in each packed movement RPC. Used to protect against bad data causing the server to allocate too much memory.\n"),
		ECVF_Default);

	// Listen server smoothing
	static int32 NetEnableListenServerSmoothing = 1;
	FAutoConsoleVariableRef CVarNetEnableListenServerSmoothing(
		TEXT("p.NetEnableListenServerSmoothing"),
		NetEnableListenServerSmoothing,
		TEXT("Whether to enable mesh smoothing on listen servers for the local view of remote clients.\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Default);

	// Latent proxy prediction
	static int32 NetEnableSkipProxyPredictionOnNetUpdate = 1;
	FAutoConsoleVariableRef CVarNetEnableSkipProxyPredictionOnNetUpdate(
		TEXT("p.NetEnableSkipProxyPredictionOnNetUpdate"),
		NetEnableSkipProxyPredictionOnNetUpdate,
		TEXT("Whether to allow proxies to skip prediction on frames with a network position update, if bNetworkSkipProxyPredictionOnNetUpdate is also true on the movement component.\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Default);

	// Logging when character is stuck. Off by default in shipping.
#if UE_BUILD_SHIPPING
	static float StuckWarningPeriod = -1.f;
#else
	static float StuckWarningPeriod = 1.f;
#endif

	FAutoConsoleVariableRef CVarStuckWarningPeriod(
		TEXT("p.CharacterStuckWarningPeriod"),
		StuckWarningPeriod,
		TEXT("How often (in seconds) we are allowed to log a message about being stuck in geometry.\n")
		TEXT("<0: Disable, >=0: Enable and log this often, in seconds."),
		ECVF_Default);

	static int32 NetEnableMoveCombining = 1;
	FAutoConsoleVariableRef CVarNetEnableMoveCombining(
		TEXT("p.NetEnableMoveCombining"),
		NetEnableMoveCombining,
		TEXT("Whether to enable move combining on the client to reduce bandwidth by combining similar moves.\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Default);

	static int32 NetEnableMoveCombiningOnStaticBaseChange = 1;
	FAutoConsoleVariableRef CVarNetEnableMoveCombiningOnStaticBaseChange(
		TEXT("p.NetEnableMoveCombiningOnStaticBaseChange"),
		NetEnableMoveCombiningOnStaticBaseChange,
		TEXT("Whether to allow combining client moves when moving between static geometry.\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Default);

	static float NetMoveCombiningAttachedLocationTolerance = 0.01f;
	FAutoConsoleVariableRef CVarNetMoveCombiningAttachedLocationTolerance(
		TEXT("p.NetMoveCombiningAttachedLocationTolerance"),
		NetMoveCombiningAttachedLocationTolerance,
		TEXT("Tolerance for relative location attachment change when combining moves. Small tolerances allow for very slight jitter due to transform updates."),
		ECVF_Default);

	static float NetMoveCombiningAttachedRotationTolerance = 0.01f;
	FAutoConsoleVariableRef CVarNetMoveCombiningAttachedRotationTolerance(
		TEXT("p.NetMoveCombiningAttachedRotationTolerance"),
		NetMoveCombiningAttachedRotationTolerance,
		TEXT("Tolerance for relative rotation attachment change when combining moves. Small tolerances allow for very slight jitter due to transform updates."),
		ECVF_Default);

	static float NetStationaryRotationTolerance = 0.1f;
	FAutoConsoleVariableRef CVarNetStationaryRotationTolerance(
		TEXT("p.NetStationaryRotationTolerance"),
		NetStationaryRotationTolerance,
		TEXT("Tolerance for GetClientNetSendDeltaTime() to remain throttled when small control rotation changes occur."),
		ECVF_Default);

	static int32 NetUseClientTimestampForReplicatedTransform = 1;
	FAutoConsoleVariableRef CVarNetUseClientTimestampForReplicatedTransform(
		TEXT("p.NetUseClientTimestampForReplicatedTransform"),
		NetUseClientTimestampForReplicatedTransform,
		TEXT("If enabled, use client timestamp changes to track the replicated transform timestamp, otherwise uses server tick time as the timestamp.\n")
		TEXT("Game session usually needs to be restarted if this is changed at runtime.\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Default);

	static int32 ReplayUseInterpolation = 0;
	FAutoConsoleVariableRef CVarReplayUseInterpolation(
		TEXT("p.ReplayUseInterpolation"),
		ReplayUseInterpolation,
		TEXT(""),
		ECVF_Default);

	static int32 ReplayLerpAcceleration = 0;
	FAutoConsoleVariableRef CVarReplayLerpAcceleration(
		TEXT("p.ReplayLerpAcceleration"),
		ReplayLerpAcceleration,
		TEXT(""),
		ECVF_Default);

	static int32 FixReplayOverSampling = 1;
	FAutoConsoleVariableRef CVarFixReplayOverSampling(
		TEXT("p.FixReplayOverSampling"),
		FixReplayOverSampling,
		TEXT("If 1, remove invalid replay samples that can occur due to oversampling (sampling at higher rate than physics is being ticked)"),
		ECVF_Default);

	static int32 ForceJumpPeakSubstep = 1;
	FAutoConsoleVariableRef CVarForceJumpPeakSubstep(
		TEXT("p.ForceJumpPeakSubstep"),
		ForceJumpPeakSubstep,
		TEXT("If 1, force a jump substep to always reach the peak position of a jump, which can often be cut off as framerate lowers."),
		ECVF_Default);

	static float NetServerMoveTimestampExpiredWarningThreshold = 1.0f;
	FAutoConsoleVariableRef CVarNetServerMoveTimestampExpiredWarningThreshold(
		TEXT("net.NetServerMoveTimestampExpiredWarningThreshold"),
		NetServerMoveTimestampExpiredWarningThreshold,
		TEXT("Tolerance for ServerMove() to warn when client moves are expired more than this time threshold behind the server."),
		ECVF_Default);

#if !UE_BUILD_SHIPPING

	int32 NetShowCorrections = 0;
	FAutoConsoleVariableRef CVarNetShowCorrections(
		TEXT("p.NetShowCorrections"),
		NetShowCorrections,
		TEXT("Whether to draw client position corrections (red is incorrect, green is corrected).\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Cheat);

	float NetCorrectionLifetime = 4.f;
	FAutoConsoleVariableRef CVarNetCorrectionLifetime(
		TEXT("p.NetCorrectionLifetime"),
		NetCorrectionLifetime,
		TEXT("How long a visualized network correction persists.\n")
		TEXT("Time in seconds each visualized network correction persists."),
		ECVF_Cheat);

#endif // !UE_BUILD_SHIPPING


#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)

	static float NetForceClientAdjustmentPercent = 0.f;
	FAutoConsoleVariableRef CVarNetForceClientAdjustmentPercent(
		TEXT("p.NetForceClientAdjustmentPercent"),
		NetForceClientAdjustmentPercent,
		TEXT("Percent of ServerCheckClientError checks to return true regardless of actual error.\n")
		TEXT("Useful for testing client correction code.\n")
		TEXT("<=0: Disable, 0.05: 5% of checks will return failed, 1.0: Always send client adjustments"),
		ECVF_Cheat);

	static float NetForceClientServerMoveLossPercent = 0.f;
	FAutoConsoleVariableRef CVarNetForceClientServerMoveLossPercent(
		TEXT("p.NetForceClientServerMoveLossPercent"),
		NetForceClientServerMoveLossPercent,
		TEXT("Percent of ServerMove calls for client to not send.\n")
		TEXT("Useful for testing server force correction code.\n")
		TEXT("<=0: Disable, 0.05: 5% of checks will return failed, 1.0: never send server moves"),
		ECVF_Cheat);

	static float NetForceClientServerMoveLossDuration = 0.f;
	FAutoConsoleVariableRef CVarNetForceClientServerMoveLossDuration(
		TEXT("p.NetForceClientServerMoveLossDuration"),
		NetForceClientServerMoveLossDuration,
		TEXT("Duration in seconds for client to drop ServerMove calls when NetForceClientServerMoveLossPercent check passes.\n")
		TEXT("Useful for testing server force correction code.\n")
		TEXT("Duration of zero means single frame loss."),
		ECVF_Cheat);

	static int32 VisualizeMovement = 0;
	FAutoConsoleVariableRef CVarVisualizeMovement(
		TEXT("p.VisualizeMovement"),
		VisualizeMovement,
		TEXT("Whether to draw in-world debug information for character movement.\n")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Cheat);

	static int32 NetVisualizeSimulatedCorrections = 0;
	FAutoConsoleVariableRef CVarNetVisualizeSimulatedCorrections(
		TEXT("p.NetVisualizeSimulatedCorrections"),
		NetVisualizeSimulatedCorrections,
		TEXT("")
		TEXT("0: Disable, 1: Enable"),
		ECVF_Cheat);

	static int32 DebugTimeDiscrepancy = 0;
	FAutoConsoleVariableRef CVarDebugTimeDiscrepancy(
		TEXT("p.DebugTimeDiscrepancy"),
		DebugTimeDiscrepancy,
		TEXT("Whether to log detailed Movement Time Discrepancy values for testing")
		TEXT("0: Disable, 1: Enable Detection logging, 2: Enable Detection and Resolution logging"),
		ECVF_Cheat);
#endif // !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
}


/**
 * Helper to change mesh bone updates within a scope.
 * Example usage:
 *	{
 *		FScopedPreventMeshBoneUpdate ScopedNoMeshBoneUpdate(PhysKartOwner->GetPhysKartMesh(), EKinematicBonesUpdateToPhysics::SkipAllBones);
 *		// Do something to move mesh, bones will not update
 *	}
 *	// Movement of mesh at this point will use previous setting.
 */
struct FScopedWheeledMeshBoneUpdateOverride
{
	FScopedWheeledMeshBoneUpdateOverride(USkeletalMeshComponent* Mesh, EKinematicBonesUpdateToPhysics::Type OverrideSetting)
		: MeshRef(Mesh)
	{
		if (MeshRef)
		{
			// Save current state.
			SavedUpdateSetting = MeshRef->KinematicBonesUpdateType;
			// Override bone update setting.
			MeshRef->KinematicBonesUpdateType = OverrideSetting;
		}
	}

	~FScopedWheeledMeshBoneUpdateOverride()
	{
		if (MeshRef)
		{
			// Restore bone update flag.
			MeshRef->KinematicBonesUpdateType = SavedUpdateSetting;
		}
	}

private:
	USkeletalMeshComponent* MeshRef;
	EKinematicBonesUpdateToPhysics::Type SavedUpdateSetting;
};


void FPhysKartMoveCompPostPhysicsTickFunction::ExecuteTick(float DeltaTime, enum ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
{
	FActorComponentTickFunction::ExecuteTickHelper(Target, /*bTickInEditor=*/ false, DeltaTime, TickType, [this](float DilatedTime)
		{
			Target->PostPhysicsTickComponent(DilatedTime, *this);
		});
}

FString FPhysKartMoveCompPostPhysicsTickFunction::DiagnosticMessage()
{
	return Target->GetFullName() + TEXT("[UPhysKartMoveComp::PreClothTick]");
}

FName FPhysKartMoveCompPostPhysicsTickFunction::DiagnosticContext(bool bDetailed)
{
	if (bDetailed)
	{
		return FName(*FString::Printf(TEXT("SkeletalMeshComponentClothTick/%s"), *GetFullNameSafe(Target)));
	}

	return FName(TEXT("SkeletalMeshComponentClothTick"));
}


UPhysKartMoveComp::UPhysKartMoveComp(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PostPhysicsTickFunction.bCanEverTick = true;
	PostPhysicsTickFunction.bStartWithTickEnabled = true;
	PostPhysicsTickFunction.SetTickFunctionEnable(true);
	PostPhysicsTickFunction.TickGroup = TG_PostPhysics;

	//-------------------------------------------------------------------------------------------//

	fThrottleWheelScale = 1000.0f;
	fSteeringWheelScale = 57.2957f;	// 1000.0f;

	UseEuclideanTransformation = true;

	RandomStream.Initialize(FApp::bUseFixedSeed ? GetFName() : NAME_None);

	MaxSimulationTimeStep = 0.05f;
	MaxSimulationIterations = 8;
	MaxJumpApexAttemptsPerSimulation = 2;
	NumJumpApexAttempts = 0;

	MaxDepenetrationWithGeometry = 500.f;
	MaxDepenetrationWithGeometryAsProxy = 100.f;
	MaxDepenetrationWithPawn = 100.f;
	MaxDepenetrationWithPawnAsProxy = 2.f;

	NetworkSimulatedSmoothLocationTime = 0.100f;
	NetworkSimulatedSmoothRotationTime = 0.050f;
	ListenServerNetworkSimulatedSmoothLocationTime = 0.040f;
	ListenServerNetworkSimulatedSmoothRotationTime = 0.033f;
	NetworkMaxSmoothUpdateDistance = 256.f;
	NetworkNoSmoothUpdateDistance = 384.f;
	NetworkSmoothingMode = ENetworkSmoothingMode::Exponential;
	ServerLastClientGoodMoveAckTime = -1.f;
	ServerLastClientAdjustmentTime = -1.f;
	NetworkMinTimeBetweenClientAckGoodMoves = 0.10f;
	NetworkMinTimeBetweenClientAdjustments = 0.10f;
	NetworkMinTimeBetweenClientAdjustmentsLargeCorrection = 0.05f;
	NetworkLargeClientCorrectionDistance = 15.0f;

	NetworkLargeClientCorrectionQuat = 0.3f;		// PaCorp
	NetworkMaxQuatErrorScale = 0.998629f;			// Cos(3�)

	MaxAcceleration = 2048.0f;
	LedgeCheckThreshold = 4.0f;
	JumpOutOfWaterPitch = 11.25f;

	bJustTeleported = true;
	LastUpdateRotation = FQuat::Identity;
	LastUpdateVelocity = FVector::ZeroVector;
	PendingImpulseToApply = FVector::ZeroVector;
	PendingLaunchVelocity = FVector::ZeroVector;
	DefaultWaterMovementMode = MOVE_Swimming;
	DefaultLandMovementMode = MOVE_Walking;
	GroundMovementMode = MOVE_Walking;
	bForceNextFloorCheck = true;

	bNetworkSmoothingComplete = true; // Initially true until we get a net update, so we don't try to smooth to an uninitialized value.
	bIgnoreClientMovementErrorChecksAndCorrection = false;
	bServerAcceptClientAuthoritativePosition = false;

	// default character can jump, walk, and swim
	NavAgentProps.bCanJump = true;
	NavAgentProps.bCanWalk = true;
	NavAgentProps.bCanSwim = true;
	ResetMoveState();

	ClientPredictionData = NULL;
	ServerPredictionData = NULL;
	SetNetworkMoveDataContainer(DefaultNetworkMoveDataContainer);
	SetMoveResponseDataContainer(DefaultMoveResponseDataContainer);
	ServerMoveBitWriter.SetAllowResize(true);
	MoveResponseBitWriter.SetAllowResize(true);

	// This should be greater than tolerated player timeout * 2.
	MinTimeBetweenTimeStampResets = 4.f * 60.f;
	LastTimeStampResetServerTime = 0.f;

	bEnableScopedMovementUpdates = true;
	// Disabled by default since it can be a subtle behavior change, you should opt in if you want to accept that.
	bEnableServerDualMoveScopedMovementUpdates = false;

	//-------------------------------------------------------------------------------------------//
	// PhysX Immediate Mode

	bRootIsSkeletalMesh = false;
	UpdatedComponentBodyIndex = INDEX_NONE;
	AccumulatedDeltaTime = 0.0f;
	WorldTimeSeconds = 0.0f;		// PaCorp * 4.26
	LastEvalTimeSeconds = 0.0f;		// PaCorp * 4.26
	ResetSimulatedTeleportType = ETeleportType::None;
	PhysicsSimulation = nullptr;
	OverridePhysicsAsset = nullptr;
	UsePhysicsAsset = nullptr;
	bOverrideWorldGravity = false;
	CachedBoundsScale = 1.2f;
	SimulationSpace = EPhysKartSimulationSpace::WorldSpace;		//EPhysKartSimulationSpace::ComponentSpace;
	ExternalForce = FVector::ZeroVector;
#if WITH_EDITORONLY_DATA
	bComponentSpaceSimulation_DEPRECATED = true;
#endif
	OverrideWorldGravity = FVector::ZeroVector;
	TotalMass = 0.f;
	CachedBounds.Center = FVector::ZeroVector;		// PaCorp * 4.26
	CachedBounds.W = 0;	
	PhysScene = nullptr;
	UnsafeWorld = nullptr;
	UnsafeOwner = nullptr;		// PaCorp * 4.26
	bSimulationStarted = false;
	bCheckForBodyTransformInit = false;
	OverlapChannel = ECC_WorldStatic;
	bEnableWorldGeometry = true;		// false;
	bTransferBoneVelocities = false;
	bFreezeIncomingPoseOnStart = false;
	bClampLinearTranslationLimitToRefPose = false;
	UnsafeOwner = nullptr;		// PaCorp * 4.26

	PreviousTransform = CurrentTransform = FTransform::Identity;
	PreviousComponentLinearVelocity = FVector::ZeroVector;

	ComponentLinearAccScale = FVector::ZeroVector;
	ComponentLinearVelScale = FVector::ZeroVector;
	ComponentAppliedLinearAccClamp = FVector(10000, 10000, 10000);
	bForceDisableCollisionBetweenConstraintBodies = false;

	EvaluationResetTime = 0.01f;

	//-------------------------------------------------------------------------------------------//
}

void UPhysKartMoveComp::BeginPlay()
{
	Super::BeginPlay();

	delete PhysicsSimulation;
	PhysicsSimulation = nullptr;

	//UsePhysicsAsset = OverridePhysicsAsset ? OverridePhysicsAsset : GetSkelMeshComponent()->GetPhysicsAsset();
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsA(UPhysicsAsset::StaticClass()))
	{
		//UsePhysicsAsset = OverridePhysicsAsset ? OverridePhysicsAsset : GetPhysKartOwner()->GetPhysKartRoot()->GetPhysicsAsset();
	}

	const bool bIsClient = (PhysKartOwner != nullptr && PhysKartOwner->GetLocalRole() == ROLE_AutonomousProxy && IsNetMode(NM_Client));
	if (bIsClient)
	{
		InitPhysics_ImmediateMode();
	}
}

void UPhysKartMoveComp::BeginDestroy()
{
	if (ClientPredictionData)
	{
		delete ClientPredictionData;
		ClientPredictionData = NULL;
	}

	if (ServerPredictionData)
	{
		delete ServerPredictionData;
		ServerPredictionData = NULL;
	}

	Super::BeginDestroy();

	delete PhysicsSimulation;
	PhysicsSimulation = nullptr;

}

void UPhysKartMoveComp::Deactivate()
{
	bStopMovementAbortPaths = false;	// Mirrors StopMovementKeepPathing(), because Super calls StopMovement() and we want that handled differently.
	Super::Deactivate();
	if (!IsActive())
	{
		ClearAccumulatedForces();
	}
	bStopMovementAbortPaths = true;
}

void UPhysKartMoveComp::InitializeComponent()
{
	Super::InitializeComponent();

	PhysKartOwner = Cast<APhysKart>(PawnOwner);

}

void UPhysKartMoveComp::OnRegister()
{
	const ENetMode NetMode = GetNetMode();

	Super::OnRegister();

	// Force linear smoothing for replays.
	const UWorld* MyWorld = GetWorld();
	const bool bIsReplay = (MyWorld && MyWorld->IsPlayingReplay());
	if (bIsReplay)
	{
		// At least one of these conditions will be true
		const UDemoNetDriver* DemoNetDriver = MyWorld ? MyWorld->GetDemoNetDriver() : nullptr;
		const bool bHasInterpolationData = DemoNetDriver && (DemoNetDriver->GetPlaybackDemoVersion() < HISTORY_CHARACTER_MOVEMENT_NOINTERP);
		const bool bHasRepMovement = DemoNetDriver && (DemoNetDriver->GetPlaybackDemoVersion() >= HISTORY_CHARACTER_MOVEMENT);

		if (PhysKartMovementCVars::ReplayUseInterpolation == 1)
		{
			if (bHasInterpolationData)
			{
				NetworkSmoothingMode = ENetworkSmoothingMode::Replay;
			}
			else
			{
				UE_LOG(LogPhysKartMovement, Warning, TEXT("p.ReplayUseInterpolation is enabled, but the replay was not recorded with interpolation data."));
				ensure(bHasRepMovement);
				NetworkSmoothingMode = ENetworkSmoothingMode::Linear;
			}
		}
		else
		{
			if (bHasRepMovement)
			{
				NetworkSmoothingMode = ENetworkSmoothingMode::Linear;
			}
			else
			{
				UE_LOG(LogPhysKartMovement, Warning, TEXT("p.ReplayUseInterpolation is disabled, but the replay was not recorded with rep movement data."));
				ensure(bHasInterpolationData);
				NetworkSmoothingMode = ENetworkSmoothingMode::Replay;
			}
		}
	}
	else if (NetMode == NM_ListenServer)
	{
		// Linear smoothing works on listen servers, but makes a lot less sense under the typical high update rate.
		if (NetworkSmoothingMode == ENetworkSmoothingMode::Linear)
		{
			NetworkSmoothingMode = ENetworkSmoothingMode::Exponential;
		}
	}
}

void UPhysKartMoveComp::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	SCOPED_NAMED_EVENT(UPhysKartMoveComp_TickComponent, FColor::Yellow);
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovement);
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementTick);
	CSV_SCOPED_TIMING_STAT_EXCLUSIVE(PhysKartMovement);
	const FVector InputVector = ConsumeInputVector();
	if (!HasValidData() || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Super tick may destroy/invalidate PhysKartOwner or UpdatedComponent, so we need to re-check.
	if (!HasValidData())
	{
		return;
	}

	// See if we fell out of the world.
	const bool bIsSimulatingPhysics = UpdatedComponent->IsSimulatingPhysics();
	if (!PhysKartOwner->CheckStillInWorld())
	{
		return;
	}

	ClearAccumulatedForces();

	if (PhysKartOwner->GetLocalRole() > ROLE_SimulatedProxy)
	{
		SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementNonSimulated);

		// If we are a client we might have received an update from the server.
		const bool bIsClient = (PhysKartOwner->GetLocalRole() == ROLE_AutonomousProxy && IsNetMode(NM_Client));
		if (bIsClient)
		{
			FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
			if (ClientData && ClientData->bUpdatePosition)
			{
				if (!bIsSimulatingPhysics)
				{
					ClientUpdatePositionAfterServerUpdate();
				}
				else
				{
					PreUpdate_ImmediateMode();
					//UpdateInternal_ImmediateMode(DeltaTime);
					bFirstEvalSinceReset = true;
					ClientUpdatePositionAfterServerUpdate_ImmediateMode();
					PostUpdate_ImmediateMode();
				}
			}
		}

		// Allow root motion to move characters that have no controller.
		if( PhysKartOwner->IsLocallyControlled() || (!PhysKartOwner->Controller && bRunPhysicsWithNoController))	// || (!PhysKartOwner->Controller && PhysKartOwner->IsPlayingRootMotion()) )
		{
			ControlledCharacterMove(InputVector, DeltaTime);
		}
		else if (PhysKartOwner->GetRemoteRole() == ROLE_AutonomousProxy)
		{
			// Smooth on listen server for local view of remote clients. We may receive updates at a rate different than our own tick rate.
			if (PhysKartMovementCVars::NetEnableListenServerSmoothing && !bNetworkSmoothingComplete && IsNetMode(NM_ListenServer))
			{
				SmoothClientPosition(DeltaTime);
			}
		}
	}
	else if (PhysKartOwner->GetLocalRole() == ROLE_SimulatedProxy)
	{
		//SimulatedTick(DeltaTime);
	}
}

void UPhysKartMoveComp::PostPhysicsTickComponent(float DeltaTime, FPhysKartMoveCompPostPhysicsTickFunction& ThisTickFunction)
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovement);
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementTick);
	CSV_SCOPED_TIMING_STAT_EXCLUSIVE(PhysKartMovement);

	// Super tick may destroy/invalidate PhysKartOwner or UpdatedComponent, so we need to re-check.
	if (!HasValidData())
	{
		return;
	}

	// See if we fell out of the world.
	const bool bIsSimulatingPhysics = UpdatedComponent->IsSimulatingPhysics();

	if (PhysKartOwner->GetLocalRole() > ROLE_SimulatedProxy)
	{
		// If we are a client we might have received an update from the server.
		const bool bIsClient = (PhysKartOwner->GetLocalRole() == ROLE_AutonomousProxy && IsNetMode(NM_Client));

		// Allow root motion to move characters that have no controller.
		if (PhysKartOwner->IsLocallyControlled() || (!PhysKartOwner->Controller && bRunPhysicsWithNoController))	// || (!PhysKartOwner->Controller && PhysKartOwner->IsPlayingRootMotion()) )
		{
			if (bIsClient && bIsSimulatingPhysics)
			{
				ReplicateMoveToServerPostPhysics(DeltaTime, Acceleration);
			}
		}
	}
}

void UPhysKartMoveComp::RegisterComponentTickFunctions(bool bRegister)
{
	Super::RegisterComponentTickFunctions(bRegister);

	if (bRegister)
	{
		if (SetupActorComponentTickFunction(&PostPhysicsTickFunction))
		{
			PostPhysicsTickFunction.Target = this;
			PostPhysicsTickFunction.AddPrerequisite(this, this->PrimaryComponentTick);
		}
	}
	else
	{
		if (PostPhysicsTickFunction.IsTickFunctionRegistered())
		{
			PostPhysicsTickFunction.UnRegisterTickFunction();
		}
	}
}

//-----------------------------------------------------------------------------------------------//
// 
// INetworkPredictionInterface

AController* UPhysKartMoveComp::GetController() const
{
	if (UpdatedComponent)
	{
		if (APawn* Pawn = Cast<APawn>(UpdatedComponent->GetOwner()))
		{
			return Pawn->Controller;
		}
	}
	return nullptr;
}

//6821
FString UPhysKartMoveComp::GetMovementName() const
{
	if (GetPhysKartOwner() && GetPhysKartOwner()->IsMatineeControlled())
	{
		return TEXT("Matinee");
	}

	// Using character movement
	switch (MovementMode)
	{
	case MOVE_None:				return TEXT("NULL"); break;
	case MOVE_Walking:			return TEXT("Walking"); break;
	case MOVE_NavWalking:		return TEXT("NavWalking"); break;
	case MOVE_Falling:			return TEXT("Falling"); break;
	case MOVE_Swimming:			return TEXT("Swimming"); break;
	case MOVE_Flying:			return TEXT("Flying"); break;
	case MOVE_Custom:			return TEXT("Custom"); break;
	default:					break;
	}
	return TEXT("Unknown");
}

//991
void UPhysKartMoveComp::SetDefaultMovementMode()
{
	// check for water volume
	if (CanEverSwim() && IsInWater())
	{
		SetMovementMode(DefaultWaterMovementMode);
	}
	else if (!GetPhysKartOwner() || MovementMode != DefaultLandMovementMode)
	{
		const float SavedVelocityZ = Velocity.Z;
		SetMovementMode(DefaultLandMovementMode);

		// Avoid 1-frame delay if trying to walk but walking fails at this location.
		if (MovementMode == MOVE_Walking)
		{
			Velocity.Z = SavedVelocityZ; // Prevent temporary walking state from zeroing Z velocity.
			SetMovementMode(MOVE_Falling);
		}
	}
}

void UPhysKartMoveComp::SetGroundMovementMode(EMovementMode NewGroundMovementMode)
{
	// Enforce restriction that it's either Walking or NavWalking.
	if (NewGroundMovementMode != MOVE_Walking && NewGroundMovementMode != MOVE_NavWalking)
	{
		return;
	}

	// Set new value
	GroundMovementMode = NewGroundMovementMode;

	// Possibly change movement modes if already on ground and choosing the other ground mode.
	const bool bOnGround = (MovementMode == MOVE_Walking || MovementMode == MOVE_NavWalking);
	if (bOnGround && MovementMode != NewGroundMovementMode)
	{
		SetMovementMode(NewGroundMovementMode);
	}
}

void UPhysKartMoveComp::SetMovementMode(EMovementMode NewMovementMode, uint8 NewCustomMode)
{
	if (NewMovementMode != MOVE_Custom)
	{
		NewCustomMode = 0;
	}

	// Do nothing if nothing is changing.
	if (MovementMode == NewMovementMode)
	{
		// Allow changes in custom sub-mode.
		if ((NewMovementMode != MOVE_Custom) || (NewCustomMode == CustomMovementMode))
		{
			return;
		}
	}

	const EMovementMode PrevMovementMode = MovementMode;
	const uint8 PrevCustomMode = CustomMovementMode;

	MovementMode = NewMovementMode;
	CustomMovementMode = NewCustomMode;

	// We allow setting movement mode before we have a component to update, in case this happens at startup.
	if (!HasValidData())
	{
		return;
	}

	// Handle change in movement mode
	OnMovementModeChanged(PrevMovementMode, PrevCustomMode);

}


void UPhysKartMoveComp::OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode)
{
	if (!HasValidData())
	{
		return;
	}

	GetPhysKartOwner()->OnMovementModeChanged(PreviousMovementMode, PreviousCustomMode);
	ensureMsgf(GroundMovementMode == MOVE_Walking || GroundMovementMode == MOVE_NavWalking, TEXT("Invalid GroundMovementMode %d. MovementMode: %d, PreviousMovementMode: %d"), GroundMovementMode.GetValue(), MovementMode.GetValue(), PreviousMovementMode);
};


namespace PackedMovementModeConstants
{
	const uint32 GroundShift = FMath::CeilLogTwo(MOVE_MAX);
	const uint8 CustomModeThr = 2 * (1 << GroundShift);
	const uint8 GroundMask = (1 << GroundShift) - 1;
}

uint8 UPhysKartMoveComp::PackNetworkMovementMode() const
{
	if (MovementMode != MOVE_Custom)
	{
		ensureMsgf(GroundMovementMode == MOVE_Walking || GroundMovementMode == MOVE_NavWalking, TEXT("Invalid GroundMovementMode %d."), GroundMovementMode.GetValue());
		const uint8 GroundModeBit = (GroundMovementMode == MOVE_Walking ? 0 : 1);
		return uint8(MovementMode.GetValue()) | (GroundModeBit << PackedMovementModeConstants::GroundShift);
	}
	else
	{
		return CustomMovementMode + PackedMovementModeConstants::CustomModeThr;
	}
}


void UPhysKartMoveComp::UnpackNetworkMovementMode(const uint8 ReceivedMode, TEnumAsByte<EMovementMode>& OutMode, uint8& OutCustomMode, TEnumAsByte<EMovementMode>& OutGroundMode) const
{
	if (ReceivedMode < PackedMovementModeConstants::CustomModeThr)
	{
		OutMode = TEnumAsByte<EMovementMode>(ReceivedMode & PackedMovementModeConstants::GroundMask);
		OutCustomMode = 0;
		const uint8 GroundModeBit = (ReceivedMode >> PackedMovementModeConstants::GroundShift);
		OutGroundMode = TEnumAsByte<EMovementMode>(GroundModeBit == 0 ? MOVE_Walking : MOVE_NavWalking);
	}
	else
	{
		OutMode = MOVE_Custom;
		OutCustomMode = ReceivedMode - PackedMovementModeConstants::CustomModeThr;
		OutGroundMode = MOVE_Walking;
	}
}

void UPhysKartMoveComp::ApplyNetworkMovementMode(const uint8 ReceivedMode)
{
	TEnumAsByte<EMovementMode> NetMovementMode(MOVE_None);
	TEnumAsByte<EMovementMode> NetGroundMode(MOVE_None);
	uint8 NetCustomMode(0);
	UnpackNetworkMovementMode(ReceivedMode, NetMovementMode, NetCustomMode, NetGroundMode);
	ensureMsgf(NetGroundMode == MOVE_Walking || NetGroundMode == MOVE_NavWalking, TEXT("Invalid NetGroundMode %d."), NetGroundMode.GetValue());

	GroundMovementMode = NetGroundMode;
	SetMovementMode(NetMovementMode, NetCustomMode);
}

void UPhysKartMoveComp::ClearInput()
{
	SteeringInput = 0.0f;
	ThrottleInput = 0.0f;
	BrakeInput = 0.0f;
	HandbrakeInput = 0.0f;
}

void UPhysKartMoveComp::SetThrottleInput(float Throttle)
{
	ThrottleInput = FMath::Clamp(Throttle, -1.0f, 1.0f);
}

void UPhysKartMoveComp::SetBrakeInput(float Brake)
{
	BrakeInput = FMath::Clamp(Brake, -1.0f, 1.0f);
}

void UPhysKartMoveComp::SetSteeringInput(float Steering)
{
	SteeringInput = FMath::Clamp(Steering, -1.0f, 1.0f);
}

void UPhysKartMoveComp::SetHandbrakeInput(bool bNewHandbrake)
{
	HandbrakeInput = bNewHandbrake;
}

void UPhysKartMoveComp::UpdateState(float DeltaTime)
{
	// update input values
	AController* Controller = GetController();

	SetThrottleInput(Acceleration.X);
	SetSteeringInput(Acceleration.Z);

}

//2152
void UPhysKartMoveComp::PerformMovement(float DeltaSeconds)
{
	// movement updates and replication
	if (UpdatedComponent)
	{
		APawn* MyOwner = Cast<APawn>(UpdatedComponent->GetOwner());
		if (MyOwner)
		{
			UpdateState(DeltaSeconds);
		}
	}

	//-------------------------------------------------------------------------------------------//

	const UWorld* MyWorld = GetWorld();
	if (!HasValidData() || MyWorld == nullptr)
	{
		return;
	}

	// Force floor update if we've moved outside of CharacterMovement since last update.
	bForceNextFloorCheck |= (IsMovingOnGround() && UpdatedComponent->GetComponentLocation() != LastUpdateLocation);

	FVector OldVelocity;
	FVector OldAngularVelocity;
	FVector OldLocation;
	FQuat OldQuat;

	// Scoped updates can improve performance of multiple MoveComponent calls.
	{
		FScopedMovementUpdate ScopedMovementUpdate(UpdatedComponent, bEnableScopedMovementUpdates ? EScopedUpdate::DeferredUpdates : EScopedUpdate::ImmediateUpdates);

		if (UpdatedComponent && GetPhysKartOwner())		// PaCorp * 4.25
		{
			OldVelocity = GetPhysKartOwner()->GetVelocity();
			OldAngularVelocity = GetPhysKartOwner()->GetAngularVelocity();
			OldLocation = UpdatedComponent->GetComponentLocation();
			OldQuat = UpdatedComponent->GetComponentQuat();
		}
		else
		{
			OldVelocity = Velocity;
			OldAngularVelocity = AngularVelocity;
			OldLocation = UpdatedComponent->GetComponentLocation();
			OldQuat = UpdatedComponent->GetComponentQuat();
		}

		// Update the character state before we do our movement
		UpdateCharacterStateBeforeMovement(DeltaSeconds);

		// NaN tracking
		devCode(ensureMsgf(!Velocity.ContainsNaN(), TEXT("UPhysKartMoveComp::PerformMovement: Velocity contains NaN (%s)\n%s"), *GetPathNameSafe(this), *Velocity.ToString()));

		// Clear jump input now, to allow movement events to trigger it for next update.
		NumJumpApexAttempts = 0;

		// change position
		StartNewPhysics(DeltaSeconds, 0);

		if (!HasValidData())
		{
			return;
		}

		// Update character state based on change from movement
		UpdateCharacterStateAfterMovement(DeltaSeconds);

		OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);
	} // End scoped movement update

	// Call external post-movement events. These happen after the scoped movement completes in case the events want to use the current state of overlaps etc.
	CallMovementUpdateDelegate(DeltaSeconds, OldLocation, OldVelocity);

	//SaveBaseLocation();
	UpdateComponentVelocity();

	const bool bHasAuthority = GetPhysKartOwner() && GetPhysKartOwner()->HasAuthority();

	// If we move we want to avoid a long delay before replication catches up to notice this change, especially if it's throttling our rate.
	if (bHasAuthority && UNetDriver::IsAdaptiveNetUpdateFrequencyEnabled() && UpdatedComponent)
	{
		UNetDriver* NetDriver = MyWorld->GetNetDriver();
		if (NetDriver && NetDriver->IsServer())
		{
			FNetworkObjectInfo* NetActor = NetDriver->FindOrAddNetworkObjectInfo(GetPhysKartOwner());

			if (NetActor && MyWorld->GetTimeSeconds() <= NetActor->NextUpdateTime && NetDriver->IsNetworkActorUpdateFrequencyThrottled(*NetActor))
			{
				if (ShouldCancelAdaptiveReplication())
				{
					NetDriver->CancelAdaptiveReplication(*NetActor);
				}
			}
		}
	}

	const FVector NewLocation = UpdatedComponent ? UpdatedComponent->GetComponentLocation() : FVector::ZeroVector;
	const FQuat NewRotation = UpdatedComponent ? UpdatedComponent->GetComponentQuat() : FQuat::Identity;

	if (bHasAuthority && UpdatedComponent && !IsNetMode(NM_Client))
	{
		const bool bLocationChanged = (NewLocation != LastUpdateLocation);
		const bool bRotationChanged = (NewRotation != LastUpdateRotation);
		if (bLocationChanged || bRotationChanged)
		{
			// Update ServerLastTransformUpdateTimeStamp. This is used by Linear smoothing on clients to interpolate positions with the correct delta time,
			// so the timestamp should be based on the client's move delta (ServerAccumulatedClientTimeStamp), not the server time when receiving the RPC.
			const bool bIsRemotePlayer = (GetPhysKartOwner()->GetRemoteRole() == ROLE_AutonomousProxy);
			const FNetworkPredictionData_Server_PhysKart* ServerData = bIsRemotePlayer ? GetPredictionData_Server_PhysKart() : nullptr;
			if (bIsRemotePlayer && ServerData && PhysKartMovementCVars::NetUseClientTimestampForReplicatedTransform)
			{
				ServerLastTransformUpdateTimeStamp = float(ServerData->ServerAccumulatedClientTimeStamp);
			}
			else
			{
				ServerLastTransformUpdateTimeStamp = MyWorld->GetTimeSeconds();
			}
		}
	}

	if (UpdatedComponent)
	{
		LastUpdateLocation = UpdatedComponent->GetComponentLocation();
		LastUpdateRotation = UpdatedComponent->GetComponentQuat();
	}


	LastUpdateLocation = NewLocation;
	LastUpdateRotation = NewRotation;

	if (GetPhysKartOwner())
	{
		LastUpdateVelocity = GetPhysKartOwner()->GetVelocity();
		LastUpdateAngularVelocity = GetPhysKartOwner()->GetAngularVelocity();
	}
	else
	{
		LastUpdateVelocity = Velocity;
		LastUpdateAngularVelocity = AngularVelocity;
	}
}

//2512
bool UPhysKartMoveComp::ShouldCancelAdaptiveReplication() const
{
	// Update sooner if important properties changed.
	const bool bVelocityChanged = (Velocity != LastUpdateVelocity);
	const bool bAngularVelocityChanged = (AngularVelocity != LastUpdateAngularVelocity);
	const bool bLocationChanged = (UpdatedComponent->GetComponentLocation() != LastUpdateLocation);
	const bool bRotationChanged = (UpdatedComponent->GetComponentQuat() != LastUpdateRotation);

	return (bVelocityChanged || bAngularVelocityChanged || bLocationChanged || bRotationChanged);
}

void UPhysKartMoveComp::UpdateCharacterStateBeforeMovement(float DeltaSeconds)
{
	//// Proxies get replicated states.
	//if (GetPhysKartOwner()->GetLocalRole() != ROLE_SimulatedProxy)
	//{
	//	// Check for a change in crouch state. Players toggle crouch by changing bWantsToCrouch.
	//	const bool bIsCrouching = IsCrouching();
	//	if (bIsCrouching && (!bWantsToCrouch || !CanCrouchInCurrentState()))
	//	{
	//		UnCrouch(false);
	//	}
	//	else if (!bIsCrouching && bWantsToCrouch && CanCrouchInCurrentState())
	//	{
	//		Crouch(false);
	//	}
	//}
}

void UPhysKartMoveComp::UpdateCharacterStateAfterMovement(float DeltaSeconds)
{
	//// Proxies get replicated crouch state.
	//if (GetPhysKartOwner()->GetLocalRole() != ROLE_SimulatedProxy)
	//{
	//	// Uncrouch if no longer allowed to be crouched
	//	if (IsCrouching() && !CanCrouchInCurrentState())
	//	{
	//		UnCrouch(false);
	//	}
	//}
}

void UPhysKartMoveComp::StartNewPhysics(float deltaTime, int32 Iterations)
{
	if ((deltaTime < MIN_TICK_TIME) || (Iterations >= MaxSimulationIterations) || !HasValidData())
	{
		return;
	}

	if (UpdatedComponent->IsSimulatingPhysics())
	{
		//UE_LOG(LogPhysKartMovement, Log, TEXT("UPhysKartMoveComp::StartNewPhysics: UpdateComponent (%s) is simulating physics - aborting."), *UpdatedComponent->GetPathName());

		const bool bSavedMovementInProgress = bMovementInProgress;
		bMovementInProgress = true;

		PhysRolling(deltaTime, Iterations);

		ApplyAccumulatedForces(deltaTime);

		bMovementInProgress = bSavedMovementInProgress;
		if (bDeferUpdateMoveComponent)
		{
			SetUpdatedComponent(DeferredUpdatedMoveComponent);
		}
		return;
	}
	else
	{
		//UE_LOG(LogPhysKartMovement, Log, TEXT("UPhysKartMoveComp::StartNewPhysics: UpdateComponent (%s) is simulating physics - aborting."), *UpdatedComponent->GetPathName());

		const bool bSavedMovementInProgress = bMovementInProgress;
		bMovementInProgress = true;

		PhysRolling(deltaTime, Iterations);

		bMovementInProgress = bSavedMovementInProgress;
		if (bDeferUpdateMoveComponent)
		{
			SetUpdatedComponent(DeferredUpdatedMoveComponent);
		}
		return;
	}
}

FArchive& operator<<(FArchive& Ar, FPhysKartReplaySample& V)
{
	SerializePackedVector<10, 24>(V.Location, Ar);
	SerializePackedVector<10, 24>(V.Velocity, Ar);
	SerializePackedVector<10, 24>(V.Acceleration, Ar);
	V.Rotation.SerializeCompressed(Ar);
	Ar << V.RemoteViewPitch;

	if (Ar.IsSaving() || (!Ar.AtEnd() && !Ar.IsError()))
	{
		Ar << V.Time;
	}

	return Ar;
}

//4790
void UPhysKartMoveComp::PhysRolling(float deltaTime, int32 Iterations)
{
	SCOPE_CYCLE_COUNTER(STAT_KartPhysWalking);
	CSV_SCOPED_TIMING_STAT_EXCLUSIVE(CharPhysWalking);

	if (deltaTime < MIN_TICK_TIME)
	{
		return;
	}

	if (!PhysKartOwner || (!PhysKartOwner->Controller && !bRunPhysicsWithNoController && (PhysKartOwner->GetLocalRole() != ROLE_SimulatedProxy)))
	{
		Acceleration = FVector::ZeroVector;
		Velocity = FVector::ZeroVector;
		return;
	}

	devCode(ensureMsgf(!Velocity.ContainsNaN(), TEXT("PhysWalking: Velocity contains NaN before Iteration (%s)\n%s"), *GetPathNameSafe(this), *Velocity.ToString()));

	bJustTeleported = false;
	bool bCheckedFall = false;
	bool bTriedLedgeMove = false;
	float remainingTime = deltaTime;

	float UpdatedComponentMass = 1000.0f;
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartMesh())
	{
		UpdatedComponentMass = GetPhysKartOwner()->GetPhysKartMesh()->GetMass();
	}

	if (UpdatedComponent->IsSimulatingPhysics())
	{
		FBodyInstance* RootBI = GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->GetBodyInstance() ? GetPhysKartOwner()->GetPhysKartRoot()->GetBodyInstance() : nullptr;
		if (RootBI)
		{
			FVector BodyInstanceCOM = RootBI->GetCOMPosition();
			float BodyInstanceMass = RootBI->GetBodyMass();
			
			FTransform NewBodyTransform = RootBI->GetUnrealWorldTransform();

			FVector FwForce = UpdatedComponent->GetForwardVector() * ThrottleInput * fThrottleWheelScale;
			FVector LinearVelocity = FwForce;		//RootBI->GetUnrealWorldVelocity() + FwForce * deltaTime;
			float fSteeringInputScaleValue = fSteeringWheelScale;
			if (UseEuclideanTransformation)
			{
				LinearVelocity = RootBI->GetUnrealWorldVelocity() + FwForce * deltaTime;
				fSteeringInputScaleValue = 180.0f / fSteeringInputScaleValue;		// The feel of rotation
			}				
				
			FVector NewLocation = NewBodyTransform.GetLocation() + LinearVelocity;

			FVector RotTorque = UpdatedComponent->GetUpVector() * SteeringInput * fSteeringInputScaleValue;			
			FVector LinearAngVelocity = (RotTorque / DEGREES_IN_RAD);
			if (UseEuclideanTransformation)
			{
				LinearAngVelocity = RootBI->GetUnrealWorldAngularVelocityInRadians() + (RotTorque * deltaTime);
			}
			FQuat DeltaQuat = FQuat(LinearAngVelocity.GetSafeNormal(), LinearAngVelocity.Size() * deltaTime);

			FQuat NewQuat = DeltaQuat * NewBodyTransform.GetRotation();

			NewBodyTransform.SetLocation(NewLocation);
			NewBodyTransform.SetRotation(NewQuat);

			RootBI->SetLinearVelocity(LinearVelocity, false);
			RootBI->SetAngularVelocityInRadians(LinearAngVelocity, false);			
		}
	}
	else
	{
		FVector FwForce = UpdatedComponent->GetForwardVector() * ThrottleInput * fThrottleWheelScale * deltaTime;
		FVector DeltaMove = FwForce;

		FVector RotTorque = UpdatedComponent->GetUpVector() * SteeringInput * fSteeringWheelScale * deltaTime;

		float DeltaQuatAngle = SteeringInput * fSteeringWheelScale * FMath::Sign(ThrottleInput) * deltaTime;
		DeltaQuatAngle /= DEGREES_IN_RAD;

		FQuat DeltaQuat = FQuat(UpdatedComponent->GetUpVector(), DeltaQuatAngle);
		FQuat NewQuat = DeltaQuat * UpdatedComponent->GetComponentQuat();

		FHitResult Hit(1.f);
		SafeMoveUpdatedComponent(DeltaMove, NewQuat, true, Hit);
	}
}

//5844
void UPhysKartMoveComp::AddImpulse(FVector Impulse, bool bVelocityChange)
{

}

//5866
void UPhysKartMoveComp::AddForce(FVector Force)
{
	PendingForceToApply += Force;
}

void UPhysKartMoveComp::AddTorque(FVector Torque)
{
	PendingTorqueToApply += Torque;
}

//5557
void UPhysKartMoveComp::ControlledCharacterMove(const FVector& InputVector, float DeltaSeconds)
{
	{
		SCOPE_CYCLE_COUNTER(STAT_KartUpdateAcceleration);

		// We need to check the jump state before adjusting input acceleration, to minimize latency
		// and to make sure acceleration respects our potentially new falling state.

		// apply input to acceleration
		Acceleration = ScaleInputAcceleration(ConstrainInputAcceleration(InputVector));
		AnalogInputModifier = ComputeAnalogInputModifier();
	}

	if (GetPhysKartOwner()->GetLocalRole() == ROLE_Authority)
	{
		PerformMovement(DeltaSeconds);
	}
	else if (GetPhysKartOwner()->GetLocalRole() == ROLE_AutonomousProxy && IsNetMode(NM_Client))
	{
		ReplicateMoveToServer(DeltaSeconds, Acceleration);
	}
}

//7278
void UPhysKartMoveComp::SmoothClientPosition(float DeltaSeconds)
{
	if (!HasValidData() || NetworkSmoothingMode == ENetworkSmoothingMode::Disabled)
	{
		return;
	}

	// We shouldn't be running this on a server that is not a listen server.
	checkSlow(GetNetMode() != NM_DedicatedServer);
	checkSlow(GetNetMode() != NM_Standalone);

	// Only client proxies or remote clients on a listen server should run this code.
	const bool bIsSimulatedProxy = (GetPhysKartOwner()->GetLocalRole() == ROLE_SimulatedProxy);
	const bool bIsRemoteAutoProxy = (GetPhysKartOwner()->GetRemoteRole() == ROLE_AutonomousProxy);
	if (!ensure(bIsSimulatedProxy || bIsRemoteAutoProxy))
	{
		return;
	}

	SmoothClientPosition_Interpolate(DeltaSeconds);
	SmoothClientPosition_UpdateVisuals();
}


void UPhysKartMoveComp::SmoothClientPosition_Interpolate(float DeltaSeconds)
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementSmoothClientPosition_Interp);
	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	if (ClientData)
	{
		if (NetworkSmoothingMode == ENetworkSmoothingMode::Linear)
		{
			const UWorld* MyWorld = GetWorld();

			// Increment client position.
			ClientData->SmoothingClientTimeStamp += DeltaSeconds;

			float LerpPercent = 0.f;
			const float LerpLimit = 1.15f;
			const float TargetDelta = ClientData->LastCorrectionDelta;
			if (TargetDelta > SMALL_NUMBER)
			{
				// Don't let the client get too far ahead (happens on spikes). But we do want a buffer for variable network conditions.
				const float MaxClientTimeAheadPercent = 0.15f;
				const float MaxTimeAhead = TargetDelta * MaxClientTimeAheadPercent;
				ClientData->SmoothingClientTimeStamp = FMath::Min<float>(ClientData->SmoothingClientTimeStamp, ClientData->SmoothingServerTimeStamp + MaxTimeAhead);

				// Compute interpolation alpha based on our client position within the server delta. We should take TargetDelta seconds to reach alpha of 1.
				const float RemainingTime = ClientData->SmoothingServerTimeStamp - ClientData->SmoothingClientTimeStamp;
				const float CurrentSmoothTime = TargetDelta - RemainingTime;
				LerpPercent = FMath::Clamp(CurrentSmoothTime / TargetDelta, 0.0f, LerpLimit);

				//UE_LOG(LogCharacterNetSmoothing, VeryVerbose, TEXT("Interpolate: WorldTime: %.6f, ServerTimeStamp: %.6f, ClientTimeStamp: %.6f, Elapsed: %.6f, Alpha: %.6f for %s"),
				//	MyWorld->GetTimeSeconds(), ClientData->SmoothingServerTimeStamp, ClientData->SmoothingClientTimeStamp, CurrentSmoothTime, LerpPercent, *GetNameSafe(GetPhysKartOwner()));
			}
			else
			{
				LerpPercent = 1.0f;
			}

			if (LerpPercent >= 1.0f - KINDA_SMALL_NUMBER)
			{
				if (Velocity.IsNearlyZero() && AngularVelocity.IsNearlyZero())
				{
					ClientData->MeshTranslationOffset = FVector::ZeroVector;
					ClientData->SmoothingClientTimeStamp = ClientData->SmoothingServerTimeStamp;
					bNetworkSmoothingComplete = true;
				}
				else
				{
					// Allow limited forward prediction.
					ClientData->MeshTranslationOffset = FMath::LerpStable(ClientData->OriginalMeshTranslationOffset, FVector::ZeroVector, LerpPercent);
					bNetworkSmoothingComplete = (LerpPercent >= LerpLimit);
				}

				ClientData->MeshRotationOffset = ClientData->MeshRotationTarget;
			}
			else
			{
				ClientData->MeshTranslationOffset = FMath::LerpStable(ClientData->OriginalMeshTranslationOffset, FVector::ZeroVector, LerpPercent);
				ClientData->MeshRotationOffset = FQuat::FastLerp(ClientData->OriginalMeshRotationOffset, ClientData->MeshRotationTarget, LerpPercent).GetNormalized();
			}
		}
		else if (NetworkSmoothingMode == ENetworkSmoothingMode::Exponential)
		{
			// Smooth interpolation of mesh translation to avoid popping of other client pawns unless under a low tick rate.
			// Faster interpolation if stopped.
			const float SmoothLocationTime = Velocity.IsZero() ? 0.5f * ClientData->SmoothNetUpdateTime : ClientData->SmoothNetUpdateTime;
			if (DeltaSeconds < SmoothLocationTime)
			{
				// Slowly decay translation offset
				ClientData->MeshTranslationOffset = (ClientData->MeshTranslationOffset * (1.f - DeltaSeconds / SmoothLocationTime));
			}
			else
			{
				ClientData->MeshTranslationOffset = FVector::ZeroVector;
			}

			// Smooth rotation
			const FQuat MeshRotationTarget = ClientData->MeshRotationTarget;
			if (DeltaSeconds < ClientData->SmoothNetUpdateRotationTime)
			{
				// Slowly decay rotation offset
				ClientData->MeshRotationOffset = FQuat::FastLerp(ClientData->MeshRotationOffset, MeshRotationTarget, DeltaSeconds / ClientData->SmoothNetUpdateRotationTime).GetNormalized();
			}
			else
			{
				ClientData->MeshRotationOffset = MeshRotationTarget;
			}

			// Check if lerp is complete
			if (ClientData->MeshTranslationOffset.IsNearlyZero(1e-2f) && ClientData->MeshRotationOffset.Equals(MeshRotationTarget, 1e-5f))
			{
				bNetworkSmoothingComplete = true;
				// Make sure to snap exactly to target values.
				ClientData->MeshTranslationOffset = FVector::ZeroVector;
				ClientData->MeshRotationOffset = MeshRotationTarget;
			}
		}
		else if (NetworkSmoothingMode == ENetworkSmoothingMode::Replay)
		{
			const UWorld* MyWorld = GetWorld();

			if (!MyWorld || !MyWorld->DemoNetDriver)
			{
				return;
			}

			const float CurrentTime = MyWorld->DemoNetDriver->DemoCurrentTime;

			// Remove old samples
			while (ClientData->ReplaySamples.Num() > 0)
			{
				if (ClientData->ReplaySamples[0].Time > CurrentTime - 1.0f)
				{
					break;
				}

				ClientData->ReplaySamples.RemoveAt(0);
			}

			FReplayExternalDataArray* ExternalReplayData = MyWorld->DemoNetDriver->GetExternalDataArrayForObject(GetPhysKartOwner());

			// Grab any samples available, deserialize them, then clear originals
			if (ExternalReplayData && ExternalReplayData->Num() > 0)
			{
				for (int i = 0; i < ExternalReplayData->Num(); i++)
				{
					FPhysKartReplaySample ReplaySample;

					(*ExternalReplayData)[i].Reader << ReplaySample;

					if (FMath::IsNearlyZero(ReplaySample.Time))
					{
						ReplaySample.Time = (*ExternalReplayData)[i].TimeSeconds;
					}

					ClientData->ReplaySamples.Add(ReplaySample);
				}

				if (PhysKartMovementCVars::FixReplayOverSampling > 0)
				{
					// Remove invalid replay samples that can occur due to oversampling (sampling at higher rate than physics is being ticked)
					// We detect this by finding samples that have the same location but have a velocity that says the character should be moving
					// If we don't do this, then characters will look like they are skipping around, which looks really bad
					for (int i = 1; i < ClientData->ReplaySamples.Num(); i++)
					{
						if (ClientData->ReplaySamples[i].Location.Equals(ClientData->ReplaySamples[i - 1].Location, KINDA_SMALL_NUMBER))
						{
							if (ClientData->ReplaySamples[i - 1].Velocity.SizeSquared() > FMath::Square(KINDA_SMALL_NUMBER) && ClientData->ReplaySamples[i].Velocity.SizeSquared() > FMath::Square(KINDA_SMALL_NUMBER))
							{
								ClientData->ReplaySamples.RemoveAt(i);
								i--;
							}
						}
					}
				}

				ExternalReplayData->Empty();
			}

			bool bFoundSample = false;

			for (int i = 0; i < ClientData->ReplaySamples.Num() - 1; i++)
			{
				if (CurrentTime >= ClientData->ReplaySamples[i].Time && CurrentTime <= ClientData->ReplaySamples[i + 1].Time)
				{
					const float EPSILON = SMALL_NUMBER;
					const float Delta = (ClientData->ReplaySamples[i + 1].Time - ClientData->ReplaySamples[i].Time);
					const float LerpPercent = Delta > EPSILON ? FMath::Clamp<float>((float)(CurrentTime - ClientData->ReplaySamples[i].Time) / Delta, 0.0f, 1.0f) : 1.0f;

					const FPhysKartReplaySample& ReplaySample1 = ClientData->ReplaySamples[i];
					const FPhysKartReplaySample& ReplaySample2 = ClientData->ReplaySamples[i + 1];

					const FVector Location = FMath::Lerp(ReplaySample1.Location, ReplaySample2.Location, LerpPercent);
					const FQuat Rotation = FQuat::FastLerp(FQuat(ReplaySample1.Rotation), FQuat(ReplaySample2.Rotation), LerpPercent).GetNormalized();
					Velocity = FMath::Lerp(ReplaySample1.Velocity, ReplaySample2.Velocity, LerpPercent);
					AngularVelocity = FMath::Lerp(ReplaySample1.AngularVelocity, ReplaySample2.AngularVelocity, LerpPercent);

					if (PhysKartMovementCVars::ReplayLerpAcceleration)
					{
						Acceleration = FMath::Lerp(ReplaySample1.Acceleration, ReplaySample2.Acceleration, LerpPercent);
					}
					else
					{
						Acceleration = ReplaySample2.Acceleration;
					}

					const FRotator Rotator1(FRotator::DecompressAxisFromByte(ReplaySample1.RemoteViewPitch), 0.0f, 0.0f);
					const FRotator Rotator2(FRotator::DecompressAxisFromByte(ReplaySample2.RemoteViewPitch), 0.0f, 0.0f);
					const FRotator FinalPitch = FQuat::FastLerp(FQuat(Rotator1), FQuat(Rotator2), LerpPercent).GetNormalized().Rotator();
					GetPhysKartOwner()->BlendedReplayViewPitch = FinalPitch.Pitch;

					UpdateComponentVelocity();

					USkeletalMeshComponent* Mesh = GetPhysKartOwner()->GetPhysKartMesh();
					bFoundSample = true;
					break;
				}
			}

			if (!bFoundSample && ClientData->ReplaySamples.Num() > 0)
			{
				int32 BestSample = 0;
				float BestDelta = FMath::Abs(ClientData->ReplaySamples[BestSample].Time - CurrentTime);

				for (int i = 1; i < ClientData->ReplaySamples.Num(); i++)
				{
					const float Delta = FMath::Abs(ClientData->ReplaySamples[i].Time - CurrentTime);
					if (Delta < BestDelta)
					{
						BestDelta = Delta;
						BestSample = i;
					}
				}

				const FPhysKartReplaySample& ReplaySample = ClientData->ReplaySamples[BestSample];

				Velocity = ReplaySample.Velocity;
				AngularVelocity = ReplaySample.AngularVelocity;
				Acceleration = ReplaySample.Acceleration;
				GetPhysKartOwner()->BlendedReplayViewPitch = FRotator::DecompressAxisFromByte(ReplaySample.RemoteViewPitch);

				UpdateComponentVelocity();

				USkeletalMeshComponent* const Mesh = GetPhysKartOwner()->GetPhysKartMesh();
			}

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
			// Show future samples
			if (PhysKartMovementCVars::NetVisualizeSimulatedCorrections >= 1)
			{
				const float Radius = 4.0f;
				const int32	Sides = 8;
				const float ArrowSize = 4.0f;
				const FColor DebugColor = FColor::White;

				// Draw points ahead up to a few seconds
				for (int i = 0; i < ClientData->ReplaySamples.Num(); i++)
				{
					const bool bHasMorePoints = i < ClientData->ReplaySamples.Num() - 1;
					const bool bActiveSamples = (bHasMorePoints && CurrentTime >= ClientData->ReplaySamples[i].Time && CurrentTime <= ClientData->ReplaySamples[i + 1].Time);

					if (ClientData->ReplaySamples[i].Time >= CurrentTime || bActiveSamples)
					{
						//const FVector Adjust = FVector( 0.f, 0.f, 300.0f + i * 15.0f );
						const FVector Adjust = FVector(0.f, 0.f, 300.0f);
						const FVector Location = ClientData->ReplaySamples[i].Location + Adjust;

						if (bHasMorePoints)
						{
							const FVector NextLocation = ClientData->ReplaySamples[i + 1].Location + Adjust;
							DrawDebugDirectionalArrow(GetWorld(), Location, NextLocation, 4.0f, FColor(0, 255, 0, 255));
						}

						DrawCircle(GetWorld(), Location, FVector(1, 0, 0), FVector(0, 1, 0), FColor(255, 0, 0, 255), Radius, Sides, false, 0.0f);

						if (PhysKartMovementCVars::NetVisualizeSimulatedCorrections >= 2)
						{
							DrawDebugDirectionalArrow(GetWorld(), Location, Location + ClientData->ReplaySamples[i].Velocity, 20.0f, FColor(255, 0, 0, 255));
						}

						if (PhysKartMovementCVars::NetVisualizeSimulatedCorrections >= 3)
						{
							DrawDebugDirectionalArrow(GetWorld(), Location, Location + ClientData->ReplaySamples[i].Acceleration, 20.0f, FColor(255, 255, 255, 255));
						}
					}

					if (ClientData->ReplaySamples[i].Time - CurrentTime > 2.0f)
					{
						break;
					}
				}
			}
#endif

			bNetworkSmoothingComplete = false;
		}
		else
		{
			// Unhandled mode
		}
	}
}

void UPhysKartMoveComp::SmoothClientPosition_UpdateVisuals()
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementSmoothClientPosition_Visual);
	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	USkeletalMeshComponent* Mesh = GetPhysKartOwner()->GetPhysKartMesh();
	if (ClientData && Mesh && !Mesh->IsSimulatingPhysics())
	{
		if (NetworkSmoothingMode == ENetworkSmoothingMode::Linear)
		{
			// Adjust capsule rotation and mesh location. Optimized to trigger only one transform chain update.
			// If we know the rotation is changing that will update children, so it's sufficient to set RelativeLocation directly on the mesh.
			const FVector NewRelLocation = ClientData->MeshRotationOffset.UnrotateVector(ClientData->MeshTranslationOffset);	// +GetPhysKartOwner()->GetBaseTranslationOffset();
			if (!UpdatedComponent->GetComponentQuat().Equals(ClientData->MeshRotationOffset, 1e-6f))
			{
				const FVector OldLocation = Mesh->GetRelativeLocation();
				const FRotator OldRotation = UpdatedComponent->GetRelativeRotation();
				Mesh->SetRelativeLocation_Direct(NewRelLocation);
				UpdatedComponent->SetWorldRotation(ClientData->MeshRotationOffset);

				// If we did not move from SetWorldRotation, we need to at least call SetRelativeLocation since we were relying on the UpdatedComponent to update the transform of the mesh
				if (UpdatedComponent->GetRelativeRotation() == OldRotation)
				{
					Mesh->SetRelativeLocation_Direct(OldLocation);
					Mesh->SetRelativeLocation(NewRelLocation, false, nullptr, GetTeleportType());
				}
			}
			else
			{
				Mesh->SetRelativeLocation(NewRelLocation, false, nullptr, GetTeleportType());
			}
		}
		else if (NetworkSmoothingMode == ENetworkSmoothingMode::Exponential)
		{
			// Adjust mesh location and rotation
			const FVector NewRelTranslation = UpdatedComponent->GetComponentToWorld().InverseTransformVectorNoScale(ClientData->MeshTranslationOffset);		// +GetPhysKartOwner()->GetBaseTranslationOffset();
			const FQuat NewRelRotation = ClientData->MeshRotationOffset;		// *GetPhysKartOwner()->GetBaseRotationOffset();
			Mesh->SetRelativeLocationAndRotation(NewRelTranslation, NewRelRotation, false, nullptr, GetTeleportType());
		}
		else if (NetworkSmoothingMode == ENetworkSmoothingMode::Replay)
		{
			if (!UpdatedComponent->GetComponentQuat().Equals(ClientData->MeshRotationOffset, SCENECOMPONENT_QUAT_TOLERANCE) || !UpdatedComponent->GetComponentLocation().Equals(ClientData->MeshTranslationOffset, KINDA_SMALL_NUMBER))
			{
				UpdatedComponent->SetWorldLocationAndRotation(ClientData->MeshTranslationOffset, ClientData->MeshRotationOffset, false, nullptr, GetTeleportType());
			}
		}
		else
		{
			// Unhandled mode
		}
	}
}
//7727

//7730
bool UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate()
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementClientUpdatePositionAfterServerUpdate);
	if (!HasValidData())
	{
		return false;
	}

	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	check(ClientData);

	if (!ClientData->bUpdatePosition)
	{
		return false;
	}

	ClientData->bUpdatePosition = false;

	// Don't do any network position updates on things running PHYS_RigidBody
	if (GetPhysKartOwner()->GetRootComponent() && GetPhysKartOwner()->GetRootComponent()->IsSimulatingPhysics())
	{
		//return false; // PaCorp *4.26
	}

	if (!GetPhysKartOwner() || !GetPhysKartOwner()->GetPhysKartRoot())
	{
		return false;
	}

	if (ClientData->SavedMoves.Num() == 0)
	{
		UE_LOG(LogNetPlayerMovement, Verbose, TEXT("ClientUpdatePositionAfterServerUpdate No saved moves to replay"), ClientData->SavedMoves.Num());

		return false;
	}

	// Save important values that might get affected by the replay.
	const float SavedAnalogInputModifier = AnalogInputModifier;

	const bool bRealForceMaxAccel = bForceMaxAccel;

	const bool bRealItem = GetPhysKartOwner()->bPressedItem;

	GetPhysKartOwner()->bClientUpdating = true;

	if (ClientData->SavedMoves.Num() > 0)
	{		
		// Replay moves that have not yet been acked.
		UE_LOG(LogNetPlayerMovement, Verbose, TEXT("ClientUpdatePositionAfterServerUpdate Replaying %d Moves, starting at Timestamp %f"), ClientData->SavedMoves.Num(), ClientData->SavedMoves[0]->TimeStamp);
		for (int32 i = 0; i < ClientData->SavedMoves.Num(); i++)
		{
			FSavedMove_PhysKart* const CurrentMove = ClientData->SavedMoves[i].Get();
			checkSlow(CurrentMove != nullptr);
			CurrentMove->PrepMoveFor(GetPhysKartOwner());

			if (ShouldUsePackedMovementRPCs())
			{
				// Make current move data accessible to MoveAutonomous or any other functions that might need it.
				if (FPhysKartNetworkMoveData* NewMove = GetNetworkMoveDataContainer().GetNewMoveData())
				{
					SetCurrentNetworkMoveData(NewMove);
					NewMove->ClientFillNetworkMoveData(*CurrentMove, FPhysKartNetworkMoveData::ENetworkMoveType::NewMove);
				}
			}

			MoveAutonomous(CurrentMove->TimeStamp, CurrentMove->DeltaTime, CurrentMove->ReplicatedInputState, CurrentMove->GetCompressedFlags(), CurrentMove->Acceleration);
			CurrentMove->PostUpdate(GetPhysKartOwner(), FSavedMove_PhysKart::PostUpdate_Replay);
			SetCurrentNetworkMoveData(nullptr);
		}
	}

	DrawDebugSphere(GetWorld(), GetPhysKartOwner()->GetPhysKartRoot()->GetComponentLocation(), 100, 16, FColor::Yellow, false, -1.0F);

	if (FSavedMove_PhysKart* const PendingMove = ClientData->PendingMove.Get())
	{
		PendingMove->bForceNoCombine = true;
	}

	// Restore saved values.
	AnalogInputModifier = SavedAnalogInputModifier;

	GetPhysKartOwner()->bClientUpdating = false;
	bForceMaxAccel = bRealForceMaxAccel;

	GetPhysKartOwner()->bPressedItem = bRealItem;

	return (ClientData->SavedMoves.Num() > 0);
}

//8000
void UPhysKartMoveComp::ReplicateMoveToServer(float DeltaTime, const FVector& NewAcceleration)
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementReplicateMoveToServer);
	check(GetPhysKartOwner() != NULL);

	// Can only start sending moves if our controllers are synced up over the network, otherwise we flood the reliable buffer.
	APlayerController* PC = Cast<APlayerController>(GetPhysKartOwner()->GetController());
	if (PC && PC->AcknowledgedPawn != GetPhysKartOwner())
	{
		return;
	}

	// Bail out if our character's controller doesn't have a Player. This may be the case when the local player
	// has switched to another controller, such as a debug camera controller.
	if (PC && PC->Player == nullptr)
	{
		return;
	}

	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	if (!ClientData)
	{
		return;
	}

	// Update our delta time for physics simulation.
	DeltaTime = ClientData->UpdateTimeStampAndDeltaTime(DeltaTime, *GetPhysKartOwner(), *this);

	// Find the oldest (unacknowledged) important move (OldMove).
	// Don't include the last move because it may be combined with the next new move.
	// A saved move is interesting if it differs significantly from the last acknowledged move
	FPhysKartSavedMovePtr OldMove = NULL;
	if (ClientData->LastAckedMove.IsValid())
	{
		const int32 NumSavedMoves = ClientData->SavedMoves.Num();
		for (int32 i = 0; i < NumSavedMoves - 1; i++)
		{
			const FPhysKartSavedMovePtr& CurrentMove = ClientData->SavedMoves[i];
			if (CurrentMove->IsImportantMove(ClientData->LastAckedMove))
			{
				OldMove = CurrentMove;
				break;
			}
		}
	}

	// Get a SavedMove object to store the movement in.
	FPhysKartSavedMovePtr NewMovePtr = ClientData->CreateSavedMove();
	FSavedMove_PhysKart* const NewMove = NewMovePtr.Get();
	if (NewMove == nullptr)
	{
		return;
	}

	NewMove->SetMoveFor(GetPhysKartOwner(), DeltaTime, NewAcceleration, *ClientData);
	const UWorld* MyWorld = GetWorld();

	// see if the two moves could be combined
	// do not combine moves which have different TimeStamps (before and after reset).
	if (const FSavedMove_PhysKart* PendingMove = ClientData->PendingMove.Get())
	{
		if (PendingMove->CanCombineWith(NewMovePtr, GetPhysKartOwner(), ClientData->MaxMoveDeltaTime * GetPhysKartOwner()->GetActorTimeDilation(*MyWorld)))
		{
			SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementCombineNetMove);

			// Only combine and move back to the start location if we don't move back in to a spot that would make us collide with something new.
			const FVector OldStartLocation = PendingMove->GetRevertedLocation();
			//const bool bAttachedToObject = (NewMovePtr->StartAttachParent != nullptr);
			//if (bAttachedToObject || !OverlapTest(OldStartLocation, PendingMove->StartRotation.Quaternion(), UpdatedComponent->GetCollisionObjectType(), GetPawnCapsuleCollisionShape(SHRINK_None), GetPhysKartOwner()))
			if (!OverlapTest(OldStartLocation, PendingMove->StartRotation.Quaternion(), UpdatedComponent->GetCollisionObjectType(), GetWheeledPawnCollisionShape(), GetPhysKartOwner()))
			{
				// Avoid updating Mesh bones to physics during the teleport back, since PerformMovement() will update it right away anyway below.
				// Note: this must be before the FScopedMovementUpdate below, since that scope is what actually moves the character and mesh.
				FScopedWheeledMeshBoneUpdateOverride ScopedNoMeshBoneUpdate(GetPhysKartOwner()->GetPhysKartMesh(), EKinematicBonesUpdateToPhysics::SkipAllBones);

				// Accumulate multiple transform updates until scope ends.
				FScopedMovementUpdate ScopedMovementUpdate(UpdatedComponent, EScopedUpdate::DeferredUpdates);
				UE_LOG(LogNetPlayerMovement, VeryVerbose, TEXT("CombineMove: add delta %f + %f and revert from %f %f to %f %f"), DeltaTime, PendingMove->DeltaTime, UpdatedComponent->GetComponentLocation().X, UpdatedComponent->GetComponentLocation().Y, OldStartLocation.X, OldStartLocation.Y);

				NewMove->CombineWith(PendingMove, GetPhysKartOwner(), PC, OldStartLocation);

				if (PC)
				{
					// We reverted position to that at the start of the pending move (above), however some code paths expect rotation to be set correctly
					// before character movement occurs (via FaceRotation), so try that now. The bOrientRotationToMovement path happens later as part of PerformMovement() and PhysicsRotation().
					GetPhysKartOwner()->FaceRotation(PC->GetControlRotation(), NewMove->DeltaTime);
				}

				//SaveBaseLocation();
				NewMove->SetInitialPosition(GetPhysKartOwner());

				// Remove pending move from move list. It would have to be the last move on the list.
				if (ClientData->SavedMoves.Num() > 0 && ClientData->SavedMoves.Last() == ClientData->PendingMove)
				{
					const bool bAllowShrinking = false;
					ClientData->SavedMoves.Pop(bAllowShrinking);
				}
				ClientData->FreeMove(ClientData->PendingMove);
				ClientData->PendingMove = nullptr;
				PendingMove = nullptr; // Avoid dangling reference, it's deleted above.
			}
			else
			{
				UE_LOG(LogNetPlayerMovement, Verbose, TEXT("Not combining move [would collide at start location]"));
			}
		}
		else
		{
			UE_LOG(LogNetPlayerMovement, Verbose, TEXT("Not combining move [not allowed by CanCombineWith()]"));
		}
	}

	// Acceleration should match what we send to the server, plus any other restrictions the server also enforces (see MoveAutonomous).
	Acceleration = NewMove->Acceleration.GetClampedToMaxSize(GetMaxAcceleration());
	AnalogInputModifier = ComputeAnalogInputModifier(); // recompute since acceleration may have changed.

	// Perform the move locally
	PerformMovement(NewMove->DeltaTime);

	PostPhysMovePtr = NewMovePtr;
	PostPhysOldMove = OldMove;

	//ClearAccumulatedForces();

	if (UpdatedComponent->IsSimulatingPhysics())
	{
		return;
	}

	// Maybe Move to PostPhysics as PhysX performs actual movement on FPhysXVehicleManager::Update

	NewMove->PostUpdate(GetPhysKartOwner(), FSavedMove_PhysKart::PostUpdate_Record);

	// Add NewMove to the list
	if (GetPhysKartOwner()->IsReplicatingMovement())
	{
		check(NewMove == NewMovePtr.Get());
		ClientData->SavedMoves.Push(NewMovePtr);

		const bool bCanDelayMove = (PhysKartMovementCVars::NetEnableMoveCombining != 0) && CanDelaySendingMove(NewMovePtr);

		if (bCanDelayMove && ClientData->PendingMove.IsValid() == false)
		{
			// Decide whether to hold off on move
			const float NetMoveDelta = FMath::Clamp(GetClientNetSendDeltaTime(PC, ClientData, NewMovePtr), 1.f / 120.f, 1.f / 5.f);

			if ((MyWorld->TimeSeconds - ClientData->ClientUpdateTime) * MyWorld->GetWorldSettings()->GetEffectiveTimeDilation() < NetMoveDelta)
			{
				// Delay sending this move.
				ClientData->PendingMove = NewMovePtr;
				return;
			}
		}

		ClientData->ClientUpdateTime = MyWorld->TimeSeconds;

		bool bSendServerMove = true;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		// Testing options: Simulated packet loss to server
		const float TimeSinceLossStart = (MyWorld->RealTimeSeconds - ClientData->DebugForcedPacketLossTimerStart);
		if (ClientData->DebugForcedPacketLossTimerStart > 0.f && (TimeSinceLossStart < PhysKartMovementCVars::NetForceClientServerMoveLossDuration))
		{
			bSendServerMove = false;
			UE_LOG(LogNetPlayerMovement, Log, TEXT("Drop ServerMove, %.2f time remains"), PhysKartMovementCVars::NetForceClientServerMoveLossDuration - TimeSinceLossStart);
		}
		else if (PhysKartMovementCVars::NetForceClientServerMoveLossPercent != 0.f && (RandomStream.FRand() < PhysKartMovementCVars::NetForceClientServerMoveLossPercent))
		{
			bSendServerMove = false;
			ClientData->DebugForcedPacketLossTimerStart = (PhysKartMovementCVars::NetForceClientServerMoveLossDuration > 0) ? MyWorld->RealTimeSeconds : 0.0f;
			UE_LOG(LogNetPlayerMovement, Log, TEXT("Drop ServerMove, %.2f time remains"), PhysKartMovementCVars::NetForceClientServerMoveLossDuration);
		}
		else
		{
			ClientData->DebugForcedPacketLossTimerStart = 0.f;
		}
#endif

		// Send move to server if this character is replicating movement
		if (bSendServerMove)
		{
			SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementCallServerMove);
			if (ShouldUsePackedMovementRPCs())
			{
				CallServerMovePacked(NewMove, ClientData->PendingMove.Get(), OldMove.Get());
			}
			else
			{
				CallServerMove(NewMove, OldMove.Get());
			}
		}
	}

	ClientData->PendingMove = NULL;
}

void UPhysKartMoveComp::CallServerMovePacked(const FSavedMove_PhysKart* NewMove, const FSavedMove_PhysKart* PendingMove, const FSavedMove_PhysKart* OldMove)
{
	// Get storage container we'll be using and fill it with movement data
	FPhysKartNetworkMoveDataContainer& MoveDataContainer = GetNetworkMoveDataContainer();
	MoveDataContainer.ClientFillNetworkMoveData(NewMove, PendingMove, OldMove);

	// Reset bit writer without affecting allocations
	FBitWriterMark BitWriterReset;
	BitWriterReset.Pop(ServerMoveBitWriter);

	// Extract the net package map used for serializing object references.
	APlayerController* PC = Cast<APlayerController>(GetPhysKartOwner()->GetController());
	UNetConnection* NetConnection = PC ? PC->GetNetConnection() : nullptr;
	ServerMoveBitWriter.PackageMap = NetConnection ? NetConnection->PackageMap : nullptr;
	if (ServerMoveBitWriter.PackageMap == nullptr)
	{
		UE_LOG(LogNetPlayerMovement, Error, TEXT("CallServerMovePacked: Failed to find a NetConnection/PackageMap for data serialization!"));
		return;
	}

	// Serialize move struct into a bit stream
	if (!MoveDataContainer.Serialize(*this, ServerMoveBitWriter, ServerMoveBitWriter.PackageMap) || ServerMoveBitWriter.IsError())
	{
		UE_LOG(LogNetPlayerMovement, Error, TEXT("CallServerMovePacked: Failed to serialize out movement data!"));
		return;
	}

	// Copy bits to our struct that we can NetSerialize to the server.
	// 'static' to avoid reallocation each invocation
	static FPhysKartServerMovePackedBits PackedBits;
	PackedBits.DataBits.SetNumUninitialized(ServerMoveBitWriter.GetNumBits());
	
	check(PackedBits.DataBits.Num() >= ServerMoveBitWriter.GetNumBits());
	FMemory::Memcpy(PackedBits.DataBits.GetData(), ServerMoveBitWriter.GetData(), ServerMoveBitWriter.GetNumBytes());

	// Send bits to server!
	ServerMovePacked_ClientSend(PackedBits);

	MarkForClientCameraUpdate();
}

void UPhysKartMoveComp::ReplicateMoveToServerPostPhysics(float DeltaTime, const FVector& NewAcceleration)
{
	//return;

	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementReplicateMoveToServerPostPhysics);
	check(GetPhysKartOwner() != NULL);

	if (!UpdatedComponent->IsSimulatingPhysics())
	{
		return;
	}

	// Can only start sending moves if our controllers are synced up over the network, otherwise we flood the reliable buffer.
	APlayerController* PC = Cast<APlayerController>(GetPhysKartOwner()->GetController());
	if (PC && PC->AcknowledgedPawn != GetPhysKartOwner())
	{
		return;
	}

	// Bail out if our character's controller doesn't have a Player. This may be the case when the local player
	// has switched to another controller, such as a debug camera controller.
	if (PC && PC->Player == nullptr)
	{
		return;
	}

	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	if (!ClientData)
	{
		return;
	}

	const UWorld* MyWorld = GetWorld();
	if (!MyWorld)
	{
		return;
	}

	//

	FPhysKartSavedMovePtr NewMovePtr = PostPhysMovePtr;
	FSavedMove_PhysKart* const NewMove = NewMovePtr.Get();
	FPhysKartSavedMovePtr OldMove = PostPhysOldMove;

	//
	// Maybe Move to PostPhysics as PhysX performs actual movement on FPhysXVehicleManager::Update

	NewMove->PostUpdate(GetPhysKartOwner(), FSavedMove_PhysKart::PostUpdate_Record);

	// Add NewMove to the list
	if (GetPhysKartOwner()->IsReplicatingMovement())
	{
		check(NewMove == NewMovePtr.Get());
		ClientData->SavedMoves.Push(NewMovePtr);

		const bool bCanDelayMove = (PhysKartMovementCVars::NetEnableMoveCombining != 0) && CanDelaySendingMove(NewMovePtr);

		if (bCanDelayMove && ClientData->PendingMove.IsValid() == false)
		{
			// Decide whether to hold off on move
			const float NetMoveDelta = FMath::Clamp(GetClientNetSendDeltaTime(PC, ClientData, NewMovePtr), 1.f / 120.f, 1.f / 5.f);

			if ((MyWorld->TimeSeconds - ClientData->ClientUpdateTime) * MyWorld->GetWorldSettings()->GetEffectiveTimeDilation() < NetMoveDelta)
			{
				// Delay sending this move.
				ClientData->PendingMove = NewMovePtr;
				return;
			}
		}

		ClientData->ClientUpdateTime = MyWorld->TimeSeconds;

		bool bSendServerMove = true;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		// Testing options: Simulated packet loss to server
		const float TimeSinceLossStart = (MyWorld->RealTimeSeconds - ClientData->DebugForcedPacketLossTimerStart);
		if (ClientData->DebugForcedPacketLossTimerStart > 0.f && (TimeSinceLossStart < PhysKartMovementCVars::NetForceClientServerMoveLossDuration))
		{
			bSendServerMove = false;
			UE_LOG(LogNetPlayerMovement, Log, TEXT("Drop ServerMove, %.2f time remains"), PhysKartMovementCVars::NetForceClientServerMoveLossDuration - TimeSinceLossStart);
		}
		else if (PhysKartMovementCVars::NetForceClientServerMoveLossPercent != 0.f && (RandomStream.FRand() < PhysKartMovementCVars::NetForceClientServerMoveLossPercent))
		{
			bSendServerMove = false;
			ClientData->DebugForcedPacketLossTimerStart = (PhysKartMovementCVars::NetForceClientServerMoveLossDuration > 0) ? MyWorld->RealTimeSeconds : 0.0f;
			UE_LOG(LogNetPlayerMovement, Log, TEXT("Drop ServerMove, %.2f time remains"), PhysKartMovementCVars::NetForceClientServerMoveLossDuration);
		}
		else
		{
			ClientData->DebugForcedPacketLossTimerStart = 0.f;
		}
#endif

		// Send move to server if this character is replicating movement
		if (bSendServerMove)
		{
			SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementCallServerMove);
			if (ShouldUsePackedMovementRPCs())
			{
				CallServerMovePacked(NewMove, ClientData->PendingMove.Get(), OldMove.Get());
			}
			else
			{
				CallServerMove(NewMove, OldMove.Get());
			}
		}
	}

	ClientData->PendingMove = NULL;
}

//8183
void UPhysKartMoveComp::CallServerMove
(
	const class FSavedMove_PhysKart* NewMove,
	const class FSavedMove_PhysKart* OldMove
)
{
	check(NewMove != nullptr);

	// Compress rotation down to 5 bytes
	uint32 ClientYawPitchINT = 0;
	uint8 ClientRollBYTE = 0;
	NewMove->GetPackedAngles(ClientYawPitchINT, ClientRollBYTE);

	// Determine if we send absolute or relative location
	const FVector SendLocation = NewMove->SavedLocation;

	FVector_NetQuantize100 QuatAxis = NewMove->SavedRotation.Quaternion().GetRotationAxis();
	uint8 QuatAngle = NewMove->SavedRotation.Quaternion().GetAngle();


	// send old move if it exists
	if (OldMove)
	{
		ServerMoveOld(OldMove->TimeStamp, ReplicatedState, OldMove->Acceleration, OldMove->GetCompressedFlags());
	}

	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	if (const FSavedMove_PhysKart* const PendingMove = ClientData->PendingMove.Get())
	{
		uint32 OldClientYawPitchINT = 0;
		uint8 OldClientRollBYTE = 0;
		ClientData->PendingMove->GetPackedAngles(OldClientYawPitchINT, OldClientRollBYTE);

		// send two moves simultaneously
		ServerMoveDual(
			PendingMove->TimeStamp,
			PendingMove->ReplicatedInputState,
			PendingMove->Acceleration,
			PendingMove->GetCompressedFlags(),
			OldClientYawPitchINT,
			NewMove->TimeStamp,
			NewMove->ReplicatedInputState,
			NewMove->Acceleration,
			SendLocation,
			NewMove->GetCompressedFlags(),
			ClientRollBYTE,
			ClientYawPitchINT,
			NewMove->EndPackedMovementMode
		);
	}
	else
	{
		ServerMove(
			NewMove->TimeStamp,
			NewMove->ReplicatedInputState,
			NewMove->Acceleration,
			SendLocation,
			NewMove->GetCompressedFlags(),
			ClientRollBYTE,
			ClientYawPitchINT,
			NewMove->EndPackedMovementMode,
			QuatAxis,
			QuatAngle
		);
	}

	MarkForClientCameraUpdate();
}

//8274
void UPhysKartMoveComp::ServerMoveOld_Implementation
(
	float OldTimeStamp,
	FReplicatedPhysKartState InReplicatedState,
	FVector_NetQuantize10 OldAccel,
	uint8 OldMoveFlags
)
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementServerMove);
	CSV_SCOPED_TIMING_STAT(PhysKartMovement, PhysKartMovementServerMove);

	if (!HasValidData() || !IsActive())
	{
		return;
	}

	FNetworkPredictionData_Server_PhysKart* ServerData = GetPredictionData_Server_PhysKart();
	check(ServerData);

	if (!VerifyClientTimeStamp(OldTimeStamp, *ServerData))
	{
		UE_LOG(LogNetPlayerMovement, VeryVerbose, TEXT("ServerMoveOld: TimeStamp expired. %f, CurrentTimeStamp: %f, Character: %s"), OldTimeStamp, ServerData->CurrentClientTimeStamp, *GetNameSafe(GetPhysKartOwner()));
		return;
	}

	UE_LOG(LogNetPlayerMovement, Verbose, TEXT("Recovered move from OldTimeStamp %f, DeltaTime: %f"), OldTimeStamp, OldTimeStamp - ServerData->CurrentClientTimeStamp);

	const UWorld* MyWorld = GetWorld();
	const float DeltaTime = ServerData->GetServerMoveDeltaTime(OldTimeStamp, GetPhysKartOwner()->GetActorTimeDilation(*MyWorld));
	if (DeltaTime > 0.f)
	{
		ServerData->CurrentClientTimeStamp = OldTimeStamp;
		ServerData->ServerAccumulatedClientTimeStamp += DeltaTime;
		ServerData->ServerTimeStamp = MyWorld->GetTimeSeconds();
		ServerData->ServerTimeStampLastServerMove = ServerData->ServerTimeStamp;

		MoveAutonomous(OldTimeStamp, DeltaTime, InReplicatedState, OldMoveFlags, OldAccel);
	}
	else
	{
		UE_LOG(LogNetPlayerMovement, Warning, TEXT("OldTimeStamp(%f) results in zero or negative actual DeltaTime(%f). Theoretical DeltaTime(%f)"),
			OldTimeStamp, DeltaTime, OldTimeStamp - ServerData->CurrentClientTimeStamp);
	}
}

//8319
void UPhysKartMoveComp::ServerMoveDual_Implementation(
	float TimeStamp0,
	FReplicatedPhysKartState InReplicatedState0,
	FVector_NetQuantize10 InAccel0,
	uint8 PendingFlags,
	uint32 View0,
	float TimeStamp,
	FReplicatedPhysKartState InReplicatedState,
	FVector_NetQuantize10 InAccel,
	FVector_NetQuantize100 ClientLoc,
	uint8 NewFlags,
	uint8 ClientRoll,
	uint32 View,
	uint8 ClientMovementMode)
{
	// Optional scoped movement update to combine moves for cheaper performance on the server.
	FScopedMovementUpdate ScopedMovementUpdate(UpdatedComponent, bEnableServerDualMoveScopedMovementUpdates ? EScopedUpdate::DeferredUpdates : EScopedUpdate::ImmediateUpdates);

	ServerMove_Implementation(TimeStamp0, InReplicatedState0, InAccel0, FVector(1.f, 2.f, 3.f), PendingFlags, ClientRoll, View0, ClientMovementMode);
	ServerMove_Implementation(TimeStamp, InReplicatedState, InAccel, ClientLoc, NewFlags, ClientRoll, View, ClientMovementMode);
}

//8364
bool UPhysKartMoveComp::VerifyClientTimeStamp(float TimeStamp, FNetworkPredictionData_Server_PhysKart& ServerData)
{
	bool bTimeStampResetDetected = false;
	bool bNeedsForcedUpdate = false;
	const bool bIsValid = IsClientTimeStampValid(TimeStamp, ServerData, bTimeStampResetDetected);
	if (bIsValid)
	{
		if (bTimeStampResetDetected)
		{
			UE_LOG(LogNetPlayerMovement, Log, TEXT("TimeStamp reset detected. CurrentTimeStamp: %f, new TimeStamp: %f"), ServerData.CurrentClientTimeStamp, TimeStamp);
			LastTimeStampResetServerTime = GetWorld()->GetTimeSeconds();
			OnClientTimeStampResetDetected();
			ServerData.CurrentClientTimeStamp -= MinTimeBetweenTimeStampResets;
		}
		else
		{
			UE_LOG(LogNetPlayerMovement, VeryVerbose, TEXT("TimeStamp %f Accepted! CurrentTimeStamp: %f"), TimeStamp, ServerData.CurrentClientTimeStamp);
			ProcessClientTimeStampForTimeDiscrepancy(TimeStamp, ServerData);
		}		
	}
	else
	{
		if (bTimeStampResetDetected)
		{
			UE_LOG(LogNetPlayerMovement, Log, TEXT("TimeStamp expired. Before TimeStamp Reset. CurrentTimeStamp: %f, TimeStamp: %f"), ServerData.CurrentClientTimeStamp, TimeStamp);
		}
		else
		{
			bNeedsForcedUpdate = (TimeStamp <= ServerData.LastReceivedClientTimeStamp);
		}
	}
	ServerData.LastReceivedClientTimeStamp = TimeStamp;
	ServerData.bLastRequestNeedsForcedUpdates = bNeedsForcedUpdate;
	return bIsValid;
}

//8397
void UPhysKartMoveComp::ProcessClientTimeStampForTimeDiscrepancy(float ClientTimeStamp, FNetworkPredictionData_Server_PhysKart& ServerData)
{
	// Should only be called on server in network games
	check(GetPhysKartOwner() != NULL);
	check(GetPhysKartOwner()->GetLocalRole() == ROLE_Authority);
	checkSlow(GetNetMode() < NM_Client);

	// Movement time discrepancy detection and resolution (potentially caused by client speed hacks, time manipulation)
	// Track client reported time deltas through ServerMove RPCs vs actual server time, when error accumulates enough
	// trigger prevention measures where client must "pay back" the time difference
	const bool bServerMoveHasOccurred = ServerData.ServerTimeStampLastServerMove != 0.f;
	const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
	if (GameNetworkManager != nullptr && GameNetworkManager->bMovementTimeDiscrepancyDetection && bServerMoveHasOccurred)
	{
		//const float 
			WorldTimeSeconds = GetWorld()->GetTimeSeconds();
		const float ServerDelta = (WorldTimeSeconds - ServerData.ServerTimeStamp) * GetPhysKartOwner()->CustomTimeDilation;
		const float ClientDelta = ClientTimeStamp - ServerData.CurrentClientTimeStamp;
		const float ClientError = ClientDelta - ServerDelta; // Difference between how much time client has ticked since last move vs server

		// Accumulate raw total discrepancy, unfiltered/unbound (for tracking more long-term trends over the lifetime of the CharacterMovementComponent)
		ServerData.LifetimeRawTimeDiscrepancy += ClientError;

		//
		// 1. Determine total effective discrepancy 
		//
		// NewTimeDiscrepancy is bounded and has a DriftAllowance to limit momentary burst packet loss or 
		// low framerate from having significant impacts, which could cause needing multiple seconds worth of 
		// slow-down/speed-up even though it wasn't intentional time manipulation
		float NewTimeDiscrepancy = ServerData.TimeDiscrepancy + ClientError;
		{
			// Apply drift allowance - forgiving percent difference per time for error
			const float DriftAllowance = GameNetworkManager->MovementTimeDiscrepancyDriftAllowance;
			if (DriftAllowance > 0.f)
			{
				if (NewTimeDiscrepancy > 0.f)
				{
					NewTimeDiscrepancy = FMath::Max(NewTimeDiscrepancy - ServerDelta * DriftAllowance, 0.f);
				}
				else
				{
					NewTimeDiscrepancy = FMath::Min(NewTimeDiscrepancy + ServerDelta * DriftAllowance, 0.f);
				}
			}

			// Enforce bounds
			// Never go below MinTimeMargin - ClientError being negative means the client is BEHIND
			// the server (they are going slower).
			NewTimeDiscrepancy = FMath::Max(NewTimeDiscrepancy, GameNetworkManager->MovementTimeDiscrepancyMinTimeMargin);
		}

		// Determine EffectiveClientError, which is error for the currently-being-processed move after 
		// drift allowances/clamping/resolution mode modifications.
		// We need to know how much the current move contributed towards actionable error so that we don't
		// count error that the server never allowed to impact movement to matter
		float EffectiveClientError = ClientError;
		{
			const float NewTimeDiscrepancyRaw = ServerData.TimeDiscrepancy + ClientError;
			if (NewTimeDiscrepancyRaw != 0.f)
			{
				EffectiveClientError = ClientError * (NewTimeDiscrepancy / NewTimeDiscrepancyRaw);
			}
		}

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		// Per-frame spew of time discrepancy-related values - useful for investigating state of time discrepancy tracking
		if (PhysKartMovementCVars::DebugTimeDiscrepancy > 0)
		{
			UE_LOG(LogNetPlayerMovement, Warning, TEXT("TimeDiscrepancyDetection: ClientError: %f, TimeDiscrepancy: %f, LifetimeRawTimeDiscrepancy: %f (Lifetime %f), Resolving: %d, ClientDelta: %f, ServerDelta: %f, ClientTimeStamp: %f"),
				ClientError, ServerData.TimeDiscrepancy, ServerData.LifetimeRawTimeDiscrepancy, WorldTimeSeconds - ServerData.WorldCreationTime, ServerData.bResolvingTimeDiscrepancy, ClientDelta, ServerDelta, ClientTimeStamp);
		}
#endif // !(UE_BUILD_SHIPPING || UE_BUILD_TEST)

		//
		// 2. If we were in resolution mode, determine if we still need to be
		//
		ServerData.bResolvingTimeDiscrepancy = ServerData.bResolvingTimeDiscrepancy && (ServerData.TimeDiscrepancy > 0.f);

		//
		// 3. Determine if NewTimeDiscrepancy is significant enough to trigger detection, and if so, trigger resolution if enabled
		//
		if (!ServerData.bResolvingTimeDiscrepancy)
		{
			if (NewTimeDiscrepancy > GameNetworkManager->MovementTimeDiscrepancyMaxTimeMargin)
			{
				// Time discrepancy detected - client timestamp ahead of where the server thinks it should be!

				// Trigger logic for resolving time discrepancies
				if (GameNetworkManager->bMovementTimeDiscrepancyResolution)
				{
					// Trigger Resolution
					ServerData.bResolvingTimeDiscrepancy = true;

					// Transfer calculated error to official TimeDiscrepancy value, which is the time that will be resolved down
					// in this and subsequent moves until it reaches 0 (meaning we equalize the error)
					// Don't include contribution to error for this move, since we are now going to be in resolution mode
					// and the expected client error (though it did help trigger resolution) won't be allowed
					// to increase error this frame
					ServerData.TimeDiscrepancy = (NewTimeDiscrepancy - EffectiveClientError);
				}
				else
				{
					// We're detecting discrepancy but not handling resolving that through movement component.
					// Clear time stamp error accumulated that triggered detection so we start fresh (maybe it was triggered
					// during severe hitches/packet loss/other non-goodness)
					ServerData.TimeDiscrepancy = 0.f;
				}

				// Project-specific resolution (reporting/recording/analytics)
				OnTimeDiscrepancyDetected(NewTimeDiscrepancy, ServerData.LifetimeRawTimeDiscrepancy, WorldTimeSeconds - ServerData.WorldCreationTime, ClientError);
			}
			else
			{
				// When not in resolution mode and still within error tolerances, accrue total discrepancy
				ServerData.TimeDiscrepancy = NewTimeDiscrepancy;
			}
		}

		//
		// 4. If we are actively resolving time discrepancy, we do so by altering the DeltaTime for the current ServerMove
		//
		if (ServerData.bResolvingTimeDiscrepancy)
		{
			// Optionally force client corrections during time discrepancy resolution
			// This is useful when default project movement error checking is lenient or ClientAuthorativePosition is enabled
			// to ensure time discrepancy resolution is enforced
			if (GameNetworkManager->bMovementTimeDiscrepancyForceCorrectionsDuringResolution)
			{
				ServerData.bForceClientUpdate = true;
			}

			// Movement time discrepancy resolution
			// When the server has detected a significant time difference between what the client ServerMove RPCs are reporting
			// and the actual time that has passed on the server (pointing to potential speed hacks/time manipulation by client),
			// we enter a resolution mode where the usual "base delta's off of client's reported timestamps" is clamped down
			// to the server delta since last movement update, so that during resolution we're not allowing further advantage.
			// Out of that ServerDelta-based move delta, we also need the client to "pay back" the time stolen from initial 
			// time discrepancy detection (held in TimeDiscrepancy) at a specified rate (AGameNetworkManager::TimeDiscrepancyResolutionRate) 
			// to equalize movement time passed on client and server before we can consider the discrepancy "resolved"
			const float ServerCurrentTimeStamp = WorldTimeSeconds;
			const float ServerDeltaSinceLastMovementUpdate = (ServerCurrentTimeStamp - ServerData.ServerTimeStamp) * GetPhysKartOwner()->CustomTimeDilation;
			const bool bIsFirstServerMoveThisServerTick = ServerDeltaSinceLastMovementUpdate > 0.f;

			// Restrict ServerMoves to server deltas during time discrepancy resolution 
			// (basing moves off of trusted server time, not client timestamp deltas)
			const float BaseDeltaTime = ServerData.GetBaseServerMoveDeltaTime(ClientTimeStamp, GetPhysKartOwner()->GetActorTimeDilation());

			if (!bIsFirstServerMoveThisServerTick)
			{
				// Accumulate client deltas for multiple ServerMoves per server tick so that the next server tick
				// can pay back the full amount of that tick and not be bounded by a single small Move delta
				ServerData.TimeDiscrepancyAccumulatedClientDeltasSinceLastServerTick += BaseDeltaTime;
			}

			float ServerBoundDeltaTime = FMath::Min(BaseDeltaTime + ServerData.TimeDiscrepancyAccumulatedClientDeltasSinceLastServerTick, ServerDeltaSinceLastMovementUpdate);
			ServerBoundDeltaTime = FMath::Max(ServerBoundDeltaTime, 0.f); // No negative deltas allowed

			if (bIsFirstServerMoveThisServerTick)
			{
				// The first ServerMove for a server tick has used the accumulated client delta in the ServerBoundDeltaTime
				// calculation above, clear it out for next frame where we have multiple ServerMoves
				ServerData.TimeDiscrepancyAccumulatedClientDeltasSinceLastServerTick = 0.f;
			}

			// Calculate current move DeltaTime and PayBack time based on resolution rate
			const float ResolutionRate = FMath::Clamp(GameNetworkManager->MovementTimeDiscrepancyResolutionRate, 0.f, 1.f);
			float TimeToPayBack = FMath::Min(ServerBoundDeltaTime * ResolutionRate, ServerData.TimeDiscrepancy); // Make sure we only pay back the time we need to
			float DeltaTimeAfterPayback = ServerBoundDeltaTime - TimeToPayBack;

			// Adjust deltas so current move DeltaTime adheres to minimum tick time
			DeltaTimeAfterPayback = FMath::Max(DeltaTimeAfterPayback, UPhysKartMoveComp::MIN_TICK_TIME);
			TimeToPayBack = ServerBoundDeltaTime - DeltaTimeAfterPayback;

			// Output of resolution: an overridden delta time that will be picked up for this ServerMove, and removing the time
			// we paid back by overriding the DeltaTime to TimeDiscrepancy (time needing resolved)
			ServerData.TimeDiscrepancyResolutionMoveDeltaOverride = DeltaTimeAfterPayback;
			ServerData.TimeDiscrepancy -= TimeToPayBack;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
			// Per-frame spew of time discrepancy resolution related values - useful for investigating state of time discrepancy tracking
			if (PhysKartMovementCVars::DebugTimeDiscrepancy > 1)
			{
				UE_LOG(LogNetPlayerMovement, Warning, TEXT("TimeDiscrepancyResolution: DeltaOverride: %f, TimeToPayBack: %f, BaseDelta: %f, ServerDeltaSinceLastMovementUpdate: %f, TimeDiscrepancyAccumulatedClientDeltasSinceLastServerTick: %f"),
					ServerData.TimeDiscrepancyResolutionMoveDeltaOverride, TimeToPayBack, BaseDeltaTime, ServerDeltaSinceLastMovementUpdate, ServerData.TimeDiscrepancyAccumulatedClientDeltasSinceLastServerTick);
			}
#endif // !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		}
	}
}

//8586
bool UPhysKartMoveComp::IsClientTimeStampValid(float TimeStamp, const FNetworkPredictionData_Server_PhysKart& ServerData, bool& bTimeStampResetDetected) const
{
	if (TimeStamp <= 0.f || !FMath::IsFinite(TimeStamp))
	{
		return false;
	}

	// Very large deltas happen around a TimeStamp reset.
	const float DeltaTimeStamp = (TimeStamp - ServerData.CurrentClientTimeStamp);
	if (FMath::Abs(DeltaTimeStamp) > (MinTimeBetweenTimeStampResets * 0.5f))
	{
		// Client is resetting TimeStamp to increase accuracy.
		bTimeStampResetDetected = true;
		if (DeltaTimeStamp < 0.f)
		{
			// Validate that elapsed time since last reset is reasonable, otherwise client could be manipulating resets.
			if (GetWorld()->TimeSince(LastTimeStampResetServerTime) < (MinTimeBetweenTimeStampResets * 0.5f))
			{
				// Reset too recently
				return false;
			}
			else
			{
				// TimeStamp accepted with reset
				return true;
			}
		}
		else
		{
			// We already reset the TimeStamp, but we just got an old outdated move before the switch, not valid.
			return false;
		}
	}

	// If TimeStamp is in the past, move is outdated, not valid.
	if (TimeStamp <= ServerData.CurrentClientTimeStamp)
	{
		return false;
	}

	// Precision issues (or reordered timestamps from old moves) can cause very small or zero deltas which cause problems.
	if (DeltaTimeStamp < UPhysKartMoveComp::MIN_TICK_TIME)
	{
		return false;
	}

	// TimeStamp valid.
	return true;
}

void UPhysKartMoveComp::OnClientTimeStampResetDetected()
{
}

//8640
void UPhysKartMoveComp::OnTimeDiscrepancyDetected(float CurrentTimeDiscrepancy, float LifetimeRawTimeDiscrepancy, float Lifetime, float CurrentMoveError)
{
	UE_LOG(LogNetPlayerMovement, Verbose, TEXT("Movement Time Discrepancy detected between client-reported time and server on character %s. CurrentTimeDiscrepancy: %f, LifetimeRawTimeDiscrepancy: %f, Lifetime: %f, CurrentMoveError %f"),
		GetPhysKartOwner() ? *GetPhysKartOwner()->GetHumanReadableName() : TEXT("<UNKNOWN>"),
		CurrentTimeDiscrepancy,
		LifetimeRawTimeDiscrepancy,
		Lifetime,
		CurrentMoveError);
}

bool FPhysKartNetworkSerializationPackedBits::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	bool bLocalSuccess = true;
	SavedPackageMap = Map;

	// Array size in bits, using minimal number of bytes to write it out.
	uint32 NumBits = DataBits.Num();
	Ar.SerializeIntPacked(NumBits);

	if (!ensure(NumBits <= (uint32)PhysKartMovementCVars::NetPackedMovementMaxBits))
	{
		// Protect against bad data that could cause server to allocate way too much memory.
		devCode(UE_LOG(LogNetPlayerMovement, Error, TEXT("FPhysKartNetworkSerializationPackedBits::NetSerialize: NumBits (%d) exceeds allowable limit!"), NumBits));
		return false;
	}

	if (Ar.IsLoading())
	{
		DataBits.Init(0, NumBits);
	}

	// Array data
	Ar.SerializeBits(DataBits.GetData(), NumBits);

	bOutSuccess = bLocalSuccess;
	return !Ar.IsError();
}

void FPhysKartNetworkMoveDataContainer::ClientFillNetworkMoveData(const FSavedMove_PhysKart* ClientNewMove, const FSavedMove_PhysKart* ClientPendingMove, const FSavedMove_PhysKart* ClientOldMove)
{
	bDisableCombinedScopedMove = false;

	if (ensure(ClientNewMove))
	{
		NewMoveData->ClientFillNetworkMoveData(*ClientNewMove, FPhysKartNetworkMoveData::ENetworkMoveType::NewMove);
		bDisableCombinedScopedMove |= ClientNewMove->bForceNoCombine;
	}

	bHasPendingMove = (ClientPendingMove != nullptr);
	if (bHasPendingMove)
	{
		PendingMoveData->ClientFillNetworkMoveData(*ClientPendingMove, FPhysKartNetworkMoveData::ENetworkMoveType::PendingMove);
		bDisableCombinedScopedMove |= ClientPendingMove->bForceNoCombine;
	}

	bHasOldMove = (ClientOldMove != nullptr);
	if (bHasOldMove)
	{
		OldMoveData->ClientFillNetworkMoveData(*ClientOldMove, FPhysKartNetworkMoveData::ENetworkMoveType::OldMove);
	}
}


bool FPhysKartNetworkMoveDataContainer::Serialize(UPhysKartMoveComp& CharacterMovement, FArchive& Ar, UPackageMap* PackageMap)
{
	// We must have data storage initialized. If not, then the storage container wasn't properly initialized.
	check(NewMoveData && PendingMoveData && OldMoveData);

	// Base move always serialized.
	if (!NewMoveData->Serialize(CharacterMovement, Ar, PackageMap, FPhysKartNetworkMoveData::ENetworkMoveType::NewMove))
	{
		return false;
	}

	// Optional pending dual move
	Ar.SerializeBits(&bHasPendingMove, 1);
	if (bHasPendingMove)
	{
		//Ar.SerializeBits(&bIsDualHybridRootMotionMove, 1);
		if (!PendingMoveData->Serialize(CharacterMovement, Ar, PackageMap, FPhysKartNetworkMoveData::ENetworkMoveType::PendingMove))
		{
			return false;
		}
	}

	// Optional old move
	Ar.SerializeBits(&bHasOldMove, 1);
	if (bHasOldMove)
	{
		if (!OldMoveData->Serialize(CharacterMovement, Ar, PackageMap, FPhysKartNetworkMoveData::ENetworkMoveType::OldMove))
		{
			return false;
		}
	}

	Ar.SerializeBits(&bDisableCombinedScopedMove, 1);

	return !Ar.IsError();
}


void FPhysKartNetworkMoveData::ClientFillNetworkMoveData(const FSavedMove_PhysKart& ClientMove, FPhysKartNetworkMoveData::ENetworkMoveType MoveType)
{
	NetworkMoveType = MoveType;

	TimeStamp = ClientMove.TimeStamp;
	Acceleration = ClientMove.Acceleration;
	//AngAcceleration = ClientMove.AngAcceleration;
	ClientQuatAxis = ClientMove.SavedRotation.Quaternion().GetRotationAxis();
	ClientQuatAngle = ClientMove.SavedRotation.Quaternion().GetAngle();
	ReplicatedInputState = ClientMove.ReplicatedInputState;
	ControlRotation = ClientMove.SavedControlRotation;
	CompressedMoveFlags = ClientMove.GetCompressedFlags();
	MovementMode = ClientMove.MovementMode;

	Location = ClientMove.SavedLocation;
	MovementBase = nullptr;
	MovementBaseBoneName = NAME_None;
}


bool FPhysKartNetworkMoveData::Serialize(UPhysKartMoveComp& CharacterMovement, FArchive& Ar, UPackageMap* PackageMap, FPhysKartNetworkMoveData::ENetworkMoveType MoveType)
{
	NetworkMoveType = MoveType;

	bool bLocalSuccess = true;
	const bool bIsSaving = Ar.IsSaving();

	Ar << TimeStamp;

	// TODO: better packing with single bit per component indicating zero/non-zero
	Acceleration.NetSerialize(Ar, PackageMap, bLocalSuccess);
	
	AngAcceleration.NetSerialize(Ar, PackageMap, bLocalSuccess); // PaCorp * 4.26
	ClientQuatAxis.NetSerialize(Ar, PackageMap, bLocalSuccess); // PaCorp * 4.26
	Ar << ClientQuatAngle;

	Location.NetSerialize(Ar, PackageMap, bLocalSuccess);

	// ControlRotation : FRotator handles each component zero/non-zero test; it uses a single signal bit for zero/non-zero, and uses 16 bits per component if non-zero.
	ControlRotation.NetSerialize(Ar, PackageMap, bLocalSuccess);

	SerializeOptionalValue<uint8>(bIsSaving, Ar, CompressedMoveFlags, 0);

	if (MoveType == ENetworkMoveType::NewMove)
	{
		// Location, relative movement base, and ending movement mode is only used for error checking, so only save for the final move.
		SerializeOptionalValue<UPrimitiveComponent*>(bIsSaving, Ar, MovementBase, nullptr);
		SerializeOptionalValue<FName>(bIsSaving, Ar, MovementBaseBoneName, NAME_None);
		SerializeOptionalValue<uint8>(bIsSaving, Ar, MovementMode, MOVE_Walking);
	}

	return !Ar.IsError();
}

void UPhysKartMoveComp::ServerMovePacked_ClientSend(const FPhysKartServerMovePackedBits& PackedBits)
{
	// Pass through RPC call to character on server, there is less RPC bandwidth overhead when used on an Actor rather than a Component.
	GetPhysKartOwner()->ServerMovePacked(PackedBits);
}

void UPhysKartMoveComp::ServerMovePacked_ServerReceive(const FPhysKartServerMovePackedBits& PackedBits)
{
	if (!HasValidData() || !IsActive())
	{
		return;
	}

	const int32 NumBits = PackedBits.DataBits.Num();
	if (!ensure(NumBits <= PhysKartMovementCVars::NetPackedMovementMaxBits))
	{
		// Protect against bad data that could cause server to allocate way too much memory.
		devCode(UE_LOG(LogNetPlayerMovement, Error, TEXT("ServerMovePacked_ServerReceive: NumBits (%d) exceeds allowable limit!"), NumBits));
		return;
	}

	// Reuse bit reader to avoid allocating memory each time.
	ServerMoveBitReader.SetData((uint8*)PackedBits.DataBits.GetData(), NumBits);
	ServerMoveBitReader.PackageMap = PackedBits.GetPackageMap();

	// Deserialize bits to move data struct.
	// We had to wait until now and use the temp bit stream because the RPC doesn't know about the virtual overrides on the possibly custom struct that is our data container.
	FPhysKartNetworkMoveDataContainer& MoveDataContainer = GetNetworkMoveDataContainer();
	if (!MoveDataContainer.Serialize(*this, ServerMoveBitReader, ServerMoveBitReader.PackageMap) || ServerMoveBitReader.IsError())
	{
		devCode(UE_LOG(LogNetPlayerMovement, Error, TEXT("ServerMovePacked_ServerReceive: Failed to serialize movement data!")));
		return;
	}

	ServerMove_HandleMoveData(MoveDataContainer);
}

void UPhysKartMoveComp::ServerMove_HandleMoveData(const FPhysKartNetworkMoveDataContainer& MoveDataContainer)
{
	// Optional "old move"
	if (MoveDataContainer.bHasOldMove)
	{
		if (FPhysKartNetworkMoveData* OldMove = MoveDataContainer.GetOldMoveData())
		{
			SetCurrentNetworkMoveData(OldMove);
			ServerMove_PerformMovement(*OldMove);
		}
	}

	// Optional scoped movement update for dual moves to combine moves for cheaper performance on the server.
	const bool bMoveAllowsScopedDualMove = MoveDataContainer.bHasPendingMove && !MoveDataContainer.bDisableCombinedScopedMove;
	FScopedMovementUpdate ScopedMovementUpdate(UpdatedComponent, (bMoveAllowsScopedDualMove && bEnableServerDualMoveScopedMovementUpdates && bEnableScopedMovementUpdates) ? EScopedUpdate::DeferredUpdates : EScopedUpdate::ImmediateUpdates);

	// Optional pending move as part of "dual move"
	if (MoveDataContainer.bHasPendingMove)
	{
		if (FPhysKartNetworkMoveData* PendingMove = MoveDataContainer.GetPendingMoveData())
		{
			SetCurrentNetworkMoveData(PendingMove);
			ServerMove_PerformMovement(*PendingMove);
		}
	}

	// Final standard move
	if (FPhysKartNetworkMoveData* NewMove = MoveDataContainer.GetNewMoveData())
	{
		SetCurrentNetworkMoveData(NewMove);
		ServerMove_PerformMovement(*NewMove);
	}

	SetCurrentNetworkMoveData(nullptr);
}


void UPhysKartMoveComp::ServerMove_PerformMovement(const FPhysKartNetworkMoveData& MoveData)
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementServerMove);
	CSV_SCOPED_TIMING_STAT(PhysKartMovement, PhysKartMovementServerMove);

	if (!HasValidData() || !IsActive())
	{
		return;
	}

	const float ClientTimeStamp = MoveData.TimeStamp;
	FVector_NetQuantize10 ClientAccel = MoveData.Acceleration;
	const FReplicatedPhysKartState InReplicatedState = MoveData.ReplicatedInputState;
	const uint8 ClientMoveFlags = MoveData.CompressedMoveFlags;
	const FRotator ClientControlRotation = MoveData.ControlRotation;

	FNetworkPredictionData_Server_PhysKart* ServerData = GetPredictionData_Server_PhysKart();
	check(ServerData);

	FQuat ClientQuat = FQuat(MoveData.ClientQuatAxis, MoveData.ClientQuatAngle);

	if (!VerifyClientTimeStamp(ClientTimeStamp, *ServerData))
	{
		const float ServerTimeStamp = ServerData->CurrentClientTimeStamp;
		// This is more severe if the timestamp has a large discrepancy and hasn't been recently reset.
		if (ServerTimeStamp > 1.0f && FMath::Abs(ServerTimeStamp - ClientTimeStamp) > PhysKartMovementCVars::NetServerMoveTimestampExpiredWarningThreshold)
		{
			UE_LOG(LogNetPlayerMovement, Warning, TEXT("ServerMove: TimeStamp expired: %f, CurrentTimeStamp: %f, Character: %s"), ClientTimeStamp, ServerTimeStamp, *GetNameSafe(GetPhysKartOwner()));
		}
		else
		{
			UE_LOG(LogNetPlayerMovement, Log, TEXT("ServerMove: TimeStamp expired: %f, CurrentTimeStamp: %f, Character: %s"), ClientTimeStamp, ServerTimeStamp, *GetNameSafe(GetPhysKartOwner()));
		}
		return;
	}

	bool bServerReadyForClient = true;
	APlayerController* PC = Cast<APlayerController>(GetPhysKartOwner()->GetController());
	if (PC)
	{
		bServerReadyForClient = PC->NotifyServerReceivedClientData(GetPhysKartOwner(), ClientTimeStamp);
		if (!bServerReadyForClient)
		{
			ClientAccel = FVector::ZeroVector;
		}
	}

	const UWorld* MyWorld = GetWorld();
	const float DeltaTime = ServerData->GetServerMoveDeltaTime(ClientTimeStamp, GetPhysKartOwner()->GetActorTimeDilation(*MyWorld));

	if (DeltaTime > 0.f)
	{
		ServerData->CurrentClientTimeStamp = ClientTimeStamp;
		ServerData->ServerAccumulatedClientTimeStamp += DeltaTime;
		ServerData->ServerTimeStamp = MyWorld->GetTimeSeconds();
		ServerData->ServerTimeStampLastServerMove = ServerData->ServerTimeStamp;

		if (PC)
		{
			PC->SetControlRotation(ClientControlRotation);
		}

		if (!bServerReadyForClient)
		{
			return;
		}

		// Perform actual movement
		if ((MyWorld->GetWorldSettings()->GetPauserPlayerState() == NULL))
		{
			if (PC)
			{
				PC->UpdateRotation(DeltaTime);
			}

			MoveAutonomous(ClientTimeStamp, DeltaTime, InReplicatedState, ClientMoveFlags, ClientAccel);
		}
	}

	// Validate move only after old and first dual portion, after all moves are completed.
	if (MoveData.NetworkMoveType == FPhysKartNetworkMoveData::ENetworkMoveType::NewMove)
	{
		ServerMoveHandleClientError(ClientTimeStamp, DeltaTime, ClientAccel, MoveData.Location, ClientQuat, MoveData.MovementMode);
	}
}

//9117
void UPhysKartMoveComp::ServerMove(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis, float ClientQuatAngle)
{
	//UE_LOG(LogPhysKartMovement, Log, TEXT("ServerMoveNoBase"));
	//GetPhysKartOwner()->ServerMoveNoBase(TimeStamp, InAccel, ClientLoc, CompressedMoveFlags, ClientRoll, View, ClientMovementMode);
	GetPhysKartOwner()->ServerMove(TimeStamp, InReplicatedState, InAccel, ClientLoc, CompressedMoveFlags, ClientRoll, View, ClientMovementMode, ClientQuatAxis, ClientQuatAngle);
}

void UPhysKartMoveComp::ServerMove_Implementation(
	float TimeStamp,
	FReplicatedPhysKartState InReplicatedState,
	FVector_NetQuantize10 InAccel,
	FVector_NetQuantize100 ClientLoc,
	uint8 MoveFlags,
	uint8 ClientRoll,
	uint32 View,
	uint8 ClientMovementMode, 
	FVector_NetQuantize100 ClientQuatAxis, 
	float ClientQuatAngle)
{
	//SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementServerMove);
	//CSV_SCOPED_TIMING_STAT(CharacterMovement, CharacterMovementServerMove);

	if (!HasValidData() || !IsActive())
	{
		return;
	}

	FNetworkPredictionData_Server_PhysKart* ServerData = GetPredictionData_Server_PhysKart();
	check(ServerData);

	FQuat ClientQuat = FQuat(ClientQuatAxis, ClientQuatAngle);

	if (!VerifyClientTimeStamp(TimeStamp, *ServerData))
	{
		const float ServerTimeStamp = ServerData->CurrentClientTimeStamp;
		// This is more severe if the timestamp has a large discrepancy and hasn't been recently reset.
		if (ServerTimeStamp > 1.0f && FMath::Abs(ServerTimeStamp - TimeStamp) > PhysKartMovementCVars::NetServerMoveTimestampExpiredWarningThreshold)
		{
			UE_LOG(LogNetPlayerMovement, Warning, TEXT("ServerMove: TimeStamp expired: %f, CurrentTimeStamp: %f, Character: %s"), TimeStamp, ServerTimeStamp, *GetNameSafe(GetPhysKartOwner()));
		}
		else
		{
			UE_LOG(LogNetPlayerMovement, Log, TEXT("ServerMove: TimeStamp expired: %f, CurrentTimeStamp: %f, Character: %s"), TimeStamp, ServerTimeStamp, *GetNameSafe(GetPhysKartOwner()));
		}
		return;
	}

	bool bServerReadyForClient = true;
	APlayerController* PC = Cast<APlayerController>(GetPhysKartOwner()->GetController());
	if (PC)
	{
		bServerReadyForClient = PC->NotifyServerReceivedClientData(GetPhysKartOwner(), TimeStamp);
		if (!bServerReadyForClient)
		{
			InAccel = FVector::ZeroVector;
		}
	}

	// View components
	const uint16 ViewPitch = (View & 65535);
	const uint16 ViewYaw = (View >> 16);

	const FVector Accel = InAccel;

	const UWorld* MyWorld = GetWorld();
	const float DeltaTime = ServerData->GetServerMoveDeltaTime(TimeStamp, GetPhysKartOwner()->GetActorTimeDilation(*MyWorld));

	ServerData->CurrentClientTimeStamp = TimeStamp;
	ServerData->ServerAccumulatedClientTimeStamp += DeltaTime;
	ServerData->ServerTimeStamp = MyWorld->GetTimeSeconds();
	ServerData->ServerTimeStampLastServerMove = ServerData->ServerTimeStamp;
	FRotator ViewRot;
	ViewRot.Pitch = FRotator::DecompressAxisFromShort(ViewPitch);
	ViewRot.Yaw = FRotator::DecompressAxisFromShort(ViewYaw);
	ViewRot.Roll = FRotator::DecompressAxisFromByte(ClientRoll);

	if (PC)
	{
		PC->SetControlRotation(ViewRot);
	}

	if (!bServerReadyForClient)
	{
		return;
	}

	// Perform actual movement
	if ((MyWorld->GetWorldSettings()->GetPauserPlayerState() == NULL) && (DeltaTime > 0.f))
	{
		if (PC)
		{
			PC->UpdateRotation(DeltaTime);
		}

		MoveAutonomous(TimeStamp, DeltaTime, InReplicatedState, MoveFlags, Accel);
	}

	//UE_CLOG(GetPhysKartOwner() && UpdatedComponent, LogNetPlayerMovement, VeryVerbose, TEXT("ServerMove Time %f Acceleration %s Velocity %s Position %s Rotation %s DeltaTime %f Mode %s MovementBase %s.%s (Dynamic:%d)"),
	//		TimeStamp, *Accel.ToString(), *Velocity.ToString(), *UpdatedComponent->GetComponentLocation().ToString(), *UpdatedComponent->GetComponentRotation().ToCompactString(), DeltaTime, *GetMovementName(),
	//		*GetNameSafe(GetMovementBase()), *GetPhysKartOwner()->GetBasedMovement().BoneName.ToString(), MovementBaseUtility::IsDynamicBase(GetMovementBase()) ? 1 : 0);

	ServerMoveHandleClientError(TimeStamp, DeltaTime, Accel, ClientLoc, ClientQuat, ClientMovementMode);
}

//8759
void UPhysKartMoveComp::ServerMoveHandleClientError(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& RelativeClientLoc, const FQuat& RelativeClientRotation, uint8 ClientMovementMode)
{
	if (!ShouldUsePackedMovementRPCs())
	{
		if (RelativeClientLoc == FVector(1.f, 2.f, 3.f)) // first part of double servermove
		{
			return;
		}
	}

	FNetworkPredictionData_Server_PhysKart* ServerData = GetPredictionData_Server_PhysKart();
	check(ServerData);

	// Don't prevent more recent updates from being sent if received this frame.
	// We're going to send out an update anyway, might as well be the most recent one.
	APlayerController* PC = Cast<APlayerController>(GetPhysKartOwner()->GetController());
	if ((ServerData->LastUpdateTime != GetWorld()->TimeSeconds))
	{
		const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
		if (GameNetworkManager->WithinUpdateDelayBounds(PC, ServerData->LastUpdateTime))
		{
			return;
		}
	}

	// Offset may be relative to base component
	FVector ClientLoc = RelativeClientLoc;
	FQuat ClientQuat = RelativeClientRotation;

	// Compute the client error from the server's position
	// If client has accumulated a noticeable positional error, correct them.
	bNetworkLargeClientCorrection = ServerData->bForceClientUpdate;
	//if (ServerData->bForceClientUpdate || ServerCheckClientError(ClientTimeStamp, DeltaTime, Accel, ClientLoc, RelativeClientLoc, ClientMovementMode))
	if (ServerData->bForceClientUpdate || ServerCheckClientError(ClientTimeStamp, DeltaTime, Accel, ClientLoc, RelativeClientLoc, ClientMovementMode, FVector::ZeroVector, ClientQuat, RelativeClientRotation))
	{
		UPrimitiveComponent* MovementBase = GetPhysKartOwner()->GetMovementBase();
		ServerData->PendingAdjustment.NewVel = GetPhysKartOwner()->GetVelocity();				//Velocity;
		ServerData->PendingAdjustment.NewQuat = UpdatedComponent->GetComponentQuat();
		ServerData->PendingAdjustment.NewAngVel = GetPhysKartOwner()->GetAngularVelocity();			//AngularVelocity;
		ServerData->PendingAdjustment.NewLoc = FRepMovement::RebaseOntoZeroOrigin(UpdatedComponent->GetComponentLocation(), this);

		ServerData->PendingAdjustment.bBaseRelativePosition = MovementBaseUtility::UseRelativeLocation(MovementBase);

#if !UE_BUILD_SHIPPING
		if (PhysKartMovementCVars::NetShowCorrections != 0)
		{
			const FVector LocDiff = UpdatedComponent->GetComponentLocation() - ClientLoc;
			const FString BaseString = MovementBase ? MovementBase->GetPathName(MovementBase->GetOutermost()) : TEXT("None");
			//UE_LOG(LogNetPlayerMovement, Warning, TEXT("*** Server: Error for %s at Time=%.3f is %3.3f LocDiff(%s) ClientLoc(%s) ServerLoc(%s) Base: %s Bone: %s Accel(%s) Velocity(%s)"),
			//	*GetNameSafe(GetPhysKartOwner()), ClientTimeStamp, LocDiff.Size(), *LocDiff.ToString(), *ClientLoc.ToString(), *UpdatedComponent->GetComponentLocation().ToString(), *BaseString, *ServerData->PendingAdjustment.NewBaseBoneName.ToString(), *Accel.ToString(), *Velocity.ToString());
			const float DebugLifetime = PhysKartMovementCVars::NetCorrectionLifetime;
			DrawDebugCapsule(GetWorld(), UpdatedComponent->GetComponentLocation(), GetPhysKartOwner()->GetSimpleCollisionHalfHeight(), GetPhysKartOwner()->GetSimpleCollisionRadius(), FQuat::Identity, FColor(100, 255, 100), false, DebugLifetime);
			DrawDebugCapsule(GetWorld(), ClientLoc, GetPhysKartOwner()->GetSimpleCollisionHalfHeight(), GetPhysKartOwner()->GetSimpleCollisionRadius(), FQuat::Identity, FColor(255, 100, 100), false, DebugLifetime);
		}
#endif

		ServerData->LastUpdateTime = GetWorld()->TimeSeconds;
		ServerData->PendingAdjustment.DeltaTime = DeltaTime;
		ServerData->PendingAdjustment.TimeStamp = ClientTimeStamp;
		ServerData->PendingAdjustment.bAckGoodMove = false;
		ServerData->PendingAdjustment.MovementMode = PackNetworkMovementMode();

#if USE_SERVER_PERF_COUNTERS
		PerfCountersIncrement(PerfCounter_NumServerMoveCorrections);
#endif
	}
	else
	{
		if (ServerShouldUseAuthoritativePosition(ClientTimeStamp, DeltaTime, Accel, ClientLoc, RelativeClientLoc, ClientMovementMode))
		{
			const FVector LocDiff = UpdatedComponent->GetComponentLocation() - ClientLoc; //-V595
			//if (!LocDiff.IsZero() || ClientMovementMode != PackNetworkMovementMode() || GetMovementBase() != ClientMovementBase || (GetPhysKartOwner() && GetPhysKartOwner()->GetBasedMovement().BoneName != ClientBaseBoneName))
			if (!LocDiff.IsZero() || ClientMovementMode != PackNetworkMovementMode())
			{
				// Just set the position. On subsequent moves we will resolve initially overlapping conditions.
				UpdatedComponent->SetWorldLocation(ClientLoc, false); //-V595

				// Trust the client's movement mode.
				ApplyNetworkMovementMode(ClientMovementMode);

				// Update base and floor at new location.
				//SetBase(ClientMovementBase, ClientBaseBoneName);
				UpdateFloorFromAdjustment();

				// Even if base has not changed, we need to recompute the relative offsets (since we've moved).
				//SaveBaseLocation();

				if (UpdatedComponent && GetPhysKartOwner())		// PaCorp * 4.25
				{
					LastUpdateLocation = UpdatedComponent->GetComponentLocation();
					LastUpdateRotation = UpdatedComponent->GetComponentQuat();
					LastUpdateVelocity = GetPhysKartOwner()->GetVelocity();
					LastUpdateAngularVelocity = GetPhysKartOwner()->GetAngularVelocity();
				}
				else
				{
					LastUpdateLocation = UpdatedComponent ? UpdatedComponent->GetComponentLocation() : FVector::ZeroVector;
					LastUpdateRotation = UpdatedComponent ? UpdatedComponent->GetComponentQuat() : FQuat::Identity;
					LastUpdateVelocity = Velocity;
					LastUpdateAngularVelocity = AngularVelocity;
				}
			}
		}

		// acknowledge receipt of this successful servermove()
		ServerData->PendingAdjustment.TimeStamp = ClientTimeStamp;
		ServerData->PendingAdjustment.bAckGoodMove = true;
	}

#if USE_SERVER_PERF_COUNTERS
	PerfCountersIncrement(PerfCounter_NumServerMoves);
#endif

	ServerData->bForceClientUpdate = false;
}

bool UPhysKartMoveComp::ServerCheckClientError(float ClientTimeStamp, float DeltaTime, const FVector& Accel,const FVector& ClientWorldLocation, const FVector& RelativeClientLocation, uint8 ClientMovementMode, const FVector& AngAccel, FQuat& ClientWorldQuat, const FQuat& RelativeClientQuat)
{
	// Check location difference against global setting
	if (!bIgnoreClientMovementErrorChecksAndCorrection)
	{
		if (ServerExceedsAllowablePositionError(ClientTimeStamp, DeltaTime, Accel, ClientWorldLocation, RelativeClientLocation, ClientMovementMode))
		{
			return true;
		}

		if (ServerExceedsAllowableQuatError(ClientTimeStamp, DeltaTime, FVector::ZeroVector, ClientWorldQuat, RelativeClientQuat, ClientMovementMode))
		{
			return true;
		}

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		if (PhysKartMovementCVars::NetForceClientAdjustmentPercent > SMALL_NUMBER)
		{
			if (RandomStream.FRand() < PhysKartMovementCVars::NetForceClientAdjustmentPercent)
			{
				UE_LOG(LogNetPlayerMovement, VeryVerbose, TEXT("** ServerCheckClientError forced by p.NetForceClientAdjustmentPercent"));
				return true;
			}
		}
#endif
	}
	else
	{
#if !UE_BUILD_SHIPPING
		if (PhysKartMovementCVars::NetShowCorrections != 0)
		{
			UE_LOG(LogNetPlayerMovement, Warning, TEXT("*** Server: %s is set to ignore error checks and corrections."), *GetNameSafe(GetPhysKartOwner()));
		}
#endif // !UE_BUILD_SHIPPING
	}

	return false;
}


bool UPhysKartMoveComp::ServerExceedsAllowablePositionError(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& ClientWorldLocation, const FVector& RelativeClientLocation, uint8 ClientMovementMode)
{
	// Check for disagreement in movement mode
	const uint8 CurrentPackedMovementMode = PackNetworkMovementMode();
	if (CurrentPackedMovementMode != ClientMovementMode)
	{
		// Consider this a major correction, see SendClientAdjustment()
		bNetworkLargeClientCorrection = true;
		return true;
	}

	const FVector LocDiff = UpdatedComponent->GetComponentLocation() - ClientWorldLocation;
	const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
	if (GameNetworkManager->ExceedsAllowablePositionError(LocDiff))
	{
		bNetworkLargeClientCorrection |= (LocDiff.SizeSquared() > FMath::Square(NetworkLargeClientCorrectionDistance));
		return true;
	}

	return false;
}

bool UPhysKartMoveComp::ServerShouldUseAuthoritativePosition(float ClientTimeStamp, float DeltaTime, const FVector& Accel, const FVector& ClientWorldLocation, const FVector& RelativeClientLocation, uint8 ClientMovementMode)
{
	if (bServerAcceptClientAuthoritativePosition)
	{
		return true;
	}

	const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
	if (GameNetworkManager->ClientAuthorativePosition)
	{
		return true;
	}

	return false;
}

bool UPhysKartMoveComp::ServerExceedsAllowableQuatError(float ClientTimeStamp, float DeltaTime, const FVector& AngAccel, const FQuat& ClientWorldQuat, const FQuat& RelativeClientQuat, uint8 ClientMovementMode)
{
	// Check for disagreement in movement mode
	const uint8 CurrentPackedMovementMode = PackNetworkMovementMode();
	if (CurrentPackedMovementMode != ClientMovementMode)
	{
		// Consider this a major correction, see SendClientAdjustment()
		bNetworkLargeClientCorrection = true;
		return true;
	}

	const FQuat QuatDiff = ClientWorldQuat * UpdatedComponent->GetComponentQuat().Inverse();

	const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
	//if (GameNetworkManager->ExceedsAllowablePositionError(LocDiff))
	bool bExceedsAllowableQuatError = FMath::Abs(QuatDiff.GetAngle()) > NetworkMaxQuatErrorScale;			//QuatDiff.GetAngle() < 0.86f;			// GetDefault<AGameNetworkManager>(GetClass())->MAXPOSITIONERRORSQUARED;
	if(bExceedsAllowableQuatError)
	{
		bNetworkLargeClientCorrection |= QuatDiff.GetAngle() > NetworkLargeClientCorrectionQuat;
		return true;
	}

	return false;
}

bool UPhysKartMoveComp::ServerShouldUseAuthoritativeQuat(float ClientTimeStamp, float DeltaTime, const FVector& AngAccel, const FQuat& ClientWorldQuat, const FQuat& RelativeClientQuat, uint8 ClientMovementMode)
{
	return false;
}

//8966
bool UPhysKartMoveComp::ServerMove_Validate(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 MoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis, float ClientQuatAngle)
{
	return true;
}

//8971
void UPhysKartMoveComp::ServerMoveDual(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode)
{
	//UE_LOG(LogPhysKartMovement, Log, TEXT("ServerMoveDualNoBase"));
	//GetPhysKartOwner()->ServerMoveDualNoBase(TimeStamp0, InAccel0, PendingFlags, View0, TimeStamp, InAccel, ClientLoc, NewFlags, ClientRoll, View, ClientMovementMode);
	GetPhysKartOwner()->ServerMoveDual(TimeStamp0, InReplicatedState0, InAccel0, PendingFlags, View0, TimeStamp, InReplicatedState, InAccel, ClientLoc, NewFlags, ClientRoll, View, ClientMovementMode);
}

bool UPhysKartMoveComp::ServerMoveDual_Validate(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode)
{
	return true;
}

//9012
void UPhysKartMoveComp::MoveAutonomous
(
	float ClientTimeStamp, 
	float DeltaTime, 
	FReplicatedPhysKartState InReplicatedState, 
	uint8 CompressedFlags, 
	const FVector& NewAccel, 
	FTransform NewTransform
)
{
	if (!HasValidData())
	{
		return;
	}

	UpdateFromCompressedFlags(CompressedFlags);

	Acceleration = ConstrainInputAcceleration(NewAccel);
	Acceleration = Acceleration.GetClampedToMaxSize(GetMaxAcceleration());
	AnalogInputModifier = ComputeAnalogInputModifier();

	const FVector OldLocation = UpdatedComponent->GetComponentLocation();
	const FQuat OldRotation = UpdatedComponent->GetComponentQuat();

	// PaCorp * Update input state from the server.
	SteeringInput = InReplicatedState.SteeringInput;
	ThrottleInput = InReplicatedState.ThrottleInput;
	BrakeInput = InReplicatedState.BrakeInput;
	HandbrakeInput = InReplicatedState.HandbrakeInput;

	PerformMovement(DeltaTime);

	// Check if data is valid as PerformMovement can mark character for pending kill
	if (!HasValidData())
	{
		return;
	}

	// If not playing root motion, tick animations after physics. We do this here to keep events, notifies, states and transitions in sync with client updates.
	if (GetPhysKartOwner() && !GetPhysKartOwner()->bClientUpdating && GetPhysKartOwner()->GetPhysKartMesh())
	{
		//if (!bWasPlayingRootMotion) // If we were playing root motion before PerformMovement but aren't anymore, we're on the last frame of anim root motion and have already ticked character
		//{
			//TickCharacterPose(DeltaTime);
		//}
		// TODO: SaveBaseLocation() in case tick moves us?

		// Trigger Events right away, as we could be receiving multiple ServerMoves per frame.
		GetPhysKartOwner()->GetPhysKartMesh()->ConditionallyDispatchQueuedAnimEvents();
	}

	if (GetPhysKartOwner() && UpdatedComponent)
	{
		// Smooth local view of remote clients on listen servers
		if (PhysKartMovementCVars::NetEnableListenServerSmoothing &&
			GetPhysKartOwner()->GetRemoteRole() == ROLE_AutonomousProxy &&
			IsNetMode(NM_ListenServer))
		{
			SmoothCorrection(OldLocation, OldRotation, UpdatedComponent->GetComponentLocation(), UpdatedComponent->GetComponentQuat());
		}
	}
}

//9000
void UPhysKartMoveComp::ServerMoveOld(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags)
{
	GetPhysKartOwner()->ServerMoveOld(OldTimeStamp, InReplicatedState, OldAccel, OldMoveFlags);
}

bool UPhysKartMoveComp::ServerMoveOld_Validate(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags)
{
	return true;
}

//9071
void UPhysKartMoveComp::UpdateFloorFromAdjustment()
{
	if (!HasValidData())
	{
		return;
	}

	//// If walking, try to update the cached floor so it is current. This is necessary for UpdateBasedMovement() and MoveAlongFloor() to work properly.
	//// If base is now NULL, presumably we are no longer walking. If we had a valid floor but don't find one now, we'll likely start falling.
	//if (GetPhysKartOwner()->GetMovementBase())
	//{
	//	FindFloor(UpdatedComponent->GetComponentLocation(), CurrentFloor, false);
	//}
	//else
	//{
	//	CurrentFloor.Clear();
	//}
}

void UPhysKartMoveComp::MoveResponsePacked_ServerSend(const FPhysKartMoveResponsePackedBits& PackedBits)
{
	// Pass through RPC call to character on client, there is less RPC bandwidth overhead when used on an Actor rather than a Component.
	GetPhysKartOwner()->ClientMoveResponsePacked(PackedBits);
}

void UPhysKartMoveComp::MoveResponsePacked_ClientReceive(const FPhysKartMoveResponsePackedBits& PackedBits)
{
	if (!HasValidData() || !IsActive())
	{
		return;
	}

	const int32 NumBits = PackedBits.DataBits.Num();
	if (!ensure(NumBits <= PhysKartMovementCVars::NetPackedMovementMaxBits))
	{
		// Protect against bad data that could cause client to allocate way too much memory.
		devCode(UE_LOG(LogNetPlayerMovement, Error, TEXT("MoveResponsePacked_ClientReceive: NumBits (%d) exceeds allowable limit!"), NumBits));
		return;
	}

	// Reuse bit reader to avoid allocating memory each time.
	MoveResponseBitReader.SetData((uint8*)PackedBits.DataBits.GetData(), NumBits);
	MoveResponseBitReader.PackageMap = PackedBits.GetPackageMap();

	// Deserialize bits to response data struct.
	// We had to wait until now and use the temp bit stream because the RPC doesn't know about the virtual overrides on the possibly custom struct that is our data container.
	FPhysKartMoveResponseDataContainer& ResponseDataContainer = GetMoveResponseDataContainer();
	if (!ResponseDataContainer.Serialize(*this, MoveResponseBitReader, MoveResponseBitReader.PackageMap) || MoveResponseBitReader.IsError())
	{
		devCode(UE_LOG(LogNetPlayerMovement, Error, TEXT("MoveResponsePacked_ClientReceive: Failed to serialize response data!")));
		return;
	}

	ClientHandleMoveResponse(ResponseDataContainer);
}


void UPhysKartMoveComp::ServerSendMoveResponse(const FPhysKartClientAdjustment& PendingAdjustment)
{
	// Get storage container we'll be using and fill it with movement data
	FPhysKartMoveResponseDataContainer& ResponseDataContainer = GetMoveResponseDataContainer();
	ResponseDataContainer.ServerFillResponseData(*this, PendingAdjustment);

	// Reset bit writer without affecting allocations
	FBitWriterMark BitWriterReset;
	BitWriterReset.Pop(MoveResponseBitWriter);

	// Extract the net package map used for serializing object references.
	APlayerController* PC = Cast<APlayerController>(GetPhysKartOwner()->GetController());
	UNetConnection* NetConnection = PC ? PC->GetNetConnection() : nullptr;
	MoveResponseBitWriter.PackageMap = NetConnection ? NetConnection->PackageMap : nullptr;
	if (MoveResponseBitWriter.PackageMap == nullptr)
	{
		UE_LOG(LogNetPlayerMovement, Error, TEXT("ServerSendMoveResponse: Failed to find a NetConnection/PackageMap for data serialization!"));
		return;
	}

	// Serialize move struct into a bit stream
	if (!ResponseDataContainer.Serialize(*this, MoveResponseBitWriter, MoveResponseBitWriter.PackageMap) || MoveResponseBitWriter.IsError())
	{
		UE_LOG(LogNetPlayerMovement, Error, TEXT("ServerSendMoveResponse: Failed to serialize out response data!"));
		return;
	}

	// Copy bits to our struct that we can NetSerialize to the client.
	// 'static' to avoid reallocation each invocation
	static FPhysKartMoveResponsePackedBits PackedBits;
	PackedBits.DataBits.SetNumUninitialized(MoveResponseBitWriter.GetNumBits());

	check(PackedBits.DataBits.Num() >= MoveResponseBitWriter.GetNumBits());
	FMemory::Memcpy(PackedBits.DataBits.GetData(), MoveResponseBitWriter.GetData(), MoveResponseBitWriter.GetNumBytes());

	// Send bits to client!
	MoveResponsePacked_ServerSend(PackedBits);
}


void FPhysKartMoveResponseDataContainer::ServerFillResponseData(const UPhysKartMoveComp& CharacterMovement, const FPhysKartClientAdjustment& PendingAdjustment)
{
	bHasRotation = false;

	ClientAdjustment = PendingAdjustment;
}

bool FPhysKartMoveResponseDataContainer::Serialize(UPhysKartMoveComp& CharacterMovement, FArchive& Ar, UPackageMap* PackageMap)
{
	bool bLocalSuccess = true;
	const bool bIsSaving = Ar.IsSaving();

	Ar.SerializeBits(&ClientAdjustment.bAckGoodMove, 1);
	Ar << ClientAdjustment.TimeStamp;

	if (IsCorrection())
	{
		Ar.SerializeBits(&bHasRotation, 1);

		ClientAdjustment.NewLoc.NetSerialize(Ar, PackageMap, bLocalSuccess);
		ClientAdjustment.NewVel.NetSerialize(Ar, PackageMap, bLocalSuccess);
		ClientAdjustment.NewQuat.NetSerialize(Ar, PackageMap, bLocalSuccess);
		ClientAdjustment.NewAngVel.NetSerialize(Ar, PackageMap, bLocalSuccess);

		if (bHasRotation)
		{
			ClientAdjustment.NewRot.NetSerialize(Ar, PackageMap, bLocalSuccess);
		}
		else if (!bIsSaving)
		{
			ClientAdjustment.NewRot = FRotator::ZeroRotator;
		}

		SerializeOptionalValue<uint8>(bIsSaving, Ar, ClientAdjustment.MovementMode, MOVE_Walking);
		Ar.SerializeBits(&ClientAdjustment.bBaseRelativePosition, 1);
	}

	return !Ar.IsError();
}

void UPhysKartMoveComp::ClientHandleMoveResponse(const FPhysKartMoveResponseDataContainer& MoveResponse)
{
	if (MoveResponse.IsGoodMove())
	{
		ClientAckGoodMove_Implementation(MoveResponse.ClientAdjustment.TimeStamp);
	}
	else
	{
		ClientAdjustPosition_Implementation(
			MoveResponse.ClientAdjustment.TimeStamp,
			MoveResponse.ClientAdjustment.NewLoc,
			MoveResponse.ClientAdjustment.NewVel,
			MoveResponse.ClientAdjustment.NewQuat,
			MoveResponse.ClientAdjustment.NewAngVel,
			//MoveResponse.ClientAdjustment.NewBase,
			//MoveResponse.ClientAdjustment.NewBaseBoneName,
			//MoveResponse.bHasBase,
			//MoveResponse.ClientAdjustment.bBaseRelativePosition,
			MoveResponse.ClientAdjustment.MovementMode);
	}
}

//9204
void UPhysKartMoveComp::ClientVeryShortAdjustPosition(float TimeStamp, FVector NewLoc, FQuat NewQuat, uint8 ServerMovementMode)
{
	GetPhysKartOwner()->ClientVeryShortAdjustPosition(TimeStamp, NewLoc, NewQuat, ServerMovementMode);
}

void UPhysKartMoveComp::ClientVeryShortAdjustPosition_Implementation
(
	float TimeStamp,
	FVector NewLoc,
	FQuat NewQuat,
	uint8 ServerMovementMode
)
{
	if (HasValidData())
	{		
		ClientAdjustPosition(TimeStamp, NewLoc, FVector::ZeroVector, NewQuat, FVector::ZeroVector, ServerMovementMode);
	}
}

//9227
void UPhysKartMoveComp::ClientAdjustPosition(float TimeStamp, FVector NewLoc, FVector NewVel, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode)
{
	GetPhysKartOwner()->ClientAdjustPosition(TimeStamp, NewLoc, NewVel, NewQuat, NewAngVel, ServerMovementMode);
}

void UPhysKartMoveComp::ClientAdjustPosition_Implementation
(
	float TimeStamp,
	FVector NewLocation,
	FVector NewVelocity,
	FQuat NewQuat, 
	FVector NewAngVel,
	uint8 ServerMovementMode
)
{
	if (!HasValidData() || !IsActive())
	{
		return;
	}

	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	check(ClientData);

	// Ack move if it has not expired.
	int32 MoveIndex = ClientData->GetSavedMoveIndex(TimeStamp);
	if (MoveIndex == INDEX_NONE)
	{
		if (ClientData->LastAckedMove.IsValid())
		{
			UE_LOG(LogNetPlayerMovement, Log, TEXT("ClientAdjustPosition_Implementation could not find Move for TimeStamp: %f, LastAckedTimeStamp: %f, CurrentTimeStamp: %f"), TimeStamp, ClientData->LastAckedMove->TimeStamp, ClientData->CurrentTimeStamp);
		}
		return;
	}

	ClientData->AckMove(MoveIndex, *this);

	FVector WorldShiftedNewLocation;
	WorldShiftedNewLocation = FRepMovement::RebaseOntoLocalOrigin(NewLocation, this);


	// Trigger event
	OnClientCorrectionReceived(*ClientData, TimeStamp, WorldShiftedNewLocation, NewVelocity, NewQuat, NewAngVel, ServerMovementMode);

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientAdjustPosition_Implementation: %s"), *NewVelocity.ToString()));
	}
	
	// Trust the server's positioning.
	if (UpdatedComponent)
	{
		UpdatedComponent->SetWorldLocation(WorldShiftedNewLocation, false, nullptr, ETeleportType::TeleportPhysics);
		UpdatedComponent->SetWorldRotation(NewQuat, false, nullptr, ETeleportType::TeleportPhysics);

		if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics())
		{
			GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsLinearVelocity(NewVelocity);
			GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsAngularVelocityInRadians(NewAngVel);
		}
	}
	Velocity = NewVelocity;
	AngularVelocity = NewAngVel;

	//ImmediatePhysics
	if (bRootIsSkeletalMesh)
	{
		if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics())
		{			
			for (int32 c = 0; c < UpdatedComponentBodiesIndex.Num(); c++)
			{
				ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;
				UpdatedBody = PhysicsSimulation->GetActorHandle(UpdatedComponentBodiesIndex[c]);
				if (UpdatedBody && GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics())
				{
					UpdatedBody->SetWorldTransform(GetPhysKartOwner()->GetPhysKartRoot()->GetComponentTransform());
					UpdatedBody->SetLinearVelocity(GetPhysKartOwner()->GetVelocity());
					UpdatedBody->SetAngularVelocity(GetPhysKartOwner()->GetAngularVelocity());

					DrawDebugSphere(GetWorld(), UpdatedBody->GetWorldTransform().GetLocation(), 100.f, 16, FColor::Cyan, false, -1.0F);
				}
			}
		}
	}
	else if (PhysicsSimulation->NumActors() > 0 && UpdatedComponentBodyIndex > INDEX_NONE && UpdatedComponentBodyIndex < PhysicsSimulation->NumActors())
	{
		ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;
		UpdatedBody = PhysicsSimulation->GetActorHandle(UpdatedComponentBodyIndex);
		if (UpdatedBody && GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics())
		{
			UpdatedBody->SetWorldTransform(GetPhysKartOwner()->GetPhysKartRoot()->GetComponentTransform());
			UpdatedBody->SetLinearVelocity(GetPhysKartOwner()->GetVelocity());
			UpdatedBody->SetAngularVelocity(GetPhysKartOwner()->GetAngularVelocity());	
		}
	}

	// Trust the server's movement mode
	UPrimitiveComponent* PreviousBase = GetPhysKartOwner()->GetMovementBase();
	ApplyNetworkMovementMode(ServerMovementMode);

	// Update floor at new location
	UpdateFloorFromAdjustment();
	bJustTeleported = true;

	if (UpdatedComponent && GetPhysKartOwner())
	{
		LastUpdateLocation = UpdatedComponent->GetComponentLocation();
		LastUpdateRotation = UpdatedComponent->GetComponentQuat();
		LastUpdateVelocity = GetPhysKartOwner()->GetVelocity();
		LastUpdateAngularVelocity = GetPhysKartOwner()->GetAngularVelocity();
	}
	else
	{
		LastUpdateLocation = UpdatedComponent ? UpdatedComponent->GetComponentLocation() : FVector::ZeroVector;
		LastUpdateRotation = UpdatedComponent ? UpdatedComponent->GetComponentQuat() : FQuat::Identity;
		LastUpdateVelocity = Velocity;
		LastUpdateAngularVelocity = AngularVelocity;
	}

	UpdateComponentVelocity();
	ClientData->bUpdatePosition = true;
}

//9359
void UPhysKartMoveComp::OnClientCorrectionReceived(FNetworkPredictionData_Client_PhysKart& ClientData, float TimeStamp, FVector NewLocation, FVector NewVelocity, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode)
{
#if !UE_BUILD_SHIPPING
	if (PhysKartMovementCVars::NetShowCorrections != 0)
	{
		const FVector ClientLocAtCorrectedMove = ClientData.LastAckedMove.IsValid() ? ClientData.LastAckedMove->SavedLocation : UpdatedComponent->GetComponentLocation();
		const FVector LocDiff = ClientLocAtCorrectedMove - NewLocation;
		
		const FString NewBaseString = TEXT("None : PaCorp; Base Removed.");
		//UE_LOG(LogNetPlayerMovement, Warning, TEXT("*** Client: Error for %s at Time=%.3f is %3.3f LocDiff(%s) ClientLoc(%s) ServerLoc(%s) NewBase: %s NewBone: %s ClientVel(%s) ServerVel(%s) SavedMoves %d"),
		//	   *GetNameSafe(GetPhysKartOwner()), TimeStamp, LocDiff.Size(), *LocDiff.ToString(), *ClientLocAtCorrectedMove.ToString(), *NewLocation.ToString(), *NewBaseString, *NewBaseBoneName.ToString(), *Velocity.ToString(), *NewVelocity.ToString(), ClientData.SavedMoves.Num());
		const float DebugLifetime = PhysKartMovementCVars::NetCorrectionLifetime;
		if (!LocDiff.IsNearlyZero())
		{
			// When server corrects us to a new location, draw red at location where client thought they were, green where the server corrected us to
			DrawDebugCapsule(GetWorld(), ClientLocAtCorrectedMove, GetPhysKartOwner()->GetSimpleCollisionHalfHeight(), GetPhysKartOwner()->GetSimpleCollisionRadius(), FQuat::Identity, FColor(255, 100, 100), false, DebugLifetime);
			DrawDebugCapsule(GetWorld(), NewLocation, GetPhysKartOwner()->GetSimpleCollisionHalfHeight(), GetPhysKartOwner()->GetSimpleCollisionRadius(), FQuat::Identity, FColor(100, 255, 100), false, DebugLifetime);
		}
		else
		{
			// When we receive a server correction that doesn't change our position from where our client move had us, draw yellow (otherwise would be overlapping)
			// This occurs when we receive an initial correction, replay moves to get us into the right location, and then receive subsequent corrections by the server (who doesn't know if we corrected already
			// so continues to send corrections). This is a "no-op" server correction with regards to location since we already corrected (occurs with latency)
			DrawDebugCapsule(GetWorld(), NewLocation, GetPhysKartOwner()->GetSimpleCollisionHalfHeight(), GetPhysKartOwner()->GetSimpleCollisionRadius(), FQuat::Identity, FColor(255, 255, 100), false, DebugLifetime);
		}
	}
#endif //!UE_BUILD_SHIPPING
}

//9565
void UPhysKartMoveComp::ClientAckGoodMove(float TimeStamp)
{
	GetPhysKartOwner()->ClientAckGoodMove(TimeStamp);
}

void UPhysKartMoveComp::ClientAckGoodMove_Implementation(float TimeStamp)
{
	if (!HasValidData() || !IsActive())
	{
		return;
	}

	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	check(ClientData);

	// Ack move if it has not expired.
	int32 MoveIndex = ClientData->GetSavedMoveIndex(TimeStamp);
	if (MoveIndex == INDEX_NONE)
	{
		if (ClientData->LastAckedMove.IsValid())
		{
			UE_LOG(LogNetPlayerMovement, Log, TEXT("ClientAckGoodMove_Implementation could not find Move for TimeStamp: %f, LastAckedTimeStamp: %f, CurrentTimeStamp: %f"), TimeStamp, ClientData->LastAckedMove->TimeStamp, ClientData->CurrentTimeStamp);
		}
		return;
	}

	ClientData->AckMove(MoveIndex, *this);
}

//9809
void UPhysKartMoveComp::ApplyAccumulatedForces(float DeltaSeconds)
{
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot())
	{
		GetPhysKartOwner()->GetPhysKartRoot()->AddForce(PendingForceToApply);
		GetPhysKartOwner()->GetPhysKartRoot()->AddTorque(PendingTorqueToApply);
		GetPhysKartOwner()->GetPhysKartRoot()->AddImpulse(PendingImpulseToApply);
	}
}

void UPhysKartMoveComp::ClearAccumulatedForces()
{
	PendingImpulseToApply = FVector::ZeroVector;
	PendingForceToApply = FVector::ZeroVector;
	PendingTorqueToApply = FVector::ZeroVector;
	PendingLaunchVelocity = FVector::ZeroVector;
}

void UPhysKartMoveComp::AddRadialForce(const FVector& Origin, float Radius, float Strength, enum ERadialImpulseFalloff Falloff)
{
	FVector Delta = UpdatedComponent->GetComponentLocation() - Origin;
	const float DeltaMagnitude = Delta.Size();

	// Do nothing if outside radius
	if(DeltaMagnitude > Radius)
	{
		return;
	}

	Delta = Delta.GetSafeNormal();

	float ForceMagnitude = Strength;
	if (Falloff == RIF_Linear && Radius > 0.0f)
	{
		ForceMagnitude *= (1.0f - (DeltaMagnitude / Radius));
	}

	AddForce(Delta * ForceMagnitude);
}
 
void UPhysKartMoveComp::AddRadialImpulse(const FVector& Origin, float Radius, float Strength, enum ERadialImpulseFalloff Falloff, bool bVelChange)
{
	FVector Delta = UpdatedComponent->GetComponentLocation() - Origin;
	const float DeltaMagnitude = Delta.Size();

	// Do nothing if outside radius
	if(DeltaMagnitude > Radius)
	{
		return;
	}

	Delta = Delta.GetSafeNormal();

	float ImpulseMagnitude = Strength;
	if (Falloff == RIF_Linear && Radius > 0.0f)
	{
		ImpulseMagnitude *= (1.0f - (DeltaMagnitude / Radius));
	}

	AddImpulse(Delta * ImpulseMagnitude, bVelChange);
}

//9931
void UPhysKartMoveComp::TickCharacterPose(float DeltaTime)
{
	if (DeltaTime < UPhysKartMoveComp::MIN_TICK_TIME)
	{
		return;
	}

	check(GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartMesh());
	USkeletalMeshComponent* CharacterMesh = GetPhysKartOwner()->GetPhysKartMesh();

	// bAutonomousTickPose is set, we control TickPose from the Character's Movement and Networking updates, and bypass the Component's update.
	// (Or Simulating Root Motion for remote clients)
	CharacterMesh->bIsAutonomousTickPose = true;

	if (CharacterMesh->ShouldTickPose())
	{
		// Keep track of if we're playing root motion, just in case the root motion montage ends this frame.
		CharacterMesh->TickPose(DeltaTime, true);
	}

	CharacterMesh->bIsAutonomousTickPose = false;
}

//11180
void UPhysKartMoveComp::UpdateFromCompressedFlags(uint8 Flags)
{
	if (!GetPhysKartOwner())
	{
		return;
	}

	const bool bWasPressingItem = GetPhysKartOwner()->bPressedItem;
	GetPhysKartOwner()->bPressedItem = ((Flags & FSavedMove_PhysKart::FLAG_ItemPressed) != 0);
}

//-----------------------------------------------------------------------------------------------//
// INetworkPredictionInterface

float UPhysKartMoveComp::GetMaxAcceleration() const
{
	return MaxAcceleration;
}

void UPhysKartMoveComp::SetLinearVelocity(FVector InAngVel)
{
	Velocity = InAngVel;

	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics())
	{
		GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsLinearVelocity(Velocity);
	}
}

void UPhysKartMoveComp::SetAngularVelocity(FVector InAngVel)
{
	AngularVelocity = InAngVel;

	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics())
	{
		GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsAngularVelocityInRadians(AngularVelocity);
	}
}

void UPhysKartMoveComp::OnMovementUpdated(float DeltaTime, const FVector& OldLocation, const FVector& OldVelocity)
{
	// empty base implementation, intended for derived classes to override.
}

void UPhysKartMoveComp::CallMovementUpdateDelegate(float DeltaTime, const FVector& OldLocation, const FVector& OldVelocity)
{
	////SCOPE_CYCLE_COUNTER(STAT_CharMoveUpdateDelegate);

	// Update component velocity in case events want to read it
	UpdateComponentVelocity();

	// Delegate (for blueprints)
	if (GetPhysKartOwner())
	{
		GetPhysKartOwner()->OnPhysKartMovementUpdated.Broadcast(DeltaTime, OldLocation, OldVelocity);
	}
}

//-----------------------------------------------------------------------------------------------//

bool UPhysKartMoveComp::HasValidData() const
{
	const bool bIsValid = UpdatedComponent && IsValid(GetPhysKartOwner());
#if ENABLE_NAN_DIAGNOSTIC
	if (bIsValid)
	{
		// NaN-checking updates
		if (Velocity.ContainsNaN())
		{
			logOrEnsureNanError(TEXT("UPhysKartMoveComp::HasValidData() detected NaN/INF for (%s) in Velocity:\n%s"), *GetPathNameSafe(this), *Velocity.ToString());
			UPhysKartMoveComp* MutableThis = const_cast<UPhysKartMoveComp*>(this);
			MutableThis->Velocity = FVector::ZeroVector;
		}
		if (!UpdatedComponent->GetComponentTransform().IsValid())
		{
			logOrEnsureNanError(TEXT("UPhysKartMoveComp::HasValidData() detected NaN/INF for (%s) in UpdatedComponent->ComponentTransform:\n%s"), *GetPathNameSafe(this), *UpdatedComponent->GetComponentTransform().ToHumanReadableString());
		}
		if (UpdatedComponent->GetComponentRotation().ContainsNaN())
		{
			logOrEnsureNanError(TEXT("UPhysKartMoveComp::HasValidData() detected NaN/INF for (%s) in UpdatedComponent->GetComponentRotation():\n%s"), *GetPathNameSafe(this), *UpdatedComponent->GetComponentRotation().ToString());
		}
	}
#endif
	return bIsValid;
}

bool UPhysKartMoveComp::ShouldUsePackedMovementRPCs() const
{
	return PhysKartMovementCVars::NetUsePackedMovementRPCs != 0;
}

FCollisionShape UPhysKartMoveComp::GetWheeledPawnCollisionShape() const
{
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartMesh())
	{
		return GetPhysKartOwner()->GetPhysKartMesh()->GetCollisionShape();
	}

	FBox OwnerBoundingBox = GetOwner()->GetComponentsBoundingBox();

	return FCollisionShape::MakeBox(OwnerBoundingBox.GetExtent());
}

void UPhysKartMoveComp::ForceReplicationUpdate()
{
	if (HasPredictionData_Server())
	{
		GetPredictionData_Server_PhysKart()->LastUpdateTime = GetWorld()->TimeSeconds - 10.f;
	}
}


void UPhysKartMoveComp::ForceClientAdjustment()
{
	ServerLastClientAdjustmentTime = -1.f;
}

FVector UPhysKartMoveComp::ConstrainInputAcceleration(const FVector& InputAcceleration) const
{
	// walking or falling pawns ignore up/down sliding
	//if (InputAcceleration.Z != 0.f && (IsMovingOnGround() || IsFalling()))
	//{
	//	return FVector(InputAcceleration.X, InputAcceleration.Y, 0.f);
	//}

	return InputAcceleration;
}


FVector UPhysKartMoveComp::ScaleInputAcceleration(const FVector& InputAcceleration) const
{
	return GetMaxAcceleration() * InputAcceleration.GetClampedToMaxSize(1.0f);
}


void UPhysKartMoveComp::SendClientAdjustment()
{
	if (!HasValidData())
	{
		return;
	}

	FNetworkPredictionData_Server_PhysKart* ServerData = GetPredictionData_Server_PhysKart();
	check(ServerData);

	if (ServerData->PendingAdjustment.TimeStamp <= 0.f)
	{
		return;
	}

	const float CurrentTime = GetWorld()->GetTimeSeconds();
	if (ServerData->PendingAdjustment.bAckGoodMove)
	{
		// just notify client this move was received
		if (CurrentTime - ServerLastClientGoodMoveAckTime > NetworkMinTimeBetweenClientAckGoodMoves)
		{
			ServerLastClientGoodMoveAckTime = CurrentTime;
			if (ShouldUsePackedMovementRPCs())
			{
				ServerSendMoveResponse(ServerData->PendingAdjustment);
			}
			else
			{
				ClientAckGoodMove(ServerData->PendingAdjustment.TimeStamp);
			}
		}
	}
	else
	{
		// We won't be back in here until the next client move and potential correction is received, so use the correct time now.
		// Protect against bad data by taking appropriate min/max of editable values.
		const float AdjustmentTimeThreshold = bNetworkLargeClientCorrection ?
			FMath::Min(NetworkMinTimeBetweenClientAdjustmentsLargeCorrection, NetworkMinTimeBetweenClientAdjustments) :
			FMath::Max(NetworkMinTimeBetweenClientAdjustmentsLargeCorrection, NetworkMinTimeBetweenClientAdjustments);

		// Check if correction is throttled based on time limit between updates.
		if (CurrentTime - ServerLastClientAdjustmentTime > AdjustmentTimeThreshold)
		{
			ServerLastClientAdjustmentTime = CurrentTime;

			if (ShouldUsePackedMovementRPCs())
			{
				ServerData->PendingAdjustment.MovementMode = PackNetworkMovementMode();
				ServerSendMoveResponse(ServerData->PendingAdjustment);
			}
			else
			{				
				if (ServerData->PendingAdjustment.NewVel.IsZero())
				{
					ClientVeryShortAdjustPosition
					(
						ServerData->PendingAdjustment.TimeStamp,
						ServerData->PendingAdjustment.NewLoc,
						ServerData->PendingAdjustment.NewQuat,
						//ServerData->PendingAdjustment.NewBase,
						//ServerData->PendingAdjustment.NewBaseBoneName,
						//ServerData->PendingAdjustment.NewBase != NULL,
						//ServerData->PendingAdjustment.bBaseRelativePosition,
						PackNetworkMovementMode()
					);
				}
				else
				{
					ClientAdjustPosition
					(
						ServerData->PendingAdjustment.TimeStamp,
						ServerData->PendingAdjustment.NewLoc,
						ServerData->PendingAdjustment.NewVel,
						ServerData->PendingAdjustment.NewQuat,
						ServerData->PendingAdjustment.NewAngVel,
						//ServerData->PendingAdjustment.NewBase,
						//ServerData->PendingAdjustment.NewBaseBoneName,
						//ServerData->PendingAdjustment.NewBase != NULL,
						//ServerData->PendingAdjustment.bBaseRelativePosition,
						PackNetworkMovementMode()
					);
				}
			}
		}
	}

	ServerData->PendingAdjustment.TimeStamp = 0;
	ServerData->PendingAdjustment.bAckGoodMove = false;
	ServerData->bForceClientUpdate = false;
}

bool UPhysKartMoveComp::ForcePositionUpdate(float DeltaTime)
{
	// TODO: smooth correction on listen server?
	return true;
}

void UPhysKartMoveComp::SmoothCorrection(const FVector& OldLocation, const FQuat& OldRotation, const FVector& NewLocation, const FQuat& NewRotation)
{
	return;
	//SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementSmoothCorrection);
	if (!HasValidData())
	{
		return;
	}

	// We shouldn't be running this on a server that is not a listen server.
	checkSlow(GetNetMode() != NM_DedicatedServer);
	checkSlow(GetNetMode() != NM_Standalone);

	// Only client proxies or remote clients on a listen server should run this code.
	const bool bIsSimulatedProxy = (GetPhysKartOwner()->GetLocalRole() == ROLE_SimulatedProxy);
	const bool bIsRemoteAutoProxy = (GetPhysKartOwner()->GetRemoteRole() == ROLE_AutonomousProxy);
	ensure(bIsSimulatedProxy || bIsRemoteAutoProxy);

	// Getting a correction means new data, so smoothing needs to run.
	bNetworkSmoothingComplete = false;

	// Handle selected smoothing mode.
	if (NetworkSmoothingMode == ENetworkSmoothingMode::Replay)
	{
		// Replays use pure interpolation in this mode, all of the work is done in SmoothClientPosition_Interpolate
		return;
	}
	else if (NetworkSmoothingMode == ENetworkSmoothingMode::Disabled)
	{
		UpdatedComponent->SetWorldLocationAndRotation(NewLocation, NewRotation, false, nullptr, ETeleportType::TeleportPhysics);
		bNetworkSmoothingComplete = true;
	}
	else if (FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart())
	{
		const UWorld* MyWorld = GetWorld();
		if (!ensure(MyWorld != nullptr))
		{
			return;
		}

		// The mesh doesn't move, but the capsule does so we have a new offset.
		FVector NewToOldVector = (OldLocation - NewLocation);

		const float DistSq = NewToOldVector.SizeSquared();
		if (DistSq > FMath::Square(ClientData->MaxSmoothNetUpdateDist))
		{
			ClientData->MeshTranslationOffset = (DistSq > FMath::Square(ClientData->NoSmoothNetUpdateDist))
				? FVector::ZeroVector
				: ClientData->MeshTranslationOffset + ClientData->MaxSmoothNetUpdateDist * NewToOldVector.GetSafeNormal();
		}
		else
		{
			ClientData->MeshTranslationOffset = ClientData->MeshTranslationOffset + NewToOldVector;
		}

		//UE_LOG(LogCharacterNetSmoothing, Verbose, TEXT("Proxy %s SmoothCorrection(%.2f)"), *GetNameSafe(GetPhysKartOwner()), FMath::Sqrt(DistSq));
		if (NetworkSmoothingMode == ENetworkSmoothingMode::Linear)
		{
			ClientData->OriginalMeshTranslationOffset = ClientData->MeshTranslationOffset;

			// Remember the current and target rotation, we're going to lerp between them
			ClientData->OriginalMeshRotationOffset = OldRotation;
			ClientData->MeshRotationOffset = OldRotation;
			ClientData->MeshRotationTarget = NewRotation;

			// Move the capsule, but not the mesh.
			// Note: we don't change rotation, we lerp towards it in SmoothClientPosition.
			if (NewLocation != OldLocation)
			{				
				UpdatedComponent->SetWorldLocation(NewLocation, false, nullptr, GetTeleportType());
			}
		}
		else
		{
			// Calc rotation needed to keep current world rotation after UpdatedComponent moves.
			// Take difference between where we were rotated before, and where we're going
			ClientData->MeshRotationOffset = (NewRotation.Inverse() * OldRotation) * ClientData->MeshRotationOffset;
			ClientData->MeshRotationTarget = FQuat::Identity;

			UpdatedComponent->SetWorldLocationAndRotation(NewLocation, NewRotation, false, nullptr, GetTeleportType());
		}

		//////////////////////////////////////////////////////////////////////////
		// Update smoothing timestamps

		// If running ahead, pull back slightly. This will cause the next delta to seem slightly longer, and cause us to lerp to it slightly slower.
		if (ClientData->SmoothingClientTimeStamp > ClientData->SmoothingServerTimeStamp)
		{
			const double OldClientTimeStamp = ClientData->SmoothingClientTimeStamp;
			ClientData->SmoothingClientTimeStamp = FMath::LerpStable(ClientData->SmoothingServerTimeStamp, OldClientTimeStamp, 0.5);

			//UE_LOG(LogCharacterNetSmoothing, VeryVerbose, TEXT("SmoothCorrection: Pull back client from ClientTimeStamp: %.6f to %.6f, ServerTimeStamp: %.6f for %s"),
			//	OldClientTimeStamp, ClientData->SmoothingClientTimeStamp, ClientData->SmoothingServerTimeStamp, *GetNameSafe(GetPhysKartOwner()));
		}

		// Using server timestamp lets us know how much time actually elapsed, regardless of packet lag variance.
		double OldServerTimeStamp = ClientData->SmoothingServerTimeStamp;
		if (bIsSimulatedProxy)
		{
			// This value is normally only updated on the server, however some code paths might try to read it instead of the replicated value so copy it for proxies as well.
			ServerLastTransformUpdateTimeStamp = GetPhysKartOwner()->GetReplicatedServerLastTransformUpdateTimeStamp();
		}
		ClientData->SmoothingServerTimeStamp = ServerLastTransformUpdateTimeStamp;

		// Initial update has no delta.
		if (ClientData->LastCorrectionTime == 0)
		{
			ClientData->SmoothingClientTimeStamp = ClientData->SmoothingServerTimeStamp;
			OldServerTimeStamp = ClientData->SmoothingServerTimeStamp;
		}

		// Don't let the client fall too far behind or run ahead of new server time.
		const double ServerDeltaTime = ClientData->SmoothingServerTimeStamp - OldServerTimeStamp;
		const double MaxOffset = ClientData->MaxClientSmoothingDeltaTime;
		const double MinOffset = FMath::Min(double(ClientData->SmoothNetUpdateTime), MaxOffset);

		// MaxDelta is the farthest behind we're allowed to be after receiving a new server time.
		const double MaxDelta = FMath::Clamp(ServerDeltaTime * 1.25, MinOffset, MaxOffset);
		ClientData->SmoothingClientTimeStamp = FMath::Clamp(ClientData->SmoothingClientTimeStamp, ClientData->SmoothingServerTimeStamp - MaxDelta, ClientData->SmoothingServerTimeStamp);

		// Compute actual delta between new server timestamp and client simulation.
		ClientData->LastCorrectionDelta = ClientData->SmoothingServerTimeStamp - ClientData->SmoothingClientTimeStamp;
		ClientData->LastCorrectionTime = MyWorld->GetTimeSeconds();

		//UE_LOG(LogCharacterNetSmoothing, VeryVerbose, TEXT("SmoothCorrection: WorldTime: %.6f, ServerTimeStamp: %.6f, ClientTimeStamp: %.6f, Delta: %.6f for %s"),
		//	MyWorld->GetTimeSeconds(), ClientData->SmoothingServerTimeStamp, ClientData->SmoothingClientTimeStamp, ClientData->LastCorrectionDelta, *GetNameSafe(GetPhysKartOwner()));

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		if (PhysKartMovementCVars::NetVisualizeSimulatedCorrections >= 2)
		{
			const float Radius = 4.0f;
			const bool	bPersist = false;
			const float Lifetime = 10.0f;
			const int32	Sides = 8;
			const float ArrowSize = 4.0f;

			const FVector SimulatedLocation = OldLocation;
			const FVector ServerLocation = NewLocation + FVector(0, 0, 0.5f);

			//const FVector SmoothLocation	= GetPhysKartOwner()->GetPhysKartMesh()->GetComponentLocation() - GetPhysKartOwner()->GetBaseTranslationOffset() + FVector( 0, 0, 1.0f );
			const FVector SmoothLocation = GetPhysKartOwner()->GetPhysKartMesh()->GetComponentLocation() + FVector(0, 0, 1.0f);

			//DrawDebugCoordinateSystem( GetWorld(), ServerLocation + FVector( 0, 0, 300.0f ), UpdatedComponent->GetComponentRotation(), 45.0f, bPersist, Lifetime );

			// Draw simulated location
			DrawCircle(GetWorld(), SimulatedLocation, FVector(1, 0, 0), FVector(0, 1, 0), FColor(255, 0, 0, 255), Radius, Sides, bPersist, Lifetime);

			// Draw server (corrected location)
			DrawCircle(GetWorld(), ServerLocation, FVector(1, 0, 0), FVector(0, 1, 0), FColor(0, 255, 0, 255), Radius, Sides, bPersist, Lifetime);

			// Draw smooth simulated location
			FRotationMatrix SmoothMatrix(GetPhysKartOwner()->GetPhysKartMesh()->GetComponentRotation());
			DrawDebugDirectionalArrow(GetWorld(), SmoothLocation, SmoothLocation + SmoothMatrix.GetScaledAxis(EAxis::Y) * 5, ArrowSize, FColor(255, 255, 0, 255), bPersist, Lifetime);
			DrawCircle(GetWorld(), SmoothLocation, FVector(1, 0, 0), FVector(0, 1, 0), FColor(0, 0, 255, 255), Radius, Sides, bPersist, Lifetime);

			if (ClientData->LastServerLocation != FVector::ZeroVector)
			{
				// Arrow showing simulated line
				DrawDebugDirectionalArrow(GetWorld(), ClientData->LastServerLocation, SimulatedLocation, ArrowSize, FColor(255, 0, 0, 255), bPersist, Lifetime);

				// Arrow showing server line
				DrawDebugDirectionalArrow(GetWorld(), ClientData->LastServerLocation, ServerLocation, ArrowSize, FColor(0, 255, 0, 255), bPersist, Lifetime);

				// Arrow showing smooth location plot
				DrawDebugDirectionalArrow(GetWorld(), ClientData->LastSmoothLocation, SmoothLocation, ArrowSize, FColor(0, 0, 255, 255), bPersist, Lifetime);

				// Line showing correction
				DrawDebugDirectionalArrow(GetWorld(), SimulatedLocation, ServerLocation, ArrowSize, FColor(128, 0, 0, 255), bPersist, Lifetime);

				// Line showing smooth vector
				DrawDebugDirectionalArrow(GetWorld(), ServerLocation, SmoothLocation, ArrowSize, FColor(0, 0, 128, 255), bPersist, Lifetime);
			}

			ClientData->LastServerLocation = ServerLocation;
			ClientData->LastSmoothLocation = SmoothLocation;
		}
#endif
	}
}

FNetworkPredictionData_Client* UPhysKartMoveComp::GetPredictionData_Client() const
{
	if (ClientPredictionData == nullptr)
	{
		UPhysKartMoveComp* MutableThis = const_cast<UPhysKartMoveComp*>(this);
		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_PhysKart(*this);
	}

	return ClientPredictionData;
}

FNetworkPredictionData_Server* UPhysKartMoveComp::GetPredictionData_Server() const
{
	if (ServerPredictionData == nullptr)
	{
		UPhysKartMoveComp* MutableThis = const_cast<UPhysKartMoveComp*>(this);
		MutableThis->ServerPredictionData = new FNetworkPredictionData_Server_PhysKart(*this);
	}

	return ServerPredictionData;
}


FNetworkPredictionData_Client_PhysKart* UPhysKartMoveComp::GetPredictionData_Client_PhysKart() const
{
	// Should only be called on client or listen server (for remote clients) in network games
	checkSlow(GetPhysKartOwner() != NULL);
	checkSlow(GetPhysKartOwner()->GetLocalRole() < ROLE_Authority || (GetPhysKartOwner()->GetRemoteRole() == ROLE_AutonomousProxy && GetNetMode() == NM_ListenServer));
	checkSlow(GetNetMode() == NM_Client || GetNetMode() == NM_ListenServer);

	if (ClientPredictionData == nullptr)
	{
		UPhysKartMoveComp* MutableThis = const_cast<UPhysKartMoveComp*>(this);
		MutableThis->ClientPredictionData = static_cast<class FNetworkPredictionData_Client_PhysKart*>(GetPredictionData_Client());
	}

	return ClientPredictionData;
}


FNetworkPredictionData_Server_PhysKart* UPhysKartMoveComp::GetPredictionData_Server_PhysKart() const
{
	// Should only be called on server in network games
	checkSlow(GetPhysKartOwner() != NULL);
	checkSlow(GetPhysKartOwner()->GetLocalRole() == ROLE_Authority);
	checkSlow(GetNetMode() < NM_Client);

	if (ServerPredictionData == nullptr)
	{
		UPhysKartMoveComp* MutableThis = const_cast<UPhysKartMoveComp*>(this);
		MutableThis->ServerPredictionData = static_cast<class FNetworkPredictionData_Server_PhysKart*>(GetPredictionData_Server());
	}

	return ServerPredictionData;
}

bool UPhysKartMoveComp::HasPredictionData_Client() const
{
	return (ClientPredictionData != nullptr) && HasValidData();
}

bool UPhysKartMoveComp::HasPredictionData_Server() const
{
	return (ServerPredictionData != nullptr) && HasValidData();
}

void UPhysKartMoveComp::ResetPredictionData_Client()
{
	ForceClientAdjustment();
	if (ClientPredictionData)
	{
		delete ClientPredictionData;
		ClientPredictionData = nullptr;
	}
}

void UPhysKartMoveComp::ResetPredictionData_Server()
{
	ForceClientAdjustment();
	if (ServerPredictionData)
	{
		delete ServerPredictionData;
		ServerPredictionData = nullptr;
	}
}

//-------------------------------------------------------------------------------------------//
// PhysX Immediate Mode 

FTransform SpaceToWorldTransform(EPhysKartSimulationSpace Space, const FTransform& ComponentToWorld, const FTransform& BaseBoneTM)
{
	switch (Space)
	{
	case EPhysKartSimulationSpace::ComponentSpace:
		return ComponentToWorld;
	case EPhysKartSimulationSpace::WorldSpace:
		return FTransform::Identity;
	case EPhysKartSimulationSpace::BaseBoneSpace:
		return BaseBoneTM * ComponentToWorld;
	default:
		return FTransform::Identity;
	}
}

FVector WorldVectorToSpaceNoScale(EPhysKartSimulationSpace Space, const FVector& WorldDir, const FTransform& ComponentToWorld, const FTransform& BaseBoneTM)
{
	switch (Space)
	{
	case EPhysKartSimulationSpace::ComponentSpace: return ComponentToWorld.InverseTransformVectorNoScale(WorldDir);
	case EPhysKartSimulationSpace::WorldSpace: return WorldDir;
	case EPhysKartSimulationSpace::BaseBoneSpace:
		return BaseBoneTM.InverseTransformVectorNoScale(ComponentToWorld.InverseTransformVectorNoScale(WorldDir));
	default: return FVector::ZeroVector;
	}
}

FVector WorldPositionToSpace(EPhysKartSimulationSpace Space, const FVector& WorldPoint, const FTransform& ComponentToWorld, const FTransform& BaseBoneTM)
{
	switch (Space)
	{
	case EPhysKartSimulationSpace::ComponentSpace: return ComponentToWorld.InverseTransformPosition(WorldPoint);
	case EPhysKartSimulationSpace::WorldSpace: return WorldPoint;
	case EPhysKartSimulationSpace::BaseBoneSpace:
		return BaseBoneTM.InverseTransformPosition(ComponentToWorld.InverseTransformPosition(WorldPoint));
	default: return FVector::ZeroVector;
	}
}

FORCEINLINE_DEBUGGABLE FTransform ConvertCSTransformToSimSpace(EPhysKartSimulationSpace SimulationSpace, const FTransform& InCSTransform, const FTransform& ComponentToWorld, const FTransform& BaseBoneTM)
{
	switch (SimulationSpace)
	{
	case EPhysKartSimulationSpace::ComponentSpace: return InCSTransform;
	case EPhysKartSimulationSpace::WorldSpace:  return InCSTransform * ComponentToWorld;
	case EPhysKartSimulationSpace::BaseBoneSpace: return InCSTransform.GetRelativeTransform(BaseBoneTM); break;
	default: ensureMsgf(false, TEXT("Unsupported Simulation Space")); return InCSTransform;
	}
}

//DECLARE_CYCLE_STAT(TEXT("UPhysKartMoveComp::UpdateWorldGeometry"), STAT_ImmediateUpdateWorldGeometry, STATGROUP_ImmediateMode);

//
	void UPhysKartMoveComp::InitSimulationSpace(
	const FTransform& ComponentToWorld,
	const FTransform& BoneToComponent)
{
	PreviousComponentToWorld = ComponentToWorld;
	PreviousBoneToComponent = BoneToComponent;
	PreviousComponentLinearVelocity = FVector::ZeroVector;
	PreviousComponentAngularVelocity = FVector::ZeroVector;
	PreviousBoneLinearVelocity = FVector::ZeroVector;
	PreviousBoneAngularVelocity = FVector::ZeroVector;
}

void UPhysKartMoveComp::CalculateSimulationSpace(
	EPhysKartSimulationSpace Space, 
	const FTransform& ComponentToWorld, 
	const FTransform& BoneToComponent,
	const float Dt,
	const FPhysKartSimSpaceSettings& Settings,
	FTransform& SpaceTransform, 
	FVector& SpaceLinearVel, 
	FVector& SpaceAngularVel, 
	FVector& SpaceLinearAcc, 
	FVector& SpaceAngularAcc)
{
	// World-space transform of the simulation space
	SpaceTransform = SpaceToWorldTransform(Space, ComponentToWorld, BoneToComponent);
	SpaceLinearVel = FVector::ZeroVector;
	SpaceAngularVel = FVector::ZeroVector;
	SpaceLinearAcc = FVector::ZeroVector;
	SpaceAngularAcc = FVector::ZeroVector;
	// If the system is disabled, nothing else to do
	if ((Settings.MasterAlpha == 0.0f) || (Dt < SMALL_NUMBER))
	{
		return;
	}
	if (Space == EPhysKartSimulationSpace::WorldSpace)
	{
		SpaceLinearVel = Settings.ExternalLinearVelocity;
		SpaceAngularVel = Settings.ExternalAngularVelocity;
		return;
	}
	// World-space component velocity and acceleration
	FVector CompLinVel = Chaos::FVec3::CalculateVelocity(PreviousComponentToWorld.GetTranslation(), ComponentToWorld.GetTranslation(), Dt);
	FVector CompAngVel = Chaos::FRotation3::CalculateAngularVelocity(PreviousComponentToWorld.GetRotation(), ComponentToWorld.GetRotation(), Dt);
	FVector CompLinAcc = (CompLinVel - PreviousComponentLinearVelocity) / Dt;
	FVector CompAngAcc = (CompAngVel - PreviousComponentAngularVelocity) / Dt;
	PreviousComponentToWorld = ComponentToWorld;
	PreviousComponentLinearVelocity = CompLinVel;
	PreviousComponentAngularVelocity = CompAngVel;
	if (Space == EPhysKartSimulationSpace::ComponentSpace)
	{
		CompLinVel.Z *= Settings.VelocityScaleZ;
		CompLinAcc.Z *= Settings.VelocityScaleZ;
		SpaceLinearVel = CompLinVel.GetClampedToMaxSize(Settings.MaxLinearVelocity) + Settings.ExternalLinearVelocity;
		SpaceAngularVel = CompAngVel.GetClampedToMaxSize(Settings.MaxAngularVelocity) + Settings.ExternalAngularVelocity;
		SpaceLinearAcc = CompLinAcc.GetClampedToMaxSize(Settings.MaxLinearAcceleration);
		SpaceAngularAcc = CompAngAcc.GetClampedToMaxSize(Settings.MaxAngularAcceleration);
		return;
	}
	
	if (Space == EPhysKartSimulationSpace::BaseBoneSpace)
	{
		// World-space component-relative bone velocity and acceleration
		FVector BoneLinVel = Chaos::FVec3::CalculateVelocity(PreviousBoneToComponent.GetTranslation(), BoneToComponent.GetTranslation(), Dt);
		FVector BoneAngVel = Chaos::FRotation3::CalculateAngularVelocity(PreviousBoneToComponent.GetRotation(), BoneToComponent.GetRotation(), Dt);
		BoneLinVel = ComponentToWorld.TransformVector(BoneLinVel);
		BoneAngVel = ComponentToWorld.TransformVector(BoneAngVel);
		FVector BoneLinAcc = (BoneLinVel - PreviousBoneLinearVelocity) / Dt;
		FVector BoneAngAcc = (BoneAngVel - PreviousBoneAngularVelocity) / Dt;
		PreviousBoneToComponent = BoneToComponent;
		PreviousBoneLinearVelocity = BoneLinVel;
		PreviousBoneAngularVelocity = BoneAngVel;
		// World-space bone velocity and acceleration
		FVector NetAngVel = CompAngVel + BoneAngVel;
		FVector NetAngAcc = CompAngAcc + BoneAngAcc;
		// If we limit the angular velocity, we also need to limit the component of linear velocity that comes from (angvel x offset)
		float AngVelScale = 1.0f;
		float NetAngVelLenSq = NetAngVel.SizeSquared();
		if (NetAngVelLenSq > FMath::Square(Settings.MaxAngularVelocity))
		{
			AngVelScale = Settings.MaxAngularVelocity * FMath::InvSqrt(NetAngVelLenSq);
		}
		// Add the linear velocity and acceleration that comes from rotation of the space about the component
		// NOTE: Component angular velocity constribution is scaled
		FVector SpaceCompOffset = ComponentToWorld.TransformVector(BoneToComponent.GetTranslation());
		FVector NetLinVel = CompLinVel + BoneLinVel + FVector::CrossProduct(AngVelScale * CompAngVel, SpaceCompOffset);
		FVector NetLinAcc = CompLinAcc + BoneLinAcc + FVector::CrossProduct(AngVelScale * CompAngAcc, SpaceCompOffset);
		NetLinVel.Z *= Settings.VelocityScaleZ;
		NetLinAcc.Z *= Settings.VelocityScaleZ;
		SpaceLinearVel = NetLinVel.GetClampedToMaxSize(Settings.MaxLinearVelocity) + Settings.ExternalLinearVelocity;
		SpaceAngularVel = NetAngVel.GetClampedToMaxSize(Settings.MaxAngularVelocity) + Settings.ExternalAngularVelocity;
		SpaceLinearAcc = NetLinAcc.GetClampedToMaxSize(Settings.MaxLinearAcceleration);
		SpaceAngularAcc = NetAngAcc.GetClampedToMaxSize(Settings.MaxAngularAcceleration);
		return;
	}
}
//

void UPhysKartMoveComp::UpdateWorldGeometry(const UWorld& World)	//, const USkeletalMeshComponent& SKC)
{
	//SCOPE_CYCLE_COUNTER(STAT_ImmediateUpdateWorldGeometry);
	QueryParams = FCollisionQueryParams(SCENE_QUERY_STAT(RagdollNodeFindGeometry), /*bTraceComplex=*/false);
	//#if WITH_EDITOR
	//	if (!World.IsGameWorld())
	//	{
	//		QueryParams.MobilityType = EQueryMobilityType::Any;	//If we're in some preview world trace against everything because things like the preview floor are not static
	//		//QueryParams.AddIgnoredComponent(&SKC);
	//	}
	//	else
	//#endif
	{
		QueryParams.MobilityType = EQueryMobilityType::Any;				//EQueryMobilityType::Static;	//We only want static actors
	}

	//if (SKC.IsValid())
	//{
	//	FSphere Bounds = SKC.CalcBounds(SKC.GetComponentToWorld()).GetSphere();
	//}
	//else
	FSphere Bounds = UpdatedComponent->CalcBounds(UpdatedComponent->GetComponentToWorld()).GetSphere();
	

	if (!Bounds.IsInside(CachedBounds))
	{
		// Since the cached bounds are no longer valid, update them.

		CachedBounds = Bounds;
		CachedBounds.W *= CachedBoundsScale;

		// Cache the PhysScene and World for use in UpdateWorldForces.
		PhysScene = World.GetPhysicsScene();
		UnsafeWorld = &World;
	}
}

//DECLARE_CYCLE_STAT(TEXT("UPhysKartMoveComp::UpdateWorldForces"), STAT_ImmediateUpdateWorldForces, STATGROUP_ImmediateMode);

void UPhysKartMoveComp::UpdateWorldForces(const FTransform& ComponentToWorld, const FTransform& BaseBoneTM)
{
	//SCOPE_CYCLE_COUNTER(STAT_ImmediateUpdateWorldForces);

	return; // PaCorp * 

	if (TotalMass > 0.f)
	{
		for (const USkeletalMeshComponent::FPendingRadialForces& PendingRadialForce : PendingRadialForces)
		{
			const FVector RadialForceOrigin = WorldPositionToSpace(SimulationSpace, PendingRadialForce.Origin, ComponentToWorld, BaseBoneTM);
			for (ImmediatePhysics::FActorHandle* Body : Bodies)
			{
				const float InvMass = Body->GetInverseMass();
				if (InvMass > 0.f)
				{
					const float StrengthPerBody = PendingRadialForce.bIgnoreMass ? PendingRadialForce.Strength : PendingRadialForce.Strength / (TotalMass * InvMass);
					ImmediatePhysics::EForceType ForceType;
					if (PendingRadialForce.Type == USkeletalMeshComponent::FPendingRadialForces::AddImpulse)
					{
						ForceType = PendingRadialForce.bIgnoreMass ? ImmediatePhysics::EForceType::AddVelocity : ImmediatePhysics::EForceType::AddImpulse;
					}
					else
					{
						ForceType = PendingRadialForce.bIgnoreMass ? ImmediatePhysics::EForceType::AddAcceleration : ImmediatePhysics::EForceType::AddForce;
					}

					Body->AddRadialForce(RadialForceOrigin, StrengthPerBody, PendingRadialForce.Radius, PendingRadialForce.Falloff, ForceType);
				}
			}
		}

		if (!ExternalForce.IsNearlyZero())
		{
			const FVector ExternalForceInSimSpace = WorldVectorToSpaceNoScale(SimulationSpace, ExternalForce, ComponentToWorld, BaseBoneTM);
			for (ImmediatePhysics::FActorHandle* Body : Bodies)
			{
				const float InvMass = Body->GetInverseMass();
				if (InvMass > 0.f)
				{
					Body->AddForce(ExternalForceInSimSpace);
				}
			}
		}
	}
}

void ComputeBodyInsertionOrder(TArray<FBoneIndexType>& InsertionOrder, const USkeletalMeshComponent& SKC)
{
	//We want to ensure simulated bodies are sorted by LOD so that the first simulated bodies are at the highest LOD.
	//Since LOD2 is a subset of LOD1 which is a subset of LOD0 we can change the number of simulated bodies without any reordering
	//For this to work we must first insert all simulated bodies in the right order. We then insert all the kinematic bodies in the right order

	if (!SKC.IsValidLowLevel())
	{
		return;
	}

	InsertionOrder.Reset();

	if (SKC.SkeletalMesh == nullptr)		// PaCorp * 4.26
	{
		return;
	}

	const int32 NumLODs = SKC.GetNumLODs();
	if (NumLODs > 0)
	{
		//TArray<bool> InSortedOrder;

		TArray<FBoneIndexType> RequiredBones0;
		TArray<FBoneIndexType> ComponentSpaceTMs0;
		SKC.ComputeRequiredBones(RequiredBones0, ComponentSpaceTMs0, 0, /*bIgnorePhysicsAsset=*/ true);

		//InSortedOrder.AddZeroed(RequiredBones0.Num());		// PaCorp * 4.26 sub
		// PaCorp * 4.26
		TArray<bool> InSortedOrder;
		InSortedOrder.AddZeroed(SKC.SkeletalMesh->RefSkeleton.GetNum());
		//

		auto MergeIndices = [&InsertionOrder, &InSortedOrder](const TArray<FBoneIndexType>& RequiredBones) -> void
		{
			for (FBoneIndexType BoneIdx : RequiredBones)
			{
				if (!InSortedOrder[BoneIdx])
				{
					InsertionOrder.Add(BoneIdx);
				}

				InSortedOrder[BoneIdx] = true;
			}
		};


		for (int32 LodIdx = NumLODs - 1; LodIdx > 0; --LodIdx)
		{
			TArray<FBoneIndexType> RequiredBones;
			TArray<FBoneIndexType> ComponentSpaceTMs;
			SKC.ComputeRequiredBones(RequiredBones, ComponentSpaceTMs, LodIdx, /*bIgnorePhysicsAsset=*/ true);
			MergeIndices(RequiredBones);
		}

		MergeIndices(RequiredBones0);
	}
}

//void UPhysKartMoveComp::InitPhysics_ImmediateMode(const UAnimInstance* InAnimInstance)
void UPhysKartMoveComp::InitPhysics_ImmediateMode()
{
	delete PhysicsSimulation;
	PhysicsSimulation = nullptr;

	//InAnimInstance->GetSkelMeshComponent();

	//USkeletalMeshComponent* SkelComp = nullptr;
	if (!GetPhysKartOwner() || !GetPhysKartOwner()->GetPhysKartRoot())
	{
		return;
	}

	if (GetPhysKartOwner()->GetPhysKartRoot()->IsA(UBoxComponent::StaticClass()))
	{
		InitPhysics_ImmediateMode_BoxComponent();
	}

	if (GetPhysKartOwner()->GetPhysKartRoot()->IsA(UStaticMeshComponent::StaticClass()))
	{
		InitPhysics_ImmediateMode_StaticMeshComponent();
	}

	if (GetPhysKartOwner()->GetPhysKartRoot()->IsA(USkeletalMeshComponent::StaticClass()))
	{
		InitPhysics_ImmediateMode_SkeletalMeshComponent();
	}
}

void UPhysKartMoveComp::InitPhysics_ImmediateMode_BoxComponent()
{
	const USkeletalMeshComponent* SkeletalMeshComp = nullptr;		//InAnimInstance->GetSkelMeshComponent();
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsA(USkeletalMeshComponent::StaticClass()))
	{
		SkeletalMeshComp = Cast<USkeletalMeshComponent>(GetPhysKartOwner()->GetPhysKartRoot());
	}

	const USkeletalMesh* SkeletalMeshAsset = SkeletalMeshComp ? SkeletalMeshComp->SkeletalMesh : nullptr;

	const FReferenceSkeleton& SkelMeshRefSkel = SkeletalMeshAsset->RefSkeleton;
	UsePhysicsAsset = OverridePhysicsAsset ? OverridePhysicsAsset : SkeletalMeshComp ? SkeletalMeshComp->GetPhysicsAsset() : nullptr;		//InAnimInstance->GetSkelMeshComponent()->GetPhysicsAsset();

	USkeleton* SkeletonAsset = SkeletalMeshComp && SkeletalMeshComp->SkeletalMesh && SkeletalMeshComp->SkeletalMesh->Skeleton ? SkeletalMeshComp->SkeletalMesh->Skeleton : nullptr;		//InAnimInstance->CurrentSkeleton;

	PreviousTransform = GetPhysKartOwner()->GetPhysKartRoot()->GetComponentToWorld();

	ComponentsInSim.Reset();
	ComponentsInSimTick = 0;

	if (UPhysicsSettings* Settings = UPhysicsSettings::Get())
	{
		AnimPhysicsMinDeltaTime = Settings->AnimPhysicsMinDeltaTime;
		bSimulateAnimPhysicsAfterReset = Settings->bSimulateAnimPhysicsAfterReset;
	}
	else
	{
		AnimPhysicsMinDeltaTime = 0.f;
		bSimulateAnimPhysicsAfterReset = false;
	}

	//

	
	bEnabled = GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot();
	if (bEnabled)
	{
		PhysicsSimulation = new ImmediatePhysics::FSimulation();

		const int32 NumBodies = 1;	
		Bodies.Empty(NumBodies);
		//ComponentsInSim.Reset();
		BodyAnimData.Reset(NumBodies);
		BodyAnimData.AddDefaulted(NumBodies);

		// Instantiate a FBodyInstance/FConstraintInstance set that will be cloned into the Immediate Physics sim.
		// NOTE: We do not have a skeleton at the moment, so we have to use the ref pose
		TArray<FBodyInstance*> HighLevelBodyInstances;
		TArray<FConstraintInstance*> HighLevelConstraintInstances;

		// Chaos relies on the initial pose to set up constraint positions
		bool bCreateBodiesInRefPose = (WITH_CHAOS != 0);

		TMap<FName, ImmediatePhysics::FActorHandle*> NamesToHandles;
		TArray<ImmediatePhysics::FActorHandle*> IgnoreCollisionActors;

		TArray<FBoneIndexType> InsertionOrder;
		ComputeBodyInsertionOrder(InsertionOrder, *SkeletalMeshComp);

		// NOTE: NumBonesLOD0 may be less than NumBonesTotal, and it may be middle bones that are missing from LOD0.
		// In this case, LOD0 bone indices may be >= NumBonesLOD0, but always < NumBonesTotal. Arrays indexed by
		// bone index must be size NumBonesTotal.
		const int32 NumBonesLOD0 = InsertionOrder.Num();
		const int32 NumBonesTotal = SkelMeshRefSkel.GetNum();

		// If our skeleton is not the one that was used to build the PhysicsAsset, some bodies may be missing, or rearranged.
		// We need to map the original indices to the new bodies for use by the CollisionDisableTable.
		// NOTE: This array is indexed by the original BodyInstance body index (BodyInstance->InstanceBodyIndex)
		TArray<ImmediatePhysics::FActorHandle*> BodyIndexToActorHandle;
		if (HighLevelBodyInstances.Num() > 0)
		{
			BodyIndexToActorHandle.AddZeroed(HighLevelBodyInstances.Num());
		}

		// PaCorp * No Skeletal
		if (1)
		{
			FBodyInstance* BodyInstance = GetPhysKartOwner()->GetPhysKartRoot()->GetBodyInstance();

			bool bSimulated = BodyInstance && BodyInstance->BodySetup.IsValid() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics() && (BodyInstance->BodySetup->PhysicsType == EPhysicsType::PhysType_Default);
			ImmediatePhysics::EActorType ActorType = bSimulated ? ImmediatePhysics::EActorType::DynamicActor : ImmediatePhysics::EActorType::KinematicActor;
			ImmediatePhysics::FActorHandle* NewBodyHandle = PhysicsSimulation->CreateActor(ActorType, BodyInstance, BodyInstance->GetUnrealWorldTransform());
			if (NewBodyHandle)
			{
				if (bSimulated)
				{
					const float InvMass = NewBodyHandle->GetInverseMass();
					TotalMass += InvMass > 0.f ? 1.f / InvMass : 0.f;
				}
				const int32 BodyIndex = Bodies.Add(NewBodyHandle);
				UpdatedComponentBodyIndex = BodyIndex;
				BodyAnimData[BodyIndex].bIsSimulated = bSimulated;

//#if WITH_CHAOS
//				NewBodyHandle->SetName(BodySetup->BoneName);
//#endif
			}

			TArray<ImmediatePhysics::FSimulation::FIgnorePair> IgnorePairs;
			if (UsePhysicsAsset)
			{
				const TMap<FRigidBodyIndexPair, bool>& DisableTable = UsePhysicsAsset->CollisionDisableTable;
				if (DisableTable.Num() > 0)
				{
					for (auto ConstItr = DisableTable.CreateConstIterator(); ConstItr; ++ConstItr)
					{
						int32 IndexA = ConstItr.Key().Indices[0];
						int32 IndexB = ConstItr.Key().Indices[1];
						if ((IndexA < BodyIndexToActorHandle.Num()) && (IndexB < BodyIndexToActorHandle.Num()))
						{
							if ((BodyIndexToActorHandle[IndexA] != nullptr) && (BodyIndexToActorHandle[IndexB] != nullptr))
							{
								ImmediatePhysics::FSimulation::FIgnorePair Pair;
								Pair.A = BodyIndexToActorHandle[IndexA];
								Pair.B = BodyIndexToActorHandle[IndexB];
								IgnorePairs.Add(Pair);
							}
						}
					}
				}
			}

			PhysicsSimulation->SetIgnoreCollisionPairTable(IgnorePairs);
			PhysicsSimulation->SetIgnoreCollisionActors(IgnoreCollisionActors);
		}
	}
}

void UPhysKartMoveComp::InitPhysics_ImmediateMode_StaticMeshComponent()
{
	const USkeletalMeshComponent* SkeletalMeshComp = nullptr;		//InAnimInstance->GetSkelMeshComponent();
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsA(USkeletalMeshComponent::StaticClass()))
	{
		SkeletalMeshComp = Cast<USkeletalMeshComponent>(GetPhysKartOwner()->GetPhysKartRoot());
	}

	const USkeletalMesh* SkeletalMeshAsset = SkeletalMeshComp ? SkeletalMeshComp->SkeletalMesh : nullptr;

	const FReferenceSkeleton& SkelMeshRefSkel = SkeletalMeshAsset->RefSkeleton;
	UsePhysicsAsset = OverridePhysicsAsset ? OverridePhysicsAsset : SkeletalMeshComp ? SkeletalMeshComp->GetPhysicsAsset() : nullptr;		//InAnimInstance->GetSkelMeshComponent()->GetPhysicsAsset();

	USkeleton* SkeletonAsset = SkeletalMeshComp && SkeletalMeshComp->SkeletalMesh && SkeletalMeshComp->SkeletalMesh->Skeleton ? SkeletalMeshComp->SkeletalMesh->Skeleton : nullptr;		//InAnimInstance->CurrentSkeleton;

	PreviousTransform = GetPhysKartOwner()->GetPhysKartRoot()->GetComponentToWorld();

	ComponentsInSim.Reset();
	ComponentsInSimTick = 0;

	if (UPhysicsSettings* Settings = UPhysicsSettings::Get())
	{
		AnimPhysicsMinDeltaTime = Settings->AnimPhysicsMinDeltaTime;
		bSimulateAnimPhysicsAfterReset = Settings->bSimulateAnimPhysicsAfterReset;
	}
	else
	{
		AnimPhysicsMinDeltaTime = 0.f;
		bSimulateAnimPhysicsAfterReset = false;
	}

	//

	bEnabled = GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot();		//&& CVarEnableRigidBodyNode.GetValueOnAnyThread() != 0;
	if (bEnabled)
	{
		PhysicsSimulation = new ImmediatePhysics::FSimulation();

		const int32 NumBodies = 1;
		Bodies.Empty(NumBodies);
		//ComponentsInSim.Reset();
		BodyAnimData.Reset(NumBodies);
		BodyAnimData.AddDefaulted(NumBodies);

		// Instantiate a FBodyInstance/FConstraintInstance set that will be cloned into the Immediate Physics sim.
		// NOTE: We do not have a skeleton at the moment, so we have to use the ref pose
		TArray<FBodyInstance*> HighLevelBodyInstances;
		TArray<FConstraintInstance*> HighLevelConstraintInstances;

		// Chaos relies on the initial pose to set up constraint positions
		bool bCreateBodiesInRefPose = (WITH_CHAOS != 0);		

		TMap<FName, ImmediatePhysics::FActorHandle*> NamesToHandles;
		TArray<ImmediatePhysics::FActorHandle*> IgnoreCollisionActors;

		TArray<FBoneIndexType> InsertionOrder;
		ComputeBodyInsertionOrder(InsertionOrder, *SkeletalMeshComp);

		// NOTE: NumBonesLOD0 may be less than NumBonesTotal, and it may be middle bones that are missing from LOD0.
		// In this case, LOD0 bone indices may be >= NumBonesLOD0, but always < NumBonesTotal. Arrays indexed by
		// bone index must be size NumBonesTotal.
		const int32 NumBonesLOD0 = InsertionOrder.Num();
		const int32 NumBonesTotal = SkelMeshRefSkel.GetNum();

		// If our skeleton is not the one that was used to build the PhysicsAsset, some bodies may be missing, or rearranged.
		// We need to map the original indices to the new bodies for use by the CollisionDisableTable.
		// NOTE: This array is indexed by the original BodyInstance body index (BodyInstance->InstanceBodyIndex)
		TArray<ImmediatePhysics::FActorHandle*> BodyIndexToActorHandle;
		if (HighLevelBodyInstances.Num() > 0)
		{
			BodyIndexToActorHandle.AddZeroed(HighLevelBodyInstances.Num());
		}

		// PaCorp * No Skeletal
		if (1)
		{
			FBodyInstance* BodyInstance = GetPhysKartOwner()->GetPhysKartRoot()->GetBodyInstance();

			bool bSimulated = BodyInstance && BodyInstance->BodySetup.IsValid() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics() && (BodyInstance->BodySetup->PhysicsType == EPhysicsType::PhysType_Default);
			ImmediatePhysics::EActorType ActorType = bSimulated ? ImmediatePhysics::EActorType::DynamicActor : ImmediatePhysics::EActorType::KinematicActor;
			ImmediatePhysics::FActorHandle* NewBodyHandle = PhysicsSimulation->CreateActor(ActorType, BodyInstance, BodyInstance->GetUnrealWorldTransform());
			if (NewBodyHandle)
			{
				if (bSimulated)
				{
					const float InvMass = NewBodyHandle->GetInverseMass();
					TotalMass += InvMass > 0.f ? 1.f / InvMass : 0.f;
				}

				const int32 BodyIndex = Bodies.Add(NewBodyHandle);
				UpdatedComponentBodyIndex = BodyIndex;
				BodyAnimData[BodyIndex].bIsSimulated = bSimulated;

//#if WITH_CHAOS
//				NewBodyHandle->SetName(BodySetup->BoneName);
//#endif
			}

			TArray<ImmediatePhysics::FSimulation::FIgnorePair> IgnorePairs;
			if (UsePhysicsAsset)
			{
				const TMap<FRigidBodyIndexPair, bool>& DisableTable = UsePhysicsAsset->CollisionDisableTable;
				if (DisableTable.Num() > 0)
				{
					for (auto ConstItr = DisableTable.CreateConstIterator(); ConstItr; ++ConstItr)
					{
						int32 IndexA = ConstItr.Key().Indices[0];
						int32 IndexB = ConstItr.Key().Indices[1];
						if ((IndexA < BodyIndexToActorHandle.Num()) && (IndexB < BodyIndexToActorHandle.Num()))
						{
							if ((BodyIndexToActorHandle[IndexA] != nullptr) && (BodyIndexToActorHandle[IndexB] != nullptr))
							{
								ImmediatePhysics::FSimulation::FIgnorePair Pair;
								Pair.A = BodyIndexToActorHandle[IndexA];
								Pair.B = BodyIndexToActorHandle[IndexB];
								IgnorePairs.Add(Pair);
							}
						}
					}
				}
			}

			PhysicsSimulation->SetIgnoreCollisionPairTable(IgnorePairs);
			PhysicsSimulation->SetIgnoreCollisionActors(IgnoreCollisionActors);
		}
	}
}

void UPhysKartMoveComp::InitPhysics_ImmediateMode_SkeletalMeshComponent()
{
	const USkeletalMeshComponent* SkeletalMeshComp = nullptr;		//InAnimInstance->GetSkelMeshComponent();
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsA(USkeletalMeshComponent::StaticClass()))
	{
		SkeletalMeshComp = Cast<USkeletalMeshComponent>(GetPhysKartOwner()->GetPhysKartRoot());
	}

	const USkeletalMesh* SkeletalMeshAsset = SkeletalMeshComp ? SkeletalMeshComp->SkeletalMesh : nullptr;

	const FReferenceSkeleton& SkelMeshRefSkel = SkeletalMeshAsset->RefSkeleton;
	UsePhysicsAsset = OverridePhysicsAsset ? OverridePhysicsAsset : SkeletalMeshComp ? SkeletalMeshComp->GetPhysicsAsset() : nullptr;		//InAnimInstance->GetSkelMeshComponent()->GetPhysicsAsset();

	USkeleton* SkeletonAsset = SkeletalMeshComp && SkeletalMeshComp->SkeletalMesh && SkeletalMeshComp->SkeletalMesh->Skeleton ? SkeletalMeshComp->SkeletalMesh->Skeleton : nullptr;		//InAnimInstance->CurrentSkeleton;

	PreviousTransform = GetPhysKartOwner()->GetPhysKartRoot()->GetComponentToWorld();

	ComponentsInSim.Reset();
	ComponentsInSimTick = 0;

	if (UPhysicsSettings* Settings = UPhysicsSettings::Get())
	{
		AnimPhysicsMinDeltaTime = Settings->AnimPhysicsMinDeltaTime;
		bSimulateAnimPhysicsAfterReset = Settings->bSimulateAnimPhysicsAfterReset;
	}
	else
	{
		AnimPhysicsMinDeltaTime = 0.f;
		bSimulateAnimPhysicsAfterReset = false;
	}

	//

	bEnabled = GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot();	
	if (bEnabled)
	{
		PhysicsSimulation = new ImmediatePhysics::FSimulation();

		const int32 NumBodies = 1;
		Bodies.Empty(NumBodies);
		//ComponentsInSim.Reset();
		BodyAnimData.Reset(NumBodies);
		BodyAnimData.AddDefaulted(NumBodies);

		// Instantiate a FBodyInstance/FConstraintInstance set that will be cloned into the Immediate Physics sim.
		// NOTE: We do not have a skeleton at the moment, so we have to use the ref pose
		TArray<FBodyInstance*> HighLevelBodyInstances;
		TArray<FConstraintInstance*> HighLevelConstraintInstances;

		// Chaos relies on the initial pose to set up constraint positions
		bool bCreateBodiesInRefPose = (WITH_CHAOS != 0);

		TMap<FName, ImmediatePhysics::FActorHandle*> NamesToHandles;
		TArray<ImmediatePhysics::FActorHandle*> IgnoreCollisionActors;

		TArray<FBoneIndexType> InsertionOrder;
		ComputeBodyInsertionOrder(InsertionOrder, *SkeletalMeshComp);

		// NOTE: NumBonesLOD0 may be less than NumBonesTotal, and it may be middle bones that are missing from LOD0.
		// In this case, LOD0 bone indices may be >= NumBonesLOD0, but always < NumBonesTotal. Arrays indexed by
		// bone index must be size NumBonesTotal.
		const int32 NumBonesLOD0 = InsertionOrder.Num();
		const int32 NumBonesTotal = SkelMeshRefSkel.GetNum();

		// If our skeleton is not the one that was used to build the PhysicsAsset, some bodies may be missing, or rearranged.
		// We need to map the original indices to the new bodies for use by the CollisionDisableTable.
		// NOTE: This array is indexed by the original BodyInstance body index (BodyInstance->InstanceBodyIndex)
		TArray<ImmediatePhysics::FActorHandle*> BodyIndexToActorHandle;
		if (HighLevelBodyInstances.Num() > 0)
		{
			BodyIndexToActorHandle.AddZeroed(HighLevelBodyInstances.Num());
		}

		// PaCorp * No Skeletal
		if (1)
		{
			FBodyInstance* BodyInstance = GetPhysKartOwner()->GetPhysKartRoot()->GetBodyInstance();
			//FBodyInstance* BodyInstance = GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->Bodies.Num() > 0 ? GetPhysKartOwner()->GetPhysKartRoot()->Bodies[0] : nullptr;

			bool bSimulated = BodyInstance && BodyInstance->BodySetup.IsValid() && GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics() && (BodyInstance->BodySetup->PhysicsType == EPhysicsType::PhysType_Default);
			ImmediatePhysics::EActorType ActorType = bSimulated ? ImmediatePhysics::EActorType::DynamicActor : ImmediatePhysics::EActorType::KinematicActor;
			ImmediatePhysics::FActorHandle* NewBodyHandle = PhysicsSimulation->CreateActor(ActorType, BodyInstance, BodyInstance->GetUnrealWorldTransform());
			if (NewBodyHandle)
			{

				if (bSimulated)
				{
					const float InvMass = NewBodyHandle->GetInverseMass();
					TotalMass += InvMass > 0.f ? 1.f / InvMass : 0.f;
				}

				const int32 BodyIndex = Bodies.Add(NewBodyHandle);
				UpdatedComponentBodyIndex = BodyIndex;
				BodyAnimData[BodyIndex].bIsSimulated = bSimulated;

//#if WITH_CHAOS
//				NewBodyHandle->SetName(BodySetup->BoneName);
//#endif
			}

			TArray<ImmediatePhysics::FSimulation::FIgnorePair> IgnorePairs;
			if (UsePhysicsAsset)
			{
				const TMap<FRigidBodyIndexPair, bool>& DisableTable = UsePhysicsAsset->CollisionDisableTable;
				if (DisableTable.Num() > 0)
				{
					for (auto ConstItr = DisableTable.CreateConstIterator(); ConstItr; ++ConstItr)
					{
						int32 IndexA = ConstItr.Key().Indices[0];
						int32 IndexB = ConstItr.Key().Indices[1];
						if ((IndexA < BodyIndexToActorHandle.Num()) && (IndexB < BodyIndexToActorHandle.Num()))
						{
							if ((BodyIndexToActorHandle[IndexA] != nullptr) && (BodyIndexToActorHandle[IndexB] != nullptr))
							{
								ImmediatePhysics::FSimulation::FIgnorePair Pair;
								Pair.A = BodyIndexToActorHandle[IndexA];
								Pair.B = BodyIndexToActorHandle[IndexB];
								IgnorePairs.Add(Pair);
							}
						}
					}
				}
			}

			PhysicsSimulation->SetIgnoreCollisionPairTable(IgnorePairs);
			PhysicsSimulation->SetIgnoreCollisionActors(IgnoreCollisionActors);
		}
	}
}

void UPhysKartMoveComp::PreUpdate_ImmediateMode()		// (const UAnimInstance* InAnimInstance)
{
	// Don't update geometry if RBN is disabled
	if (!bEnabled)
	{
		return;
	}

	if (!PhysicsSimulation)
	{
		return;
	}

	//SCOPE_CYCLE_COUNTER(STAT_RigidBody_PreUpdate);

	//FBodyInstance* RootBI = nullptr;
	//UBoxComponent* PhysKartRootComponent = nullptr;
	//UStaticMeshComponent* PhysKartRootComponent = nullptr;
	USkeletalMeshComponent* PhysKartRootComponent = nullptr;
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot())
	{
		PhysKartRootComponent = GetPhysKartOwner()->GetPhysKartRoot();		// ->GetBodyInstance();
	}
	else
	{
		return;
	}

	//APawn* PawnOwner = InAnimInstance->TryGetPawnOwner();
	//UPawnMovementComponent* MovementComp = PawnOwner ? PawnOwner->GetMovementComponent() : nullptr;

//#if WITH_EDITOR
//	if (bEnableWorldGeometry && SimulationSpace != EPhysKartSimulationSpace::WorldSpace)
//	{
//		FMessageLog("PIE").Warning(FText::Format(LOCTEXT("WorldCollisionComponentSpace", "Trying to use world collision without world space simulation for ''{0}''. This is not supported, please change SimulationSpace to WorldSpace"),
//			FText::FromString(GetPathNameSafe(SKC))));
//	}
//#endif

	ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;

	UWorld* World = GetPhysKartOwner()->GetWorld();		//InAnimInstance->GetWorld();
	if (World)
	{
		//WorldSpaceGravity = bOverrideWorldGravity ? OverrideWorldGravity : FVector(0.f, 0.f, GetGravityZ());		//(MovementComp ? FVector(0.f, 0.f, MovementComp->GetGravityZ()) : FVector(0.f, 0.f, World->GetGravityZ()));

		if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot())
		{
			if (GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics())
			{
				WorldSpaceGravity = bOverrideWorldGravity ? OverrideWorldGravity :
					GetPhysKartOwner()->GetPhysKartRoot()->IsGravityEnabled() ?
					FVector(0.f, 0.f, GetGravityZ()) : FVector::ZeroVector;
			}
			else
			{
				WorldSpaceGravity = bOverrideWorldGravity ? OverrideWorldGravity : FVector(0.f, 0.f, GetGravityZ());
			}
		}


		if (PhysKartRootComponent)
		{
			if (PhysicsSimulation && bEnableWorldGeometry && SimulationSpace == EPhysKartSimulationSpace::WorldSpace)
			{
				UpdateWorldGeometry(*World);	// , * SKC);
			}

			//PendingRadialForces = SKC->GetPendingRadialForces();

			PreviousTransform = CurrentTransform;
			//CurrentTransform = BoxComp->GetComponentToWorld();
			CurrentTransform = PhysKartRootComponent->GetComponentToWorld();

			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::PreUpdate_ImmediateMode()")));
			}
		}
	}
}

void UPhysKartMoveComp::UpdateInternal_ImmediateMode(float DeltaSeconds)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(UpdateInternal)
	// Avoid this work if RBN is disabled, as the results would be discarded
	if (!bEnabled)
	{
		return;
	}

	if (!PhysicsSimulation)
	{
		return;
	}

	// SCOPE_CYCLE_COUNTER(STAT_RigidBody_Update);

	// Accumulate deltatime elapsed during update. To be used during evaluation.
	AccumulatedDeltaTime += DeltaSeconds;	// Should get last DeltaSeconds	// Context.AnimInstanceProxy->GetDeltaSeconds();

	if (UnsafeWorld != nullptr)
	{
		// Node is valid to evaluate. Simulation is starting.
		bSimulationStarted = true;

		TArray<FOverlapResult> Overlaps;
		//UnsafeWorld->OverlapMultiByChannel(Overlaps, Bounds.Center, FQuat::Identity, OverlapChannel, FCollisionShape::MakeSphere(Bounds.W), QueryParams, FCollisionResponseParams(ECR_Overlap));
		UnsafeWorld->OverlapMultiByChannel(Overlaps, CachedBounds.Center, FQuat::Identity, OverlapChannel, FCollisionShape::MakeSphere(CachedBounds.W), QueryParams, FCollisionResponseParams(ECR_Overlap));

		// @todo(ccaulfield): is there an engine-independent way to do this?
#if WITH_PHYSX && PHYSICS_INTERFACE_PHYSX
		SCOPED_SCENE_READ_LOCK(PhysScene ? PhysScene->GetPxScene() : nullptr); //TODO: expose this part to the anim node
#endif

		TArray<FOverlapResult> CurrentOverlaps;
		for (const FOverlapResult& Overlap : Overlaps)
		{
			if (UPrimitiveComponent* OverlapComp = Overlap.GetComponent())
			{
				if (ComponentsInSim.Contains(OverlapComp) == false)
				{
					if (&OverlapComp->BodyInstance != GetPhysKartOwner()->GetPhysKartRoot()->GetBodyInstance())
					{
						ComponentsInSim.Add(OverlapComp);

						PhysicsSimulation->CreateActor(ImmediatePhysics::EActorType::StaticActor, &OverlapComp->BodyInstance, OverlapComp->BodyInstance.GetUnrealWorldTransform());

						CurrentOverlaps.Add(Overlap);
					}
				}
			}
		}
		UnsafeWorld = nullptr;
		PhysScene = nullptr;
	}
}

void UPhysKartMoveComp::PostUpdate_ImmediateMode()
{
	//DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(UpdateInternal)
	// Avoid this work if RBN is disabled, as the results would be discarded
	if (!bEnabled)
	{
		return;
	}

	if (!PhysicsSimulation)
	{
		return;
	}

	if (GetPhysKartOwner() && GetPhysKartOwner()->GetPhysKartRoot() && GetPhysKartOwner()->GetPhysKartRoot()->IsA(USkeletalMeshComponent::StaticClass()))
	{
		return;
	}
	else
	{
		for (int32 c = 0; c < PhysicsSimulation->NumActors(); c++)
		{
			if (c != UpdatedComponentBodyIndex)
			{
				PhysicsSimulation->RemoveActor(PhysicsSimulation->GetActorHandle(c));
			}
		}
	}

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::PostUpdate_ImmediateMode()")));
	}
}


//void UPhysKartMoveComp::InitializeBoneReferences(const FBoneContainer& RequiredBones)
//{
//
//}


bool UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode()
{
	SCOPE_CYCLE_COUNTER(STAT_PhysKartMovementClientUpdatePositionAfterServerUpdate);
	if (!HasValidData())
	{
		return false;
	}

	FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
	check(ClientData);

	if (!ClientData->bUpdatePosition)
	{
		return false;
	}

	ClientData->bUpdatePosition = false;

	// Don't do any network position updates on things NOT running PHYS_RigidBody
	if (GetPhysKartOwner() && GetPhysKartOwner()->GetRootComponent() && !GetPhysKartOwner()->GetRootComponent()->IsSimulatingPhysics())
	{
		return false;
	}

	if (ClientData->SavedMoves.Num() == 0)
	{
		UE_LOG(LogNetPlayerMovement, Verbose, TEXT("ClientUpdatePositionAfterServerUpdate No saved moves to replay"), ClientData->SavedMoves.Num());

		// With no saved moves to resimulate, the move the server updated us with is the last move we've done, no resimulation needed.
		//GetPhysKartOwner()->bClientResimulateRootMotion = false;
		//if (GetPhysKartOwner()->bClientResimulateRootMotionSources)
		//{
		//	// With no resimulation, we just update our current root motion to what the server sent us
		//	UE_LOG(LogRootMotion, VeryVerbose, TEXT("CurrentRootMotion getting updated to ServerUpdate state: %s"), *GetPhysKartOwner()->GetName());
		//	CurrentRootMotion.UpdateStateFrom(GetPhysKartOwner()->SavedRootMotion);
		//	GetPhysKartOwner()->bClientResimulateRootMotionSources = false;
		//}

		return false;
	}

	// Save important values that might get affected by the replay.
	const float SavedAnalogInputModifier = AnalogInputModifier;

	//const FRootMotionMovementParams BackupRootMotionParams = RootMotionParams; // For animation root motion
	//const FRootMotionSourceGroup BackupRootMotion = CurrentRootMotion;

	//const bool bRealJump = GetPhysKartOwner()->bPressedJump;
	//const bool bRealCrouch = bWantsToCrouch;
	const bool bRealForceMaxAccel = bForceMaxAccel;
	//GetPhysKartOwner()->bClientWasFalling = (MovementMode == MOVE_Falling);

	const bool bRealItem = GetPhysKartOwner()->bPressedItem;

	GetPhysKartOwner()->bClientUpdating = true;

	//bForceNextFloorCheck = true;

	//bool bRootIsSimulatingPhysics = GetPhysKartOwner()->GetPhysKartRoot()->IsSimulatingPhysics();
	//GetPhysKartOwner()->GetPhysKartRoot()->SetSimulatePhysics(false);

	//-------------------------------------------------------------------------------------------//

	// Scoped updates can improve performance of multiple MoveComponent calls.
	{
		//FScopedMovementUpdate ScopedMovementUpdate(UpdatedComponent, bEnableScopedMovementUpdates ? EScopedUpdate::DeferredUpdates : EScopedUpdate::ImmediateUpdates);
		FScopedMovementUpdate ScopedMovementUpdate(GetPhysKartOwner()->GetPhysKartRoot(), bEnableScopedMovementUpdates ? EScopedUpdate::DeferredUpdates : EScopedUpdate::ImmediateUpdates);

		FTransform NewBodyTransform = FTransform::Identity;
		FVector NewBodyVelocity = FVector::ZeroVector;
		FVector NewBodyAngVelocity = FVector::ZeroVector;

		if (PhysicsSimulation && ClientData->SavedMoves.Num() > 0)
		{
			// Replay moves that have not yet been acked.
			UE_LOG(LogNetPlayerMovement, Verbose, TEXT("ClientUpdatePositionAfterServerUpdate Replaying %d Moves, starting at Timestamp %f"), ClientData->SavedMoves.Num(), ClientData->SavedMoves[0]->TimeStamp);
			for (int32 i = 0; i < ClientData->SavedMoves.Num(); i++)
			{
				FSavedMove_PhysKart* const CurrentMove = ClientData->SavedMoves[i].Get();
				checkSlow(CurrentMove != nullptr);
				CurrentMove->PrepMoveFor(GetPhysKartOwner());
				
				//UpdateInternal_ImmediateMode(CurrentMove->DeltaTime);
				UpdateInternal_ImmediateMode(ClientData->SavedMoves[i].Get()->DeltaTime);

				//*---------------------------------------------------------------------------------*//

					//if (!HasValidData())
					//{
					//	return;
					//}

				if (!GetPhysKartOwner() || !GetPhysKartOwner()->GetPhysKartRoot())
				{
					return false;
				}

				const float DeltaSeconds = AccumulatedDeltaTime;
				AccumulatedDeltaTime = 0.f;

				//FComponentSpacePoseContext Output;
				const FTransform ComponentTransform = FTransform::Identity;
				const FTransform BaseBoneTM = FTransform::Identity;

				if (bEnabled && PhysicsSimulation)
				{
					//const FTransform CompWorldSpaceTM = Output.AnimInstanceProxy->GetComponentTransform();
					const FTransform CompWorldSpaceTM = GetPhysKartOwner()->GetPhysKartRoot()->GetComponentTransform();

					//

					//PhysicsSimulation->SetSimulationSpaceTransform(SpaceToWorldTransform(SimulationSpace, CompWorldSpaceTM, BaseBoneTM));

					// Initialize potential new bodies because of LOD change.
					if (ResetSimulatedTeleportType == ETeleportType::None && bCheckForBodyTransformInit)
					{
						bCheckForBodyTransformInit = false;
						//InitializeNewBodyTransformsDuringSimulation(Output, CompWorldSpaceTM, BaseBoneTM);
					}

					//

					// If time advances, update simulation
					// Reset if necessary
					bool bDynamicsReset = (ResetSimulatedTeleportType != ETeleportType::None);

					//

					// Only need to tick physics if we didn't reset and we have some time to simulate
					if ((bSimulateAnimPhysicsAfterReset || !bDynamicsReset) && DeltaSeconds > AnimPhysicsMinDeltaTime)
					{
						//// Transfer bone velocities previously captured.
						//if (bTransferBoneVelocities && (CapturedBoneVelocityPose.GetPose().GetNumBones() > 0))
						//{
						//	for (const FPhysKartOutputBoneData& OutputData : OutputBoneData)
						//	{
						//		const int32 BodyIndex = OutputData.BodyIndex;
						//		const FPhysKartBodyAnimData& BodyData = BodyAnimData[BodyIndex];

						//		if (BodyData.bIsSimulated)
						//		{
						//			ImmediatePhysics::FActorHandle* Body = Bodies[BodyIndex];
						//			Body->SetLinearVelocity(BodyData.TransferedBoneLinearVelocity);

						//			const FQuat BodyAngularVelocity = BodyData.TransferedBoneAngularVelocity;
						//			Body->SetAngularVelocity(BodyAngularVelocity.GetRotationAxis() * BodyAngularVelocity.GetAngle());

						//			GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() 0")));
						//		}
						//	}

						//	// Free up our captured pose after it's been used.
						//	CapturedBoneVelocityPose.Empty();
						//}
						//else if (SimulationSpace != EPhysKartSimulationSpace::WorldSpace)
						//{
						//	// Calc linear velocity
						//	const FVector ComponentDeltaLocation = CurrentTransform.GetTranslation() - PreviousTransform.GetTranslation();
						//	const FVector ComponentLinearVelocity = ComponentDeltaLocation / DeltaSeconds;
						//	// Apply acceleration that opposed velocity (basically 'drag')
						//	FVector ApplyLinearAcc = WorldVectorToSpaceNoScale(SimulationSpace, -ComponentLinearVelocity, CompWorldSpaceTM, BaseBoneTM) * ComponentLinearVelScale;

						//	// Calc linear acceleration
						//	const FVector ComponentLinearAcceleration = (ComponentLinearVelocity - PreviousComponentLinearVelocity) / DeltaSeconds;
						//	PreviousComponentLinearVelocity = ComponentLinearVelocity;
						//	// Apply opposite acceleration to bodies
						//	ApplyLinearAcc += WorldVectorToSpaceNoScale(SimulationSpace, -ComponentLinearAcceleration, CompWorldSpaceTM, BaseBoneTM) * ComponentLinearAccScale;

						//	// Iterate over bodies
						//	for (const FPhysKartOutputBoneData& OutputData : OutputBoneData)
						//	{
						//		const int32 BodyIndex = OutputData.BodyIndex;
						//		const FPhysKartBodyAnimData& BodyData = BodyAnimData[BodyIndex];

						//		if (BodyData.bIsSimulated)
						//		{
						//			ImmediatePhysics::FActorHandle* Body = Bodies[BodyIndex];

						//			// Apply 
						//			const float BodyInvMass = Body->GetInverseMass();
						//			if (BodyInvMass > 0.f)
						//			{
						//				// Final desired acceleration to apply to body
						//				FVector FinalBodyLinearAcc = ApplyLinearAcc;

						//				// Clamp if desired
						//				if (!ComponentAppliedLinearAccClamp.IsNearlyZero())
						//				{
						//					FinalBodyLinearAcc = FinalBodyLinearAcc.BoundToBox(-ComponentAppliedLinearAccClamp, ComponentAppliedLinearAccClamp);
						//				}

						//				// Apply to body
						//				Body->AddForce(FinalBodyLinearAcc / BodyInvMass);

						//				GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() 1")));
						//			}
						//		}
						//	}
						//}

						//// @todo(ccaulfield): We should be interpolating kinematic targets for each sub-step below
						//for (const FPhysKartOutputBoneData& OutputData : OutputBoneData)
						//{
						//	const int32 BodyIndex = OutputData.BodyIndex;
						//	if (!BodyAnimData[BodyIndex].bIsSimulated)
						//	{
						//		//const FTransform& ComponentSpaceTM = Output.Pose.GetComponentSpaceTransform(OutputData.CompactPoseBoneIndex);
						//		const FTransform& ComponentSpaceTM = FTransform::Identity;
						//		const FTransform BodyTM = ConvertCSTransformToSimSpace(SimulationSpace, ComponentSpaceTM, CompWorldSpaceTM, BaseBoneTM);

						//		Bodies[BodyIndex]->SetKinematicTarget(BodyTM);

						//		GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() 2")));
						//	}
						//}

						UpdateWorldForces(CompWorldSpaceTM, BaseBoneTM);

						//ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;

						//if (bRootIsSkeletalMesh)
						//{
						//	//for (int32 c = 0; c < Bodies.Num(); c++)
						//	for (int32 c = 0; c < UpdatedComponentBodiesIndex.Num(); c++)
						//	{
						//		//if (GEngine)
						//		//{
						//		//	GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() UpdatedComponentBodiesIndex: %d"), UpdatedComponentBodiesIndex[c]));
						//		//}

						//		//UpdatedComponentBodiesIndex[c]->AddForce(ClientData->SavedMoves[i].Get()->SavedForce);
						//		//UpdatedComponentBodiesIndex[c]->SetLinearVelocity(ClientData->SavedMoves[i].Get()->SavedVelocity);
						//		//UpdatedComponentBodiesIndex[c]->SetAngularVelocity(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[i].Get()->StartAngVelocity));

						//		//if (GEngine)
						//		//{
						//		//	//GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() SavedForce: %s"), *ClientData->SavedMoves[i].Get()->SavedForce.ToString()));
						//		//	GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() SavedVelocity: %s"), *ClientData->SavedMoves[i].Get()->SavedVelocity.ToString()));
						//		//	//GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() SavedAngVelocity: %s"), *ClientData->SavedMoves[i].Get()->SavedAngVelocity.ToString()));
						//		//}

						//		ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;
						//		UpdatedBody = PhysicsSimulation->GetActorHandle(UpdatedComponentBodiesIndex[c]);
						//		UpdatedBody->AddForce(ClientData->SavedMoves[i].Get()->SavedForce);
						//		UpdatedBody->SetLinearVelocity(ClientData->SavedMoves[i].Get()->SavedVelocity);
						//		UpdatedBody->SetAngularVelocity(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[i].Get()->StartAngVelocity));

						//		//DrawDebugSphere(GetWorld(), UpdatedBody->GetWorldTransform().GetLocation(), 50.0f, 16, FColor::Emerald, false, -1.0F);
						//	}
						//}
						//else 
						if (PhysicsSimulation->NumActors() > 0 && UpdatedComponentBodyIndex > INDEX_NONE && UpdatedComponentBodyIndex < PhysicsSimulation->NumActors())
						{
							ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;
							UpdatedBody = PhysicsSimulation->GetActorHandle(UpdatedComponentBodyIndex);

							//FTransform BodyInstTransform = FTransform::Identity;
							//BodyInstTransform.SetLocation(ClientData->SavedMoves[i].Get()->SavedLocation);
							//BodyInstTransform.SetRotation(ClientData->SavedMoves[i].Get()->SavedRotation.Quaternion());
							//UpdatedBody->SetWorldTransform(BodyInstTransform);

							UpdatedBody->AddForce(ClientData->SavedMoves[i].Get()->SavedForce);
							UpdatedBody->SetLinearVelocity(ClientData->SavedMoves[i].Get()->SavedVelocity);
							////UpdatedBody->SetAngularVelocity(ClientData->SavedMoves[i].Get()->SavedAngVelocity * FMath::Cos(ClientData->SavedMoves[i].Get()->SavedAngVelocity.Size()));
							//UpdatedBody->SetAngularVelocity(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[i].Get()->StartAngVelocity * ClientData->SavedMoves[i].Get()->DeltaTime));
							////UpdatedBody->SetAngularVelocity(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[i].Get()->SavedVelocity * ClientData->SavedMoves[i].Get()->DeltaTime));

							UpdatedBody->SetAngularVelocity(ClientData->SavedMoves[i].Get()->StartAngVelocity);		//(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[i].Get()->StartAngVelocity));
							//UpdatedBody->SetAngularVelocity(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[i].Get()->SavedAngVelocity));

							//if (i > 0)
							//{
							//	int32 lastIdx = i - 1;
							//	//UpdatedBody->SetAngularVelocity(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[lastIdx].Get()->StartAngVelocity * ClientData->SavedMoves[lastIdx].Get()->DeltaTime));
							//	UpdatedBody->SetAngularVelocity(ImmediateModeAngularVelocityFactored(ClientData->SavedMoves[lastIdx].Get()->SavedAngVelocity * ClientData->SavedMoves[lastIdx].Get()->DeltaTime));
							//}

						}

						const FVector SimSpaceGravity = WorldVectorToSpaceNoScale(SimulationSpace, WorldSpaceGravity, CompWorldSpaceTM, BaseBoneTM);
						// Run simulation at a minimum of 30 FPS to prevent system from exploding.
						// DeltaTime can be higher due to URO, so take multiple iterations in that case.
						const int32 MaxSteps = RBAN_MaxSubSteps;
						const float MaxDeltaSeconds = 1.f / 30.f;

						const int32 NumSteps = FMath::Clamp(FMath::CeilToInt(DeltaSeconds / MaxDeltaSeconds), 1, MaxSteps);
						//const float StepDeltaTime = DeltaSeconds / float(NumSteps);
						//for (int32 Step = 1; Step <= NumSteps; Step++)
						{
							//const float StepDeltaTime = DeltaSeconds / ClientData->SavedMoves.Num();
							const float StepDeltaTime = ClientData->SavedMoves[i].Get()->DeltaTime;

							// We call the _AssumesLocked version here without a lock as the simulation is local to this node and we know
							// we're not going to alter anything while this is running.
							PhysicsSimulation->Simulate(StepDeltaTime, SimSpaceGravity);
							//PhysicsSimulation->Simulate_AssumesLocked(StepDeltaTime, SimSpaceGravity);

							//NewBodyTransform = UpdatedBody != nullptr ? UpdatedBody->GetWorldTransform() : NewBodyTransform;
							//if (bRootIsSkeletalMesh)
							//{
							//	//for (int32 c = 0; c < Bodies.Num(); c++)
							//	for (int32 c = 0; c < UpdatedComponentBodiesIndex.Num(); c++)
							//	{
							//		//NewBodyTransform = Bodies[c]->GetWorldTransform();
							//		//NewBodyVelocity = Bodies[c]->GetLinearVelocity();
							//		//NewBodyAngVelocity = Bodies[c]->GetAngularVelocity();

							//		ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;
							//		UpdatedBody = PhysicsSimulation->GetActorHandle(UpdatedComponentBodiesIndex[c]);
							//		NewBodyTransform = UpdatedBody->GetWorldTransform();
							//		NewBodyVelocity = UpdatedBody->GetLinearVelocity();
							//		NewBodyAngVelocity = UpdatedBody->GetAngularVelocity();

							//		//GetPhysKartOwner()->GetPhysKartRoot()->SetWorldLocation(NewBodyTransform.GetLocation());
							//		//GetPhysKartOwner()->GetPhysKartRoot()->SetWorldRotation(NewBodyTransform.GetRotation());
							//		//GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsLinearVelocity(NewBodyVelocity);
							//		////GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsAngularVelocity(NewBodyAngVelocity);
							//		//GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsAngularVelocityInRadians(NewBodyAngVelocity);

							//		USkeletalMeshComponent* SkelMeshComp = Cast<USkeletalMeshComponent>(GetPhysKartOwner()->GetPhysKartRoot());
							//		if (Bodies.Num() > 0)
							//		{
							//			SkelMeshComp->Bodies[c]->SetBodyTransform(NewBodyTransform, ETeleportType::TeleportPhysics);
							//			SkelMeshComp->Bodies[c]->SetLinearVelocity(NewBodyVelocity, false);
							//			//GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsAngularVelocity(NewBodyAngVelocity);
							//			SkelMeshComp->Bodies[c]->SetAngularVelocityInRadians(NewBodyAngVelocity, false);

							//			//DrawDebugSphere(GetWorld(), NewBodyTransform.GetLocation(), 50.0f, 16, FColor::Green, false, -1.0F);
							//			DrawDebugSphere(GetWorld(), GetPhysKartOwner()->GetPhysKartRoot()->GetComponentToWorld().GetLocation(), 50.0f, 16, FColor::Green, false, -1.0F);
							//		}
							//	}
							//}
							//else 
							if (PhysicsSimulation->GetActorHandle(UpdatedComponentBodyIndex))		//(UpdatedBody)
							{
								ImmediatePhysics::FActorHandle* UpdatedBody = nullptr;
								UpdatedBody = PhysicsSimulation->GetActorHandle(UpdatedComponentBodyIndex);
								NewBodyTransform = UpdatedBody->GetWorldTransform();
								NewBodyVelocity = UpdatedBody->GetLinearVelocity();
								NewBodyAngVelocity = UpdatedBody->GetAngularVelocity();

								DrawDebugSphere(GetWorld(), NewBodyTransform.GetLocation(), 50.0f, 16, FColor::Green, false, -1.0F);

								GetPhysKartOwner()->GetPhysKartRoot()->SetWorldLocation(NewBodyTransform.GetLocation(), false, nullptr, ETeleportType::TeleportPhysics);
								GetPhysKartOwner()->GetPhysKartRoot()->SetWorldRotation(NewBodyTransform.GetRotation(), false, nullptr, ETeleportType::TeleportPhysics);
								GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsLinearVelocity(NewBodyVelocity);
								GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsAngularVelocityInRadians(NewBodyAngVelocity);

								if (GEngine)
								{
									GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1.0f, FColor::Red, FString::Printf(TEXT("UPhysKartMoveComp::ClientUpdatePositionAfterServerUpdate_ImmediateMode() PhysicsSimulation->Simulate")));
								}
							}
						}
					}
				}

				//*---------------------------------------------------------------------------------*//

				//MoveAutonomous_ImmediateMode(CurrentMove->TimeStamp, CurrentMove->DeltaTime, CurrentMove->ReplicatedInputState, CurrentMove->GetCompressedFlags(), CurrentMove->Acceleration);			

				CurrentMove->PostUpdate(GetPhysKartOwner(), FSavedMove_PhysKart::PostUpdate_Replay);

				//GetPhysKartOwner()->GetPhysKartRoot()->SetWorldLocation(NewBodyTransform.GetLocation());
				//GetPhysKartOwner()->GetPhysKartRoot()->SetWorldRotation(NewBodyTransform.GetRotation());
				//GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsLinearVelocity(NewBodyVelocity);
				//GetPhysKartOwner()->GetPhysKartRoot()->SetPhysicsAngularVelocity(NewBodyAngVelocity);
			}

			DrawDebugSphere(GetWorld(), NewBodyTransform.GetLocation(), 100.0f, 16, FColor::Yellow, false, -1.0F);
		}

		//-------------------------------------------------------------------------------------------//
	}

	//-------------------------------------------------------------------------------------------//

	if (FSavedMove_PhysKart* const PendingMove = ClientData->PendingMove.Get())
	{
		PendingMove->bForceNoCombine = true;
	}

	// Restore saved values.
	AnalogInputModifier = SavedAnalogInputModifier;
	//RootMotionParams = BackupRootMotionParams;
	//CurrentRootMotion = BackupRootMotion;
	//if (GetPhysKartOwner()->bClientResimulateRootMotionSources)
	//{
	//	// If we were resimulating root motion sources, it's because we had mismatched state
	//	// with the server - we just resimulated our SavedMoves and now need to restore
	//	// CurrentRootMotion with the latest "good state"
	//	UE_LOG(LogRootMotion, VeryVerbose, TEXT("CurrentRootMotion getting updated after ServerUpdate replays: %s"), *GetPhysKartOwner()->GetName());
	//	CurrentRootMotion.UpdateStateFrom(GetPhysKartOwner()->SavedRootMotion);
	//	GetPhysKartOwner()->bClientResimulateRootMotionSources = false;
	//}
	//GetPhysKartOwner()->SavedRootMotion.Clear();
	//GetPhysKartOwner()->bClientResimulateRootMotion = false;

	GetPhysKartOwner()->bClientUpdating = false;

	//GetPhysKartOwner()->bPressedJump = bRealJump;
	//bWantsToCrouch = bRealCrouch;
	bForceMaxAccel = bRealForceMaxAccel;

	//bForceNextFloorCheck = true;

	GetPhysKartOwner()->bPressedItem = bRealItem;

	return (ClientData->SavedMoves.Num() > 0);
}

//-----------------------------------------------------------------------------------------------//
// 

float FNetworkPredictionData_Client_PhysKart::UpdateTimeStampAndDeltaTime(float DeltaTime, class APhysKart& PhysKartOwner, class UPhysKartMoveComp& CharacterMovementComponent)
{
	// Reset TimeStamp regularly to combat float accuracy decreasing over time.
	if (CurrentTimeStamp > CharacterMovementComponent.MinTimeBetweenTimeStampResets)
	{
		UE_LOG(LogNetPlayerMovement, Log, TEXT("Resetting Client's TimeStamp %f"), CurrentTimeStamp);
		CurrentTimeStamp -= CharacterMovementComponent.MinTimeBetweenTimeStampResets;

		// Mark all buffered moves as having old time stamps, so we make sure to not resend them.
		// That would confuse the server.
		for (int32 MoveIndex = 0; MoveIndex < SavedMoves.Num(); MoveIndex++)
		{
			const FPhysKartSavedMovePtr& CurrentMove = SavedMoves[MoveIndex];
			SavedMoves[MoveIndex]->bOldTimeStampBeforeReset = true;
		}
		// Do LastAckedMove as well. No need to do PendingMove as that move is part of the SavedMoves array.
		if (LastAckedMove.IsValid())
		{
			LastAckedMove->bOldTimeStampBeforeReset = true;
		}

		// Also apply the reset to any active root motions.
	}

	// Update Current TimeStamp.
	CurrentTimeStamp += DeltaTime;
	float ClientDeltaTime = DeltaTime;

	// Server uses TimeStamps to derive DeltaTime which introduces some rounding errors.
	// Make sure we do the same, so MoveAutonomous uses the same inputs and is deterministic!!
	if (SavedMoves.Num() > 0)
	{
		const FPhysKartSavedMovePtr& PreviousMove = SavedMoves.Last();
		if (!PreviousMove->bOldTimeStampBeforeReset)
		{
			// How server will calculate its deltatime to update physics.
			const float ServerDeltaTime = CurrentTimeStamp - PreviousMove->TimeStamp;
			// Have client always use the Server's DeltaTime. Otherwise our physics simulation will differ and we'll trigger too many position corrections and increase our network traffic.
			ClientDeltaTime = ServerDeltaTime;
		}
	}

	return FMath::Min(ClientDeltaTime, MaxMoveDeltaTime * PhysKartOwner.GetActorTimeDilation());
}

//-----------------------------------------------------------------------------------------------//

PRAGMA_DISABLE_DEPRECATION_WARNINGS // For deprecated members of FNetworkPredictionData_Client_PhysKart

FNetworkPredictionData_Client_PhysKart::FNetworkPredictionData_Client_PhysKart(const UPhysKartMoveComp& ClientMovement)
	: ClientUpdateTime(0.f)
	, CurrentTimeStamp(0.f)
	, LastReceivedAckRealTime(0.f)
	, PendingMove(NULL)
	, LastAckedMove(NULL)
	, MaxFreeMoveCount(96)
	, MaxSavedMoveCount(96)
	, bUpdatePosition(false)
	, bSmoothNetUpdates(false) // Deprecated
	, OriginalMeshTranslationOffset(ForceInitToZero)
	, MeshTranslationOffset(ForceInitToZero)
	, OriginalMeshRotationOffset(FQuat::Identity)
	, MeshRotationOffset(FQuat::Identity)
	, MeshRotationTarget(FQuat::Identity)
	, LastCorrectionDelta(0.f)
	, LastCorrectionTime(0.f)
	, MaxClientSmoothingDeltaTime(0.5f)
	, SmoothingServerTimeStamp(0.f)
	, SmoothingClientTimeStamp(0.f)
	, CurrentSmoothTime(0.f) // Deprecated
	, bUseLinearSmoothing(false) // Deprecated
	, MaxSmoothNetUpdateDist(0.f)
	, NoSmoothNetUpdateDist(0.f)
	, SmoothNetUpdateTime(0.f)
	, SmoothNetUpdateRotationTime(0.f)
	, MaxResponseTime(0.125f) // Deprecated, use MaxMoveDeltaTime instead
	, MaxMoveDeltaTime(0.125f)
	, LastSmoothLocation(FVector::ZeroVector)
	, LastServerLocation(FVector::ZeroVector)
	, SimulatedDebugDrawTime(0.0f)
	, DebugForcedPacketLossTimerStart(0.0f)
{
	MaxSmoothNetUpdateDist = ClientMovement.NetworkMaxSmoothUpdateDistance;
	NoSmoothNetUpdateDist = ClientMovement.NetworkNoSmoothUpdateDistance;

	const bool bIsListenServer = (ClientMovement.GetNetMode() == NM_ListenServer);
	SmoothNetUpdateTime = (bIsListenServer ? ClientMovement.ListenServerNetworkSimulatedSmoothLocationTime : ClientMovement.NetworkSimulatedSmoothLocationTime);
	SmoothNetUpdateRotationTime = (bIsListenServer ? ClientMovement.ListenServerNetworkSimulatedSmoothRotationTime : ClientMovement.NetworkSimulatedSmoothRotationTime);

	const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
	if (GameNetworkManager)
	{
		MaxMoveDeltaTime = GameNetworkManager->MaxMoveDeltaTime;
		MaxClientSmoothingDeltaTime = FMath::Max(GameNetworkManager->MaxClientSmoothingDeltaTime, MaxMoveDeltaTime * 2.0f);
	}

	MaxResponseTime = MaxMoveDeltaTime; // MaxResponseTime is deprecated, use MaxMoveDeltaTime instead

	if (ClientMovement.GetOwnerRole() == ROLE_AutonomousProxy)
	{
		SavedMoves.Reserve(MaxSavedMoveCount);
		FreeMoves.Reserve(MaxFreeMoveCount);
	}
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS // For deprecated members of FNetworkPredictionData_Client_PhysKart


FNetworkPredictionData_Client_PhysKart::~FNetworkPredictionData_Client_PhysKart()
{
	SavedMoves.Empty();
	FreeMoves.Empty();
	PendingMove = NULL;
	LastAckedMove = NULL;
}

FPhysKartSavedMovePtr FNetworkPredictionData_Client_PhysKart::CreateSavedMove()
{
	if (SavedMoves.Num() >= MaxSavedMoveCount)
	{
		UE_LOG(LogNetPlayerMovement, Warning, TEXT("CreateSavedMove: Hit limit of %d saved moves (timing out or very bad ping?)"), SavedMoves.Num());

		// Free all saved moves
		for (int32 i = 0; i < SavedMoves.Num(); i++)
		{
			FreeMove(SavedMoves[i]);
		}
		SavedMoves.Reset();
	}

	if (FreeMoves.Num() == 0)
	{
		// No free moves, allocate a new one.
		FPhysKartSavedMovePtr NewMove = AllocateNewMove();
		checkSlow(NewMove.IsValid());
		NewMove->Clear();
		return NewMove;
	}
	else
	{
		// Pull from the free pool
		const bool bAllowShrinking = false;
		FPhysKartSavedMovePtr FirstFree = FreeMoves.Pop(bAllowShrinking);
		FirstFree->Clear();
		return FirstFree;
	}
}


FPhysKartSavedMovePtr FNetworkPredictionData_Client_PhysKart::AllocateNewMove()
{
	return FPhysKartSavedMovePtr(new FSavedMove_PhysKart());
}

void FNetworkPredictionData_Client_PhysKart::FreeMove(const FPhysKartSavedMovePtr& Move)
{
	if (Move.IsValid())
	{
		// Only keep a pool of a limited number of moves.
		if (FreeMoves.Num() < MaxFreeMoveCount)
		{
			FreeMoves.Push(Move);
		}

		// Shouldn't keep a reference to the move on the free list.
		if (PendingMove == Move)
		{
			PendingMove = NULL;
		}
		if (LastAckedMove == Move)
		{
			LastAckedMove = NULL;
		}
	}
}

int32 FNetworkPredictionData_Client_PhysKart::GetSavedMoveIndex(float TimeStamp) const
{
	if (SavedMoves.Num() > 0)
	{
		// If LastAckedMove isn't using an old TimeStamp (before reset), we can prevent the iteration if incoming TimeStamp is outdated
		if (LastAckedMove.IsValid() && !LastAckedMove->bOldTimeStampBeforeReset && (TimeStamp <= LastAckedMove->TimeStamp))
		{
			return INDEX_NONE;
		}

		// Otherwise see if we can find this move.
		for (int32 Index = 0; Index < SavedMoves.Num(); Index++)
		{
			const FSavedMove_PhysKart* CurrentMove = SavedMoves[Index].Get();
			checkSlow(CurrentMove != nullptr);
			if (CurrentMove->TimeStamp == TimeStamp)
			{
				return Index;
			}
		}
	}
	return INDEX_NONE;
}

void FNetworkPredictionData_Client_PhysKart::AckMove(int32 AckedMoveIndex, UPhysKartMoveComp& CharacterMovementComponent)
{
	// It is important that we know the move exists before we go deleting outdated moves.
	// Timestamps are not guaranteed to be increasing order all the time, since they can be reset!
	if (AckedMoveIndex != INDEX_NONE)
	{
		// Keep reference to LastAckedMove
		const FPhysKartSavedMovePtr& AckedMove = SavedMoves[AckedMoveIndex];
		UE_LOG(LogNetPlayerMovement, VeryVerbose, TEXT("AckedMove Index: %2d (%2d moves). TimeStamp: %f, CurrentTimeStamp: %f"), AckedMoveIndex, SavedMoves.Num(), AckedMove->TimeStamp, CurrentTimeStamp);
		if (LastAckedMove.IsValid())
		{
			FreeMove(LastAckedMove);
		}
		LastAckedMove = AckedMove;

		// Free expired moves.
		for (int32 MoveIndex = 0; MoveIndex < AckedMoveIndex; MoveIndex++)
		{
			const FPhysKartSavedMovePtr& Move = SavedMoves[MoveIndex];
			FreeMove(Move);
		}

		// And finally cull all of those, so only the unacknowledged moves remain in SavedMoves.
		const bool bAllowShrinking = false;
		SavedMoves.RemoveAt(0, AckedMoveIndex + 1, bAllowShrinking);
	}

	if (const UWorld* const World = CharacterMovementComponent.GetWorld())
	{
		LastReceivedAckRealTime = World->GetRealTimeSeconds();
	}
}

PRAGMA_DISABLE_DEPRECATION_WARNINGS // For deprecated members of FNetworkPredictionData_Server_PhysKart

FNetworkPredictionData_Server_PhysKart::FNetworkPredictionData_Server_PhysKart(const UPhysKartMoveComp& ServerMovement)
	: PendingAdjustment()
	, CurrentClientTimeStamp(0.f)
	, ServerAccumulatedClientTimeStamp(0.0)
	, LastUpdateTime(0.f)
	, ServerTimeStampLastServerMove(0.f)
	, MaxResponseTime(0.125f) // Deprecated, use MaxMoveDeltaTime instead
	, MaxMoveDeltaTime(0.125f)
	, bForceClientUpdate(false)
	, LifetimeRawTimeDiscrepancy(0.f)
	, TimeDiscrepancy(0.f)
	, bResolvingTimeDiscrepancy(false)
	, TimeDiscrepancyResolutionMoveDeltaOverride(0.f)
	, TimeDiscrepancyAccumulatedClientDeltasSinceLastServerTick(0.f)
	, WorldCreationTime(0.f)
{
	const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
	if (GameNetworkManager)
	{
		MaxMoveDeltaTime = GameNetworkManager->MaxMoveDeltaTime;
		if (GameNetworkManager->MaxMoveDeltaTime > GameNetworkManager->MAXCLIENTUPDATEINTERVAL)
		{
			UE_LOG(LogNetPlayerMovement, Warning, TEXT("GameNetworkManager::MaxMoveDeltaTime (%f) is greater than GameNetworkManager::MAXCLIENTUPDATEINTERVAL (%f)! Server will interfere with move deltas that large!"), GameNetworkManager->MaxMoveDeltaTime, GameNetworkManager->MAXCLIENTUPDATEINTERVAL);
		}
	}

	const UWorld* World = ServerMovement.GetWorld();
	if (World)
	{
		WorldCreationTime = World->GetTimeSeconds();
		ServerTimeStamp = World->GetTimeSeconds();
	}

	MaxResponseTime = MaxMoveDeltaTime; // Deprecated, use MaxMoveDeltaTime instead
}

PRAGMA_ENABLE_DEPRECATION_WARNINGS // For deprecated members of FNetworkPredictionData_Server_PhysKart


FNetworkPredictionData_Server_PhysKart::~FNetworkPredictionData_Server_PhysKart()
{
}


float FNetworkPredictionData_Server_PhysKart::GetServerMoveDeltaTime(float ClientTimeStamp, float ActorTimeDilation) const
{
	if (bResolvingTimeDiscrepancy)
	{
		return TimeDiscrepancyResolutionMoveDeltaOverride;
	}
	else
	{
		return GetBaseServerMoveDeltaTime(ClientTimeStamp, ActorTimeDilation);
	}
}

float FNetworkPredictionData_Server_PhysKart::GetBaseServerMoveDeltaTime(float ClientTimeStamp, float ActorTimeDilation) const
{
	const float DeltaTime = FMath::Min(MaxMoveDeltaTime * ActorTimeDilation, ClientTimeStamp - CurrentClientTimeStamp);
	return DeltaTime;
}


FSavedMove_PhysKart::FSavedMove_PhysKart()
{
	AccelMagThreshold = 1.f;
	AccelDotThreshold = 0.9f;
	AccelDotThresholdCombine = 0.996f; // approx 5 degrees.
	MaxSpeedThresholdCombine = 10.0f;
}

FSavedMove_PhysKart::~FSavedMove_PhysKart()
{
}

void FSavedMove_PhysKart::Clear()
{
	bForceMaxAccel = false;

	ReplicatedInputState.SteeringInput = 0.0f;
	ReplicatedInputState.ThrottleInput = 0.0f;
	ReplicatedInputState.BrakeInput = 0.0f;
	ReplicatedInputState.HandbrakeInput = 0.0f;


	bForceNoCombine = false;
	bOldTimeStampBeforeReset = false;
	bWasJumping = false;

	TimeStamp = 0.f;
	DeltaTime = 0.f;
	CustomTimeDilation = 1.0f;
	MovementMode = 0; // Deprecated, keep backwards compat until removed

	StartPackedMovementMode = 0;
	StartLocation = FVector::ZeroVector;
	StartRelativeLocation = FVector::ZeroVector;
	StartVelocity = FVector::ZeroVector;
	StartRotation = FRotator::ZeroRotator;
	StartControlRotation = FRotator::ZeroRotator;
	StartActorOverlapCounter = 0;
	StartComponentOverlapCounter = 0;

	SavedLocation = FVector::ZeroVector;
	SavedRotation = FRotator::ZeroRotator;

	SavedRelativeLocation = FVector::ZeroVector;
	SavedControlRotation = FRotator::ZeroRotator;

	Acceleration = FVector::ZeroVector;
	MaxSpeed = 0.0f;
	AccelMag = 0.0f;
	AccelNormal = FVector::ZeroVector;

	EndActorOverlapCounter = 0;
	EndComponentOverlapCounter = 0;
	EndPackedMovementMode = 0;
}


void FSavedMove_PhysKart::SetMoveFor(APhysKart* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_PhysKart& ClientData)
{
	PhysKartOwner = Character;
	DeltaTime = InDeltaTime;

	SetInitialPosition(Character);

	AccelMag = NewAccel.Size();
	AccelNormal = (AccelMag > SMALL_NUMBER ? NewAccel / AccelMag : FVector::ZeroVector);

	// Round value, so that client and server match exactly (and so we can send with less bandwidth). This rounded value is copied back to the client in ReplicateMoveToServer.
	// This is done after the AccelMag and AccelNormal are computed above, because those are only used client-side for combining move logic and need to remain accurate.
	Acceleration = Character->GetPhysKartMoveComp()->RoundAcceleration(NewAccel);

	MaxSpeed = Character->GetPhysKartMoveComp()->GetMaxSpeed();

	// CheckJumpInput will increment JumpCurrentCount.
	// Therefore, for replicated moves we want it to set it at 1 less to properly
	// handle the change.
	bForceMaxAccel = Character->GetPhysKartMoveComp()->bForceMaxAccel;

	// Launch velocity gives instant and potentially huge change of velocity
	// Avoid combining move to prevent from reverting locations until server catches up
	const bool bHasLaunchVelocity = !Character->GetPhysKartMoveComp()->PendingLaunchVelocity.IsZero();
	if (bHasLaunchVelocity)
	{
		bForceNoCombine = true;
	}

	TimeStamp = ClientData.CurrentTimeStamp;
}

void FSavedMove_PhysKart::SetInitialPosition(APhysKart* Character)
{
	StartLocation = Character->GetActorLocation();
	StartRotation = Character->GetActorRotation();
	StartVelocity = Character->GetVelocity();
	StartAngVelocity = Character->GetAngularVelocity();

	CustomTimeDilation = Character->CustomTimeDilation;
	StartActorOverlapCounter = Character->NumActorOverlapEventsCounter;
	StartComponentOverlapCounter = UPrimitiveComponent::GlobalOverlapEventsCounter;

	StartControlRotation = Character->GetControlRotation().Clamp();
	bPressedItem = Character->bPressedItem;

	ReplicatedInputState.SteeringInput = Character->GetPhysKartMoveComp()->GetSteeringInput();
	ReplicatedInputState.ThrottleInput = Character->GetPhysKartMoveComp()->GetThrottleInput();
	ReplicatedInputState.BrakeInput = Character->GetPhysKartMoveComp()->GetBrakeInput();
	ReplicatedInputState.HandbrakeInput = Character->GetPhysKartMoveComp()->GetHandbrakeInput();
}

void FSavedMove_PhysKart::PostUpdate(APhysKart* Character, FSavedMove_PhysKart::EPostUpdateMode PostUpdateMode)
{
	// Common code for both recording and after a replay.
	{
		SavedLocation = Character->GetActorLocation();
		SavedRotation = Character->GetActorRotation();
		SavedVelocity = Character->GetVelocity();
		SavedAngVelocity = Character->GetAngularVelocity();

		SavedForce = Character->GetPhysKartMoveComp()->GetPendingForceToApply();
		SavedTorque = Character->GetPhysKartMoveComp()->GetPendingTorqueToApply();
		SavedImpulse = Character->GetPhysKartMoveComp()->GetPendingImpulseToApply();

		SavedControlRotation = Character->GetControlRotation().Clamp();
	}

	// Only save RootMotion params when initially recording
	if (PostUpdateMode == PostUpdate_Record)
	{
		// Don't want to combine moves that trigger overlaps, because by moving back and replaying the move we could retrigger overlaps.
		EndActorOverlapCounter = Character->NumActorOverlapEventsCounter;
		EndComponentOverlapCounter = UPrimitiveComponent::GlobalOverlapEventsCounter;
		if ((StartActorOverlapCounter != EndActorOverlapCounter) || (StartComponentOverlapCounter != EndComponentOverlapCounter))
		{
			bForceNoCombine = true;
		}

		// Don't combine or delay moves where velocity changes to/from zero.
		if (StartVelocity.IsZero() != SavedVelocity.IsZero())
		{
			bForceNoCombine = true;
		}

		// Don't combine if this move caused us to change movement modes during the move.
		if (StartPackedMovementMode != EndPackedMovementMode)
		{
			bForceNoCombine = true;
		}

		if (bPressedItem != PhysKartOwner->bPressedItem)
		{
			bForceNoCombine = true;
		}
	}
}

bool FSavedMove_PhysKart::IsImportantMove(const FPhysKartSavedMovePtr& LastAckedMovePtr) const
{
	const FSavedMove_PhysKart* LastAckedMove = LastAckedMovePtr.Get();

	// Check if any important movement flags have changed status.
	if (GetCompressedFlags() != LastAckedMove->GetCompressedFlags())
	{
		return true;
	}

	if (StartPackedMovementMode != LastAckedMove->EndPackedMovementMode)
	{
		return true;
	}

	if (EndPackedMovementMode != LastAckedMove->EndPackedMovementMode)
	{
		return true;
	}

	// check if acceleration has changed significantly
	if (Acceleration != LastAckedMove->Acceleration)
	{
		// Compare magnitude and orientation
		if ((FMath::Abs(AccelMag - LastAckedMove->AccelMag) > AccelMagThreshold) || ((AccelNormal | LastAckedMove->AccelNormal) < AccelDotThreshold))
		{
			return true;
		}
	}
	return false;
}

FVector FSavedMove_PhysKart::GetRevertedLocation() const
{
	return StartLocation;
}

bool UPhysKartMoveComp::CanDelaySendingMove(const FPhysKartSavedMovePtr& NewMovePtr)
{
	const FSavedMove_PhysKart* NewMove = NewMovePtr.Get();

	// Don't delay moves that change movement mode over the course of the move.
	if (NewMove->StartPackedMovementMode != NewMove->EndPackedMovementMode)
	{
		return false;
	}

	// If we know we don't want to combine this move, reduce latency and avoid misprediction by flushing immediately.
	if (NewMove->bForceNoCombine)
	{
		return false;
	}

	return true;
}

float UPhysKartMoveComp::GetClientNetSendDeltaTime(const APlayerController* PC, const FNetworkPredictionData_Client_PhysKart* ClientData, const FPhysKartSavedMovePtr& NewMove) const
{
	const UPlayer* Player = (PC ? PC->Player : nullptr);
	const UWorld* MyWorld = GetWorld();
	const AGameStateBase* const GameState = MyWorld->GetGameState();
	const AGameNetworkManager* GameNetworkManager = (const AGameNetworkManager*)(AGameNetworkManager::StaticClass()->GetDefaultObject());
	float NetMoveDelta = GameNetworkManager->ClientNetSendMoveDeltaTime;

	if (PC && Player)
	{
		// send moves more frequently in small games where server isn't likely to be saturated
		if ((Player->CurrentNetSpeed > GameNetworkManager->ClientNetSendMoveThrottleAtNetSpeed) && (GameState != nullptr) && (GameState->PlayerArray.Num() <= GameNetworkManager->ClientNetSendMoveThrottleOverPlayerCount))
		{
			NetMoveDelta = GameNetworkManager->ClientNetSendMoveDeltaTime;
		}
		else
		{
			NetMoveDelta = FMath::Max(GameNetworkManager->ClientNetSendMoveDeltaTimeThrottled, 2 * GameNetworkManager->MoveRepSize / Player->CurrentNetSpeed);
		}

		// Lower frequency for standing still and not rotating camera
		if (Acceleration.IsZero() && Velocity.IsZero() && ClientData->LastAckedMove.IsValid() && ClientData->LastAckedMove->IsMatchingStartControlRotation(PC))
		{
			NetMoveDelta = FMath::Max(GameNetworkManager->ClientNetSendMoveDeltaTimeStationary, NetMoveDelta);
		}
	}

	return NetMoveDelta;
}

bool FSavedMove_PhysKart::IsMatchingStartControlRotation(const APlayerController* PC) const
{
	return PC ? StartControlRotation.Equals(PC->GetControlRotation(), PhysKartMovementCVars::NetStationaryRotationTolerance) : false;
}

void FSavedMove_PhysKart::GetPackedAngles(uint32& YawAndPitchPack, uint8& RollPack) const
{
	// Compress rotation down to 5 bytes
	YawAndPitchPack = UPhysKartMoveComp::PackYawAndPitchTo32(SavedControlRotation.Yaw, SavedControlRotation.Pitch);
	RollPack = FRotator::CompressAxisToByte(SavedControlRotation.Roll);
}

bool FSavedMove_PhysKart::CanCombineWith(const FPhysKartSavedMovePtr& NewMovePtr, APhysKart* Character, float MaxDelta) const
{
	const FSavedMove_PhysKart* NewMove = NewMovePtr.Get();

	if (bForceNoCombine || NewMove->bForceNoCombine)
	{
		return false;
	}

	if (bOldTimeStampBeforeReset)
	{
		return false;
	}

	if (NewMove->Acceleration.IsZero())
	{
		if (!Acceleration.IsZero())
		{
			return false;
		}
	}
	else
	{
		if (NewMove->DeltaTime + DeltaTime >= MaxDelta)
		{
			return false;
		}

		if (!FVector::Coincident(AccelNormal, NewMove->AccelNormal, AccelDotThresholdCombine))
		{
			return false;
		}

	}

	// Don't combine moves where velocity changes to zero or from zero.
	if (StartVelocity.IsZero() != NewMove->StartVelocity.IsZero())
	{
		return false;
	}

	if (!FMath::IsNearlyEqual(MaxSpeed, NewMove->MaxSpeed, MaxSpeedThresholdCombine))
	{
		return false;
	}

	if ((MaxSpeed == 0.0f) != (NewMove->MaxSpeed == 0.0f))
	{
		return false;
	}

	// Compressed flags not equal, can't combine. This covers jump and crouch as well as any custom movement flags from overrides.
	if (GetCompressedFlags() != NewMove->GetCompressedFlags())
	{
		return false;
	}

	if (StartPackedMovementMode != NewMove->StartPackedMovementMode)
	{
		return false;
	}

	if (EndPackedMovementMode != NewMove->StartPackedMovementMode)
	{
		return false;
	}

	if (CustomTimeDilation != NewMove->CustomTimeDilation)
	{
		return false;
	}

	// Don't combine moves with overlap event changes, since reverting back and then moving forward again can cause event spam.
	// This catches events between movement updates; moves that trigger events already set bForceNoCombine to false.
	if (EndActorOverlapCounter != NewMove->StartActorOverlapCounter)
	{
		return false;
	}

	return true;
}

void FSavedMove_PhysKart::CombineWith(const FSavedMove_PhysKart* OldMove, APhysKart* InCharacter, APlayerController* PC, const FVector& OldStartLocation)
{
	UPhysKartMoveComp* CharMovement = InCharacter->GetPhysKartMoveComp();

	CharMovement->UpdatedComponent->SetWorldLocationAndRotation(OldStartLocation, OldMove->StartRotation, false, nullptr, CharMovement->GetTeleportType());

	CharMovement->SetLinearVelocity(OldMove->StartVelocity);
	CharMovement->SetAngularVelocity(OldMove->StartAngVelocity);

	// Now that we have reverted to the old position, prepare a new move from that position,
	// using our current velocity, acceleration, and rotation, but applied over the combined time from the old and new move.

	// Combine times for both moves
	DeltaTime += OldMove->DeltaTime;
}

void FSavedMove_PhysKart::PrepMoveFor(APhysKart* Character)
{
	Character->GetPhysKartMoveComp()->bForceMaxAccel = bForceMaxAccel;

	Character->bPressedItem = bPressedItem;

	Character->GetPhysKartMoveComp()->SetSteeringInput(ReplicatedInputState.SteeringInput);
	Character->GetPhysKartMoveComp()->SetThrottleInput(ReplicatedInputState.ThrottleInput);
	Character->GetPhysKartMoveComp()->SetBrakeInput(ReplicatedInputState.BrakeInput);
	bool bHandbrakeInput = ReplicatedInputState.HandbrakeInput <= KINDA_SMALL_NUMBER ? false : true;
	Character->GetPhysKartMoveComp()->SetHandbrakeInput(bHandbrakeInput);

	StartPackedMovementMode = Character->GetPhysKartMoveComp()->PackNetworkMovementMode();
}

uint8 FSavedMove_PhysKart::GetCompressedFlags() const
{
	uint8 Result = 0;

	if (bPressedItem)
	{
		Result |= FLAG_ItemPressed;
	}

	return Result;
}

void UPhysKartMoveComp::FlushServerMoves()
{
	// Send pendingMove to server if this character is replicating movement
	if (GetPhysKartOwner() && GetPhysKartOwner()->IsReplicatingMovement() && (GetPhysKartOwner()->GetLocalRole() == ROLE_AutonomousProxy))
	{
		FNetworkPredictionData_Client_PhysKart* ClientData = GetPredictionData_Client_PhysKart();
		if (!ClientData)
		{
			return;
		}

		if (ClientData->PendingMove.IsValid())
		{
			const UWorld* MyWorld = GetWorld();
			ClientData->ClientUpdateTime = MyWorld->TimeSeconds;

			FPhysKartSavedMovePtr NewMove = ClientData->PendingMove;
			ClientData->PendingMove = nullptr;

			//UE_CLOG(GetPhysKartOwner() && UpdatedComponent, LogNetPlayerMovement, Verbose, TEXT("ClientMove (Flush) Time %f Acceleration %s Velocity %s Position %s DeltaTime %f Mode %s MovementBase %s.%s (Dynamic:%d) DualMove? %d"),
			//	NewMove->TimeStamp, *NewMove->Acceleration.ToString(), *Velocity.ToString(), *UpdatedComponent->GetComponentLocation().ToString(), NewMove->DeltaTime, *GetMovementName(),
			//	*GetNameSafe(NewMove->EndBase.Get()), *NewMove->EndBoneName.ToString(), MovementBaseUtility::IsDynamicBase(NewMove->EndBase.Get()) ? 1 : 0, ClientData->PendingMove.IsValid() ? 1 : 0);

			if (ShouldUsePackedMovementRPCs())
			{
				CallServerMovePacked(NewMove.Get(), nullptr, nullptr);
			}
			else
			{
				CallServerMove(NewMove.Get(), nullptr);
			}
		}
	}
}


FVector UPhysKartMoveComp::RoundAcceleration(FVector InAccel) const
{
	// Match FVector_NetQuantize10 (1 decimal place of precision).
	InAccel.X = FMath::RoundToFloat(InAccel.X * 10.f) / 10.f;
	InAccel.Y = FMath::RoundToFloat(InAccel.Y * 10.f) / 10.f;
	InAccel.Z = FMath::RoundToFloat(InAccel.Z * 10.f) / 10.f;
	return InAccel;
}


float UPhysKartMoveComp::ComputeAnalogInputModifier() const
{
	const float MaxAccel = GetMaxAcceleration();
	if (Acceleration.SizeSquared() > 0.f && MaxAccel > SMALL_NUMBER)
	{
		return FMath::Clamp(Acceleration.Size() / MaxAccel, 0.f, 1.f);
	}

	return 0.f;
}

ETeleportType UPhysKartMoveComp::GetTeleportType() const
{
	return bJustTeleported || bNetworkLargeClientCorrection ? ETeleportType::TeleportPhysics : ETeleportType::None;
}
