// Francisco Elizalde * PaCorp * 2021

#include "PhysKart.h"
#include "UObject/UObjectGlobals.h"
#include "Engine/CollisionProfile.h"

#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"

#include "Net/UnrealNetwork.h"
#include "GameFramework/GameNetworkManager.h"

FName APhysKart::PhysKartMoveCompName(TEXT("PhysKartMovementComp"));
FName APhysKart::PhysKartRootName(TEXT("PhysKartRootComp"));

// Sets default values
APhysKart::APhysKart(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PhysKartMovementComponent = CreateDefaultSubobject<UPhysKartMoveComp>(APhysKart::PhysKartMoveCompName);
	if (PhysKartMovementComponent)
	{
		PhysKartMovementComponent->UpdatedComponent = RootComponent;
	}
}

void APhysKart::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (GetRootComponent())
	{
		PhysKartRoot = Cast<USkeletalMeshComponent>(GetRootComponent());
	}
}

// Called when the game starts or when spawned
void APhysKart::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APhysKart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APhysKart::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &APhysKart::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APhysKart::MoveRight);
}

void APhysKart::MoveForward(float Value)
{
	if (PhysKartMovementComponent == nullptr) return;

	FVector Direction = FVector::ForwardVector;
	AddMovementInput(Direction, Value, false);
}

void APhysKart::MoveRight(float Value)
{
	if (PhysKartMovementComponent == nullptr) return;

	FVector Direction = FVector::UpVector;
	AddMovementInput(Direction, Value, false);
}

//-----------------------------------------------------------------------------------------------//

FVector APhysKart::GetVelocity() const
{
	if (GetPhysKartRoot() && GetPhysKartRoot()->IsSimulatingPhysics())
	{
		return GetPhysKartRoot()->GetBodyInstance()->GetUnrealWorldVelocity();
	}

	return GetPhysKartMoveComp() ? GetPhysKartMoveComp()->Velocity : FVector::ZeroVector;
}

FVector APhysKart::GetAngularVelocity() const
{
	if (GetPhysKartRoot() && GetPhysKartRoot()->IsSimulatingPhysics())
	{
		return GetPhysKartRoot()->GetBodyInstance()->GetUnrealWorldAngularVelocityInRadians();
	}

	return GetPhysKartMoveComp() ? GetPhysKartMoveComp()->GetAngularVelocity() : FVector::ZeroVector;
}

//////////////////////////////////////////////////////////////////////////
// Replication

//
// Static variables for networking.
//

void APhysKart::PreNetReceive()
{
	Super::PreNetReceive();

}

void APhysKart::PostNetReceive()
{
	Super::PostNetReceive();

}

void APhysKart::PostNetReceivePhysicState()
{
	UPrimitiveComponent* RootPrimComp = Cast<UPrimitiveComponent>(RootComponent);
	if (RootPrimComp)
	{
		FRigidBodyState NewState;
		GetReplicatedMovement().CopyTo(NewState, this);

		FVector DeltaPos(FVector::ZeroVector);

		if (GetLocalRole() == ROLE_SimulatedProxy)
		{
			RootPrimComp->SetRigidBodyReplicatedTarget(NewState);
		}
	}
}

void APhysKart::OnRep_ReplayLastTransformUpdateTimeStamp()
{
	ReplicatedServerLastTransformUpdateTimeStamp = ReplayLastTransformUpdateTimeStamp;
}

void APhysKart::PreReplication(IRepChangedPropertyTracker& ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);
}

void APhysKart::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(APhysKart, ReplicatedServerLastTransformUpdateTimeStamp, COND_SimulatedOnlyNoReplay);
}

bool APhysKart::IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer)
{
	return Super::IsReplicationPausedForConnection(ConnectionOwnerNetViewer);
}

void APhysKart::OnReplicationPausedChanged(bool bIsReplicationPaused)
{
	Super::OnReplicationPausedChanged(bIsReplicationPaused);
}

//-----------------------------------------------------------------------------------------------//
// 

void APhysKart::OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PrevCustomMode)
{
	MovementModeChangedDelegate.Broadcast(this, PrevMovementMode, PrevCustomMode);
}

void APhysKart::NotifyActorBeginOverlap(AActor* OtherActor)
{
	NumActorOverlapEventsCounter++;
	Super::NotifyActorBeginOverlap(OtherActor);
}

void APhysKart::NotifyActorEndOverlap(AActor* OtherActor)
{
	NumActorOverlapEventsCounter++;
	Super::NotifyActorEndOverlap(OtherActor);
}

// ServerMovePacked
void APhysKart::ServerMovePacked_Implementation(const FPhysKartServerMovePackedBits& PackedBits)
{
	GetPhysKartMoveComp()->ServerMovePacked_ServerReceive(PackedBits);
}
bool APhysKart::ServerMovePacked_Validate(const FPhysKartServerMovePackedBits& PackedBits)
{
	// Can't really validate the bit stream without unpacking, and that is done in ServerMovePacked_ServerReceive() and can be rejected after unpacking.
	return true;
}

// ServerMove
void APhysKart::ServerMove_Implementation(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis, uint8 ClientQuatAngle)
{
	GetPhysKartMoveComp()->ServerMove_Implementation(TimeStamp, InReplicatedState, InAccel, ClientLoc, CompressedMoveFlags, ClientRoll, View, ClientMovementMode, ClientQuatAxis, ClientQuatAngle);
}

bool APhysKart::ServerMove_Validate(float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 CompressedMoveFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode, FVector_NetQuantize100 ClientQuatAxis, uint8 ClientQuatAngle)
{
	return GetPhysKartMoveComp()->ServerMove_Validate(TimeStamp, InReplicatedState, InAccel, ClientLoc, CompressedMoveFlags, ClientRoll, View, ClientMovementMode, ClientQuatAxis, ClientQuatAngle);
}

// ServerMoveDual
void APhysKart::ServerMoveDual_Implementation(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode)
{
	GetPhysKartMoveComp()->ServerMoveDual_Implementation(TimeStamp0, InReplicatedState0, InAccel0, PendingFlags, View0, TimeStamp, InReplicatedState, InAccel, ClientLoc, NewFlags, ClientRoll, View, ClientMovementMode);
}

bool APhysKart::ServerMoveDual_Validate(float TimeStamp0, FReplicatedPhysKartState InReplicatedState0, FVector_NetQuantize10 InAccel0, uint8 PendingFlags, uint32 View0, float TimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 InAccel, FVector_NetQuantize100 ClientLoc, uint8 NewFlags, uint8 ClientRoll, uint32 View, uint8 ClientMovementMode)
{
	return GetPhysKartMoveComp()->ServerMoveDual_Validate(TimeStamp0, InReplicatedState0, InAccel0, PendingFlags, View0, TimeStamp, InReplicatedState, InAccel, ClientLoc, NewFlags, ClientRoll, View, ClientMovementMode);
}

// ServerMoveOld
void APhysKart::ServerMoveOld_Implementation(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags)
{
	GetPhysKartMoveComp()->ServerMoveOld_Implementation(OldTimeStamp, InReplicatedState, OldAccel, OldMoveFlags);
}

bool APhysKart::ServerMoveOld_Validate(float OldTimeStamp, FReplicatedPhysKartState InReplicatedState, FVector_NetQuantize10 OldAccel, uint8 OldMoveFlags)
{
	return GetPhysKartMoveComp()->ServerMoveOld_Validate(OldTimeStamp, InReplicatedState, OldAccel, OldMoveFlags);
}

// ClientMoveResponsePacked
void APhysKart::ClientMoveResponsePacked_Implementation(const FPhysKartMoveResponsePackedBits& PackedBits)
{
	GetPhysKartMoveComp()->MoveResponsePacked_ClientReceive(PackedBits);
}
bool APhysKart::ClientMoveResponsePacked_Validate(const FPhysKartMoveResponsePackedBits& PackedBits)
{
	// Can't really validate the bit stream without unpacking, and that is done in MoveResponsePacked_ClientReceive() and can be rejected after unpacking.
	return true;
}

// ClientAckGoodMove
void APhysKart::ClientAckGoodMove_Implementation(float TimeStamp)
{
	GetPhysKartMoveComp()->ClientAckGoodMove_Implementation(TimeStamp);
}

// ClientAdjustPosition
void APhysKart::ClientAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FVector NewVel, FQuat NewQuat, FVector NewAngVel, uint8 ServerMovementMode)
{
	GetPhysKartMoveComp()->ClientAdjustPosition_Implementation(TimeStamp, NewLoc, NewVel, NewQuat, NewAngVel, ServerMovementMode);
}

// ClientVeryShortAdjustPosition
void APhysKart::ClientVeryShortAdjustPosition_Implementation(float TimeStamp, FVector NewLoc, FQuat NewQuat, uint8 ServerMovementMode)
{
	GetPhysKartMoveComp()->ClientVeryShortAdjustPosition_Implementation(TimeStamp, NewLoc, NewQuat, ServerMovementMode);
}
